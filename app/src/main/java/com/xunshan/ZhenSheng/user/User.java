package com.xunshan.ZhenSheng.user;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.widget.Toast;

import com.xunshan.ZhenSheng.application.BaseApplication;

public class User {

    public static User user;
    //手机号
    private String phone = "";

    private String user_name = "";
    private String user_ID = "";
    private String user_level = "";
    private String user_IM_sign = "";


    private String token = "";
    private String user_Charm = "";
    private String user_Headpic = "";
    private String visitor_name = "";
    private String visitor_Charm = "";
    private String visitor_Headpic = "";
    private String birthaday = "";
    private String address = "";


    private String Roomname_name = "";
    private String Roomname_Charm = "";
    private String Roomname_Headpic = "";

    private String roompassword = "";
    private String user_balance = "";
    private String sex = "";
    private String sign = "";

    public String getSign() {
        sign = UserInfo.getStringValue(BaseApplication.getContext(),"sign");
        return sign;
    }

    public void setSign(String sign) {
        UserInfo.SharedPreferences(BaseApplication.getContext(), "sign", sign);
        this.sign = sign;
    }

    private String roomlock = "";

    public String getAddress() {
        address = UserInfo.getStringValue(BaseApplication.getContext(),"address");
        return address;
    }

    public void setAddress(String address) {
        UserInfo.SharedPreferences(BaseApplication.getContext(), "address", address);
        this.address = address;
    }

    public User() {
        //UserInfo info = new UserInfo();
        token = UserInfo.getStringValue(BaseApplication.getContext(), "token");
        phone = UserInfo.getStringValue(BaseApplication.getContext(), "phone");
        user_ID = UserInfo.getStringValue(BaseApplication.getContext(), "user_ID");
        user_level = UserInfo.getStringValue(BaseApplication.getContext(), "user_level");
        user_name = UserInfo.getStringValue(BaseApplication.getContext(), "user_name");
        user_Charm = UserInfo.getStringValue(BaseApplication.getContext(), "user_Charm");
        user_Headpic = UserInfo.getStringValue(BaseApplication.getContext(), "user_Headpic");

        visitor_name = UserInfo.getStringValue(BaseApplication.getContext(), "visitor_name");
        visitor_Charm = UserInfo.getStringValue(BaseApplication.getContext(), "visitor_Charm");
        visitor_Headpic = UserInfo.getStringValue(BaseApplication.getContext(), "visitor_Headpic");


        Roomname_name = UserInfo.getStringValue(BaseApplication.getContext(), "Roomname_name");
        Roomname_Charm = UserInfo.getStringValue(BaseApplication.getContext(), "Roomname_Charm");
        Roomname_Headpic = UserInfo.getStringValue(BaseApplication.getContext(), "Roomname_Headpic");

        roompassword = UserInfo.getStringValue(BaseApplication.getContext(), "roompassword");

        user_IM_sign = UserInfo.getStringValue(BaseApplication.getContext(), "user_IM_sign");
        user_balance = UserInfo.getStringValue(BaseApplication.getContext(), "user_balance");

        roomlock= UserInfo.getStringValue(BaseApplication.getContext(), "roomlock");
        sex = UserInfo.getStringValue(BaseApplication.getContext(),"sex");
        birthaday = UserInfo.getStringValue(BaseApplication.getContext(),"birthaday");
        address = UserInfo.getStringValue(BaseApplication.getContext(),"address");
        sign = UserInfo.getStringValue(BaseApplication.getContext(),"sign");
    }

    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        User.user = user;
    }

    public String getSex() {
        sex = UserInfo.getStringValue(BaseApplication.getContext(),"sex");
        return sex;
    }

    public void setSex(String sex) {
        UserInfo.SharedPreferences(BaseApplication.getContext(), "sex", sex);
        this.sex = sex;
    }
    public String getBirthaday() {
        birthaday = UserInfo.getStringValue(BaseApplication.getContext(),"birthaday");
        return birthaday;
    }

    public void setBirthaday(String birthaday) {
        UserInfo.SharedPreferences(BaseApplication.getContext(), "birthaday", birthaday);
        this.birthaday = birthaday;
    }

    public String getRoomlock() {
        return roomlock;
    }

    public void setRoomlock(String roomlock) {
        this.roomlock = roomlock;
        UserInfo.SharedPreferences(BaseApplication.getContext(), "roomlock", roomlock);
    }

    public String getRoomname_name() {
        return Roomname_name;
    }

    public void setRoomname_name(String roomname_name) {
        this.Roomname_name = roomname_name;
        UserInfo.SharedPreferences(BaseApplication.getContext(), "roomname_name", roomname_name);
    }

    public String getRoomname_Charm() {
        return Roomname_Charm;
    }

    public void setRoomname_Charm(String roomname_Charm) {
        this.Roomname_Charm = roomname_Charm;
        UserInfo.SharedPreferences(BaseApplication.getContext(), "roomname_Charm", roomname_Charm);

    }

    public String getRoomname_Headpic() {
        return Roomname_Headpic;
    }

    public void setRoomname_Headpic(String roomname_Headpic) {

        this.Roomname_Headpic = roomname_Headpic;
        UserInfo.SharedPreferences(BaseApplication.getContext(), "roomname_Headpic", roomname_Headpic);
    }

    public String getRoompassword() {
        return roompassword;
    }

    public void setRoompassword(String roompassword) {
        this.roompassword = roompassword;
        UserInfo.SharedPreferences(BaseApplication.getContext(), "roompassword", roompassword);
    }


    public String getUser_balance() {
        return user_balance;
    }

    public void setUser_balance(String user_balance) {
        this.user_balance = user_balance;
        UserInfo.SharedPreferences(BaseApplication.getContext(), "user_balance", user_balance);
    }

    public String getUser_IM_sign() {
        return user_IM_sign;
    }

    public void setUser_IM_sign(String user_IM_sign) {
        this.user_IM_sign = user_IM_sign;
        UserInfo.SharedPreferences(BaseApplication.getContext(), "user_IM_sign", user_IM_sign);
    }

    public String getVisitor_name() {
        return visitor_name;
    }

    public void setVisitor_name(String visitor_name) {
        this.visitor_name = visitor_name;
        UserInfo.SharedPreferences(BaseApplication.getContext(), "visitor_name", visitor_name);
    }

    public String getVisitor_Charm() {
        return visitor_Charm;
    }

    public void setVisitor_Charm(String visitor_Charm) {
        this.visitor_Charm = visitor_Charm;
        UserInfo.SharedPreferences(BaseApplication.getContext(), "visitor_Charm", visitor_Charm);

    }

    public String getVisitor_Headpic() {
        return visitor_Headpic;
    }

    public void setVisitor_Headpic(String visitor_Headpic) {
        this.visitor_Headpic = visitor_Headpic;
        UserInfo.SharedPreferences(BaseApplication.getContext(), "visitor_Headpic", visitor_Headpic);

    }

    public String getUser_Charm() {
        return user_Charm;
    }

    public void setUser_Charm(String user_Charm) {
        this.user_Charm = user_Charm;
        UserInfo.SharedPreferences(BaseApplication.getContext(), "user_Charm", user_Charm);
    }

    public String getUser_Headpic() {
        return user_Headpic;
    }

    public void setUser_Headpic(String user_Headpic) {
        this.user_Headpic = user_Headpic;
        UserInfo.SharedPreferences(BaseApplication.getContext(), "user_Headpic", user_Headpic);
    }

    public String getUser_name() {
        user_name = UserInfo.getStringValue(BaseApplication.getContext(), "user_name");
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
        UserInfo.SharedPreferences(BaseApplication.getContext(), "user_name", user_name);
    }

    public String getUser_level() {
        return user_level;
    }

    public void setUser_level(String user_level) {
        this.user_level = user_level;
        UserInfo.SharedPreferences(BaseApplication.getContext(), "user_level", user_level);
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        UserInfo.SharedPreferences(BaseApplication.getContext(), "phone", phone);

    }

    public String getUser_ID() {
        return user_ID;
    }

    public void setUser_ID(String user_ID) {
        this.user_ID = user_ID;
        UserInfo.SharedPreferences(BaseApplication.getContext(), "user_ID", user_ID);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
        UserInfo.SharedPreferences(BaseApplication.getContext(), "token", token);

    }

    public void End(Context context) {

        User.user().setPhone(null);
        User.user().setToken(null);
        User.user().setUser_level(null);
        User.user().setUser_ID(null);
        User.user().setUser_name(null);
        User.user().setVisitor_name(null);
        User.user().setVisitor_Charm(null);
        User.user().setUser_Charm(null);
        User.user().setVisitor_Headpic(null);

        Toast.makeText(context, "退出成功", Toast.LENGTH_SHORT).show();
        //JPushInterface.deleteAlias(context,1);
    }


    public static User user() {
        if (user == null) {
            user = new User();
        }
        return user;
    }


    public void clearUserInfo() {
        if (TextUtils.isEmpty(phone)) {
            return;
        }
        phone = null;
        token = null;
    }


    public static class UserInfo {
        private static final String SharedPreferences_Name = "data";


        public static void SharedPreferences(Context context, String key, int value) {
            SharedPreferences sp1 = context.getSharedPreferences(SharedPreferences_Name, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp1.edit();
            editor.putInt(key, value);
            editor.commit();
        }

        public static void SharedPreferences(Context context, String key, String value) {
            SharedPreferences sp1 = context.getSharedPreferences(SharedPreferences_Name, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp1.edit();
            editor.putString(key, value);
            editor.commit();
        }

        public static void SharedPreferences(Context context, String key, Long value) {
            SharedPreferences sp1 = context.getSharedPreferences(SharedPreferences_Name, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp1.edit();
            editor.putLong(key, value);
            editor.commit();
        }

        public static void SharedPreferences(Context context, String key, Boolean value) {
            SharedPreferences sp1 = context.getSharedPreferences(SharedPreferences_Name, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sp1.edit();
            editor.putBoolean(key, value);
            editor.commit();
        }

        public static String getStringValue(Context context, String key) {
            SharedPreferences sp1 = context.getSharedPreferences(SharedPreferences_Name, Context.MODE_PRIVATE);
            String Value = sp1.getString(key, "");
            return Value;
        }

        public static int getintValue(Context context, String key) {
            SharedPreferences sp1 = context.getSharedPreferences(SharedPreferences_Name, Context.MODE_PRIVATE);
            int Value = sp1.getInt(key, 0);
            return Value;
        }

        public static Long getLongValue(Context context, String key) {
            SharedPreferences sp1 = context.getSharedPreferences(SharedPreferences_Name, Context.MODE_PRIVATE);
            Long Value = sp1.getLong(key, 0L);
            return Value;
        }

        public static Boolean getBooleanValue(Context context, String key, boolean b) {
            SharedPreferences sp1 = context.getSharedPreferences(SharedPreferences_Name, Context.MODE_PRIVATE);
            Boolean Value = sp1.getBoolean(key, false);
            return Value;
        }


    }


}
