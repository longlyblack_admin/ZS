package com.xunshan.ZhenSheng.activity;

import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.hb.dialog.dialog.ConfirmDialog;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.adapter.MyEarnigsAdapter;
import com.xunshan.ZhenSheng.adapter.ViewPagerAdapter;
import com.xunshan.ZhenSheng.entity.Earnigs;
import com.xunshan.ZhenSheng.fragment.ExpendFragment;
import com.xunshan.ZhenSheng.fragment.IncomeFragment;
import com.xunshan.ZhenSheng.util.NavitationLayout;

import java.util.ArrayList;
import java.util.List;


public class MyEarnigsActivity extends AppCompatActivity implements View.OnClickListener {
    ListView lv_myearnigs;
    List<Fragment> list;
    MyEarnigsAdapter adapter;
    TextView tv_myearnigsquestion;//?
    View view;
    PopupWindow popupWindow;
    LinearLayout ll_myearnigs;
    TextView tv_myearnigsmoney;//兑换钻石
    TextView tv_myearnigsmoneydetails;//金币明细
    TextView tv_TX;
    TextView tv_shouyiback;
    ViewPager vp_moneydetails;
    private LinearLayout ll_myeasr;
    private ViewPagerAdapter viewPagerAdapter111;
    private String[] titles1 = new String[]{"收入", "支出"};
    private NavitationLayout navitationLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.myearnigs_activity);
        initView();
        initData();
    }

    private void initView() {
        tv_myearnigsquestion = (TextView) findViewById(R.id.tv_myearnigsquestion);
        ll_myearnigs = (LinearLayout) findViewById(R.id.ll_myearnigs);
        ll_myeasr = (LinearLayout) findViewById(R.id.ll_myeasr);



        tv_myearnigsmoney = (TextView) findViewById(R.id.tv_myearnigsmoney);
        tv_myearnigsmoneydetails = (TextView) findViewById(R.id.tv_myearnigsmoneydetails);
        tv_TX = (TextView) findViewById(R.id.tv_TX);
        tv_shouyiback = findViewById(R.id.tv_shouyiback);
        vp_moneydetails = findViewById(R.id.vp_moneydetails);
        navitationLayout = (NavitationLayout) findViewById(R.id.bar111);

        tv_myearnigsquestion.setOnClickListener(this);
        tv_myearnigsmoney.setOnClickListener(this);
        tv_myearnigsmoneydetails.setOnClickListener(this);
        tv_TX.setOnClickListener(this);
        tv_shouyiback.setOnClickListener(this);
    }

    private void initData() {
        list = new ArrayList();
        list.add(new IncomeFragment());
        list.add(new ExpendFragment());
        viewPagerAdapter111 = new ViewPagerAdapter(getSupportFragmentManager(), list);
        vp_moneydetails.setAdapter(viewPagerAdapter111);
        navitationLayout.setViewPager(this, titles1, vp_moneydetails, R.color.color_777777, R.color.color_333333, 18, 20, 0, 0, true);
        navitationLayout.setBgLine(this, 1, R.color.white);
        navitationLayout.setNavLine(this, 4, R.color.color_zi, 0);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_myearnigsquestion:
                showPop();
                break;
            case R.id.tv_myearnigsmoney:
                showMoney();
                break;
//            case R.id.tv_myearnigsmoneydetails:
//                startActivity(new Intent(this, MoneyDetailsActivity.class));
//                break;
            case R.id.tv_TX:
                showZFTX();
            case R.id.tv_shouyiback:
                finish();
                break;
        }
    }

    private void showPop() {
        ConfirmDialog confirmDialog = new ConfirmDialog(this);
        confirmDialog.setLogoImg(R.mipmap.dialog_notice).setMsg("获得金币数=礼物数×100×平台\\n结算比例：当收到1钻石礼物，平\\n台结算比例为70%，提现到账=金\\n币数÷1000×（1元手续费）");
        confirmDialog.setClickListener(new ConfirmDialog.OnBtnClickListener() {
            @Override
            public void ok() {

            }

            @Override
            public void cancel() {

            }
        });
        confirmDialog.show();
//        view = LayoutInflater.from(this).inflate(R.layout.question_pop, null);
//        popupWindow = new PopupWindow(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
//        popupWindow.setBackgroundDrawable(new BitmapDrawable());
//        popupWindow.setOutsideTouchable(true);
//        popupWindow.showAsDropDown(ll_myearnigs);
    }

    private void showMoney() {
        View view= LayoutInflater.from(this).inflate(R.layout.money_pop, null);
        popupWindow = new PopupWindow(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAsDropDown(ll_myearnigs);
    }

    private void showZFTX() {
       View view = LayoutInflater.from(this).inflate(R.layout.zf_dialog_layout, null);

       LinearLayout  zhifubao = view.findViewById(R.id.ll_zfbtx);
        if (popupWindow == null) {
            popupWindow = new PopupWindow(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
            popupWindow.setBackgroundDrawable(new BitmapDrawable());
            popupWindow.setOutsideTouchable(true);
        }
        popupWindow.showAsDropDown(ll_myearnigs);

        zhifubao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                Uri content_url = Uri.parse("https://api3.dayushaiwang.com/baoxiang/tixian");//此处填链接
                intent.setData(content_url);
                startActivity(intent);

            }
        });
    }
}
