package com.xunshan.ZhenSheng.activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;


import com.tencent.qcloud.tim.uikit.base.BaseActvity;
import com.tencent.qcloud.tim.uikit.modules.chat.base.ChatInfo;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.util.Constants;

public class ChatIMActivity extends BaseActvity {

    private ChatFragment mChatFragment;
    private ChatInfo mChatInfo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.em_activity_chat);

        chat(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {

        super.onNewIntent(intent);
        chat(intent);
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    private void chat(Intent intent) {
        Bundle bundle = intent.getExtras();

        // 离线推送测试代码结束

        mChatInfo = (ChatInfo) bundle.getSerializable(Constants.CHAT_INFO);
        mChatFragment = new ChatFragment();
        mChatFragment.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.empty_view, mChatFragment).commitAllowingStateLoss();
    }
}



