package com.xunshan.ZhenSheng.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.scwang.smartrefresh.layout.api.RefreshHeader;
import com.scwang.smartrefresh.layout.api.RefreshLayout;
import com.scwang.smartrefresh.layout.constant.SpinnerStyle;
import com.scwang.smartrefresh.layout.footer.BallPulseFooter;
import com.scwang.smartrefresh.layout.header.BezierRadarHeader;
import com.scwang.smartrefresh.layout.listener.OnLoadMoreListener;
import com.scwang.smartrefresh.layout.listener.OnRefreshListener;
import com.scwang.smartrefresh.layout.listener.SimpleMultiPurposeListener;
import com.tencent.qcloud.tim.uikit.TUIKit;
import com.tencent.qcloud.tim.uikit.base.IUIKitCallBack;
import com.tencent.qcloud.tim.uikit.utils.ToastUtil;
import com.xunshan.ZhenSheng.entity.BannerLunBoBean;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.adapter.HomeRoomAdapter;
import com.xunshan.ZhenSheng.adapter.MyRecyclerViewAdapter;
import com.xunshan.ZhenSheng.adapter.MyRecyclerViewLabelAdapter;
import com.xunshan.ZhenSheng.adapter.RoomTypeAdapter;
import com.xunshan.ZhenSheng.entity.HoomListBean;
import com.xunshan.ZhenSheng.entity.InsertRoomBean;
import com.xunshan.ZhenSheng.entity.PersonalDataBean;
import com.xunshan.ZhenSheng.entity.Room;
import com.xunshan.ZhenSheng.entity.RoomInforBean;
import com.xunshan.ZhenSheng.entity.Wheel;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.HorizontalItemDecoration;
import com.xunshan.ZhenSheng.util.LogUtil;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.fragment.PayFragment;
import com.xunshan.ZhenSheng.util.PayPwdView;
import com.xunshan.ZhenSheng.util.RetrofitUtils;
import com.xunshan.ZhenSheng.util.RoundImageView;
import com.youth.banner.Banner;
import com.youth.banner.BannerConfig;
import com.youth.banner.Transformer;
import com.youth.banner.listener.OnBannerListener;
import com.youth.banner.loader.ImageLoader;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener,
        GestureDetector.OnGestureListener, RoomTypeAdapter.roomTypeCallBack, HomeRoomAdapter.roomAdapterCallBack, OnBannerListener, PayPwdView.InputCallBack {
    private RecyclerView rv_type;
    List<Wheel> rotainList;
    List<Room> typeList;
    private List<String> MList2 = new ArrayList<>();//应该是这个new了两次的问题，我之前以为不影响，现在没出先数据
    GridView gv_home;
    HomeRoomAdapter roomAdapter;
    Room room;

    private MyRecyclerViewLabelAdapter myRecyclerViewLabelAdapter;
    private RecyclerView recyclerView;

    private List<HoomListBean.DataBean> mList = new ArrayList<>();
    private List<HoomListBean.DataBean> mListSecond = new ArrayList<>();
    List<Room> listRoom;
    ImageView img_home_createhome;
    LinearLayout ll_home_search;
    private static String[] PERMISSIONS_REQUEST = {
            "android.permission.READ_EXTERNAL_STORAGE",
            "android.permission.WRITE_EXTERNAL_STORAGE", Manifest.permission.RECORD_AUDIO};
    private static final int PERMISSIONS_REQUEST_CODE = 1002;
    GestureDetector gesture_detector;
    private static final int MaxWidth = 80;
    private static final int MaxSpeed = 200;
    //private ImageView mIvSpeakerState;
    private RequestOptions options = new RequestOptions();//如果不用规则就不要用这个,上次这个没初始化，这次还是这个没初始化
    //不初始化这个不就空了吗


    String strBack = "";
    JSONObject obj;
    JSONArray arr;
    RotainHandler rotainHandler;
    int rotainIndex = 0;
    LinearLayoutManager linearLayoutManager;
    public static int typeIndex = 0;
    RoomTypeAdapter typeAdapter;
    Message msg;
    private RefreshLayout mRefreshLayout;
    private ViewFlipper tvbannertitle;
    private String usernmae;
    private String userID;
    private String token;

    private Banner banner;
    private ArrayList<String> list_path = new ArrayList<>();//远程有问题
    private ArrayList<String> list_title = new ArrayList<>();
    private String currentPage;
    private String totalPage;
    private String Room_Id;
    //记录当前位置
    private int currentPosition = 0;
    private int page = 0;
    private int totalPage1;
    private int currentPage1;
    private MyRecyclerViewAdapter myRecyclerViewAdapter;
    private int mCurrPos;
    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.home_activity);

        sharedPreferences = getSharedPreferences("data", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        token = sharedPreferences.getString("token", "");
        usernmae = sharedPreferences.getString("user_name", "");
        userID = sharedPreferences.getString("user_ID", "");
        initView();
        initData2();
        initData3(page);
        ShuaXinJiaZai();
    }

    private void initView() {
        img_home_createhome = (ImageView) findViewById(R.id.img_home_createhome);
        rv_type = (RecyclerView) findViewById(R.id.rv_type);
        recyclerView = (RecyclerView) findViewById(R.id.rv_home);
        tvbannertitle = (ViewFlipper) findViewById(R.id.tv_banner_title);
        mRefreshLayout = findViewById(R.id.refreshLayout);
        ll_home_search = findViewById(R.id.ll_home_search);

        myRecyclerViewLabelAdapter = new MyRecyclerViewLabelAdapter(HomeActivity.this, MList2);
        LinearLayoutManager manager = new LinearLayoutManager(HomeActivity.this);
        manager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rv_type.setLayoutManager(manager);
        rv_type.addItemDecoration(new HorizontalItemDecoration(10, HomeActivity.this));
        rv_type.setAdapter(myRecyclerViewLabelAdapter);
        myRecyclerViewLabelAdapter.setOnItemClickListener(new MyRecyclerViewLabelAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                myRecyclerViewLabelAdapter.setSelectPosition(position);
                myRecyclerViewLabelAdapter.notifyDataSetChanged();
                //2020.06.01 vic 修改
                //                type = MList2.get(position);
                mList.clear();
                type = "0";
                page = 0;
                initData3(page);
            }
        });

        GridLayoutManager layoutManager = new GridLayoutManager(HomeActivity.this, 3, LinearLayoutManager.VERTICAL, false);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        myRecyclerViewAdapter = new MyRecyclerViewAdapter(HomeActivity.this, mList);
        recyclerView.setAdapter(myRecyclerViewAdapter);

        myRecyclerViewAdapter.setOnItemClickListener(new MyRecyclerViewAdapter.ItemClickListener() {
            @Override
            public void onItemClick(int position) {
                Room_Id = mList.get(position).getRoom_id();
                initDT(Room_Id,position);
            }
        });


        mRefreshLayout.setRefreshFooter(new BallPulseFooter(this));
        mRefreshLayout.setRefreshHeader(new BezierRadarHeader(this));
        //设置样式后面的北京颜色
        mRefreshLayout.setPrimaryColorsId(R.color.white, android.R.color.white);

        //纵向线性布局
        //LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        //纵向线性布局
        banner = findViewById(R.id.banner);
        list_path = new ArrayList<>();
        list_title = new ArrayList<>();
        img_home_createhome.setOnClickListener(this);
        ll_home_search.setOnClickListener(this);


        mRefreshLayout.setEnableRefresh(true);//是否启用下拉刷新功能
        mRefreshLayout.setEnableLoadMore(true);//是否启用上拉加载功能

    }

    private void moveNext() {
//        setView(this.mCurrPos, this.mCurrPos + 1);
        int count = list_title.size();
        for (int i = 0; i < count; i++) {
            final View ll_content = View.inflate(this, R.layout.item_flipper, null);
            TextView tv_content = (TextView) ll_content.findViewById(R.id.tv_content);
            tv_content.setText(list_title.get(i));
            tvbannertitle.addView(ll_content);
        }
        if (count == 1) {
            tvbannertitle.stopFlipping();
            tvbannertitle.setAutoStart(false);
        }
        tvbannertitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(HomeActivity.this, list_title.get(tvbannertitle.getDisplayedChild()).toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setView(int curr, int next) {
        //        View noticeView = getLayoutInflater().inflate(R.layout.notice_item, null);
        //        TextView notice_tv = (TextView) noticeView.findViewById(R.id.notice_tv);
        //        if ((curr < next) && (next > (list_title.size() - 1))) {
        //            next = 0;
        //        } else if ((curr > next) && (next < 0)) {
        //            next = list_title.size() - 1;
        //        }
        //        notice_tv.setText(list_title.get(next));
        //        if (tvbannertitle.getChildCount() > 1) {
        //            tvbannertitle.removeViewAt(0);
        //        }
        //        tvbannertitle.addView(noticeView, tvbannertitle.getChildCount());
        //        mCurrPos = next;

    }

    private void setBannerData() {
        MyImageLoader mMyImageLoader = new MyImageLoader();
        //设置图片加载器
        banner.isAutoPlay(true);
        banner.setBannerStyle(BannerConfig.NOT_INDICATOR);
        banner.setImageLoader(mMyImageLoader);
        //        banner.setImageLoader(new MyLoader());
        banner.setBannerAnimation(Transformer.DepthPage);
        // banner.setBannerTitles(list_title);
        banner.setDelayTime(3000);
        banner.setIndicatorGravity(BannerConfig.CENTER);
        banner.setImages(list_path)
                .setOnBannerListener(HomeActivity.this)
                .start();
    }

    private void initData2() {
        Map<String, String> map = new HashMap<>();
        map.put("token", User.user().getToken());
        map.put("user_id", User.user().getUser_ID());
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/index/index", map, new NetCallBack<BannerLunBoBean>() {
            @Override
            public void onSucceed(final BannerLunBoBean bannerLunBoBean, int type) {
                if (bannerLunBoBean.getErr().equals("0") && bannerLunBoBean.getData() != null) {
                    boolean isFirst = true;
                    if (!list_path.isEmpty() && list_path.size() != 0) {
                        list_path.clear();
                        list_title.clear();
                        isFirst = false;//ge
                    }
                    if (bannerLunBoBean.getIdentity().equals("1")) {
                        if (bannerLunBoBean.getUsersig().equals(User.user().getUser_IM_sign())) {
                            TX_im_login();
                        } else {
                            User.user().setUser_IM_sign(bannerLunBoBean.getUsersig());
                            TX_im_login();
                        }
                        for (BannerLunBoBean.DataBean.PicBean pic : bannerLunBoBean.getData().getPic()) {
                            list_path.add(AllInterface.addr + "/" + pic.getPic());
                        }
                        //前面是Object对象的类型  中间是变量名:这里是具体数据//这控件有点问题，首次必须要有数据，不能为空
                        //为空不加载
                        //其实fori也行，就是层数多了看着头晕
                        for (BannerLunBoBean.DataBean.TxtBean txt : bannerLunBoBean.getData().getTxt()) {
                            list_title.add(txt.getText());
                        }//没有显示数据
                        MList2.addAll(bannerLunBoBean.getData().getLabel());
                        //MList2=bannerLunBoBean.getData().getLabel();
                        myRecyclerViewLabelAdapter.notifyDataSetChanged();
                    }
                    if (isFirst) {
                        setBannerData();
                        moveNext();
                    } else {
                        banner.update(list_path, list_title);//这个的作用是换成新的图片标题
                    }
                }
            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int type) {
                Log.e("1234567", "onError: 1");
            }
        }, 1);
    }

    private void TX_im_login() {
        TUIKit.login(userID, User.user().getUser_IM_sign(), new IUIKitCallBack() {
            @Override
            public void onSuccess(Object data) {

            }

            @Override
            public void onError(String module, int errCode, String errMsg) {
                Toast.makeText(HomeActivity.this, errMsg, Toast.LENGTH_LONG).show();
            }
        });


    }


    private int mOffset = 0;
    private void ShuaXinJiaZai() {
        mRefreshLayout.setOnRefreshListener(new OnRefreshListener() {
            @Override
            public void onRefresh(@NonNull RefreshLayout refreshLayout) {
                page = 0;
                mList.clear();
                initData3(page);
            }
        });

        //上拉加载
        mRefreshLayout.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore(@NonNull RefreshLayout refreshLayout) {
                initData3(++page);
            }
        });
    }

    private PayFragment fragment;
    private String room_userid = "";
    private String type = "0";
    private void initData3(int page) {
        Map<String, String> map = new HashMap<>();
        map.put("type", type);
        map.put("token", token);
        map.put("currentPage", String.valueOf(page));
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/index/room", map, new NetCallBack<HoomListBean>() {
            @Override
            public void onSucceed(final HoomListBean hoomListBean, int type) {
                mRefreshLayout.finishLoadMore();
                mRefreshLayout.finishRefresh();

                if (hoomListBean.getData().size() == 0) {
                    return;
                }

                currentPage1 = Integer.parseInt(hoomListBean.getCurrentPage());
                totalPage1 = Integer.parseInt(hoomListBean.getTotalPage());

                mListSecond = hoomListBean.getData();
                mList.addAll(mListSecond);
                myRecyclerViewAdapter.notifyDataSetChanged();
            }

            @Override
            public void SucceedError(Exception e, int types) {
                mRefreshLayout.finishLoadMore();
                mRefreshLayout.finishRefresh();
            }


            @Override
            public void onError(Throwable throwable, int type) {
                mRefreshLayout.finishLoadMore();
                mRefreshLayout.finishRefresh();
            }
        }, 1);
    }

    private void getRoomPass(int position) {
        Room_Id = mList.get(position).getRoom_id();
        Map<String, String> map = new HashMap<>();
        map.put("user_id", userID);
        map.put("token", token);
        map.put("room_id", Room_Id);

        sharedPreferences = getSharedPreferences("data", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("room_ID", String.valueOf(Room_Id));
        //步骤4：提交
        editor.commit();
        Log.e("roomInforBean===", "" + userID + token + Room_Id);
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/index", map, new NetCallBack<RoomInforBean>() {
            @Override
            public void onSucceed(final RoomInforBean roomInforBean, int type) {
                room_userid = roomInforBean.getRoom().getUser_id();
                SharedPreferences.Editor editor = sharedPreferences.edit();
                //步骤3：将获取过来的值放入文件
                //   editor.putString("Online_room_id", mList.get(position).getNumber());
                editor.putString("room_name", mList.get(position).getName());
                editor.putString("room_id", roomInforBean.getRoom().getRoom_id());
                editor.putString("Over_pipe", roomInforBean.getOver_pipe());
                //步骤4：提交
                editor.commit();

                if (roomInforBean.getOver_pipe().equals("1")) {
                    Intent intent = new Intent();
                    //intent.putExtra("x123",mList.get(position));
                    intent.putExtra("room_id", Room_Id);
                    intent.putExtra("Online_room_id", mList.get(position).getNumber());
                    intent.putExtra("room_name", mList.get(position).getName());
                    intent.putExtra("room_userid", roomInforBean.getRoom().getUser_id());
                    intent.putExtra("Over_pipe", roomInforBean.getOver_pipe());
                    LogUtil.e("获取个人信息");
                    LogUtil.e("获取个人信息");
                    intent.setClass(HomeActivity.this, RoomActivity.class);
                    startActivity(intent);
                } else {

                    User.user().setRoomlock(roomInforBean.getRoom().getIslock());
                    if (roomInforBean.getRoom().getIslock().equals("1")) {
                        Bundle bundle = new Bundle();
                        bundle.putString(PayFragment.EXTRA_CONTENT, "房间密码");
                        bundle.putString(PayFragment.PASS_CONTENT, roomInforBean.getRoom().getPass());
                        fragment = new PayFragment();
                        fragment.setArguments(bundle);
                        fragment.setPaySuccessCallBack(HomeActivity.this);
                        Log.e("hahahha", "dlalpsa====" + getSupportFragmentManager());
                        fragment.show(getSupportFragmentManager(), "Pay");
                        LogUtil.e("获取个人信息");


                    } else if (roomInforBean.getRoom().getIslock().equals("0")) {
                        Intent intent = new Intent();
                        //intent.putExtra("x123",mList.get(position));
                        intent.putExtra("room_id", Room_Id);
                        intent.putExtra("Online_room_id", mList.get(position).getNumber());
                        intent.putExtra("room_name", mList.get(position).getName());
                        intent.putExtra("room_userid", roomInforBean.getRoom().getUser_id());
                        intent.putExtra("Over_pipe", roomInforBean.getOver_pipe());

                        intent.setClass(HomeActivity.this, RoomActivity.class);
                        startActivity(intent);
                    }
                }
            }

            @Override
            public void SucceedError(Exception e, int types) {

            }

            @Override
            public void onError(Throwable throwable, int type) {

            }
        }, 1);
    }

    @Override
    public void typePosition(int position) {
        typeIndex = position;
        //  initRoom(typeList.get(position).getrId());
        typeAdapter.notifyDataSetChanged();
    }

    @Override
    public void getPosition(int position) {
        startActivity(new Intent(this, RoomActivity.class)
                .putExtra("room_userid", room_userid)
                .putExtra("roomid", listRoom.get(position).getrId()));
    }

    @Override
    public void onInputFinish(String result) {
        //房间密码获取toast
        // PayFragment fragment = new PayFragment();
        fragment.isOk(room_userid);


    }

    class RotainHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    rotainIndex = Integer.parseInt(msg.obj + "");
                    if (rotainIndex != rotainIndex) {
                        rotainIndex = rotainIndex;
                    }
                    break;
                case 2:
                    break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_home_createhome:
                Log.e("TAGonClick", "onClick: " + token + userID);
                Map<String, String> map = new HashMap<>();
                map.put("user_id", User.user().getUser_ID());
                map.put("token", User.user().getToken());
                Log.e("TAGonClick", "onClick: " + token + userID);
                RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/room/insertRoom", map, new NetCallBack<InsertRoomBean>() {
                    @Override
                    public void onSucceed(final InsertRoomBean insertRoomBean, int type) {

                        Intent intent = new Intent(HomeActivity.this, RoomActivity.class);
                        intent.putExtra("room_id", insertRoomBean.getRoom().getRoom_id());
                        intent.putExtra("room_name", insertRoomBean.getRoom().getName());
                        intent.putExtra("room_userid", insertRoomBean.getRoom().getUser_id());
                        startActivity(intent);

                    }

                    @Override
                    public void SucceedError(Exception e, int types) {

                    }

                    @Override
                    public void onError(Throwable throwable, int type) {

                    }
                }, 1);

                break;
            case R.id.ll_home_search:
                startActivity(new Intent(this, SearchActivity.class));
                break;
            default:
                break;
        }
    }

    protected boolean checkOrRequestPermission(int requestCode) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, "android.permission.READ_EXTERNAL_STORAGE") != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(this, "android.permission.WRITE_EXTERNAL_STORAGE") != PackageManager.PERMISSION_GRANTED) {
                this.requestPermissions(PERMISSIONS_REQUEST, requestCode);
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        try {
            return this.gesture_detector.onTouchEvent(event);
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public boolean onDown(MotionEvent e) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent e) {
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @SuppressLint("WrongConstant")
    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        try {
            if (e1.getX() - e2.getX() > MaxWidth && Math.abs(velocityX) > MaxSpeed) {
                if (typeIndex + 1 == typeList.size()) {
                } else {
                    typeIndex++;
                }
                return true;
            } else if (e2.getX() - e1.getX() > MaxWidth && Math.abs(velocityX) > MaxSpeed) {
                if (typeIndex - 1 < 0) {
                    typeIndex = 0;
                } else {
                    typeIndex--;
                }
                return true;
            }
        } catch (Exception e) {
            Log.e("fling", "" + e);
            return false;
        }
        return true;
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        try {
            return super.dispatchTouchEvent(ev);
        } catch (Exception e) {
            return false;
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    /*
     * 轮播监听
     *
     * @param position
     */
    @Override
    public void OnBannerClick(int position) {
        //        Intent intent = new Intent(HomeActivity.this, ShenhaiActivity.class);
        //        intent.putExtra("url", LunBoBean.getData().getPic().get(position).getDic().getUrl() + "?token=" + User.user().getToken() + "&user_id=" + User.user().getUser_ID());
        //        Log.e("TAG", "OnBannerClick: " + LunBoBean.getData().getPic().get(position).getDic().getUrl() + "?token=" + User.user().getToken() + "&user_id=" + User.user().getUser_ID());
        //        startActivity(intent);
    }


    /**
     * 网络加载图片
     * 使用了Glide图片加载框架
     */
    private class MyLoader extends ImageLoader {
        @Override
        public void displayImage(Context context, Object path, ImageView imageView) {
            //比如这里，不过这个因为是个循环在这里会反复初始化不太好就在这个方法上也行，比如465，离的近看的到
            //能运行吗，看看效果好了没
            Glide.with(context.getApplicationContext())
                    .load((String) path)
//                    .apply(options)//你不需要定性其他规则的时候就不要用这个，或者在使用之前直接初始化
                    .into(imageView);
        }
    }

    /**
     * 图片加载类
     */
    private class MyImageLoader extends ImageLoader {
        @Override
        public void displayImage(Context context, Object path, ImageView imageView) {
            Glide.with(context).load(path).into(imageView);
        }

        /**
         * 自定义圆角类
         * @param context
         * @return
         */
        @Override
        public ImageView createImageView(Context context) {
            RoundImageView img = new RoundImageView(context);
            return img;
        }
    }

    private void initDT(String room_Id,int position) {
        Map<String, String> map = new HashMap<>();
        map.put("user_id", User.user().getUser_ID());
        map.put("token", User.user().getToken());
        map.put("room_id", room_Id);
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/room/information", map, new NetCallBack<PersonalDataBean>() {
            @Override
            public void onSucceed(final PersonalDataBean personalDataBean, int type) {
                User.user().setRoomname_name(personalDataBean.getRoom_user().getName());
                User.user().setRoomname_Charm(personalDataBean.getRoom_user().getCharm());
                User.user().setRoomname_Headpic(personalDataBean.getRoom_user().getHeadpic());
                getRoomPass(position);
            }

            @Override
            public void SucceedError(Exception e, int types) {

            }

            @Override
            public void onError(Throwable throwable, int type) {

            }
        }, 1);
    }
}


