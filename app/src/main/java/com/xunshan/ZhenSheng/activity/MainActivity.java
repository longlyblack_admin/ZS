package com.xunshan.ZhenSheng.activity;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TabWidget;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tencent.qcloud.tim.uikit.utils.ToastUtil;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.data.ZegoDataCenter;
import com.xunshan.ZhenSheng.entity.MyPrincipalBean;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.LogUtil;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.RetrofitUtils;
import com.zego.chatroom.ZegoChatroom;
import com.zego.chatroom.entity.ZegoChatroomUser;

import java.util.HashMap;
import java.util.Map;

/**
 * 主界面
 */
public class MainActivity extends android.app.TabActivity {
    public static TabHost tabHost;
    @SuppressWarnings("unused")
    private TabWidget tabWidget;
    private View view1 = null;
    private View view2 = null;
    private View view3 = null;
    //    private TextView tv1 = null;
//    private TextView tv2 = null;
//    private TextView tv3 = null;
    private ImageView img1 = null;
    private ImageView img2 = null;
    private ImageView img3 = null;
    private TextView text1 = null;
    private TextView text2 = null;
    private TextView text3 = null;
    private LinearLayout ll1 = null;
    private LinearLayout ll2 = null;
    private LinearLayout ll3 = null;
    private String usernmae;
    private String token;
    private String userID;
    public static Activity actmain;
    public ZegoChatroomUser zegoChatroomUser;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        actmain = this;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        SharedPreferences sharedPreferences = getSharedPreferences("data", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        userID = User.user().getUser_ID();
        setContentView(R.layout.activity_main);
        Log.e("TAGuserID", "onCreate: " + userID);

        if (!TextUtils.isEmpty(User.user().getUser_ID()) && !TextUtils.isEmpty(User.user().getUser_name())) {
            zegoChatroomUser = new ZegoChatroomUser(); // 根据自己情况初始化唯一识别USER
            zegoChatroomUser.userID = User.user().getUser_ID(); // 使用 SERIAL 作为用户的唯一识别
            zegoChatroomUser.userName = User.user().getUser_name();
            ZegoChatroom.setupContext(this, zegoChatroomUser, ZegoDataCenter.APP_ID, ZegoDataCenter.APP_SIGN);
            ZegoChatroom.setUseTestEnv(ZegoDataCenter.IS_TEST_ENV);
        }else{

        }

        init();
        date();
    }


    private void initUserInfo() {
        if (TextUtils.isEmpty(User.user().getUser_ID())) {
            ToastUtil.toastShortMessage("userId==null");
            return;
        }

        if (TextUtils.isEmpty(User.user().getToken())) {
            ToastUtil.toastShortMessage("token==null");
            return;
        }

        Map<String, String> map = new HashMap<>();
        map.put("user_id", User.user().getUser_ID());
        map.put("token", User.user().getToken());
        map.put("touser_id", User.user().getUser_ID());
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/home/index", map, new NetCallBack<MyPrincipalBean>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onSucceed(final MyPrincipalBean myPrincipalBean, int type) {
                com.zego.ve.Log.e("myActivity", "home/home/index-->" + new Gson().toJson(myPrincipalBean));
                User.user().setUser_name(myPrincipalBean.getUser().getName());
                User.user().setAddress(myPrincipalBean.getUser().getAddress());
                User.user().setUser_ID(myPrincipalBean.getUser().getUser_id());
                User.user().setUser_level(myPrincipalBean.getUser().getLevel());
                User.user().setSex(myPrincipalBean.getUser().getSex());
                User.user().setBirthaday(myPrincipalBean.getUser().getBirthaday());
                User.user().setSign(myPrincipalBean.getUser().getSign());
                User.user().setUser_Headpic(myPrincipalBean.getUser().getHeadpic());

                zegoChatroomUser = new ZegoChatroomUser(); // 根据自己情况初始化唯一识别USER
                zegoChatroomUser.userID = User.user().getUser_ID(); // 使用 SERIAL 作为用户的唯一识别
                zegoChatroomUser.userName = User.user().getUser_name();
                ZegoChatroom.setupContext(MainActivity.this, zegoChatroomUser, ZegoDataCenter.APP_ID, ZegoDataCenter.APP_SIGN);
                ZegoChatroom.setUseTestEnv(ZegoDataCenter.IS_TEST_ENV);
            }

            @Override
            public void SucceedError(Exception e, int types) {
                LogUtil.e("个人信息获取失败-->"+ e.getMessage());
            }

            @Override
            public void onError(Throwable throwable, int types) {
                LogUtil.e("个人信息获取失败-->"+ throwable.getMessage());
            }
        }, 1);
    }



    //判断是否第一次打开软件
    private void date() {
        String[] permissionNeeded = {"android.permission.RECORD_AUDIO"};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, "android.permission.RECORD_AUDIO") != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(permissionNeeded, 101);
            }
        }
    }

    private void init() {
        tabHost = this.getTabHost();
        initView();
        tabHost.setup();
        tabWidget = tabHost.getTabWidget();
        tabHost.addTab(tabHost.newTabSpec("msg").setIndicator(view1)
                .setContent(new Intent(this, MsgActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)));
        tabHost.addTab(tabHost.newTabSpec("home").setIndicator(view2)
                .setContent(new Intent(this, HomeActivity.class)));
        tabHost.addTab(tabHost.newTabSpec("my").setIndicator(view3)
                .setContent(new Intent(this, MyActivity.class)));
        tabHost.setCurrentTab(1);
    }

    private void origin() {

    }

    private void initView() {


        LayoutInflater inflater = this.getLayoutInflater();
        view1 = inflater.inflate(R.layout.tabmini, null);
        ll1 = (LinearLayout) view1.findViewById(R.id.ll_tab);
        text1 = view1.findViewById(R.id.tab_lable);
        img1 = (ImageView) view1.findViewById(R.id.img_tab);
        img1.setImageResource(R.mipmap.msg_no_pitch);
        text1.setText("消息");
        ll1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                text1.setTextColor(Color.parseColor("#FA11F2"));
                text2.setTextColor(Color.parseColor("#9D68FC"));
                text3.setTextColor(Color.parseColor("#9D68FC"));
                img1.setImageResource(R.mipmap.msg_pitch);
                img2.setImageResource(R.mipmap.room_no_pitch);
                img3.setImageResource(R.mipmap.my_no_pitch);
                origin();
                tabHost.setCurrentTab(0);
            }
        });

        view2 = inflater.inflate(R.layout.tabmini, null);
        ll2 = (LinearLayout) view2.findViewById(R.id.ll_tab);
        text2 = view2.findViewById(R.id.tab_lable);
        img2 = (ImageView) view2.findViewById(R.id.img_tab);
        img2.setImageResource(R.mipmap.room_pitch);
        text2.setText("房间");
        text2.setTextColor(Color.parseColor("#FA11F2"));
        ll2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                text1.setTextColor(Color.parseColor("#9D68FC"));
                text2.setTextColor(Color.parseColor("#FA11F2"));
                text3.setTextColor(Color.parseColor("#9D68FC"));

                img1.setImageResource(R.mipmap.msg_no_pitch);
                img2.setImageResource(R.mipmap.room_pitch);
                img3.setImageResource(R.mipmap.my_no_pitch);
                origin();
                tabHost.setCurrentTab(1);

            }
        });

        view3 = inflater.inflate(R.layout.tabmini, null);
        ll3 = (LinearLayout) view3.findViewById(R.id.ll_tab);
        text3 = view3.findViewById(R.id.tab_lable);
        img3 = (ImageView) view3.findViewById(R.id.img_tab);
        img3.setImageResource(R.mipmap.my_no_pitch);
        text3.setText("我的");
        ll3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                text1.setTextColor(Color.parseColor("#9D68FC"));
                text2.setTextColor(Color.parseColor("#9D68FC"));
                text3.setTextColor(Color.parseColor("#FA11F2"));
                img1.setImageResource(R.mipmap.msg_no_pitch);
                img2.setImageResource(R.mipmap.room_no_pitch);
                img3.setImageResource(R.mipmap.my_picth);
                origin();
                tabHost.setCurrentTab(2);
            }
        });
    }


}
