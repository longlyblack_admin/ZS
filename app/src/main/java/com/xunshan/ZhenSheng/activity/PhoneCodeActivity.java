package com.xunshan.ZhenSheng.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.os.StrictMode;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import com.tencent.qcloud.tim.uikit.TUIKit;
import com.tencent.qcloud.tim.uikit.base.IUIKitCallBack;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.entity.PhoenLoginBean;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.NetWork;
import com.xunshan.ZhenSheng.util.PhoneCode;
import com.xunshan.ZhenSheng.util.RetrofitUtils;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;

public class PhoneCodeActivity extends AppCompatActivity implements View.OnClickListener {
    Button btn_getCode;
    Button btn_fanhui1;
    EditText edt_code;
    numHandler handler;
    Button btn_login;
    PhoneCode pc_1;
    private String number;
    private String code;
    private String user_id;
    private String token;
    private String user_name;
    private String sex;
    private String user_level;
    private String suer_IM_sign;

    private static final String SMS_RECEIVED_ACTION = "android.provider.Telephony.SMS_RECEIVED";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.phonecode_activity);
        initView();
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        User.user().setPhone(edt_code.getText().toString().trim());
    }

    private void initView() {
        btn_getCode = (Button) findViewById(R.id.btn_getCode);
        btn_fanhui1 = (Button) findViewById(R.id.btn_fanhui1);
        edt_code = (EditText) findViewById(R.id.edt_code);
        btn_login = (Button) findViewById(R.id.btn_login);
        pc_1 = (PhoneCode) findViewById(R.id.pc_1);

        btn_getCode.setOnClickListener(this);
        btn_login.setOnClickListener(this);
        btn_fanhui1.setOnClickListener(this);
        handler = new numHandler();
    }

    private void initData() {
        Map<String, String> map = new HashMap<>();
        map.put("phone", edt_code.getText().toString().trim());
        map.put("code", code);
        final ProgressDialog pd = new ProgressDialog(PhoneCodeActivity.this);
        pd.setMessage("加载中...");
        pd.show();
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "revo/login/save", map, new NetCallBack<PhoenLoginBean>() {
            @Override
            public void onSucceed(PhoenLoginBean phoenLoginBean, int types) {
                pd.dismiss();
                if (phoenLoginBean.getErr() == 0) {
                    if (phoenLoginBean.getIsone() == 1) {
                        user_id = String.valueOf(phoenLoginBean.getUser_id());
                        token = phoenLoginBean.getToken();
                        user_name = phoenLoginBean.getName();
                        sex = String.valueOf(phoenLoginBean.getSex());
                        user_level = String.valueOf(phoenLoginBean.getLevel());
                        suer_IM_sign = phoenLoginBean.getUsersig();
                        User.user().setUser_IM_sign(suer_IM_sign);
                        TUIKit.login(user_id, User.user().getUser_IM_sign(), new IUIKitCallBack() {
                            @Override
                            public void onSuccess(Object data) {
                                User.user().setUser_name(user_name);
                                User.user().setPhone(edt_code.getText().toString().trim());
                                User.user().setUser_ID(user_id);
                                User.user().setUser_level(user_level);
                                User.user().setToken(token);

                                Toast.makeText(PhoneCodeActivity.this, "验证码成功，真声带来美好生活", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(PhoneCodeActivity.this, MainActivity.class);
                                startActivity(intent);
                                PhoneCodeActivity.this.finish();
                                LoginActivity.mContext.finish();
                            }

                            @Override
                            public void onError(String module, int errCode, String errMsg) {
                                Toast.makeText(PhoneCodeActivity.this, errMsg, Toast.LENGTH_LONG).show();
                            }
                        });
                    } else {
                        Intent intent = new Intent(PhoneCodeActivity.this, LoginInfoActivity.class);
                        intent.putExtra("Phone", edt_code.getText().toString().trim());
                        startActivity(intent);
                    }
                }
                if (phoenLoginBean.getErr() == 1) {
                    pd.dismiss();
                    Toast.makeText(PhoneCodeActivity.this, "验证码错误，请重新输入", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void SucceedError(Exception e, int types) {
                Log.e("12345678", "onSucceed: 4");
                pd.dismiss();
            }

            @Override
            public void onError(Throwable throwable, int type) {
                Log.e("12345678", "onError: 5");
                pd.dismiss();
            }
        }, 1);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_getCode:
                final MyCountDownTimer myCountDownTimer = new MyCountDownTimer(60000, 1000);
                if (btn_getCode.getText().toString().equals(" ")) {
                    Toast.makeText(this, "输入手机号格式不对", Toast.LENGTH_SHORT);
                }
                number = btn_getCode.getText().toString().trim();
                if (number.equals("获取验证码") || number.equals("重新获取验证码")) {
                    getCode();
                }
                myCountDownTimer.start();
                break;
            case R.id.btn_login:
                login();
                break;
            case R.id.btn_fanhui1:
                finish();
                break;
        }
    }

    private void login() {
        code = pc_1.getPhoneCode();
        if (code.length() == 6) {

            // Toast.makeText(this, "验证码成功，真声带来美好生活", Toast.LENGTH_LONG).show();
            initData();
        }
    }


    private String edtToStr(EditText edt) {
        return edt.getText().toString().trim();
    }

    private void getCode() {
        new codeThread().start();
    }

    class codeThread extends Thread {
        @Override
        public void run() {
            super.run();
            try {
                String back = NetWork.post(AllInterface.getCode + "?phone=" + edt_code.getText().toString().trim(), "");
                JSONObject obj = new JSONObject(back);
                if (obj.getString("err").equalsIgnoreCase("0")) {
                    toastText("发送成功");
                    Intent intent = new Intent(PhoneCodeActivity.this, LoginInfoActivity.class);
                    intent.putExtra("Phone", edt_code.getText().toString().trim());
                    startActivity(intent);
                } else {
                    toastText(obj.getString("msg"));
                }
            } catch (Exception e) {
                Log.e("getcode", "" + e);
                e.printStackTrace();
            }
        }
    }


    class numHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    int obj = Integer.parseInt(msg.obj + "");
                    if (obj == 0) {
                        btn_getCode.setText("重新获取验证码");
                    } else {
                        btn_getCode.setText("重新获取(" + obj + "s)");
                    }
                    break;
            }
        }
    }

    private void toastText(String str) {
        Toast.makeText(this, str, Toast.LENGTH_LONG).show();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    class MyCountDownTimer extends CountDownTimer {

        public MyCountDownTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        //计时过程
        @Override
        public void onTick(long l) {
            //防止计时过程中重复点击
            btn_getCode.setClickable(false);
            btn_getCode.setText( l/1000+"s"+"重发");
            btn_getCode.setTextColor(Color.parseColor("#AAAAAA"));
            btn_getCode.setBackground(getDrawable(R.drawable.butto_daojishi));

        }

        //计时完毕的方法
        @Override
        public void onFinish() {
            //重新给Button设置文字
            btn_getCode.setText("重新获取");
            //设置可点击
            btn_getCode.setClickable(true);
        }
    }

}

