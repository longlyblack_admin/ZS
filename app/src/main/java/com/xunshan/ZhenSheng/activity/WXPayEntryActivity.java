package com.xunshan.ZhenSheng.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.xunshan.ZhenSheng.R;



public class WXPayEntryActivity extends Activity implements IWXAPIEventHandler {



    private IWXAPI iwxapi;
    private static final String APP_ID = "wx62258d793a349833";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // 由第三方App个性化展示支付结果
        setContentView(R.layout.activity_wxpay_entry);

        iwxapi = WXAPIFactory.createWXAPI(this, APP_ID,true);
        iwxapi.registerApp(APP_ID);
        iwxapi.handleIntent(getIntent(), this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        iwxapi.handleIntent(intent, this);
    }

    @Override
    public void onReq(BaseReq baseReq) {

    }

    /**
     * TODO 微信支付回调
     */
    @Override
    public  void onResp(BaseResp baseResp) {
        Log.d("TAG", "onResp: " + baseResp.errStr);
        Log.d("TAG", "onResp: 错误码" + baseResp.errCode);
        int errCode = baseResp.errCode;

        if (errCode == 0) {
            // 成功
            showMessage("支付成功");
        } else if (errCode == -1) {
            // 失败
            showMessage("失败");
        } else if (errCode == -2) {
            // 取消
            showMessage("取消");
        }
    }

    private  void showMessage(String str) {
        Toast.makeText(WXPayEntryActivity.this, str, Toast.LENGTH_SHORT).show();
    }

}
