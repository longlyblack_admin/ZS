package com.xunshan.ZhenSheng.activity;

import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.fragment.ExpendFragment;
import com.xunshan.ZhenSheng.fragment.IncomeFragment;

import java.util.ArrayList;
import java.util.List;

public class MoneyDetailsActivity extends AppCompatActivity {
    ViewPager vp_moneydetails;
    List<Fragment> list;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.moneydetails_activity);
        initView();
        initData();
    }

    private void initView() {
        vp_moneydetails = (ViewPager) findViewById(R.id.vp_moneydetails);
    }
    private void initData(){
        list = new ArrayList<>();
        list.add(new IncomeFragment());
        list.add(new ExpendFragment());
        vp_moneydetails.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return list.get(position);
            }

            @Override
            public int getCount() {
                return list.size();
            }
        });
    }
}
