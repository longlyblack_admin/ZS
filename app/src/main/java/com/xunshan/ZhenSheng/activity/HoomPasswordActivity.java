package com.xunshan.ZhenSheng.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.entity.InsertRoomBean;
import com.xunshan.ZhenSheng.entity.RoomPasswordSetingBean;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.RetrofitUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class HoomPasswordActivity extends AppCompatActivity {
    private EditText ethoompasswrd;
    private Button btpasswrodhoom;
    private String hoompassword;
    private String token;
    private String roomid;
    private TextView tvfanhuihoompassword;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.hoom_password);

        Intent intent = getIntent();

        roomid = intent.getStringExtra("room_idid");
        initView();
        btpasswrodhoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "提交完成", Toast.LENGTH_LONG).show();
                hoompassword = ethoompasswrd.getText().toString();
                Map<String, String> map = new HashMap<>();
                map.put("token", User.user().getToken());
                map.put("room_id", roomid);
                map.put("pass", hoompassword);
                RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/room/save", map, new NetCallBack<RoomPasswordSetingBean>() {
                    @Override
                    public void onSucceed(RoomPasswordSetingBean roomPasswordSetingBean, int types) {
                        Toast.makeText(HoomPasswordActivity.this, roomPasswordSetingBean.getData(),Toast.LENGTH_SHORT).show();


                    }
                    @Override
                    public void SucceedError(Exception e, int types) {
                    }

                    @Override
                    public void onError(Throwable throwable, int type) {
                    }
                }, 1);

                finish();
            }
        });
        tvfanhuihoompassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    private void initView() {
        ethoompasswrd = (EditText) findViewById(R.id.et_hoom_passwrd);
        btpasswrodhoom = (Button) findViewById(R.id.bt_passwrod_hoom);
        tvfanhuihoompassword = (TextView) findViewById(R.id.tv_fanhui_hoompassword);

    }


}
