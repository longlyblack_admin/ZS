package com.xunshan.ZhenSheng.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.xunshan.ZhenSheng.R;

public class ShenhaiActivity extends Activity {


    WebView we;

    @SuppressLint("JavascriptInterface")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shenhai);

        we = findViewById(R.id.wb_shenhai);

        WebSettings settings = we.getSettings();
        //如果访问的页面中有Javascript，则webview必须设置支持Javascript
        settings.setJavaScriptEnabled(true);

        //清除网页访问留下的缓存
        //由于内核缓存是全局的因此这个方法不仅仅针对webview而是针对整个应用程序.
        we.clearCache(true);

        //清除当前webview访问的历史记录
        //只会webview访问历史记录里的所有记录除了当前访问记录
        we.clearHistory();

        //这个api仅仅清除自动完成填充的表单数据，并不会清除WebView存储到本地的数据
        we.clearFormData();

        //WebView加载web资源
        we.addJavascriptInterface(new JsInteration(), "android");

        Intent it = getIntent();
        String dizhi = it.getStringExtra("url");

        we.loadUrl(dizhi);
    }

    public class JsInteration {

        @JavascriptInterface//一定要写，不然H5调不到这个方法
        public void back() {
            finish();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (we != null) {
            ((ViewGroup) we.getParent()).removeView(we);
            we.destroy();
            we = null;
        }
        super.onDestroy();
    }
}
