package com.xunshan.ZhenSheng.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.huantansheng.easyphotos.EasyPhotos;
import com.huantansheng.easyphotos.callback.SelectCallback;
import com.huantansheng.easyphotos.models.album.entity.Photo;
import com.scwang.smartrefresh.layout.SmartRefreshLayout;
import com.tencent.imsdk.TIMConversationType;
import com.tencent.qcloud.tim.uikit.modules.chat.base.ChatInfo;
import com.tencent.qcloud.tim.uikit.utils.Constants;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.adapter.MainAdapter;
import com.xunshan.ZhenSheng.adapter.MyGiftWallRecyclerViewAdapter;
import com.xunshan.ZhenSheng.adapter.UserPhotoAdapter;
import com.xunshan.ZhenSheng.adapter.ViewPagerAdapter;
import com.xunshan.ZhenSheng.entity.AddPicBean;
import com.xunshan.ZhenSheng.entity.GiftWallBean;
import com.xunshan.ZhenSheng.entity.GuanZhuYongHuBean;
import com.xunshan.ZhenSheng.entity.MyPrincipalBean;
import com.xunshan.ZhenSheng.entity.PicAlbumWallBean;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.ApiService;
import com.xunshan.ZhenSheng.util.GlideEngine;
import com.xunshan.ZhenSheng.util.LogUtil;
import com.xunshan.ZhenSheng.util.NavitationLayout;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.RetrofitUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class UserInfoActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tv_userinfoback;
    TextView tv_userinfoupdate;
    ViewPager vp;
    GridView gvuserinfo;
    List<Fragment> list;
    ImageView img_userinfoface;
    private List<PicAlbumWallBean.DataBean> list1;
    TextView tv_userinfoname;
    TextView tv_userinfoid;
    TextView tv_userinfolevel;
    TextView tv_userinfocharm;
    TextView tv_userinfofollow;
    TextView tv_userinfofans;
    private File file;
    String strBack = "";
    JSONObject obj;
    private Button btselectimages;
    private RecyclerView rvImage;
    private MainAdapter adapter;
    private MyGiftWallRecyclerViewAdapter myGiftWallRecyclerViewAdapter;
    JSONArray arr;
    private String token;
    private String userID;
    private List<MyPrincipalBean.UserBean> Mylist;
    private TextView tv_sign_pr_pr;
    private String[] titles1 = new String[]{"相册", "礼物墙"};
    private NavitationLayout navitationLayout;
    private ViewPagerAdapter viewPagerAdapter11;
    private RecyclerView rvgiftwall;
    private String pictu = "";
    private TextView giftwallsex;
    private UserPhotoAdapter adapter1;

    private List<GiftWallBean.DataBean> giftwallbeanlist;
    private SmartRefreshLayout mRefreshLayout;
    private String to_userid;
    private String to_username;
    private Button btguanzhu;
    private Button btsixin;
    private Boolean gz;
    private MyPrincipalBean.UserBean dianjiuserbean;
    private MyPrincipalBean myPrincipalBean1;
    /**
     * 选择的图片集
     */
    private ArrayList<Photo> selectedPhotoList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.userinfo_activity);
        SharedPreferences sharedPreferences = getSharedPreferences("data", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        token = sharedPreferences.getString("token", "");
        userID = sharedPreferences.getString("user_ID", "");

        editor.commit();
        Intent intent = getIntent();
        to_userid = intent.getStringExtra("to_userid");
        to_username = intent.getStringExtra("to_usename");
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogUtil.e("onResume");
        initData();
    }

    private void initView() {
        img_userinfoface = (ImageView) findViewById(R.id.img_userinfoface);
        tv_userinfoname = (TextView) findViewById(R.id.tv_userinfoname);
        tv_userinfoid = (TextView) findViewById(R.id.tv_userinfoid);
        tv_userinfolevel = (TextView) findViewById(R.id.tv_userinfolevel);
        tv_userinfocharm = (TextView) findViewById(R.id.tv_userinfocharm);
        tv_userinfofans = (TextView) findViewById(R.id.tv_userinfofans);
        tv_sign_pr_pr = (TextView) findViewById(R.id.tv_sign_pr_pr);
        tv_userinfoback = (TextView) findViewById(R.id.tv_userinfoback);
        tv_userinfoupdate = (TextView) findViewById(R.id.tv_userinfoupdate);
        giftwallsex = (TextView) findViewById(R.id.gift_wall_sex);
        // mRefreshLayout = findViewById(R.id.refreshLayout);
        gvuserinfo = findViewById(R.id.grid_gift);
        btguanzhu = (Button) findViewById(R.id.bt_guanzhu);
        btsixin = (Button) findViewById(R.id.bt_sixin);


//        mRefreshLayout.setRefreshFooter(new BallPulseFooter(this));
//        mRefreshLayout.setRefreshHeader(new BezierRadarHeader(this));
//        //设置样式后面的北京颜色
//        mRefreshLayout.setPrimaryColorsId(R.color.white, android.R.color.white);

        rvgiftwall = findViewById(R.id.rv_gift_wall);
        btselectimages = findViewById(R.id.bt_select_images1);
        rvImage = findViewById(R.id.rv_image);


        tv_userinfoback.setOnClickListener(this);
        tv_userinfoupdate.setOnClickListener(this);
        btselectimages.setOnClickListener(this);
        btguanzhu.setOnClickListener(this);
        btsixin.setOnClickListener(this);
        if (User.user().getUser_ID().equals(to_userid)) {
            btselectimages.setVisibility(View.VISIBLE);
            btguanzhu.setVisibility(View.GONE);
            btsixin.setVisibility(View.GONE);
        } else {
            btselectimages.setVisibility(View.GONE);
            tv_userinfoupdate.setVisibility(View.GONE);

        }


//        mRefreshLayout.setEnableRefresh(true);//是否启用下拉刷新功能
//        mRefreshLayout.setEnableLoadMore(true);//是否启用上拉加载功能


    }

    private void initData() {

        if (TextUtils.isEmpty(User.user().getUser_ID())) {
            LogUtil.e("userInfoActivity userId == null");
            return;
        }

        if (TextUtils.isEmpty(User.user().getToken())) {
            LogUtil.e("userInfoActivity token == null");
            return;
        }

        Map<String, String> map = new HashMap<>();
        map.put("user_id", User.user().getUser_ID());
        map.put("token", User.user().getToken());
        map.put("touser_id", to_userid);
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/home/index", map, new NetCallBack<MyPrincipalBean>() {
            @Override
            public void onSucceed(final MyPrincipalBean myPrincipalBean, int type) {
                LogUtil.e("获取个人信息-->"+ new Gson().toJson(myPrincipalBean));
                dianjiuserbean = myPrincipalBean.getUser();
                myPrincipalBean1 = myPrincipalBean;
                if (myPrincipalBean1.getIsguanzhu().equals("0")) {
                    gz = false;
                } else if (myPrincipalBean1.getIsguanzhu().equals("1")) {
                    gz = true;
                }
                btguanzhu.setText(gz ? "已关注" : "未关注");
                tv_userinfoname.setText(dianjiuserbean.getName());
                tv_userinfoid.setText("ID:" + dianjiuserbean.getUser_id());
                Glide.with(UserInfoActivity.this).load(AllInterface.addr + "/" + dianjiuserbean.getHeadpic()).into(img_userinfoface);
                img_userinfoface.setAlpha(155);
                tv_userinfofans.setText("Ta的粉丝人数" + dianjiuserbean.getFans());
                // tv_userinfofollow.setText(dianjiuserbean.getFollow()+"");
                tv_sign_pr_pr.setText("我的个性签名：" + dianjiuserbean.getSign());
                tv_userinfolevel.setText("等级：" + dianjiuserbean.getLevel());
                tv_userinfocharm.setText("魅力：" + dianjiuserbean.getCharm());
                if (dianjiuserbean.getSex().equals("0")) {
                    giftwallsex.setBackgroundResource(R.mipmap.zssexnan);
                } else {
                    giftwallsex.setBackgroundResource(R.mipmap.zssexnv);
                }

                User.user().setUser_name(myPrincipalBean.getUser().getName());
                User.user().setAddress(myPrincipalBean.getUser().getAddress());
                User.user().setUser_ID(myPrincipalBean.getUser().getUser_id());
                User.user().setUser_level(myPrincipalBean.getUser().getLevel());
                User.user().setSex(myPrincipalBean.getUser().getSex());
                User.user().setBirthaday(myPrincipalBean.getUser().getBirthaday());
                User.user().setSign(myPrincipalBean.getUser().getSign());
                User.user().setUser_Headpic(myPrincipalBean.getUser().getHeadpic());

            }

            @Override
            public void SucceedError(Exception e, int types) {
                LogUtil.e("个人信息-->"+ e.getMessage());
            }

            @Override
            public void onError(Throwable throwable, int types) {
                LogUtil.e("个人信息-->"+ throwable.getMessage());
            }
        }, 1);

        //        Map<String, String> map2 = new HashMap<>();
        //        map2.put("user_id", to_userid);
        //        map2.put("token", User.user().getToken());
        //        map2.put("currentPage", "1");
        //        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/home/gift", map2, new NetCallBack<GiftWallBean>() {
        //            @Override
        //            public void onSucceed(final GiftWallBean giftWallBean, int type) {
        //                giftwallbeanlist = giftWallBean.getData();
        //                GridLayoutManager layoutManager = new GridLayoutManager(UserInfoActivity.this, 4, LinearLayoutManager.VERTICAL, false);
        //                myGiftWallRecyclerViewAdapter = new MyGiftWallRecyclerViewAdapter(giftwallbeanlist, UserInfoActivity.this);
        //                rvgiftwall.setLayoutManager(layoutManager);
        //                rvgiftwall.setAdapter(myGiftWallRecyclerViewAdapter);
        //
        //            }
        //
        //            @Override
        //            public void SucceedError(Exception e, int types) {
        //            }
        //
        //            @Override
        //            public void onError(Throwable throwable, int type) {
        //            }
        //        }, 1);
        //
        //        //相册
        //        Map<String, String> map1 = new HashMap<>();
        //        map1.put("user_id", to_userid);
        //        map1.put("token", User.user().getToken());
        //        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/home/pic", map1, new NetCallBack<PicAlbumWallBean>() {
        //            @Override
        //            public void onSucceed(final PicAlbumWallBean picAlbumWallBean, int type) {
        //                list1 = picAlbumWallBean.getData();
        //                // 2.为数据源设置适配器
        //                adapter1 = new UserPhotoAdapter(list1, UserInfoActivity.this);
        //                // 3.将适配过后点数据显示在GridView 上
        //                gvuserinfo.setAdapter(adapter1);
        //                adapter.notifyDataSetChanged();
        //
        //            }
        //
        //            @Override
        //            public void SucceedError(Exception e, int types) {
        //            }
        //
        //            @Override
        //            public void onError(Throwable throwable, int type) {
        //            }
        //        }, 1);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_userinfoback:
                UserInfoActivity.this.finish();
                break;
            case R.id.tv_userinfoupdate:
                //        to_userid = intent.getStringExtra("to_userid");
                startActivity(new Intent(this, UpdateActivity.class)
                .putExtra("intent_userId",to_userid));
                break;
            case R.id.bt_guanzhu:
                Map<String, String> map = new HashMap<>();
                map.put("token", to_userid);
                map.put("user_id", User.user().getUser_ID());
                map.put("touser__id", to_userid);
                RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/user/follow", map, new NetCallBack<GuanZhuYongHuBean>() {
                    @Override
                    public void onSucceed(final GuanZhuYongHuBean guanZhuYongHuBean, int type) {

                        if (gz == true) {
                            Toast.makeText(UserInfoActivity.this, "你已经取消关注", Toast.LENGTH_SHORT).show();
                            gz = false;
                        } else if (gz == false) {
                            gz = true;
                            Toast.makeText(UserInfoActivity.this, "你已经成功关注", Toast.LENGTH_SHORT).show();
                        }
                        btguanzhu.setText(gz ? "已关注" : "未关注");


                    }

                    @Override
                    public void SucceedError(Exception e, int types) {
                    }

                    @Override
                    public void onError(Throwable throwable, int type) {
                    }
                }, 1);
                break;
            case R.id.bt_sixin:
                ChatInfo chatInfo = new ChatInfo();
                chatInfo.setType(TIMConversationType.C2C);
                chatInfo.setId(to_userid);
                chatInfo.setChatName(to_username);
                Intent intent = new Intent(this, ChatIMActivity.class);
                intent.putExtra(Constants.CHAT_INFO, chatInfo);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

                break;
            case R.id.bt_select_images1:
                SnapHelper snapHelper = new PagerSnapHelper();
                EasyPhotos.createAlbum(UserInfoActivity.this, true, GlideEngine.getInstance())//参数说明：上下文，是否显示相机按钮，[配置Glide为图片加载引擎](https://github.com/HuanTanSheng/EasyPhotos/wiki/12-%E9%85%8D%E7%BD%AEImageEngine%EF%BC%8C%E6%94%AF%E6%8C%81%E6%89%80%E6%9C%89%E5%9B%BE%E7%89%87%E5%8A%A0%E8%BD%BD%E5%BA%93)
                        .setFileProviderAuthority("com.xunshan.ZhenSheng.provider")//参数说明：见下方`FileProvider的配置`
                        .setCount(1)//参数说明：最大可选数，默认1
                        .start(new SelectCallback() {
                            @Override
                            public void onResult(ArrayList<Photo> photos, boolean isOriginal) {
                                for (int i = 0; i < photos.size(); i++) {
                                    String pa = photos.get(i).path;
                                    file = new File(pa);
                                    pictu = bitmapToBase64(compressPixel(pa));
                                }
                                uploadImage2(file);
                                selectedPhotoList.clear();
                                selectedPhotoList.addAll(photos);
                                adapter.notifyDataSetChanged();
                                rvImage.smoothScrollToPosition(0);
                            }
                        });
                GridLayoutManager layoutManager = new GridLayoutManager(this, 4, LinearLayoutManager.VERTICAL, false);
                adapter = new MainAdapter(this, selectedPhotoList);
                rvImage.setLayoutManager(layoutManager);
                rvImage.setAdapter(adapter);
                rvImage.setOnFlingListener(null);
                snapHelper.attachToRecyclerView(rvImage);

                Map<String, String> map1 = new HashMap<>();
                map1.put("user_id", to_userid);
                map1.put("token", User.user().getToken());
                RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/home/pic", map1, new NetCallBack<PicAlbumWallBean>() {
                    @Override
                    public void onSucceed(final PicAlbumWallBean picAlbumWallBean, int type) {
                        list1 = picAlbumWallBean.getData();
                        // 2.为数据源设置适配器
                        adapter1 = new UserPhotoAdapter(list1, UserInfoActivity.this);
                        // 3.将适配过后点数据显示在GridView 上
                        gvuserinfo.setAdapter(adapter1);
                        adapter1.notifyDataSetChanged();
                    }

                    @Override
                    public void SucceedError(Exception e, int types) {
                    }

                    @Override
                    public void onError(Throwable throwable, int type) {
                    }
                }, 1);


                break;
        }
    }


    private void uploadImage2(File file) {


//        List<MultipartBody.Part> map = new ArrayList<>();
//        MultipartBody.Builder builder = new MultipartBody.Builder()
//                .setType(MultipartBody.FORM);
//        RequestBody imageBody = RequestBody.create(MediaType.parse("image/jpeg"), file);
//        builder.addFormDataPart("image_photo", file.getName(), imageBody);//imgfile 后台接收图片流的参数名
//        map = builder.build().parts();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api3.dayushaiwang.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        Map<String, String> map2 = new HashMap<>();
        map2.put("user_id", userID);
        map2.put("token", token);
        map2.put("pic", pictu);
        ApiService apiService = retrofit.create(ApiService.class);
        apiService.uploadImages(map2).enqueue(new Callback<AddPicBean>() {
            @Override
            public void onResponse(Call<AddPicBean> call, Response<AddPicBean> response) {
                if (response.body().getMsg() != null) {
                    Toast.makeText(UserInfoActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onFailure(Call<AddPicBean> call, Throwable t) {

            }
        });


    }


    /**
     * bitmap转为base64
     *
     * @param bitmap
     * @return
     */
    public static String bitmapToBase64(Bitmap bitmap) {

        String result = null;
        ByteArrayOutputStream baos = null;
        try {
            if (bitmap != null) {
                baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                baos.flush();
                baos.close();

                byte[] bitmapBytes = baos.toByteArray();
                result = Base64.encodeToString(bitmapBytes, Base64.DEFAULT);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (baos != null) {
                    baos.flush();
                    baos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }


    private Bitmap compressPixel(String filePath) {
        Bitmap bmp = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        //setting inSampleSize value allows to load a scaled down version of the original image
        options.inSampleSize = 4;

        //inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;
        options.inTempStorage = new byte[16 * 1024];
        try {
            //load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
            if (bmp == null) {

                InputStream inputStream = null;
                try {
                    inputStream = new FileInputStream(filePath);
                    BitmapFactory.decodeStream(inputStream, null, options);
                    inputStream.close();
                } catch (FileNotFoundException exception) {
                    exception.printStackTrace();
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        } finally {
            return bmp;
        }
    }


}
