package com.xunshan.ZhenSheng.activity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.adapter.KnapsackClassifyAdapter;
import com.xunshan.ZhenSheng.adapter.KnapsackGoodsAdapter;
import com.xunshan.ZhenSheng.entity.Goods;
import com.xunshan.ZhenSheng.entity.KnapsackClassify;
import com.xunshan.ZhenSheng.entity.MyBackageBean;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.RetrofitUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class KnapsackActivity extends AppCompatActivity implements KnapsackClassifyAdapter.classifyCallBack,KnapsackGoodsAdapter.knapsackGoodsCallBack {
    TextView tv_knapsack_back;
    ListView lv_knapsack;
    GridView gv_knapsack;
    KnapsackClassify knapsackClassify;
    List<KnapsackClassify> classifyList;
    public static int classifyIndex = 0;
    KnapsackClassifyAdapter classifyAdapter;
    Goods goods;
    List<Goods> goodsList;
    KnapsackGoodsAdapter goodsAdapter;

    private List<MyBackageBean.GiftBean> giftBeanList;
    private List<MyBackageBean.HeadBean> headBeanList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.knapsack_activity);
        initView();
        initData();
    }

    private void initView() {
        tv_knapsack_back = (TextView) findViewById(R.id.tv_knapsack_back);
        lv_knapsack = (ListView) findViewById(R.id.lv_knapsack);
        gv_knapsack = (GridView) findViewById(R.id.gv_knapsack);

        tv_knapsack_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                KnapsackActivity.this.finish();
            }
        });






    }

    private void initData() {
        classifyList = new ArrayList<>();
        knapsackClassify = new KnapsackClassify();
        knapsackClassify.setkName("礼物");
        classifyList.add(knapsackClassify);
        knapsackClassify = new KnapsackClassify();
        knapsackClassify.setkName("头像框");
        classifyList.add(knapsackClassify);
        knapsackClassify = new KnapsackClassify();
        knapsackClassify.setkName("进场特效");
        classifyList.add(knapsackClassify);

        classifyAdapter = new KnapsackClassifyAdapter(classifyList, this, this);
        lv_knapsack.setAdapter(classifyAdapter);


        getitem();



    }


    private void getitem(){



        Map<String, String> map = new HashMap<>();
        map.put("user_id", User.user().getUser_ID());
        map.put("token", User.user().getToken());
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/user/knapsack", map, new NetCallBack<MyBackageBean>() {
            @Override
            public void onSucceed(MyBackageBean myBackageBean, int types) {
                giftBeanList = myBackageBean.getGift();
                headBeanList = myBackageBean.getHead();
                goodsAdapter = new KnapsackGoodsAdapter(giftBeanList,KnapsackActivity.this,KnapsackActivity.this,headBeanList,classifyIndex);
                gv_knapsack.setAdapter(goodsAdapter);

            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int type) {
            }
        }, 1);




    }








    @Override
    public void classifyPositin(int position) {
        classifyIndex = position;

        getitem();

        goodsAdapter.notifyDataSetChanged();
        classifyAdapter.notifyDataSetChanged();
    }

    @Override
    public void goodsPosition(int position) {

    }
}
