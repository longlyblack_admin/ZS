package com.xunshan.ZhenSheng.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.hyphenate.EMCallBack;
import com.hyphenate.chat.EMClient;
import com.tencent.qcloud.tim.uikit.TUIKit;
import com.tencent.qcloud.tim.uikit.base.IUIKitCallBack;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.user.User;

public class SetActivity extends AppCompatActivity {
    private TextView tvlogoff;
    private TextView tv_shouyiback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set);
        initView();
    }

    private void initView() {
        tvlogoff = (TextView) findViewById(R.id.tv_log_off);
        tvlogoff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
            }
        });
        tv_shouyiback = findViewById(R.id.tv_shouyiback);

        tv_shouyiback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


    private void logout() {

        TUIKit.unInit();
        User.user().End(SetActivity.this);
        (SetActivity.this).finish();
        MainActivity.actmain.finish();
        startActivity(new Intent(SetActivity.this, LoginActivity.class));


    }

}
