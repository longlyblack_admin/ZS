package com.xunshan.ZhenSheng.activity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.xunshan.ZhenSheng.R;

import java.util.Set;

public class SplashActivity extends Activity {

    private static final String TAG = SplashActivity.class.getSimpleName();
    private static final int SPLASH_TIME = 1500;
    private View mFlashView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // 离线推送测试代码
        Bundle bundle = getIntent().getExtras();
        if (bundle == null) {
            // oppo scheme url解析
            Uri uri = getIntent().getData();
            Set<String> set = null;
            if (uri != null) {
                set = uri.getQueryParameterNames();
            }
            if (set != null) {
                for (String key : set) {
                    String value = uri.getQueryParameter(key);
                }
            }
        } else {
            String ext = bundle.getString("ext");

            Set<String> set = bundle.keySet();
            if (set != null) {
                for (String key : set) {
                    String value = bundle.getString(key);
                }
            }
        }
        // 离线推送测试代码结束

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);

        mFlashView = findViewById(R.id.flash_view);
        handleData();
    }

    private void handleData() {
        mFlashView.postDelayed(new Runnable() {
            @Override
            public void run() {
                startLogin();
            }
        }, SPLASH_TIME);
    }

    private void startLogin() {
//        Intent intent = new Intent(SplashActivity.this, LoginForDevActivity.class);
//        startActivity(intent);
        finish();
    }
}
