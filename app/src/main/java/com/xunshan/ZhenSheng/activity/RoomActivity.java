package com.xunshan.ZhenSheng.activity;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.hb.dialog.myDialog.ActionSheetDialog;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.adapter.ChatroomSeatsAdapter;
import com.xunshan.ZhenSheng.adapter.GiftToUserAdapter;
import com.xunshan.ZhenSheng.adapter.MaiWeiAdapter;
import com.xunshan.ZhenSheng.adapter.MsgAdapter;
import com.xunshan.ZhenSheng.adapter.MyAdapter1;
import com.xunshan.ZhenSheng.adapter.MyAuthSetingRecyclerViewAdapter;
import com.xunshan.ZhenSheng.adapter.MybackPackadapter;
import com.xunshan.ZhenSheng.adapter.RoomDialogAdapter;
import com.xunshan.ZhenSheng.adapter.ViewPagerAdapter;
import com.xunshan.ZhenSheng.data.ZegoDataCenter;
import com.xunshan.ZhenSheng.entity.ChatroomSeatInfo;
import com.xunshan.ZhenSheng.entity.CollectionRoomBean;
import com.xunshan.ZhenSheng.entity.ExitChatRoomBean;
import com.xunshan.ZhenSheng.entity.GZShengQingShangMaiBean;
import com.xunshan.ZhenSheng.entity.GiftSLogBean;
import com.xunshan.ZhenSheng.entity.GuanZhuYongHuBean;
import com.xunshan.ZhenSheng.entity.HoomListBean;
import com.xunshan.ZhenSheng.entity.MyRoomBean;
import com.xunshan.ZhenSheng.entity.MybackcardBean;
import com.xunshan.ZhenSheng.entity.OnlineBean;
import com.xunshan.ZhenSheng.entity.OnlineUserListBean;
import com.xunshan.ZhenSheng.entity.PersonalDataBean;
import com.xunshan.ZhenSheng.entity.RoomInforBean;
import com.xunshan.ZhenSheng.entity.SheGuanLiBean;
import com.xunshan.ZhenSheng.entity.SheZhiMaiXuFangShiBean;
import com.xunshan.ZhenSheng.entity.UserRoomList2Bean;
import com.xunshan.ZhenSheng.entity.UserRoomListBean;
import com.xunshan.ZhenSheng.entity.topaybean;
import com.xunshan.ZhenSheng.fragment.DialogFragmentUpper;
import com.xunshan.ZhenSheng.helper.ChatroomInfoHelper;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.CheckableLayout;
import com.xunshan.ZhenSheng.util.FileUtils;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.PayUtils;
import com.xunshan.ZhenSheng.util.RetrofitUtils;
import com.xunshan.ZhenSheng.util.RoundTransform;
import com.xunshan.ZhenSheng.view.PickUpUserSelectDialog;
import com.xunshan.ZhenSheng.view.SeatOperationDialog;
import com.xunshan.ZhenSheng.view.TipDialog;
import com.zego.chatroom.ZegoChatroom;
import com.zego.chatroom.callback.ZegoChatroomCMDCallback;
import com.zego.chatroom.callback.ZegoChatroomCallback;
import com.zego.chatroom.callback.ZegoChatroomIMCallback;
import com.zego.chatroom.callback.ZegoChatroomSendRoomMessageCallback;
import com.zego.chatroom.callback.ZegoSeatUpdateCallback;
import com.zego.chatroom.config.ZegoChatroomLiveConfig;
import com.zego.chatroom.constants.ZegoChatroomReconnectStopReason;
import com.zego.chatroom.constants.ZegoChatroomSeatStatus;
import com.zego.chatroom.constants.ZegoChatroomUserLiveStatus;
import com.zego.chatroom.entity.ZegoChatroomMessage;
import com.zego.chatroom.entity.ZegoChatroomSeat;
import com.zego.chatroom.entity.ZegoChatroomUser;
import com.zego.chatroom.manager.entity.ResultCode;
import com.zego.chatroom.manager.musicplay.ZegoMusicPlayer;
import com.zego.chatroom.manager.musicplay.ZegoMusicResource;
import com.zego.chatroom.manager.room.ZegoUserLiveQuality;

import java.io.UnsupportedEncodingException;
import java.lang.reflect.Field;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import pl.droidsonroids.gif.GifImageView;

@RequiresApi(api = Build.VERSION_CODES.O)
public class RoomActivity extends BaseActivity implements View.OnClickListener, ChatroomSeatsAdapter.OnChatroomSeatClickListener, SeatOperationDialog.OnOperationItemClickListener,
        PickUpUserSelectDialog.OnPickUserUpListener, ZegoChatroomCallback, ZegoChatroomIMCallback, ZegoChatroomCMDCallback, MyAdapter1.myCallBack {
    GridView gvroomseat;
    private ZegoChatroomUser[] useruser;
    //    List<User> listGv;
    //    RoomSeatAdapter adapter;
    public static Set<Integer> positionSet = new HashSet<>();//用于存储选择的位置
    private boolean selectMode = false; //选择模式 多选或者单选 true 多选
    public Set<String> checkTYpeNameSet = new HashSet<>(); //用于存储选择项的名称
    RecyclerView rvSeats;
    public MsgAdapter mMsgAdapter;
    private ChatroomSeatsAdapter mSeatsAdapter;
    private MybackPackadapter mybackpackadapter;
    public RoomInforBean.RoomBean listLv1;
    private String[] titles1 = new String[]{"麦上全员", "麦上申请列表"};
    public List<UserRoomListBean.DataBean> list = new ArrayList<>();
    public PersonalDataBean.UserBean PersonalUser;
    private ViewPagerAdapter viewPagerAdapter11;
    List<OnlineBean> listDate;
    public ListView lv_roommsg;
    // 是否正在离开房间
    public boolean isLeavingRoom = false;
    // private View mFlLoading;
    private List<ChatroomSeatInfo> mSeats = new ArrayList<>();
    public Set<ZegoChatroomUser> mRoomUsers = new HashSet<>();
    MyAdapter1 adapter;
    List<Integer> listRoom;

    public ImageView img_roomcomment;
    public TextView TvOnlineDisplayNumber;//tv_Online_Display_Number
    public FrameLayout mFlInput;
    public TextView tvfanhuiroom1;
    public EditText mEtComment;
    public ImageView mIvSpeakerState;
    public ImageView Ivbottomdialog;
    public ImageView imgroomgift;
    public int screenHeight = 0;
    public View inflate;
    public Dialog dialog;
    public final static String USER_MESSAGE_FORMAT = "%1$s:%2$s";
    GifImageView giftresult;
    public RelativeLayout rltuichuroom;
    View view;
    public RoomDialogAdapter OnlineDialogAdapter;
    PopupWindow popupWindow;
    private Boolean mick = true;
    TextView tv_pop_sendgift;
    TextView tv_notice;
    RecyclerView rv_gifttouser;
    public GiftToUserAdapter giftToUserAdapter;
    public TextView tv_Number;
    RecyclerView rv_giftlist;
    public ImageView ivfanhuihoom;
    LinearLayoutManager linearLayoutManager;
    public List<HoomListBean.DataBean> mList;
    public TextView tvroomID;
    public TextView tv_room_user_name;
    public String user_ID;
    //本直播间id
    public String room_ID;
    public String roomuser_ID;
    public TextView tv_play_method;
    public TextView tv_giftnum;
    public String user_name;
    public String OnlineID;
    public String room_name;
    public String token;
    public List<OnlineUserListBean.DataBean> MlistOline;
    public int Online_onlineCount;
    private ImageView tvguanzhu;
    private String useretsearchssroom;
    private String userll;
    private TextView tvcancel11;
    // 当前房间支持声道数
    public int mAudioChannelCount;
    public TipDialog mTipDialog;
    ImageView ivphotoheaderpersonal;
    TextView tvseatuserIDpersonal;
    TextView tvseatcharmpersonal;
    TextView tvseatdialognamepersonal;
    TextView tvseatlevelpersonal;
    TextView tvseatsexpersonal;
    TextView tvseatsignpersonal;
    TextView tvseatfollowpersonal;
    TextView tvgifmpersonalpersonal;
    TextView tvbalanc_personal;
    private TextView tvshangmaiuser;
    private List<UserRoomListBean.DataBean> listDataBeans = new ArrayList<>();
    private MaiWeiAdapter maiWeiAdapter;
    private SeatOperationDialog mSeatOperationDialog;
    private PersonalDataBean.RoomUserBean PersonalRoomZhu;
    SharedPreferences sharedPreferences;
    private UserRoomList2Bean RoomUser;
    private SVGAImageView svgaImageView;
    private ChatroomSeatInfo seatInfo11;
    private int pos11 = 10000;
    private List<String> room_auth;
    private String manager;
    private List<String> marr;
    //需要跳转到自己的直播间id
    private String room_id;
    //房间名字
    String ROOMNAME;
    //房间玩法
    String ROOMNAMEWANFA;
    //是否是管理员
    Boolean RMManager = false;
    private LinearLayout llroombg;
    private String backimageID;
    private String isshoucang;
    private ImageView ivSpecialeffects;
    private ImageView kongtou;
    private String kongtouurl = "";
    View vi;
    private String Over_pipe = "0";
    List<String> model = new ArrayList();
    private Boolean SC;
    private ImageView iv_roomlock;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.room_activity);
        loginChatroomWithIntent();
        mIvSpeakerState = (ImageView) findViewById(R.id.iv_speaker_state);
        mIvSpeakerState.setImageResource(R.mipmap.jingyinkai);//静音关
        mIvSpeakerState.setTag(true);
        changeSpeakerState();
        sharedPreferences = getSharedPreferences("data", MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");
        user_name = sharedPreferences.getString("user_name", "");
        user_ID = sharedPreferences.getString("user_ID", "");
        Intent intent = getIntent();
        room_ID = intent.getStringExtra("room_id");
        OnlineID = intent.getStringExtra("Online_room_id");
        room_name = intent.getStringExtra("room_name");
        roomuser_ID = intent.getStringExtra("room_userid");
        Over_pipe = intent.getStringExtra("Over_pipe");
        if (roomuser_ID != null) {
            model.add(roomuser_ID);
        }
        //  initData1();
        initData();
        initDatashangmaiuser();
        initView();

    }



    /**
     * 登入房间内部实现
     */
    public ZegoChatroomUser mOwner;
    private void loginChatroomWithIntent() {
        mOwner = new ZegoChatroomUser();
        mOwner.userID = User.user().getUser_ID();
        mOwner.userName = User.user().getUser_name();
        // 初始化 ZegoChatroom 单例对象
        ZegoChatroom.setupContext(this, mOwner, ZegoDataCenter.APP_ID, ZegoDataCenter.APP_SIGN);
        // 将房间配置设置成默认状态：Mic开，静音关
        ZegoChatroom.shared().shared().muteSpeaker(true);
        ZegoChatroom.shared().shared().muteMic(false);
        // 无限重试
        //        ZegoChatroom.shared().shared().setReconnectTimeoutSec(0);
        // 设置测试环境
        ZegoChatroom.shared().setUseTestEnv(false);
        // 允许接收用户更新回调
        ZegoChatroom.shared().setEnableUserStateUpdate(true);
        ZegoChatroom.shared().addZegoChatroomCallback(this);
        //接受聊天消息
        ZegoChatroom.shared().addIMCallback(this);
        //接受信令
        // ZegoChatroom.shared().addCMDCallback(this);
        boolean isOwner = isOwner();
        //        if (isOwner) {
        //            createChatroomWithIntent();
        //        } else {
        //            joinChatroomWithIntent();
        //
        //        }
        joinChatroomWithIntent();
        ZegoChatroom.shared().muteSpeaker(false);
    }

    //绑定插件
    public void initView() {
        //        mFlLoading = findViewById(R.id.fl_loading);
        //        mFlLoading.setVisibility(View.VISIBLE);
        //   gvroomseat = (GridView) findViewById(R.id.gv_roomseat);
        findViewById(R.id.tv_send_msg).setOnClickListener(this);
        img_roomcomment = (ImageView) findViewById(R.id.img_roomcomment);
        mFlInput = findViewById(R.id.fl_input);
        mEtComment = findViewById(R.id.comment_edit_text);
        tvguanzhu = findViewById(R.id.tv_guanzhu);
        llroombg = findViewById(R.id.ll_room_bg);
        iv_roomlock = findViewById(R.id.iv_roomlock);
        if (User.user().getRoomlock().equals("1")) {
            iv_roomlock.setVisibility(View.VISIBLE);
        } else {
            iv_roomlock.setVisibility(View.INVISIBLE);
        }
        tvguanzhu.setTag(true);
        giftHandler = new giftHandler();
        //初始化主体
        //lv_roommsg = (ListView)findViewById(R.id.lv_roommsg);
        tv_notice = (TextView) findViewById(R.id.tv_notice);

        imgroomgift = (ImageView) findViewById(R.id.img_roomgift);
        tv_Number = (TextView) findViewById(R.id.tv_Number);
        Ivbottomdialog = (ImageView) findViewById(R.id.iv_bottom_dialog);
        TvOnlineDisplayNumber = (TextView) findViewById(R.id.tv_Online_Display_Number);
        tvroomID = (TextView) findViewById(R.id.tv_room_ID);
        ivfanhuihoom = (ImageView) findViewById(R.id.iv_fanhui_hoom);
        ivSpecialeffects = (ImageView) findViewById(R.id.iv_Special_effects);
        tv_play_method = (TextView) findViewById(R.id.tv_play_method);
        kongtou = findViewById(R.id.iv_kongtou);

        //  img_userinfo_face=(RoundedImageView)findViewById(R.id.img_userinfo_face);

        tvshangmaiuser = (TextView) findViewById(R.id.tv_shangmai_user);
        svgaImageView = findViewById(R.id.svgaimage);


        tvroomID.setText("ID:" + room_ID);

        tv_Number.setText(room_name);


        Log.e("TAG666", "onCreate: " + token + "666" + user_name + "666" + user_ID + "6666" + room_ID);





        mPickUpUserSelectDialog = new PickUpUserSelectDialog(this);
        mPickUpUserSelectDialog.setOnPickUpUserListener(this);
        mIvSpeakerState.setOnClickListener(this);
        img_roomcomment.setOnClickListener(this);
        TvOnlineDisplayNumber.setOnClickListener(this);
        Ivbottomdialog.setOnClickListener(this);
        ivfanhuihoom.setOnClickListener(this);
        imgroomgift.setOnClickListener(this);
        tvguanzhu.setOnClickListener(this);
        tvshangmaiuser.setOnClickListener(this);
        kongtou.setOnClickListener(this);

        // tvOlinelist.setOnClickListener(this);
        //SVsearchView.setOnCloseListener(this);
        // rltuichuroom.setOnClickListener(this);


        RecyclerView rvMessage = findViewById(R.id.rv_message);
        rvMessage.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvMessage.setAdapter(mMsgAdapter);
        mMsgAdapter.setRecyclerView(rvMessage);

        initInputLayout();
        // 到这才能获取到房主到信息。。
    }


    public boolean isOwner() {
        return mOwner.equals(ZegoDataCenter.ZEGO_USER);
    }

    private final static int DEFAULT_SEATS_COUNT = 9;

    private List<ZegoChatroomSeat> createDefaultZegoSeats() {
        ArrayList<ZegoChatroomSeat> seats = new ArrayList<>(DEFAULT_SEATS_COUNT);
        // 默认房主上麦
        ZegoChatroomSeat ownerSeat = ZegoChatroomSeat.seatForUser(ZegoDataCenter.ZEGO_USER);
        seats.add(ownerSeat);
        // 其他8个麦位置为空麦位
        for (int i = 1; i < DEFAULT_SEATS_COUNT; i++) {
            seats.add(ZegoChatroomSeat.emptySeat());
        }

        return seats;
    }

    private boolean isMicMute() {
        // return (boolean) mTvMicState.getTag();
        return true;
    }

    private void showPickedUpTipDialog(int seatIndex) {
        // 获取麦克风当前状态，如果是打开的，则改变其状态
        final boolean shouldChangeMicState = !isMicMute();
        if (shouldChangeMicState) {
            changeMicState();
        }

        if (mTipDialog == null) {
            mTipDialog = new TipDialog(this);
        }
        mTipDialog.reset();
        mTipDialog.mTitleTv.setText("你被抱上" + seatIndex + "号麦位");
        mTipDialog.mDescTv.setText("快打开麦克风聊天吧");
        mTipDialog.mButton1.setText("下麦");
        mTipDialog.mButton1.setVisibility(View.VISIBLE);
        mTipDialog.mButtonOk.setText("确定");
        mTipDialog.mButtonOk.setVisibility(View.VISIBLE);
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.button1:
                        ZegoChatroom.shared().leaveSeat(new ZegoSeatUpdateCallbackWrapper() {
                            @Override
                            public void onCompletion(ResultCode resultCode) {
                                super.onCompletion(resultCode);
                                if (resultCode.isSuccess()) {
                                    mTipDialog.dismiss();
                                    // 将 麦克风状态回复到执行之前到状态
                                    if (shouldChangeMicState) {
                                        changeMicState();
                                    }
                                } else {
                                    Toast.makeText(RoomActivity.this, "下麦失败，请重试！", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                        break;
                    case R.id.button_ok:
                        mTipDialog.dismiss();
                        // 将 麦克风状态回复到执行之前到状态
                        if (shouldChangeMicState) {
                            changeMicState();
                        }
                        break;
                }
            }
        };
        mTipDialog.mButton1.setOnClickListener(onClickListener);
        mTipDialog.mButtonOk.setOnClickListener(onClickListener);
        mTipDialog.show();
    }

    private void changeMicState() {
        boolean isMicMute = isMicMute();
        if (isMicMute) {
//            mTvMicState.setText("Mic开");
//            mTvMicState.setTag(false);
            ZegoChatroom.shared().muteMic(false);
        } else {
//            mTvMicState.setText("Mic关");
//            mTvMicState.setTag(true);
            ZegoChatroom.shared().muteMic(true);
        }
    }

    private void initData() {
        rvSeats = findViewById(R.id.rv_seats);
        mSeatsAdapter = new ChatroomSeatsAdapter(RoomActivity.this);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 4);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position == 0) {
                    return 4;
                }
                return 1;
            }
        });
        rvSeats.setLayoutManager(layoutManager);
        rvSeats.setItemAnimator(new DefaultItemAnimator());
        rvSeats.setAdapter(mSeatsAdapter);

        mSeats = createDefaultSeats();

        Map<String, String> map = new HashMap<>();
        map.put("user_id", User.user().getUser_ID());
        map.put("token", User.user().getToken());
        map.put("room_id", room_ID);
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/index", map, new NetCallBack<RoomInforBean>() {
            @Override
            public void onSucceed(final RoomInforBean roomInforBean, int type) {
                if (roomInforBean.getErr().equals("0")) {
                    tv_play_method.setText("房间玩法" + roomInforBean.getRoom().getRoomtake());
                    room_auth = roomInforBean.getRoom_auth();
                    for (int i = 0; i < room_auth.size(); i++) {
                        //管理员id
                        manager = room_auth.get(i);

                        if (manager.equals(User.user().getUser_ID())) {
                            //是管理员
                            RMManager = true;
                        } else {
                            RMManager = false;
                        }


                    }
                    marr = roomInforBean.getRoom().getMarr();
                    //这里为了添加头部数据marr{0}
                    List<String> strList2 = new ArrayList<>();
                    strList2.add("0");
                    marr.addAll(0, strList2);
                    mSeatsAdapter.setSeats(mSeats, marr);
                }
            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int type) {
            }
        }, 1);
        mSeatsAdapter.setOnChatroomSeatClickListener(RoomActivity.this);
        //初始化适配器
        mMsgAdapter = new MsgAdapter();

    }


//    //房主
//    private void initData1() {
//
//
//
//    }


    private void createChatroomWithIntent() {
        ZegoChatroomLiveConfig config = new ZegoChatroomLiveConfig();
        ZegoChatroom.shared().joinChatroom(room_ID, room_name, createDefaultZegoSeats(), config);
    }

    private void joinChatroomWithIntent() {
        ZegoChatroomLiveConfig config = new ZegoChatroomLiveConfig();
        config.setBitrate(ChatroomInfoHelper.getAudioBitrateFromString(""));
        config.setAudioChannelCount(ChatroomInfoHelper.getAudioChannelCountFromString(""));
        config.setLatencyMode(ChatroomInfoHelper.getLatencyModeFromString(""));
        ZegoChatroom.shared().joinChatroom(room_ID, room_name, joinroom(), config);
    }


    private List<ZegoChatroomSeat> joinroom() {
        ArrayList<ZegoChatroomSeat> seats = new ArrayList<>(DEFAULT_SEATS_COUNT);
        // 默认房主上麦
//        ZegoChatroomSeat ownerSeat = ZegoChatroomSeat.seatForUser(ZegoDataCenter.ZEGO_USER);
//        seats.add(ownerSeat);
        // 其他8个麦位置为空麦位
        for (int i = 0; i < DEFAULT_SEATS_COUNT; i++) {
            seats.add(ZegoChatroomSeat.emptySeat());
        }

        return seats;


    }


    private List<ChatroomSeatInfo> createDefaultSeats() {
        ArrayList<ChatroomSeatInfo> seats = new ArrayList<>(DEFAULT_SEATS_COUNT);
        for (int i = 0; i < DEFAULT_SEATS_COUNT; i++) {
            seats.add(ChatroomSeatInfo.emptySeat());
        }
        return seats;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        User.user().setRoomname_Headpic("");
        User.user().setRoomname_Charm("");
        User.user().setRoomname_name("");
        roomuser_ID = "";
        UserRoomListBean.UserRoomListBean().setData(null);
        model.clear();
        exitRoom();
        mMsgAdapter.clear();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_roomcomment:
                //显示输入法
                mFlInput.setVisibility(View.VISIBLE);
                mEtComment.requestFocus();
                ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                        .showSoftInput(mEtComment, InputMethodManager.SHOW_IMPLICIT);
                break;
            case R.id.tv_send_msg:
                if (send()) {
                    // 隐藏输入法
                    InputMethodManager im = ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE));
                    if (im.isActive()) {
                        im.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
                    }
                }
                break;
            //音量关闭
            case R.id.iv_speaker_state:
                changeSpeakerState();
                break;
            case R.id.tv_Online_Display_Number:
                Dialog(seatInfo11, pos11);
                break;
            case R.id.iv_bottom_dialog:
                BottomDialog();
                break;
            case R.id.img_roomgift:
                liwulist();
                break;
            case R.id.iv_fanhui_hoom:
                exitRoom();
                break;
            case R.id.tv_guanzhu:

                initDataBK();
                break;
            case R.id.tv_shangmai_user:
//               initDataUserRoom();
                //如果房主显示弹窗
                if (User.user().getUser_ID().equals(roomuser_ID)) {
                    shangmaiDialog();
                    tvshangmaiuser.setText("上麦");
                }
                if (RMManager) {
                    //管理权限判断
                    shangmaiDialog();
                    tvshangmaiuser.setText("上麦");
                } else {
                    //观众权限
                    Toast.makeText(RoomActivity.this, "点击麦位上麦", Toast.LENGTH_SHORT).show();

//                    tvshangmaiuser.setText("");
//                    tvshangmaiuser.setBackground(getDrawable(R.mipmap.jinyanmai));

                }
//                    if(User.user().getUser_ID().equals("0"){
//                    //观众权限
//                    tvshangmaiuser.setBackground(getDrawable(R.mipmap.jinmai));
//                }
                break;
            case R.id.iv_Special_effects:
                Toast.makeText(RoomActivity.this, "正在努力开发中", Toast.LENGTH_SHORT).show();
                break;
            case R.id.iv_kongtou:
                Intent intent = new Intent(RoomActivity.this, ShenhaiActivity.class);
                intent.putExtra("url", kongtouurl);
                startActivity(intent);
                break;

        }
    }

    //实现接收聊天信息回调
    @Override
    public void onRecvRoomMessage(ZegoChatroomMessage[] messageList) {


        String sign = "";
        String liwuname = "";
        String songname = "";
        String shouname = "";
        String liwunum = "";
        String context = "";
        for (ZegoChatroomMessage message : messageList) {
            String[] array1 = message.mContent.split(",");
            String type = array1[0];

            switch (type) {
                case "1":
                    context = "欢迎" + array1[1] + "进入房间";
                    break;
                case "2":
                    // 2 特权用户进入房间
                    context = "";
                    break;
                case "3":
                    try {
                        //  普通人发送的聊天消息
                        context = URLDecoder.decode(array1[3], "UTF-8");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "4":
                    //  特权用户发送的聊天消息
                    try {
                        context = URLDecoder.decode(array1[3], "UTF-8");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "5":
                    // 5 送礼物
                    try {
                        sign = URLDecoder.decode(array1[5], "UTF-8");
                        liwuname = array1[13];
                        songname = array1[1];
                        shouname = array1[12];
                        liwunum = array1[11];
                        context = songname + "送给 " + shouname + liwunum + "个" + liwuname;
                        liwu = Integer.parseInt(array1[14]);
                        new giftThread().start();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "6":
                    //6 设置坐席模式申请上麦
                    break;
                case "7":
                    //7 设置坐席模式:取消申请上麦
                    break;
                case "8":
                    // 8 发送设置房间密码的消息
                    context = "系统提示 房主设置了房间密码";
                    break;
                case "9":
                    // 9 被抱下麦
                    break;
                case "10":
                    // 10被抱上麦
                    break;
                case "11":
                    //  @某人
//                    try {
//                        context = array1[]+URLDecoder.decode(array1[3], "UTF-8");
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
                    break;
                case "12":
                    // 12 设为房管

                    try {
                        context = "系统提示 " + array1[10] + "被设置为管理员";
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    break;
                case "13":
                    //  13 取消房管
                    break;
                case "14":
                    //14 禁麦
                    break;
                case "15":
                    //  15 踢出房间
                    break;
                case "16":
                    // 16重设或者取消房间锁
                    if (array1.length > 1) {
                        context = "系统提示  房主设置了房间密码";
                    } else {
                        context = "系统提示  房主取消了房间密码";
                    }

                    break;
                case "17":
                    // 17房主修改了房间背景
                    break;
                case "18":
                    // 18 正在连击礼物
                    try {

                        liwuname = array1[13];
                        songname = array1[1];
                        shouname = array1[12];
                        liwunum = array1[11];
                        liwu = Integer.parseInt(array1[14]);
                        context = songname + "送给 " + shouname + liwunum + "个" + liwuname;

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case "19":
                    // 19 连击完成

                    break;

            }


            mMsgAdapter.addRoomMsg(String.format(Locale.CHINA, USER_MESSAGE_FORMAT, message.mFromUser.userName, context));
        }


    }

    @Override
    public void onOnlineCountUpdate(int onlineCount) {

        Online_onlineCount = onlineCount;
        TvOnlineDisplayNumber.setText("在线人数: " + onlineCount);

    }

    @Override
    public void onUserJoin(ZegoChatroomUser[] users) {


        Log.e("join", "onUserJoin users.length: " + users.length);
        mRoomUsers.addAll(Arrays.asList(users));
        Log.e("TAGjoin", "onUserJoin: " + mRoomUsers.addAll(Arrays.asList(users)));
        notifyPickUpUserDataSet();
    }

    @Override
    public void onUserLeave(ZegoChatroomUser[] users) {
        Log.e("leave", "onUserLeave users.length: " + users.length);

        mRoomUsers.removeAll(Arrays.asList(users));

        notifyPickUpUserDataSet();
    }

    private void Dialog(ChatroomSeatInfo seatInfo, int pos) {
        if (pos == 10000) {
            Toast.makeText(RoomActivity.this, "你点击了用户", Toast.LENGTH_SHORT).show();
        }
        Dialog dialog = new Dialog(this);
        dialog.setCanceledOnTouchOutside(true);


        LayoutInflater inflater = getLayoutInflater();
        final View view = inflater.inflate(R.layout.dialog_search, null);
        EditText etsearchssroom = (EditText) view.findViewById(R.id.et_search_ss_room);
        RecyclerView mListView = (RecyclerView) view.findViewById(R.id.listView);
        TextView tvOlinelist = (TextView) view.findViewById(R.id.tv_Oline_list);
        TextView tvssroom = (TextView) view.findViewById(R.id.tv_ss_room);
        tvssroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                useretsearchssroom = etsearchssroom.getText().toString();
                Map<String, String> map = new HashMap<>();
                map.put("token", User.user().getToken());
                map.put("room_id", room_ID);
                map.put("searchStr", useretsearchssroom);
                RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/search", map, new NetCallBack<OnlineUserListBean>() {
                    @Override
                    public void onSucceed(final OnlineUserListBean onlineUserListBean, int type) {
                        MlistOline = onlineUserListBean.getData();
                        if (onlineUserListBean.getErr().equals("0")) {
                            //MainActivity.this :context     listDate:数据源
                            LinearLayoutManager layoutManager1 = new LinearLayoutManager(RoomActivity.this);
                            layoutManager1.setOrientation(RecyclerView.VERTICAL);
                            mListView.setLayoutManager(layoutManager1);
                            OnlineDialogAdapter = new RoomDialogAdapter(RoomActivity.this, MlistOline);
                            mListView.setAdapter(OnlineDialogAdapter);
                            OnlineDialogAdapter.setOnItemClickListener(new RoomDialogAdapter.ItemdiClickListener() {

                                @Override
                                public void onItemClick(int position) {
                                    onOperationItemClick(pos, 0, seatInfo, vi);
                                    dialog.dismiss();

                                }
                            });


                        }
                    }

                    @Override
                    public void SucceedError(Exception e, int types) {

                    }

                    @Override
                    public void onError(Throwable throwable, int type) {
                    }
                }, 1);
            }
        });
        Map<String, String> map = new HashMap<>();
        map.put("token", User.user().getToken());
        map.put("room_id", room_ID);
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/userlist", map, new NetCallBack<OnlineUserListBean>() {
            @Override
            public void onSucceed(final OnlineUserListBean onlineUserListBean, int type) {
                MlistOline = onlineUserListBean.getData();
                if (onlineUserListBean.getErr().equals("0")) {
                    //MainActivity.this :context     listDate:数据源
                    LinearLayoutManager layoutManager1 = new LinearLayoutManager(RoomActivity.this);
                    layoutManager1.setOrientation(RecyclerView.VERTICAL);
                    mListView.setLayoutManager(layoutManager1);
                    OnlineDialogAdapter = new RoomDialogAdapter(RoomActivity.this, MlistOline);
                    mListView.setAdapter(OnlineDialogAdapter);
                    //true表示listview获得当前焦点的时候，与相应用户输入的匹配符进行比对，筛选出匹配的ListView的列表中的项
                    OnlineDialogAdapter.setOnItemClickListener(new RoomDialogAdapter.ItemdiClickListener() {
                        @Override
                        public void onItemClick(int position) {
                            onOperationItemClick(pos, 0, seatInfo, vi);
                            dialog.dismiss();
                        }
                    });

                }
            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int type) {
            }
        }, 1);

        tvOlinelist.setText("在线人数" + Online_onlineCount + "人");
        dialog.setContentView(view);
        dialog.show();

    }

    private void shangmaiDialog() {
        DialogFragmentUpper dialogFragment = new DialogFragmentUpper();
        dialogFragment.show(getSupportFragmentManager(), null);
        dialogFragment.setCancelable(true);

    }

    //宿主activity中的getRoom_ID方法
    public String getRoom_ID() {
        return room_ID;
    }

    private void exitRoom() {
        if (isLeavingRoom) {
            return;
        }
        isLeavingRoom = true;
        boolean shouldLeaveSeat = (getSeatForUser(ZegoDataCenter.ZEGO_USER) != null);
        if (shouldLeaveSeat) {
            ZegoChatroom.shared().leaveSeat(new ZegoSeatUpdateCallbackWrapper() {
                @Override
                public void onCompletion(ResultCode resultCode) {
                    super.onCompletion(resultCode);
                    boolean isSuccess = resultCode.isSuccess();
                    if (!isSuccess) {
                        Toast.makeText(RoomActivity.this, "下麦失败", Toast.LENGTH_SHORT).show();
                    } else {
                        ZegoChatroom.shared().getMusicPlayer().stop();
                    }
                    exitChatRoom();
                    exitRoomInner();
                }
            });
        } else {
            exitChatRoom();
            exitRoomInner();
        }
    }


    private void exitRoomInner() {
        //releaseDialog();

        // 重置音效相关设置
        ZegoChatroom.shared().setVoiceChangeValue(0.0f);
        ZegoChatroom.shared().setVirtualStereoAngle(90);
        ZegoChatroom.shared().setAudioReverbConfig(null);
        ZegoChatroom.shared().setEnableLoopback(false);
        ZegoChatroom.shared().leaveRoom();
        ZegoChatroom.shared().removeZegoChatroomCallback(this);
        ZegoChatroom.shared().removeIMCallback(this);
        ZegoChatroom.shared().removeCMDCallback(this);

        finish();
    }

    private void releaseDialog() {
        mTipDialog = null;
        if (mSeatOperationDialog != null) {
            mSeatOperationDialog.setOnOperationItemClickListener(null);
            mSeatOperationDialog = null;
        }
        if (mPickUpUserSelectDialog != null) {
            mPickUpUserSelectDialog.setOnPickUpUserListener(null);
            mPickUpUserSelectDialog = null;
        }
    }

    private void BottomDialog() {
        Dialog dialog0 = new Dialog(this, R.style.ActionSheetDialogStyle);
        //填充对话框的布局
        inflate = LayoutInflater.from(this).inflate(R.layout.bottom_projectile_window, null);
        LinearLayout ivfanhuihoomroom1 = inflate.findViewById(R.id.iv_fanhui_hoom_room11);
        LinearLayout ivroomlock = inflate.findViewById(R.id.iv_room_lock1);
        LinearLayout ivjubaozhibojian = inflate.findViewById(R.id.iv_jubaozhibojian1);
        LinearLayout ivzhibojian = inflate.findViewById(R.id.iv_wodezhibojian1);
        LinearLayout ivyaoqingfensi = inflate.findViewById(R.id.iv_yaoqingfensi1);
        LinearLayout ivwodeshezhi = inflate.findViewById(R.id.iv_wodeshezhi1);

        //管理权限判断
        if (RMManager == true) {
            ivroomlock.setVisibility(View.VISIBLE);
            ivfanhuihoomroom1.setVisibility(View.VISIBLE);
            ivyaoqingfensi.setVisibility(View.GONE);
            ivzhibojian.setVisibility(View.GONE);
            ivjubaozhibojian.setVisibility(View.GONE);
        } else {
            //观众权限
            ivzhibojian.setVisibility(View.GONE);
            ivfanhuihoomroom1.setVisibility(View.VISIBLE);
            ivjubaozhibojian.setVisibility(View.VISIBLE);
            ivyaoqingfensi.setVisibility(View.GONE);
        }
        //房主权限
        if (User.user().getUser_ID().equals(roomuser_ID)) {
            ivroomlock.setVisibility(View.VISIBLE);
            ivfanhuihoomroom1.setVisibility(View.VISIBLE);
            ivwodeshezhi.setVisibility(View.VISIBLE);
            ivyaoqingfensi.setVisibility(View.GONE);
            ivzhibojian.setVisibility(View.GONE);
            ivjubaozhibojian.setVisibility(View.GONE);
        }
        //退出直播间
        ivfanhuihoomroom1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                exitRoom();
                dialog0.dismiss();
            }
        });
        //房间上锁
        ivroomlock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RoomActivity.this, HoomPasswordActivity.class);
                intent.putExtra("room_idid", room_ID);
                startActivity(intent);
                dialog0.dismiss();

            }
        });
        //举报直播间
        ivjubaozhibojian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RoomActivity.this, JuBaoActivity.class);
                intent.putExtra("room_idid", room_ID);
                startActivity(intent);
                dialog0.dismiss();

            }
        });
        //去我的直播间
        ivzhibojian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gomyroom();
                dialog0.dismiss();

            }
        });
        //邀请粉丝
        ivzhibojian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(RoomActivity.this, "正在全力开发中，感谢您的支持", Toast.LENGTH_SHORT).show();
                dialog0.dismiss();

            }
        });
        //我的直播间设置
        ivwodeshezhi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog00();
                Toast.makeText(RoomActivity.this, "正在全力开发中didiid，感谢您的支持", Toast.LENGTH_SHORT).show();
                dialog0.dismiss();


            }
        });


        //将布局设置给Dialog
        dialog0.setContentView(inflate);
        //获取当前Activity所在的窗体
        Window dialogWindow = dialog0.getWindow();
        //设置Dialog从窗体底部弹出
        dialogWindow.setGravity(Gravity.BOTTOM);
        //获得窗体的属性
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        lp.y = 20;//设置Dialog距离底部的距离
        //    将属性设置给窗体
        dialogWindow.setAttributes(lp);
        dialog0.show();//显示对话框


    }


    private void Dialog00() {

        Dialog dialog00 = new Dialog(RoomActivity.this, R.style.ActionSheetDialogStyle);
        //填充对话框的布局
        View inflate = LayoutInflater.from(RoomActivity.this).inflate(R.layout.room_with_bullet_windows, null);
        TextView tvroomsetup = inflate.findViewById(R.id.tv_room_set_up);

        RelativeLayout rlmanager = inflate.findViewById(R.id.rl_manager);

        RelativeLayout rlRoomEaring = inflate.findViewById(R.id.rl_RoomEaring);

        RelativeLayout rlRoomBackgroundphoto = inflate.findViewById(R.id.rl_RoomBackground_photo);
        GridView lvbackground = inflate.findViewById(R.id.lv_background);


        EditText etRoomname = inflate.findViewById(R.id.et_Room_name);
        TextView ideditodetailfontcount = inflate.findViewById(R.id.id_editor_detail_font_count);


        EditText etRoomname1 = inflate.findViewById(R.id.et_Room_Notice);
        TextView ideditodetailfontcount1 = inflate.findViewById(R.id.id_editor_Room_Notice_count);
        //房间名字
        /**
         * 获取et_username编辑框的焦点
         */
        etRoomname.setFocusable(true);
        etRoomname.setFocusableInTouchMode(true);
        etRoomname.requestFocus();
        //进入页面显示输入字数
        ideditodetailfontcount.setText(etRoomname.getText().toString().length() + "/10");
        /**
         * et_username输入监听与限制
         */
        TextWatcher watcher = new TextWatcher() {
            private CharSequence temp;
            private int editStart;
            private int editEnd;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                temp = s;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                editStart = etRoomname.getSelectionStart();
                editEnd = etRoomname.getSelectionEnd();
                ideditodetailfontcount.setText(temp.length() + "/15");//输入后字数显示
                if (temp.length() > 15) {//输入字数限制
                    s.delete(editStart - 1, editEnd);//删除限制外的内容
                    int tempSelection = editStart;
                    etRoomname.setText(s);//显示限制内的内容
                    etRoomname.setSelection(tempSelection);//光标焦点设置在行末
                }
            }
        };
        etRoomname.addTextChangedListener(watcher);


        //房间玩法
        /**
         * 获取et_username编辑框的焦点
         */
        etRoomname1.setFocusable(true);
        etRoomname1.setFocusableInTouchMode(true);
        etRoomname1.requestFocus();
        //进入页面显示输入字数
        ideditodetailfontcount1.setText(etRoomname1.getText().toString().length() + "/10");
        /**
         * et_username输入监听与限制
         */
        TextWatcher watcher1 = new TextWatcher() {
            private CharSequence temp;
            private int editStart;
            private int editEnd;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                temp = s;
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                editStart = etRoomname1.getSelectionStart();
                editEnd = etRoomname1.getSelectionEnd();
                ideditodetailfontcount1.setText(temp.length() + "/300");//输入后字数显示
                if (temp.length() > 300) {//输入字数限制
                    s.delete(editStart - 1, editEnd);//删除限制外的内容
                    int tempSelection = editStart;
                    etRoomname1.setText(s);//显示限制内的内容
                    etRoomname1.setSelection(tempSelection);//光标焦点设置在行末
                }
            }
        };
        etRoomname1.addTextChangedListener(watcher1);


        //确定
        tvroomsetup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ROOMNAME = etRoomname.getText().toString();
                ROOMNAMEWANFA = etRoomname1.getText().toString();
                ChatRoomSetUp();
                dialog00.dismiss();
            }
        });
        //管理员点击
        rlmanager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog000 = new Dialog(RoomActivity.this, R.style.ActionSheetDialogStyle);
                //填充对话框的布局
                View inflate = LayoutInflater.from(RoomActivity.this).inflate(R.layout.room_seting_manager_auth_windows, null);
                RecyclerView rvauthmanagerseting = inflate.findViewById(R.id.rv_auth_manager_seting);


                LinearLayoutManager layoutManager = new LinearLayoutManager(RoomActivity.this);
                layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                MyAuthSetingRecyclerViewAdapter myAuthSetingRecyclerViewAdapter = new MyAuthSetingRecyclerViewAdapter(room_auth, RoomActivity.this, room_ID);
                rvauthmanagerseting.setLayoutManager(layoutManager);
                rvauthmanagerseting.setAdapter(myAuthSetingRecyclerViewAdapter);

                dialog000.setContentView(inflate);
                //获取当前Activity所在的窗体
                Window dialogWindow = dialog000.getWindow();
                //设置Dialog从窗体底部弹出
                dialogWindow.setGravity(Gravity.BOTTOM);
                //获得窗体的属性
                WindowManager.LayoutParams lp = dialogWindow.getAttributes();
                lp.y = 0;//设置Dialog距离底部的距离
                //    将属性设置给窗体
                dialogWindow.setAttributes(lp);
                dialog000.show();//显示对话框

                // Toast.makeText(RoomActivity.this,"正在全力开发中管理员，感谢您的支持",Toast.LENGTH_SHORT).show();
                dialog00.dismiss();
            }
        });


        //房间明细点击
        rlRoomEaring.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RoomActivity.this, ShenhaiActivity.class);
                intent.putExtra("url", "https://api3.dayushaiwang.com/baoxiang/roomharvest.html" + "?token=" + User.user().getToken() + "&room_id=" + room_ID);
                startActivity(intent);
                dialog00.dismiss();
            }
        });
        //房间背景
        rlRoomBackgroundphoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                listRoom = new ArrayList<>();
                listRoom.add(R.mipmap.mlzsaz);
                listRoom.add(R.mipmap.dfcaz);
                listRoom.add(R.mipmap.dyaz);
                listRoom.add(R.mipmap.mhwtaz);
                listRoom.add(R.mipmap.tnaz);
                listRoom.add(R.mipmap.xdaz);
                listRoom.add(R.mipmap.xxzhaz);
                listRoom.add(R.mipmap.yhyhaz);
                adapter = new MyAdapter1(listRoom, RoomActivity.this, RoomActivity.this);
                lvbackground.setAdapter(adapter);
                //Toast.makeText(RoomActivity.this,"正在全力开发中房间背景，感谢您的支持",Toast.LENGTH_SHORT).show();
                // dialog00.dismiss();
            }
        });
        dialog00.setContentView(inflate);
        //获取当前Activity所在的窗体
        Window dialogWindow = dialog00.getWindow();
        //设置Dialog从窗体底部弹出
        dialogWindow.setGravity(Gravity.BOTTOM);
        //获得窗体的属性
        WindowManager.LayoutParams lp = dialogWindow.getAttributes();
        lp.y = 10;//设置Dialog距离底部的距离
        //    将属性设置给窗体
        dialogWindow.setAttributes(lp);
        dialog00.show();//显示对话框

    }

    //查询砖石数
    private void initDatashangmaiuser() {
        Map<String, String> map = new HashMap<>();
        map.put("user_id", User.user().getUser_ID());
        map.put("token", User.user().getToken());
        map.put("room_id", room_ID);
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/room/information", map, new NetCallBack<PersonalDataBean>() {
            @Override
            public void onSucceed(final PersonalDataBean personalDataBean, int type) {

                isshoucang = personalDataBean.getIsshoucang();
                Glide.with(RoomActivity.this).load(personalDataBean.getPicarr().get(0).getPic());
                kongtouurl = personalDataBean.getPicarr().get(0).getUrl();
                if (isshoucang.equals("0")) {
                    SC = false;
                    tvguanzhu.setBackgroundResource(SC ? R.mipmap.dianjiguanzhu : R.mipmap.love);
                }
                if (isshoucang.equals("1")) {
                    SC = true;
                    tvguanzhu.setBackgroundResource(SC ? R.mipmap.dianjiguanzhu : R.mipmap.love);
                }

                tv_play_method.setText("房间玩法" + personalDataBean.getRoom().getRoomtake());
                String A = personalDataBean.getRoom().getBack();
                if (A.equals("0")) {
                    llroombg.setBackgroundResource(R.mipmap.mlzsaz);
                } else if (A.equals("1")) {
                    llroombg.setBackgroundResource(R.mipmap.dfcaz);
                } else if (A.equals("2")) {
                    llroombg.setBackgroundResource(R.mipmap.dyaz);
                } else if (A.equals("3")) {
                    llroombg.setBackgroundResource(R.mipmap.mhwtaz);
                } else if (A.equals("4")) {
                    llroombg.setBackgroundResource(R.mipmap.tnaz);
                } else if (A.equals("5")) {
                    llroombg.setBackgroundResource(R.mipmap.xdaz);
                } else if (A.equals("6")) {
                    llroombg.setBackgroundResource(R.mipmap.xxzhaz);
                } else if (A.equals("7")) {
                    llroombg.setBackgroundResource(R.mipmap.yhyhaz);
                }
                com.xunshan.ZhenSheng.user.User.user().setUser_balance(personalDataBean.getBalance());
            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int type) {
            }
        }, 1);
    }


    private boolean isSpeakerMute() {
        return (boolean) mIvSpeakerState.getTag();
    }

    private void changeSpeakerState() {
        boolean isSpeakerMute = isSpeakerMute();
        if (isSpeakerMute) {
            //静音
//            ZegoChatroom.shared().muteSpeaker(false);
            mIvSpeakerState.setImageResource(R.mipmap.jingyinkai);
            mIvSpeakerState.setTag(false);

        } else {
            //非静音
            mIvSpeakerState.setImageResource(R.mipmap.sound);
            mIvSpeakerState.setTag(true);
//            ZegoChatroom.shared().muteSpeaker(true);

        }
    }

    private ChatroomSeatInfo getSeatForUser(ZegoChatroomUser user) {
        for (ChatroomSeatInfo seat : mSeats) {
            if (seat.mStatus == ZegoChatroomSeatStatus.Used && seat.mUser.equals(user)) {
                return seat;
            }
        }
        return null;
    }

    private void notifyPickUpUserDataSet() {
        List<ZegoChatroomUser> mPickUsers = new ArrayList<>();
        for (ZegoChatroomUser user : mRoomUsers) {
            if (getSeatForUser(user) == null) {
                mPickUsers.add(user);

//                if (user.userID.length() > 6) {
//                    listCG.add(user.userID);
//                }

            }
        }
        mPickUpUserSelectDialog.setUserList(mPickUsers);
        //有超管
//        if (listCG.size() > 0) {
//            String IM_MESSAGE_CONTENT = "20," + listToString1(listCG);
//
//            ZegoChatroom.shared().sendRoomMessage(IM_MESSAGE_CONTENT, 20, new ZegoChatroomSendRoomMessageCallback() {
//                @Override
//                public void onSendRoomMessage(ResultCode resultCode, ZegoChatroomMessage message) {
//                    listCG.clear();
//
//                }
//            });
//        }
    }


    private PickUpUserSelectDialog mPickUpUserSelectDialog;

    private void showPickUpUserSelectDialog(int pickUpTargetIndex) {
        mPickUpUserSelectDialog.setPickUpTargetIndex(pickUpTargetIndex);
        mPickUpUserSelectDialog.show();
    }

    // --------------- implements ChatroomSeatsAdapter.OnChatroomSeatClickListener --------------- //
    @Override
    public void onChatroomSeatClick(ChatroomSeatInfo chatroomSeatInfo, View view) {


        //显示弹窗
        showOperationMenu(chatroomSeatInfo, view);

    }

    private void showOperationMenu(ChatroomSeatInfo seatInfo, View view) {
        int position = mSeats.indexOf(seatInfo);
        boolean isOwner = isOwner();
        if (position == -1) {
            return;
        }
        boolean isOnMic = getSeatForUser(ZegoDataCenter.ZEGO_USER) != null;


        //麦位是否有人
        if (seatInfo.mStatus == ZegoChatroomSeatStatus.Empty) {


            //房主权限
            if (User.user().getUser_ID().equals(roomuser_ID)) {

                if (marr.get(position).equals("0")) {
                    if ((seatInfo.mStatus == ZegoChatroomSeatStatus.Empty || seatInfo.mStatus == ZegoChatroomSeatStatus.Closed)) {
                        ActionSheetDialog dialog1 = new ActionSheetDialog(RoomActivity.this).builder().setTitle("请选择")
                                .addSheetItem("设置座位抱人上麦卡座", null, new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        Map<String, String> map = new HashMap<>();
                                        map.put("marr_k", position - 1 + "");
                                        map.put("marr_v", "1");
                                        map.put("token", User.user().getToken());
                                        map.put("room_id", room_ID);
                                        Log.e("TAGonClick", "onClick: " + position + room_ID + User.user().getToken() + "1");

                                        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/maixu", map, new NetCallBack<SheZhiMaiXuFangShiBean>() {
                                            @Override
                                            public void onSucceed(final SheZhiMaiXuFangShiBean sheZhiMaiXuFangShiBean, int type) {
//                                                        ImageView rlitembg = view.findViewById(R.id.rlitembg);
//                                                        rlitembg.setBackgroundResource(0);
//                                                        rlitembg.setBackgroundResource(R.mipmap.yizi);
                                                Toast.makeText(RoomActivity.this, sheZhiMaiXuFangShiBean.getData(), Toast.LENGTH_SHORT).show();
                                                String IM_MESSAGE_CONTENT = position - 1 + "";
                                                ZegoChatroom.shared().sendRoomMessage(IM_MESSAGE_CONTENT, 6, new ZegoChatroomSendRoomMessageCallback() {
                                                    @Override
                                                    public void onSendRoomMessage(ResultCode resultCode, ZegoChatroomMessage message) {
                                                        if (resultCode.isSuccess()) {
                                                            // 发送成功
                                                            // Toast.makeText(RoomActivity.this, "发送成功", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            // 发送失败
                                                            // Toast.makeText(RoomActivity.this, "发送失敗", Toast.LENGTH_SHORT).show();

                                                        }
                                                    }
                                                });
                                                Map<String, String> map = new HashMap<>();
                                                map.put("user_id", User.user().getUser_ID());
                                                map.put("token", User.user().getToken());
                                                map.put("room_id", room_ID);
                                                RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/index", map, new NetCallBack<RoomInforBean>() {
                                                    @Override
                                                    public void onSucceed(final RoomInforBean roomInforBean, int type) {
                                                        if (roomInforBean.getErr().equals("0")) {
                                                            marr = roomInforBean.getRoom().getMarr();
                                                            //这里为了添加头部数据marr{0}
                                                            List<String> strList2 = new ArrayList<>();
                                                            strList2.add("0");
                                                            marr.addAll(0, strList2);
                                                            mSeatsAdapter.setSeats(mSeats, marr);
                                                        }
                                                    }

                                                    @Override
                                                    public void SucceedError(Exception e, int types) {
                                                    }

                                                    @Override
                                                    public void onError(Throwable throwable, int type) {
                                                    }
                                                }, 1);
                                                mSeatsAdapter.notifyDataSetChanged();
                                            }

                                            @Override
                                            public void SucceedError(Exception e, int types) {
                                            }

                                            @Override
                                            public void onError(Throwable throwable, int type) {
                                            }
                                        }, 1);

                                    }
                                }).addSheetItem("已经可直接上麦", null, new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        Toast.makeText(RoomActivity.this, "已经设置可直接上麦", Toast.LENGTH_SHORT).show();
                                        mSeatsAdapter.notifyDataSetChanged();
                                    }

                                });
                        dialog1.show();
////
////
                    }

                }
                if (marr.get(position).equals("1")) {

                    ActionSheetDialog dialog2 = new ActionSheetDialog(RoomActivity.this).builder().setTitle("请选择")
                            .addSheetItem("抱人上麦", null, new ActionSheetDialog.OnSheetItemClickListener() {
                                @Override
                                public void onClick(int which) {
                                    Dialog(seatInfo, position);
                                }
                            }).addSheetItem("取消申请上麦", null, new ActionSheetDialog.OnSheetItemClickListener() {
                                @Override
                                public void onClick(int which) {
                                    Map<String, String> map = new HashMap<>();
                                    map.put("marr_k", position - 1 + "");
                                    map.put("marr_v", "0");
                                    map.put("token", User.user().getToken());
                                    map.put("room_id", room_ID);
                                    RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/maixu", map, new NetCallBack<SheZhiMaiXuFangShiBean>() {
                                        @Override
                                        public void onSucceed(final SheZhiMaiXuFangShiBean sheZhiMaiXuFangShiBean, int type) {
//                                                            ImageView rlitembg = view.findViewById(R.id.rlitembg);
//                                                            rlitembg.setBackgroundResource(0);
//                                                            rlitembg.setBackgroundResource(R.mipmap.addshangmai);
                                            Toast.makeText(RoomActivity.this, sheZhiMaiXuFangShiBean.getData(), Toast.LENGTH_SHORT).show();
                                            String IM_MESSAGE_CONTENT = position - 1 + "";
                                            ZegoChatroom.shared().sendRoomMessage(IM_MESSAGE_CONTENT, 7, new ZegoChatroomSendRoomMessageCallback() {
                                                @Override
                                                public void onSendRoomMessage(ResultCode resultCode, ZegoChatroomMessage message) {
                                                    if (resultCode.isSuccess()) {
                                                        // 发送成功
                                                        // Toast.makeText(RoomActivity.this, "发送成功", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        // 发送失败
                                                        //  Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                                                    }
                                                }
                                            });
                                            Map<String, String> map = new HashMap<>();
                                            map.put("user_id", User.user().getUser_ID());
                                            map.put("token", User.user().getToken());
                                            map.put("room_id", room_ID);
                                            RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/index", map, new NetCallBack<RoomInforBean>() {
                                                @Override
                                                public void onSucceed(final RoomInforBean roomInforBean, int type) {
                                                    if (roomInforBean.getErr().equals("0")) {
                                                        marr = roomInforBean.getRoom().getMarr();
                                                        //这里为了添加头部数据marr{0}
                                                        List<String> strList2 = new ArrayList<>();
                                                        strList2.add("0");
                                                        marr.addAll(0, strList2);
                                                        mSeatsAdapter.setSeats(mSeats, marr);
                                                    }
                                                }

                                                @Override
                                                public void SucceedError(Exception e, int types) {
                                                }

                                                @Override
                                                public void onError(Throwable throwable, int type) {
                                                }
                                            }, 1);
                                            mSeatsAdapter.notifyDataSetChanged();

                                        }

                                        @Override
                                        public void SucceedError(Exception e, int types) {
                                        }

                                        @Override
                                        public void onError(Throwable throwable, int type) {
                                        }
                                    }, 1);
                                }

                            });
                    dialog2.show();


                    //换麦
                }

            }


            //判断是否管理员
            if (RMManager == true) {

                if (marr.get(position).equals("0")) {
                    if ((seatInfo.mStatus == ZegoChatroomSeatStatus.Empty || seatInfo.mStatus == ZegoChatroomSeatStatus.Closed)) {
                        ActionSheetDialog dialog1 = new ActionSheetDialog(RoomActivity.this).builder().setTitle("请选择")
                                .addSheetItem("设置座位抱人上麦卡座", null, new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        Map<String, String> map = new HashMap<>();
                                        map.put("marr_k", position - 1 + "");
                                        map.put("marr_v", "1");
                                        map.put("token", User.user().getToken());
                                        map.put("room_id", room_ID);
                                        Log.e("TAGonClick", "onClick: " + position + room_ID + User.user().getToken() + "1");

                                        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/maixu", map, new NetCallBack<SheZhiMaiXuFangShiBean>() {
                                            @Override
                                            public void onSucceed(final SheZhiMaiXuFangShiBean sheZhiMaiXuFangShiBean, int type) {
//                                                        ImageView rlitembg = view.findViewById(R.id.rlitembg);
//                                                        rlitembg.setBackgroundResource(0);
//                                                        rlitembg.setBackgroundResource(R.mipmap.yizi);
                                                Toast.makeText(RoomActivity.this, sheZhiMaiXuFangShiBean.getData(), Toast.LENGTH_SHORT).show();
                                                String IM_MESSAGE_CONTENT = position - 1 + "";
                                                ZegoChatroom.shared().sendRoomMessage(IM_MESSAGE_CONTENT, 6, new ZegoChatroomSendRoomMessageCallback() {
                                                    @Override
                                                    public void onSendRoomMessage(ResultCode resultCode, ZegoChatroomMessage message) {
                                                        if (resultCode.isSuccess()) {
                                                            // 发送成功
                                                            //  Toast.makeText(RoomActivity.this, "发送成功", Toast.LENGTH_SHORT).show();
                                                        } else {
                                                            // 发送失败
                                                            // Toast.makeText(RoomActivity.this, "发送失敗", Toast.LENGTH_SHORT).show();

                                                        }
                                                    }
                                                });
                                                Map<String, String> map = new HashMap<>();
                                                map.put("user_id", User.user().getUser_ID());
                                                map.put("token", User.user().getToken());
                                                map.put("room_id", room_ID);
                                                RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/index", map, new NetCallBack<RoomInforBean>() {
                                                    @Override
                                                    public void onSucceed(final RoomInforBean roomInforBean, int type) {
                                                        if (roomInforBean.getErr().equals("0")) {
                                                            marr = roomInforBean.getRoom().getMarr();
                                                            //这里为了添加头部数据marr{0}
                                                            List<String> strList2 = new ArrayList<>();
                                                            strList2.add("0");
                                                            marr.addAll(0, strList2);
                                                            mSeatsAdapter.setSeats(mSeats, marr);
                                                        }
                                                    }

                                                    @Override
                                                    public void SucceedError(Exception e, int types) {
                                                    }

                                                    @Override
                                                    public void onError(Throwable throwable, int type) {
                                                    }
                                                }, 1);
                                                mSeatsAdapter.notifyDataSetChanged();
                                            }

                                            @Override
                                            public void SucceedError(Exception e, int types) {
                                            }

                                            @Override
                                            public void onError(Throwable throwable, int type) {
                                            }
                                        }, 1);

                                    }
                                }).addSheetItem("已经可直接上麦", null, new ActionSheetDialog.OnSheetItemClickListener() {
                                    @Override
                                    public void onClick(int which) {
                                        Toast.makeText(RoomActivity.this, "已经设置可直接上麦", Toast.LENGTH_SHORT).show();
                                        mSeatsAdapter.notifyDataSetChanged();
                                    }

                                });
                        dialog1.show();
////
////
                    }

                }
                if (marr.get(position).equals("1")) {

                    ActionSheetDialog dialog2 = new ActionSheetDialog(RoomActivity.this).builder().setTitle("请选择")
                            .addSheetItem("抱人上麦", null, new ActionSheetDialog.OnSheetItemClickListener() {
                                @Override
                                public void onClick(int which) {
                                    Dialog(seatInfo, position);
                                }
                            }).addSheetItem("取消申请上麦", null, new ActionSheetDialog.OnSheetItemClickListener() {
                                @Override
                                public void onClick(int which) {
                                    Map<String, String> map = new HashMap<>();
                                    map.put("marr_k", position - 1 + "");
                                    map.put("marr_v", "0");
                                    map.put("token", User.user().getToken());
                                    map.put("room_id", room_ID);
                                    RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/maixu", map, new NetCallBack<SheZhiMaiXuFangShiBean>() {
                                        @Override
                                        public void onSucceed(final SheZhiMaiXuFangShiBean sheZhiMaiXuFangShiBean, int type) {
//                                                            ImageView rlitembg = view.findViewById(R.id.rlitembg);
//                                                            rlitembg.setBackgroundResource(0);
//                                                            rlitembg.setBackgroundResource(R.mipmap.addshangmai);
                                            Toast.makeText(RoomActivity.this, sheZhiMaiXuFangShiBean.getData(), Toast.LENGTH_SHORT).show();
                                            String IM_MESSAGE_CONTENT = position - 1 + "";
                                            ZegoChatroom.shared().sendRoomMessage(IM_MESSAGE_CONTENT, 7, new ZegoChatroomSendRoomMessageCallback() {
                                                @Override
                                                public void onSendRoomMessage(ResultCode resultCode, ZegoChatroomMessage message) {
                                                    if (resultCode.isSuccess()) {
                                                        // 发送成功
                                                        // Toast.makeText(RoomActivity.this, "发送成功", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        // 发送失败
                                                        //  Toast.makeText(RoomActivity.this, "发送失敗", Toast.LENGTH_SHORT).show();

                                                    }
                                                }
                                            });
                                            Map<String, String> map = new HashMap<>();
                                            map.put("user_id", User.user().getUser_ID());
                                            map.put("token", User.user().getToken());
                                            map.put("room_id", room_ID);
                                            RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/index", map, new NetCallBack<RoomInforBean>() {
                                                @Override
                                                public void onSucceed(final RoomInforBean roomInforBean, int type) {
                                                    if (roomInforBean.getErr().equals("0")) {
                                                        marr = roomInforBean.getRoom().getMarr();
                                                        //这里为了添加头部数据marr{0}
                                                        List<String> strList2 = new ArrayList<>();
                                                        strList2.add("0");
                                                        marr.addAll(0, strList2);
                                                        mSeatsAdapter.setSeats(mSeats, marr);
                                                    }
                                                }

                                                @Override
                                                public void SucceedError(Exception e, int types) {
                                                }

                                                @Override
                                                public void onError(Throwable throwable, int type) {
                                                }
                                            }, 1);
                                            mSeatsAdapter.notifyDataSetChanged();

                                        }

                                        @Override
                                        public void SucceedError(Exception e, int types) {
                                        }

                                        @Override
                                        public void onError(Throwable throwable, int type) {
                                        }
                                    }, 1);
                                }

                            });
                    dialog2.show();


                    //换麦
                }

            }


            //用户是否在麦上
            if (!isOnMic) {


                if (marr.get(position).equals("1")) {
                    // TODO: 2020/5/26 观众申请上麦
                    Map<String, String> map = new HashMap<>();
                    map.put("token", User.user().getToken());
                    map.put("user_id", User.user().getUser_ID());
                    map.put("room_id", room_ID);
                    map.put("key", position + "");
                    RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/apply", map, new NetCallBack<GZShengQingShangMaiBean>() {
                        @Override
                        public void onSucceed(final GZShengQingShangMaiBean gzShengQingShangMaiBean, int type) {
                            if (gzShengQingShangMaiBean.getErr().equals("0")) {
                                Toast.makeText(RoomActivity.this, "你申请上麦成功，房主已经在审核", Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void SucceedError(Exception e, int types) {
                        }

                        @Override
                        public void onError(Throwable throwable, int type) {
                        }
                    }, 1);
//                    Toast.makeText(this, "观众申请上麦", Toast.LENGTH_SHORT).show();
                }
            } else {

            }


        }
//        else {
//
//        }


        RequestOptions options = new RequestOptions().centerCrop().transform(new RoundTransform(this, 50)).error(R.mipmap.logo);
        dialog = new Dialog(this, R.style.ActionSheetDialogStyle);
        //填充对话框的布局
        View inflate = LayoutInflater.from(this).inflate(R.layout.personal_data, null);
        ImageView ivphotoheaderpersonal = (ImageView) inflate.findViewById(R.id.iv_photo_header_personal);
        TextView tvseatuserIDpersonal = (TextView) inflate.findViewById(R.id.tv_seat_user_ID_personal);
        TextView tvseatcharmpersonal = (TextView) inflate.findViewById(R.id.tv_seat_charm_personal);
        TextView tvseatdialognamepersonal = (TextView) inflate.findViewById(R.id.tv_seat_dialog_name_personal);
        TextView tvseatlevelpersonal = (TextView) inflate.findViewById(R.id.tv_seat_level_personal);
        TextView tvseatsexpersonal = (TextView) inflate.findViewById(R.id.tv_seat_sex_personal);
        TextView tvseatsignpersonal = (TextView) inflate.findViewById(R.id.tv_seat_sign_personal);
        TextView tvseatfollowpersonal = (TextView) inflate.findViewById(R.id.tv_seat_follow_personal);
        TextView tvgifmpersonalpersonal = (TextView) inflate.findViewById(R.id.tv_gifm_personal_personal);
        TextView tvbalanc_personal = (TextView) inflate.findViewById(R.id.tv_balance_personal);


        LinearLayout ll_gerenzhuye = inflate.findViewById(R.id.ll_gerenzhuye);


        LinearLayout iv_baotxiamai = inflate.findViewById(R.id.iv_baoxiamai);
        LinearLayout iv_jubao = inflate.findViewById(R.id.iv_jubao);
        LinearLayout iv_xiamai = inflate.findViewById(R.id.iv_xiamai);
        LinearLayout iv_jinmai = inflate.findViewById(R.id.iv_jinmai);
        ImageView jinmaiiv = inflate.findViewById(R.id.jinmaiiv);
        TextView jinmaitv = inflate.findViewById(R.id.jinmaitv);

        // ImageView iv_gift = (ImageView) inflate.findViewById(R.id.iv_gift);
        LinearLayout iv_seweiguanli = inflate.findViewById(R.id.iv_seweiguanli);
        ImageView iv_guanzuuser = (ImageView) inflate.findViewById(R.id.iv_guanzuuser);

        TextView tvcancel11 = (TextView) inflate.findViewById(R.id.tv_cancel_11);

        jinmaiiv.setBackground(null);
        jinmaiiv.setBackgroundResource(mick ? R.mipmap.jinmai : R.mipmap.jiemai);
        jinmaitv.setText(mick ? "禁麦" : "解麦");

        //空麦
        if (marr.get(position).equals("0")) {

            if (position == 0) {
                //房主权限
                if (User.user().getUser_ID().equals(roomuser_ID)) {
                    ll_gerenzhuye.setVisibility(View.VISIBLE);
                }
                //管理权限判断
                if (RMManager == true) {
                    ll_gerenzhuye.setVisibility(View.VISIBLE);
                } else {
                    //观众权限
                    ll_gerenzhuye.setVisibility(View.VISIBLE);

                }


                Map<String, String> map = new HashMap<>();
                map.put("userArr", roomuser_ID);
                map.put("token", User.user().getToken());
                RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/userslist", map, new NetCallBack<UserRoomList2Bean>() {
                    @Override
                    public void onSucceed(final UserRoomList2Bean userRoomList2Bean, int type) {

                        if (userRoomList2Bean.getErr().equals("0")) {
                            RoomUser = userRoomList2Bean;
                            Glide.with(RoomActivity.this)
                                    .load(AllInterface.addr + "/" + RoomUser.getData().get(0).getHeadpic())
                                    .apply(options)
                                    .into(ivphotoheaderpersonal);
                            tvseatdialognamepersonal.setText("姓名：" + RoomUser.getData().get(0).getName());
                            tvseatuserIDpersonal.setText("ID：" + RoomUser.getData().get(0).getUser_id());
                            tvseatlevelpersonal.setText("等级：" + RoomUser.getData().get(0).getLevel());
                            tvseatcharmpersonal.setText("魅力值：" + RoomUser.getData().get(0).getCharm());
                            tvseatsignpersonal.setText("签名：" + RoomUser.getData().get(0).getSign());
                            if (RoomUser.getData().get(0).getSex().equals("0")) {
                                tvseatsexpersonal.setText("性别：" + "男");
                            } else {
                                tvseatsexpersonal.setText("性别：" + "女");
                            }
                            tvgifmpersonalpersonal.setText("礼物：" + RoomUser.getData().get(0).getGiftnum());


                        }
                    }

                    @Override
                    public void SucceedError(Exception e, int types) {
                    }

                    @Override
                    public void onError(Throwable throwable, int type) {
                    }
                }, 1);

                //举报
                iv_jubao.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(RoomActivity.this, JuBaoActivity.class);
                        intent.putExtra("room_idid", room_ID);
                        startActivity(intent);
                        dialog.dismiss();
                    }
                });

                //下麦
                iv_xiamai.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onOperationItemClick(position, 2, seatInfo, view);
                        dialog.dismiss();
                    }
                });


                //抱下麦
                iv_baotxiamai.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onOperationItemClick(position, 4, seatInfo, view);
                        dialog.dismiss();
                    }
                });


                //禁麦
                iv_jinmai.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        onOperationItemClick(position, 5, seatInfo, view);
                        dialog.dismiss();
                    }
                });
//                //送礼物
//                iv_gift.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        onOperationItemClick(position, 8, seatInfo);
//                        dialog.dismiss();
//                    }
//                });
                //设管理员
                iv_seweiguanli.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        Map<String, String> map = new HashMap<>();
                        map.put("user_id", seatInfo.mUser.userID);
                        map.put("token", User.user().getToken());
                        map.put("room_id", room_ID);
                        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/auth", map, new NetCallBack<SheGuanLiBean>() {
                            @Override
                            public void onSucceed(final SheGuanLiBean sheGuanLiBean, int type) {

                                String IM_MESSAGE_CONTENT = "12," + User.user().getUser_name() + "," + User.user().getUser_ID() + ",0,"
                                        + User.user().getUser_level() + ",,," + User.user().getUser_Charm() + "," + User.user().getUser_Headpic() + "," + seatInfo.mUser.userID + "," + seatInfo.mUser.userName;

                                ZegoChatroom.shared().sendRoomMessage(IM_MESSAGE_CONTENT, 12, new ZegoChatroomSendRoomMessageCallback() {
                                    @Override
                                    public void onSendRoomMessage(ResultCode resultCode, ZegoChatroomMessage message) {

                                        mMsgAdapter.addRoomMsg("系统提示 " + seatInfo.mUser.userName + "被设置为管理员");

                                    }
                                });


                                Toast.makeText(RoomActivity.this, sheGuanLiBean.getData(), Toast.LENGTH_SHORT).show();


                            }

                            @Override
                            public void SucceedError(Exception e, int types) {
                            }

                            @Override
                            public void onError(Throwable throwable, int type) {
                            }
                        }, 1);
                        dialog.dismiss();

                    }
                });
                //关注用户
                iv_guanzuuser.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Map<String, String> map = new HashMap<>();
                        map.put("token", User.user().getToken());
                        map.put("user_id", seatInfo.mUser.userID);
                        map.put("touser__id", User.user().getUser_ID());
                        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/user/follow", map, new NetCallBack<GuanZhuYongHuBean>() {
                            @Override
                            public void onSucceed(final GuanZhuYongHuBean guanZhuYongHuBean, int type) {
                                Toast.makeText(RoomActivity.this, guanZhuYongHuBean.getData(), Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void SucceedError(Exception e, int types) {
                            }

                            @Override
                            public void onError(Throwable throwable, int type) {
                            }
                        }, 1);
                        dialog.dismiss();

                    }
                });
                //去主页
                ll_gerenzhuye.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(RoomActivity.this, UserInfoActivity.class);
                        intent.putExtra("to_userid", roomuser_ID);
                        intent.putExtra("to_usename", RoomUser.getData().get(0).getName());
                        startActivity(intent);
                        dialog.dismiss();

                    }
                });


                //将布局设置给Dialog
                dialog.setContentView(inflate);
                //获取当前Activity所在的窗体
                Window dialogWindow = dialog.getWindow();
                //设置Dialog从窗体底部弹出
                dialogWindow.setGravity(Gravity.CENTER);
                //获得窗体的属性
                WindowManager.LayoutParams lp = dialogWindow.getAttributes();
                lp.y = 100;//设置Dialog距离底部的距离
                // 将属性设置给窗体
                dialogWindow.setAttributes(lp);

                dialog.show();//显示对话框
                // Toast.makeText(RoomActivity.this, "房主麦位用戶和管理不能上", Toast.LENGTH_SHORT).show();
            }

            if (position != 0) {
                onOperationItemClick(position, 0, seatInfo, vi);
                //onOperationItemClick(position, 5, seatInfo);
            }

        }


        if (seatInfo.mStatus == ZegoChatroomSeatStatus.Used) {


            if (ZegoDataCenter.ZEGO_USER.equals(seatInfo.mUser)) {
                //房主权限
                if (User.user().getUser_ID().equals(roomuser_ID)) {
                    iv_xiamai.setVisibility(View.VISIBLE);
                    iv_jinmai.setVisibility(View.VISIBLE);
                    iv_seweiguanli.setVisibility(View.GONE);
                    ll_gerenzhuye.setVisibility(View.VISIBLE);
                }
                //管理权限判断
                if (RMManager == true) {
                    iv_xiamai.setVisibility(View.VISIBLE);
                    iv_jinmai.setVisibility(View.VISIBLE);
                    ll_gerenzhuye.setVisibility(View.VISIBLE);

                } else {
                    //观众权限
                    iv_xiamai.setVisibility(View.VISIBLE);
                    iv_jinmai.setVisibility(View.VISIBLE);
                    ll_gerenzhuye.setVisibility(View.VISIBLE);
                }

            } else {


                //房主权限
                if (User.user().getUser_ID().equals(roomuser_ID)) {

                    iv_jinmai.setVisibility(View.VISIBLE);
                    iv_seweiguanli.setVisibility(View.GONE);
                    iv_baotxiamai.setVisibility(View.VISIBLE);
                    ll_gerenzhuye.setVisibility(View.VISIBLE);
                }
                //管理权限判断
                if (RMManager == true) {
                    iv_xiamai.setVisibility(View.VISIBLE);
                    iv_jinmai.setVisibility(View.VISIBLE);
                    iv_baotxiamai.setVisibility(View.VISIBLE);
                    ll_gerenzhuye.setVisibility(View.VISIBLE);

                } else {
                    //观众权限
                    iv_xiamai.setVisibility(View.GONE);
                    iv_jinmai.setVisibility(View.GONE);
                    ll_gerenzhuye.setVisibility(View.VISIBLE);

                }


            }


            Map<String, String> map = new HashMap<>();
            map.put("userArr", seatInfo.mUser.userID);
            map.put("token", User.user().getToken());
            RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/userslist", map, new NetCallBack<UserRoomList2Bean>() {
                @Override
                public void onSucceed(final UserRoomList2Bean userRoomList2Bean, int type) {

                    if (userRoomList2Bean.getErr().equals("0")) {
                        RoomUser = userRoomList2Bean;
                        Glide.with(RoomActivity.this)
                                .load(AllInterface.addr + "/" + RoomUser.getData().get(0).getHeadpic())
                                .apply(options)
                                .into(ivphotoheaderpersonal);
                        tvseatdialognamepersonal.setText("姓名：" + RoomUser.getData().get(0).getName());
                        tvseatuserIDpersonal.setText("ID：" + RoomUser.getData().get(0).getUser_id());
                        tvseatlevelpersonal.setText("等级：" + RoomUser.getData().get(0).getLevel());
                        tvseatcharmpersonal.setText("魅力值：" + RoomUser.getData().get(0).getCharm());
                        tvseatsignpersonal.setText("签名：" + RoomUser.getData().get(0).getSign());
                        if (RoomUser.getData().get(0).getSex().equals("0")) {
                            tvseatsexpersonal.setText("性别：" + "男");
                        } else {
                            tvseatsexpersonal.setText("性别：" + "女");
                        }
                        tvgifmpersonalpersonal.setText("礼物：" + RoomUser.getData().get(0).getGiftnum());


                    }
                }

                @Override
                public void SucceedError(Exception e, int types) {
                }

                @Override
                public void onError(Throwable throwable, int type) {
                }
            }, 1);


            //举报
            iv_jubao.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(RoomActivity.this, JuBaoActivity.class);
                    intent.putExtra("room_idid", room_ID);
                    startActivity(intent);
                    dialog.dismiss();
                }
            });

            //下麦
            iv_xiamai.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onOperationItemClick(position, 2, seatInfo, view);
                    dialog.dismiss();
                }
            });


            //抱下麦
            iv_baotxiamai.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onOperationItemClick(position, 4, seatInfo, view);
                    dialog.dismiss();
                }
            });


            //禁麦
            iv_jinmai.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    onOperationItemClick(position, 5, seatInfo, view);
                    dialog.dismiss();
                }
            });
//                //送礼物
//                iv_gift.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        onOperationItemClick(position, 8, seatInfo);
//                        dialog.dismiss();
//                    }
//                });
            //设管理员
            iv_seweiguanli.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    Map<String, String> map = new HashMap<>();
                    map.put("user_id", seatInfo.mUser.userID);
                    map.put("token", User.user().getToken());
                    map.put("room_id", room_ID);
                    RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/auth", map, new NetCallBack<SheGuanLiBean>() {
                        @Override
                        public void onSucceed(final SheGuanLiBean sheGuanLiBean, int type) {

                            String IM_MESSAGE_CONTENT = "12," + User.user().getUser_name() + "," + User.user().getUser_ID() + ",0,"
                                    + User.user().getUser_level() + ",,," + User.user().getUser_Charm() + "," + User.user().getUser_Headpic() + "," + seatInfo.mUser.userID + "," + seatInfo.mUser.userName;

                            ZegoChatroom.shared().sendRoomMessage(IM_MESSAGE_CONTENT, 12, new ZegoChatroomSendRoomMessageCallback() {
                                @Override
                                public void onSendRoomMessage(ResultCode resultCode, ZegoChatroomMessage message) {

                                    mMsgAdapter.addRoomMsg("系统提示 " + seatInfo.mUser.userName + "被设置为管理员");

                                }
                            });


                            Toast.makeText(RoomActivity.this, sheGuanLiBean.getData(), Toast.LENGTH_SHORT).show();


                        }

                        @Override
                        public void SucceedError(Exception e, int types) {
                        }

                        @Override
                        public void onError(Throwable throwable, int type) {
                        }
                    }, 1);
                    dialog.dismiss();

                }
            });
            //关注用户
            iv_guanzuuser.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Map<String, String> map = new HashMap<>();
                    map.put("token", User.user().getToken());
                    map.put("user_id", seatInfo.mUser.userID);
                    map.put("touser__id", User.user().getUser_ID());
                    RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/user/follow", map, new NetCallBack<GuanZhuYongHuBean>() {
                        @Override
                        public void onSucceed(final GuanZhuYongHuBean guanZhuYongHuBean, int type) {
                            Toast.makeText(RoomActivity.this, guanZhuYongHuBean.getData(), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void SucceedError(Exception e, int types) {
                        }

                        @Override
                        public void onError(Throwable throwable, int type) {
                        }
                    }, 1);
                    dialog.dismiss();

                }
            });
            //去主页
            ll_gerenzhuye.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(RoomActivity.this, UserInfoActivity.class);
                    intent.putExtra("to_userid", seatInfo.mUser.userID);
                    intent.putExtra("to_usename", seatInfo.mUser.userName);
                    startActivity(intent);
                    dialog.dismiss();

                }
            });


            //将布局设置给Dialog
            dialog.setContentView(inflate);
            //获取当前Activity所在的窗体
            Window dialogWindow = dialog.getWindow();
            //设置Dialog从窗体底部弹出
            dialogWindow.setGravity(Gravity.CENTER);
            //获得窗体的属性
            WindowManager.LayoutParams lp = dialogWindow.getAttributes();
            lp.y = 100;//设置Dialog距离底部的距离
            // 将属性设置给窗体
            dialogWindow.setAttributes(lp);

            dialog.show();//显示对话框
            //下麦
//          onOperationItemClick(position, 2, seatInfo);


        }


        //关闭右上角
        tvcancel11.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        mSeatsAdapter.notifyDataSetChanged();

    }


    @Override
    public void onPickUpUser(ZegoChatroomUser user, int index) {
        ZegoChatroom.shared().pickUp(user, index, new ZegoSeatUpdateCallbackWrapper() {
            @Override
            public void onCompletion(ResultCode resultCode) {
                super.onCompletion(resultCode);
                if (!resultCode.isSuccess()) {
                    Toast.makeText(RoomActivity.this, "操作失败！", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setLiveStatusForUser(ZegoChatroomUser user, int liveStatus) {
        ChatroomSeatInfo seat = getSeatForUser(user);
        if (seat != null) {
            seat.mLiveStatus = liveStatus;
            mSeatsAdapter.notifyDataSetChanged();
        }
    }

    private void setSeatDelayForUser(ZegoChatroomUser user, int delay) {
        ChatroomSeatInfo seat = getSeatForUser(user);
        if (seat != null) {
            seat.mDelay = delay;
            mSeatsAdapter.notifyDataSetChanged();
        }
    }

    private void setSeatSoundLevelForUser(ZegoChatroomUser user, float soundLevel) {
        ChatroomSeatInfo seat = getSeatForUser(user);
        if (seat != null) {
            seat.mSoundLevel = soundLevel;
            mSeatsAdapter.notifyDataSetChanged();
        }
    }


    String liwulieuserid = "";

    Boolean isOne = false;

    @Override//麦位更新回调
    public void onSeatsUpdate(List<ZegoChatroomSeat> zegoChatroomSeats) {
        Log.e("TAGonSeatsUpdate", "onSeatsUpdate: " + zegoChatroomSeats);


        if (zegoChatroomSeats.size() != mSeats.size()) {
            Log.w("TAG", "onSeatsUpdate zegoChatroomSeats.size() != mSeats.size() ");
            return;
        }

        ChatroomSeatInfo seatInfo = new ChatroomSeatInfo();
        for (int i = 0; i < zegoChatroomSeats.size(); i++) {
            seatInfo = mSeats.get(i);
            ZegoChatroomSeat zegoChatroomSeat = zegoChatroomSeats.get(i);
            if (zegoChatroomSeat.mStatus == ZegoChatroomSeatStatus.Used) {
                if (zegoChatroomSeat.mZegoUser.equals(seatInfo.mUser)) {
                    seatInfo.isMute = zegoChatroomSeat.isMute;
                    continue;
                }
            }

            if (zegoChatroomSeat.mStatus == 1) {
                liwulieuserid = zegoChatroomSeat.mZegoUser.userID;
            }


            seatInfo.mStatus = zegoChatroomSeat.mStatus;
            seatInfo.mUser = zegoChatroomSeat.mZegoUser;
            seatInfo.isMute = zegoChatroomSeat.isMute;
            seatInfo.mSoundLevel = 0;
            seatInfo.mDelay = 0;
            seatInfo.mLiveStatus = ZegoChatroomUserLiveStatus.WAIT_CONNECT;
        }

        //房主上麦
        if (User.user().getUser_ID().equals(roomuser_ID)) {

            onOperationItemClick(0, 0, seatInfo, vi);
        }


        if (!isOne) {
            isOne = true;
            for (int i = 0; i < zegoChatroomSeats.size(); i++) {
                ZegoChatroomSeat zegoChatroomSeat = zegoChatroomSeats.get(i);
                if (zegoChatroomSeat.mStatus == 1) {
                    if (!model.get(0).equals(zegoChatroomSeat.mZegoUser.userID)) {
                        model.add(zegoChatroomSeat.mZegoUser.userID);

                    }

                }
            }
        }


        mSeatsAdapter.notifyDataSetChanged();

        mSeatsAdapter.notifyItemChanged(0);
        notifyPickUpUserDataSet();
    }


    private void liwulist() {
        //礼物人数
        Map<String, String> map = new HashMap<>();
        map.put("token", com.xunshan.ZhenSheng.user.User.user().getToken());
        map.put("userArr", listToString1(model));
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/userslist", map, new NetCallBack<UserRoomListBean>() {
            @Override
            public void onSucceed(final UserRoomListBean userRoomListBean, int type) {
                UserRoomListBean.UserRoomListBean().setData(userRoomListBean.getData());
                showGiftView();
            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int type) {
            }
        }, 1);


    }


    @Override
    public void onLoginEventOccur(int event, int status, ResultCode errorCode) {


        String IM_MESSAGE_CONTENT = "1," + User.user().getUser_name() + "," + ","
                + User.user().getUser_Headpic() + ",";
        //判断特权用户
        ZegoChatroom.shared().sendRoomMessage(IM_MESSAGE_CONTENT, 1, new ZegoChatroomSendRoomMessageCallback() {
            @Override
            public void onSendRoomMessage(ResultCode resultCode, ZegoChatroomMessage message) {

                //mMsgAdapter.addRoomMsg(User.user().getUser_name() + " 进入房间");

            }
        });


    }

    @Override
    public void onAutoReconnectStop(int stopReason) {
        Log.d("TAG", "onAutoReconnectStop stopReason: " + stopReason);

        mMsgAdapter.addRoomMsg("系统:自动重新连接停止  stopReason: " + ZegoChatroomReconnectStopReason.getReconnectStopReasonString(stopReason));
        // mFlLoading.setVisibility(View.GONE);
    }

    @Override
    public void onLiveStatusUpdate(ZegoChatroomUser user, int liveStatus) {
        Log.d("TAG", "onLiveStatusUpdate 用户: " + user + " liveStatus: " + liveStatus);
        setLiveStatusForUser(user, liveStatus);
    }

    @Override
    public void onLiveQualityUpdate(ZegoChatroomUser user, ZegoUserLiveQuality quality) {
        Log.v("TAG", "onLiveQualityUpdate 用户: " + user + " quality: " + quality);
        setSeatDelayForUser(user, quality.mAudioDelay);
    }

    @Override
    public void onSoundLevelUpdate(ZegoChatroomUser user, float soundLevel) {
        Log.v("TAG", "onSoundLevelUpdate 用户: " + user + " soundLevel: " + soundLevel);
        setSeatSoundLevelForUser(user, soundLevel);
    }

    @Override
    public void onLiveExtraInfoUpdate(ZegoChatroomUser user, String extraInfo) {
        Log.d("TAG", "onLiveExtraInfoUpdate 用户: " + user + " extraInfo: " + extraInfo);
        mMsgAdapter.addRoomMsg("extraInfoUpdate 用户: " + user.userName + " extraInfo: " + extraInfo);
    }

    @Override
    public void onUserTakeSeat(ZegoChatroomUser user, int index) {
        if (model != null) {
            for (int i = 0; i < model.size(); i++) {
                if (model.get(i).equals(user.userID)) {
                    break;
                } else {
                    model.add(user.userID);
                }
            }

        }


        Log.d("TAG", "onUserTakeSeat user: " + user + " index: " + index);

        mMsgAdapter.addRoomMsg("用户: " + user.userName + "，上麦，位置:" + index);
    }

    @Override
    public void onUserLeaveSeat(ZegoChatroomUser user, int fromIndex) {
        if (model != null) {
            for (int i = 0; i < model.size(); i++) {
                if (model.get(i).equals(user.userID)) {
                    model.remove(user.userID);
                    break;
                }
            }
        }


        Log.d("TAG", "onUserLeaveSeat user: " + user + " fromIndex: " + fromIndex);

        mMsgAdapter.addRoomMsg("用户: " + user.userName + "，下麦，位置:" + fromIndex);
    }

    @Override
    public void onUserChangeSeat(ZegoChatroomUser user, int fromIndex, int toIndex) {
        Log.d("TAG", "onUserChangeSeat user: " + user + " fromIndex: " + fromIndex + " toIndex: " + toIndex);

        mMsgAdapter.addRoomMsg("用户: " + user.userName + "，换麦，从:" + fromIndex + "->" + toIndex);
    }

    @Override
    public void onUserPickUp(ZegoChatroomUser fromUser, ZegoChatroomUser toUser, int index) {
        Log.d("TAG", "onUserPickUp fromUser: " + fromUser + " toUser: " + toUser + " index: " + index);


        String fromUserName = fromUser == null ? null : fromUser.userName;
        mMsgAdapter.addRoomMsg("用户: " + fromUserName + "，将user: " + toUser.userName + "，抱上麦，位置:" + index);

        if (ZegoDataCenter.ZEGO_USER.equals(toUser)) {
            showPickedUpTipDialog(index);
        }
    }

    @Override
    public void onUserKickOut(ZegoChatroomUser fromUser, ZegoChatroomUser toUser, int fromIndex) {
        if (ZegoDataCenter.ZEGO_USER.equals(toUser) && isOwner()) {
            ZegoChatroom.shared().getMusicPlayer().stop();
        }
        String fromUserName = fromUser == null ? null : fromUser.userName;
        mMsgAdapter.addRoomMsg("用户: " + fromUserName + "，将: " + toUser.userName + "，抱下麦");
    }

    @Override
    public void onSeatMute(ZegoChatroomUser fromUser, boolean isMute, int index) {
        Log.d("TAG", "onSeatMute fromUser: " + fromUser + " isMute: " + isMute + " index: " + index);

        String muteOperation = isMute ? "禁" : "解禁";
        mMsgAdapter.addRoomMsg("用户: " + fromUser.userName + "，" + muteOperation + "麦位，位置:" + index);
    }

    @Override
    public void onSeatClose(ZegoChatroomUser fromUser, boolean isClose, int index) {
        Log.d("TAG", "onSeatClose fromUser: " + fromUser + " isMute: " + isClose + " index: " + index);

        String muteOperation = isClose ? "封" : "解封";
        mMsgAdapter.addRoomMsg("用户: " + fromUser.userName + "，" + muteOperation + "麦位，位置:" + index);
    }

    @Override
    public void onAVEngineStop() {

    }

    @Override
    public void onRecvCustomCommand(String s, ZegoChatroomUser zegoChatroomUser) {

    }

    // --------------- implements SeatOperationDialog.OnOperationItemClickListener --------------- //
    @Override
    public void onOperationItemClick(int position, int operationType, ChatroomSeatInfo seat, View view) {

        switch (operationType) {
            case SeatOperationDialog.OPERATION_TYPE_TAKE_SEAT:
                ZegoChatroom.shared().takeSeatAtIndex(position, new ZegoSeatUpdateCallbackWrapper() {
                    @Override
                    public void onCompletion(ResultCode resultCode) {
                        super.onCompletion(resultCode);

                        if (!resultCode.isSuccess()) {
                            Toast.makeText(RoomActivity.this, "你已经上麦！", Toast.LENGTH_SHORT).show();

                        } else {

                            Map<String, String> map = new HashMap<>();
                            map.put("user_id", User.user().getUser_ID());
                            map.put("token", User.user().getToken());
                            map.put("room_id", room_ID);
                            RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/information", map, new NetCallBack<PersonalDataBean>() {
                                @Override
                                public void onSucceed(final PersonalDataBean personalDataBean, int type) {
                                    tv_play_method.setText("房间玩法" + personalDataBean.getRoom().getRoomtake());
                                    PersonalUser = personalDataBean.getUser();
//
                                    com.xunshan.ZhenSheng.user.User.user().setUser_name(PersonalUser.getName());
                                    com.xunshan.ZhenSheng.user.User.user().setUser_Charm(PersonalUser.getCharm());
                                    com.xunshan.ZhenSheng.user.User.user().setUser_Headpic(PersonalUser.getHeadpic());
                                    com.xunshan.ZhenSheng.user.User.user().setUser_balance(PersonalUser.getBalance());

                                }

                                @Override
                                public void SucceedError(Exception e, int types) {
                                }

                                @Override
                                public void onError(Throwable throwable, int type) {
                                }
                            }, 1);

                        }
                    }
                });
                break;
            case SeatOperationDialog.OPERATION_TYPE_CHANGE_SEAT:
                ZegoChatroom.shared().changeSeatTo(position, new ZegoSeatUpdateCallbackWrapper() {
                    @Override
                    public void onCompletion(ResultCode resultCode) {
                        super.onCompletion(resultCode);
                        if (!resultCode.isSuccess()) {
                            Toast.makeText(RoomActivity.this, "操作失败！", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                break;
            case SeatOperationDialog.OPERATION_TYPE_LEAVE_SEAT:
                ZegoChatroom.shared().leaveSeat(new ZegoSeatUpdateCallbackWrapper() {
                    @Override
                    public void onCompletion(ResultCode resultCode) {
                        super.onCompletion(resultCode);
                        if (!resultCode.isSuccess()) {
                            Toast.makeText(RoomActivity.this, "操作失败！", Toast.LENGTH_SHORT).show();
                        } else {
                            ZegoChatroom.shared().getMusicPlayer().stop();
                        }
                    }
                });
                break;
            case SeatOperationDialog.OPERATION_TYPE_PICK_UP:
                showPickUpUserSelectDialog(position);
                break;
            case SeatOperationDialog.OPERATION_TYPE_KIT_OUT:
                String IM_MESSAGE_CONTENT = seat.mUser.userID;
                ZegoChatroom.shared().kickOut(seat.mUser, new ZegoSeatUpdateCallbackWrapper() {
                    @Override
                    public void onCompletion(ResultCode resultCode) {
                        super.onCompletion(resultCode);


                        if (!resultCode.isSuccess()) {
                            Toast.makeText(RoomActivity.this, "操作失败！", Toast.LENGTH_SHORT).show();
                        } else {


                            //判断特权用户
                            ZegoChatroom.shared().sendRoomMessage(IM_MESSAGE_CONTENT, 9, new ZegoChatroomSendRoomMessageCallback() {
                                @Override
                                public void onSendRoomMessage(ResultCode resultCode, ZegoChatroomMessage message) {


                                }
                            });

                        }
                    }
                });
                break;
            case SeatOperationDialog.OPERATION_TYPE_MUTE_SEAT:
                if (mick == true) {
                    mick = false;
                } else if (mick == false) {
                    mick = true;
                }
                ImageView ivyinmairoomuserxin = view.findViewById(R.id.iv_yinmai_roomuser_xin);
                ImageView ivyinmairoomuserxin2 = view.findViewById(R.id.iv_yinmai_roomuser_xin2);


                if (seat.isMute) {
                    ivyinmairoomuserxin2.setVisibility(View.VISIBLE);
                    ivyinmairoomuserxin.setVisibility(View.GONE);
                } else {
                    ivyinmairoomuserxin2.setVisibility(View.GONE);
                    ivyinmairoomuserxin.setVisibility(View.VISIBLE);
                }


                ZegoChatroom.shared().muteSeat(!seat.isMute, position, new ZegoSeatUpdateCallbackWrapper() {

                    @Override
                    public void onCompletion(ResultCode resultCode) {
                        super.onCompletion(resultCode);

                        if (!resultCode.isSuccess()) {
                            Toast.makeText(RoomActivity.this, "操作失败！", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                break;
            case SeatOperationDialog.OPERATION_TYPE_CLOSE_SEAT:
                boolean isClosed = seat.mStatus == ZegoChatroomSeatStatus.Closed;
                ZegoChatroom.shared().closeSeat(!isClosed, position, new ZegoSeatUpdateCallbackWrapper() {
                    @Override
                    public void onCompletion(ResultCode resultCode) {
                        super.onCompletion(resultCode);
                        if (!resultCode.isSuccess()) {
                            Toast.makeText(RoomActivity.this, "操作失败！", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                break;
            case SeatOperationDialog.OPERATION_TYPE_SEND_GIFT:
                showGiftView();

                break;
            default:
                break;

        }
    }


//    @Override
//    public boolean onClose() {
//        return false;
//    }


    // ---------------- 工具类 ---------------- //
    abstract class ZegoSeatUpdateCallbackWrapper implements ZegoSeatUpdateCallback {
        private ZegoSeatUpdateCallbackWrapper() {
            // mFlLoading.setVisibility(View.VISIBLE);
        }

        @Override
        public void onCompletion(ResultCode resultCode) {
            //mFlLoading.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        // 当暂停时，强制隐藏输入框和输入法
        mFlInput.setVisibility(View.GONE);
        ((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
                .hideSoftInputFromWindow(mEtComment.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    /**
     * 初始化输入框显示layout显示、隐藏逻辑
     * 主要是监控输入法的显示，然后将输入框显示或者隐藏
     */
    private void initInputLayout() {
        final ViewGroup rootLayout = findViewById(R.id.root_layout);
        rootLayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                rootLayout.getWindowVisibleDisplayFrame(r);
                // 存储r.bottom
                screenHeight = Math.max(screenHeight, r.bottom);
                //r.top 是状态栏高度
                final int softHeight = screenHeight - r.bottom;
                if (softHeight > 100) {//当输入法高度大于100判定为输入法打开了
                    mFlInput.postDelayed(new Runnable() {
                                             @Override
                                             public void run() {
                                                 mFlInput.scrollTo(0, softHeight);
                                             }
                                         },
                            100);
                    mFlInput.setVisibility(View.VISIBLE);
                    mEtComment.requestFocus();
                } else {//否则判断为输入法隐藏了
                    mFlInput.scrollTo(0, 0);
                    mFlInput.setVisibility(View.GONE);
                }
            }
        });
    }

    private String getEditTextContent() {
        Editable msg = mEtComment.getText();
        if (msg != null && !TextUtils.isEmpty(msg.toString())) {
            String s = msg.toString();
            mEtComment.setText("");
            return s;
        }
        return null;
    }

    /**
     * @return 是否发送消息成功，只要输入框有内容即输入成功
     */
    public boolean send() {
        final String msg = getEditTextContent();

        if (msg == null) {
            Toast.makeText(this, "请输入需要发送的内容", Toast.LENGTH_LONG).show();
            return false;
        } else {
//        3 type类型
//        进房人的名字
//        进房人的ID
//        发送的消息 进房人的性别 进房人的等级
//        进房人的签名  进房人在本房间收到的礼物 进房人的魅力值
//        进房人的头像
            String mmm = "3," + user_name + "," + user_ID + "," + URLEncoder.encode(msg) + ",0,"
                    + User.user().getUser_level() + ",,0," + User.user().getUser_Charm() + "," + User.user().getUser_Headpic();
            ZegoChatroom.shared().sendRoomMessage(mmm, 3, new ZegoChatroomSendRoomMessageCallback() {
                @Override
                public void onSendRoomMessage(ResultCode resultCode, ZegoChatroomMessage message) {

                    if (resultCode.isSuccess()) {
                        mMsgAdapter.addRoomMsg(String.format(Locale.CHINA, USER_MESSAGE_FORMAT, ZegoDataCenter.ZEGO_USER.userName, msg));
                    } else {
                        mMsgAdapter.addRoomMsg("系统:消息发送失败");
                    }
                }
            });

            return true;
        }
    }


    private LinearLayout liwu_liuliuliu;//1
    private LinearLayout liwu_jindan;//2
    private LinearLayout liwu_lihe;//3
    private LinearLayout liwu_dianzan;//4
    private LinearLayout liwu_bixin;//5
    private LinearLayout liwu_xiangbin;//6
    private LinearLayout liwu_taohuayu;//7
    private LinearLayout liwu_kouhong;//8
    private LinearLayout liwu_zhaocaimao;//9
    private LinearLayout liwu_menghuanhudie;//10
    private LinearLayout liwu_lanseyaoji;//11
    private LinearLayout liwu_zijingbaobao;//12
    private LinearLayout liwu_yongbaorewen;//13
    private LinearLayout liwu_falali;//14
    private LinearLayout liwu_nvshendengchan;//15
    private LinearLayout liwu_feiji;//16
    private LinearLayout liwu_huojian;//17
    private LinearLayout liwu_zhuguangwancan;//18
    private LinearLayout liwu_menghuanchengbao;//19
    private LinearLayout liwu_gongzhudechengbao;//20
    private LinearLayout liwu_huangsepaoche;
    private LinearLayout liwu_hongsepaoche;
    private LinearLayout liwu_meiguihuachuan;
    private LinearLayout liwu_zaiyiqi;

    private ScrollView liwu_songliliqu;
    private RecyclerView liwu_myback;
    private ScrollView ll_liwucard;
    private TextView tv_qiansongliqu;

    private TextView tv_giftview_num;//钻石
    private TextView tv_jiaqian;//充值
    private TextView myliwubackpack;

    private int liwu = 0;
    private int qian = 0;
    private int zhifutype = 0;
    private String liwuname = "";
    private String liwupname = "";
    private String liwumoney = "";
    List<String> songliid = new ArrayList();
    List<String> songliname = new ArrayList();

    private int type = 0;
    private int cardnumber;

    private List<MybackcardBean.DataBean> mybackcard;

    //送礼物
    private void showGiftView() {
        hideBottomUIMenu();
        getliwukaback();
        view = LayoutInflater.from(RoomActivity.this).inflate(R.layout.pop_giftview, null);
        popupWindow = new PopupWindow(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
        popupWindow.showAsDropDown(rvSeats);
        tv_pop_sendgift = (TextView) view.findViewById(R.id.tv_pop_sendgift);
        rv_gifttouser = (RecyclerView) view.findViewById(R.id.rv_gifttouser);
        linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        rv_gifttouser.setLayoutManager(linearLayoutManager);
        giftToUserAdapter = new GiftToUserAdapter(list, this);
        rv_gifttouser.setAdapter(giftToUserAdapter);
        tv_giftview_num = view.findViewById(R.id.tv_giftview_num);
        tv_jiaqian = view.findViewById(R.id.tv_jiaqian);
        tv_jiaqian = view.findViewById(R.id.tv_jiaqian);
        liwu_liuliuliu = view.findViewById(R.id.liwu_liuliuliu);
        liwu_jindan = view.findViewById(R.id.liwu_jindan);
        liwu_lihe = view.findViewById(R.id.liwu_lihe);
        liwu_dianzan = view.findViewById(R.id.liwu_dianzan);
        liwu_bixin = view.findViewById(R.id.liwu_bixin);
        liwu_xiangbin = view.findViewById(R.id.liwu_xiangbin);
        liwu_taohuayu = view.findViewById(R.id.liwu_taohuayu);
        liwu_kouhong = view.findViewById(R.id.liwu_kouhong);
        liwu_zhaocaimao = view.findViewById(R.id.liwu_zhaocaimao);
        liwu_menghuanhudie = view.findViewById(R.id.liwu_menghuanhudie);
        liwu_lanseyaoji = view.findViewById(R.id.liwu_lanseyaoji);
        liwu_zijingbaobao = view.findViewById(R.id.liwu_zijingbaobao);
        liwu_yongbaorewen = view.findViewById(R.id.liwu_yongbaorewen);
        liwu_huangsepaoche = view.findViewById(R.id.liwu_huangsepaoche);
        liwu_hongsepaoche = view.findViewById(R.id.liwu_hongsepaoche);
        liwu_meiguihuachuan = view.findViewById(R.id.liwu_meiguihuachuan);
        liwu_falali = view.findViewById(R.id.liwu_falali);
        liwu_nvshendengchan = view.findViewById(R.id.liwu_nvshendengchan);
        liwu_feiji = view.findViewById(R.id.liwu_feiji);
        liwu_huojian = view.findViewById(R.id.liwu_huojian);
        liwu_zhuguangwancan = view.findViewById(R.id.liwu_zhuguangwancan);
        liwu_menghuanchengbao = view.findViewById(R.id.liwu_menghuanchengbao);
        liwu_gongzhudechengbao = view.findViewById(R.id.liwu_gongzhudechengbao);
        liwu_zaiyiqi = view.findViewById(R.id.liwu_zaiyiqi);
        tv_qiansongliqu = view.findViewById(R.id.tv_qiansongliqu);
        liwu_songliliqu = view.findViewById(R.id.songliliwu);
        liwu_myback = view.findViewById(R.id.liwu_myback);
        ll_liwucard = view.findViewById(R.id.ll_liwucard);
        myliwubackpack = view.findViewById(R.id.my_liwubackpack);
        tv_qiansongliqu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_liwucard.setVisibility(View.GONE);
                liwu_songliliqu.setVisibility(View.VISIBLE);
            }
        });

        myliwubackpack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_liwucard.setVisibility(View.VISIBLE);
                liwu_songliliqu.setVisibility(View.GONE);
                mybackpackadapter = new MybackPackadapter(RoomActivity.this, mybackcard);
                liwu_myback.setLayoutManager(new GridLayoutManager(RoomActivity.this, 3));
                liwu_myback.setAdapter(mybackpackadapter);
                mybackpackadapter.setItemClickListener(new MybackPackadapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position, String liwuid, String number, String pname, String liname) {
                        if (selectMode) {
                            // 如果当前处于多选状态，则进入多选状态的逻辑
                            // 维护当前已选的position
                            addOrRemove(position, view);
                        } else {
                            // 如果不是多选状态，则进入单选事件的业务逻辑
                            if (!positionSet.contains(position)) {
                                // 选择不同的单位时取消之前选中的单位
                                positionSet.clear();
                            }
                            cardnumber = Integer.parseInt(number);
                            type = 1;
                            addOrRemove(position, view);
                            liwu = Integer.parseInt(mybackcard.get(position).getGifid());
                            liwupname = pname;
                            liwuname = liname;
                        }
                    }
                });
            }
        });
        tv_giftview_num.setText(User.user().getUser_balance());
        tv_jiaqian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
                View view = LayoutInflater.from(RoomActivity.this).inflate(R.layout.pop_topup, null);
                PopupWindow topaytWindow = new PopupWindow(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
                topaytWindow.setOutsideTouchable(true);
                topaytWindow.showAtLocation(view, Gravity.BOTTOM, 0, 0);
                topaytWindow.showAsDropDown(rvSeats);
                LinearLayout LL_pay_six = view.findViewById(R.id.LL_pay_six);
                LinearLayout LL_pay_oneeight = view.findViewById(R.id.LL_pay_oneeight);
                LinearLayout LL_pay_three = view.findViewById(R.id.LL_pay_three);
                LinearLayout LL_pay_Eight = view.findViewById(R.id.LL_pay_Eight);
                LinearLayout LL_pay_Threeeight = view.findViewById(R.id.LL_pay_Threeeight);
                RadioGroup topay = view.findViewById(R.id.topay);
                RadioButton topay_weixin = view.findViewById(R.id.topay_weixin);
                RadioButton topay_zhifubao = view.findViewById(R.id.topay_zhifubao);
                TextView tv_giftview_num = view.findViewById(R.id.tv_giftview_num);
                TextView tv_jiaqian = view.findViewById(R.id.tv_jiaqian);
                tv_giftview_num.setText(User.user().getUser_balance());
                LL_pay_six.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        qian = 6;
                        LL_pay_six.setBackgroundResource(R.drawable.shape_pay_ra8);
                        LL_pay_oneeight.setBackgroundResource(R.drawable.shape_nopay_ra8);
                        LL_pay_three.setBackgroundResource(R.drawable.shape_nopay_ra8);
                        LL_pay_Eight.setBackgroundResource(R.drawable.shape_nopay_ra8);
                        LL_pay_Threeeight.setBackgroundResource(R.drawable.shape_nopay_ra8);
                    }
                });
                LL_pay_oneeight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        qian = 18;
                        LL_pay_six.setBackgroundResource(R.drawable.shape_nopay_ra8);
                        LL_pay_oneeight.setBackgroundResource(R.drawable.shape_pay_ra8);
                        LL_pay_three.setBackgroundResource(R.drawable.shape_nopay_ra8);
                        LL_pay_Eight.setBackgroundResource(R.drawable.shape_nopay_ra8);
                        LL_pay_Threeeight.setBackgroundResource(R.drawable.shape_nopay_ra8);
                    }
                });
                LL_pay_three.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        qian = 30;
                        LL_pay_six.setBackgroundResource(R.drawable.shape_nopay_ra8);
                        LL_pay_oneeight.setBackgroundResource(R.drawable.shape_nopay_ra8);
                        LL_pay_three.setBackgroundResource(R.drawable.shape_pay_ra8);
                        LL_pay_Eight.setBackgroundResource(R.drawable.shape_nopay_ra8);
                        LL_pay_Threeeight.setBackgroundResource(R.drawable.shape_nopay_ra8);
                    }
                });
                LL_pay_Eight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        qian = 88;
                        LL_pay_six.setBackgroundResource(R.drawable.shape_nopay_ra8);
                        LL_pay_oneeight.setBackgroundResource(R.drawable.shape_nopay_ra8);
                        LL_pay_three.setBackgroundResource(R.drawable.shape_nopay_ra8);
                        LL_pay_Eight.setBackgroundResource(R.drawable.shape_pay_ra8);
                        LL_pay_Threeeight.setBackgroundResource(R.drawable.shape_nopay_ra8);
                    }
                });
                LL_pay_Threeeight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        qian = 388;
                        LL_pay_six.setBackgroundResource(R.drawable.shape_nopay_ra8);
                        LL_pay_oneeight.setBackgroundResource(R.drawable.shape_nopay_ra8);
                        LL_pay_three.setBackgroundResource(R.drawable.shape_nopay_ra8);
                        LL_pay_Eight.setBackgroundResource(R.drawable.shape_nopay_ra8);
                        LL_pay_Threeeight.setBackgroundResource(R.drawable.shape_pay_ra8);
                    }
                });
                topay.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        if (topay_weixin.getId() == checkedId) {
                            zhifutype = 2;
                        }
                        if (topay_zhifubao.getId() == checkedId) {
                            zhifutype = 3;
                        }
                    }
                });

                tv_jiaqian.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (qian == 0) {
                            Toast.makeText(RoomActivity.this, "请选择金额", Toast.LENGTH_LONG).show();
                        } else {
                            if (zhifutype == 0) {
                                Toast.makeText(RoomActivity.this, "请选择支付方式", Toast.LENGTH_LONG).show();
                            } else if (zhifutype == 3) {
                                Map<String, String> map = new HashMap<>();
                                map.put("user_id", User.user().getUser_ID());
                                map.put("token", User.user().getToken());
                                map.put("money", md5("a8r7wyr!$#@#^$&^*)(*&^5qW_QW_EWQE_W@!##!$&^$#^#" + qian));
                                map.put("type", String.valueOf(zhifutype));
                                RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/identity/payment", map, new NetCallBack<topaybean>() {

                                    @Override
                                    public void onSucceed(topaybean topaybean, int types) {
                                        PayUtils.getInstance(RoomActivity.this);
                                        PayUtils.pay(Integer.parseInt(topaybean.getType()), topaybean.getData().getZhifubao());
                                    }

                                    @Override
                                    public void SucceedError(Exception e, int types) {
                                    }

                                    @Override
                                    public void onError(Throwable throwable, int type) {
                                    }
                                }, 1);
                            } else if (zhifutype == 2) {
                                Map<String, String> map = new HashMap<>();
                                map.put("user_id", User.user().getUser_ID());
                                map.put("token", User.user().getToken());
                                map.put("money", md5("a8r7wyr!$#@#^$&^*)(*&^5qW_QW_EWQE_W@!##!$&^$#^#" + qian));
                                map.put("type", String.valueOf(zhifutype));
                                RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/weixin/weixin", map, new NetCallBack<topaybean>() {
                                    @Override
                                    public void onSucceed(topaybean topaybean, int types) {
                                        PayUtils.getInstance(RoomActivity.this);
                                        Gson j = new Gson();
                                        PayUtils.pay(Integer.parseInt(topaybean.getType()), j.toJson(topaybean));
                                    }

                                    @Override
                                    public void SucceedError(Exception e, int types) {
                                    }

                                    @Override
                                    public void onError(Throwable throwable, int type) {
                                        Toast.makeText(RoomActivity.this, "请求失败，请稍后重试", Toast.LENGTH_SHORT).show();
                                    }
                                }, 1);
                            }
                        }
                    }
                });
            }
        });

        liwu_liuliuliu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 1;
                liwuname = "666";
                liwupname = "liuliuliu";
                liwumoney = "10";
                liwu_liuliuliu.setBackgroundResource(R.drawable.whitecircle4);
                liwu_jindan.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_jindan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 2;
                liwuname = "金蛋";
                liwupname = "jindan";
                liwumoney = "18";
                liwu_jindan.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_lihe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 3;
                liwuname = "礼盒";
                liwupname = "lihe";
                liwumoney = "20";
                liwu_lihe.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_jindan.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_dianzan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 4;
                liwuname = "点赞";
                liwupname = "dianzan";
                liwumoney = "33";
                liwu_dianzan.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_jindan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_bixin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 5;
                liwuname = "比心";
                liwupname = "bixin";
                liwumoney = "52";
                liwu_bixin.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_jindan.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_xiangbin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 6;
                liwuname = "香槟";
                liwupname = "xiangbin";
                liwumoney = "88";
                liwu_xiangbin.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_jindan.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_taohuayu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 7;
                liwuname = "桃花雨";
                liwupname = "taohuayu";
                liwumoney = "99";
                liwu_taohuayu.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_jindan.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_kouhong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 8;
                liwuname = "口红";
                liwupname = "kouhong";
                liwumoney = "170";
                liwu_kouhong.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_jindan.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_zhaocaimao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 9;
                liwuname = "招财猫";
                liwupname = "zhaocaimao";
                liwumoney = "188";
                liwu_zhaocaimao.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_jindan.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_menghuanhudie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 10;
                liwuname = "梦幻蝴蝶";
                liwupname = "menghuanhudie";
                liwumoney = "199";
                liwu_menghuanhudie.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_jindan.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_lanseyaoji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 11;
                liwuname = "蓝色妖姬";
                liwupname = "lanseyaoji";
                liwumoney = "214";
                liwu_lanseyaoji.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_jindan.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_zijingbaobao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 12;
                liwuname = "紫晶包包";
                liwupname = "zijingbaobao";
                liwumoney = "330";
                liwu_zijingbaobao.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_jindan.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_yongbaorewen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 13;
                liwuname = "拥抱热吻";
                liwupname = "yongbaorewen";
                liwumoney = "520";
                liwu_yongbaorewen.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_jindan.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_huangsepaoche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 14;
                liwuname = "黄色跑车";
                liwupname = "huangsepaoche";
                liwumoney = "880";
                liwu_huangsepaoche.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_jindan.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_hongsepaoche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 15;
                liwuname = "红色跑车";
                liwupname = "hongsepaoche";
                liwumoney = "888";
                liwu_hongsepaoche.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_jindan.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_meiguihuachuan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 16;
                liwuname = "玫瑰花船";
                liwupname = "meiguihuachuan";
                liwumoney = "999";
                liwu_meiguihuachuan.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_jindan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_zaiyiqi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 17;
                liwuname = "在一起";
                liwupname = "zaiyiqi";
                liwumoney = "1314";
                liwu_zaiyiqi.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_jindan.setBackground(null);
            }
        });
        liwu_falali.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 18;
                liwuname = "法拉利";
                liwupname = "falali";
                liwumoney = "1888";
                liwu_falali.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_jindan.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_nvshendengchan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 19;
                liwuname = "女神登场";
                liwupname = "nvshendengchang";
                liwumoney = "2888";
                liwu_nvshendengchan.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_jindan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_feiji.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 20;
                liwuname = "飞机";
                liwupname = "feiji";
                liwumoney = "3300";
                liwu_feiji.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_jindan.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_huojian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 21;
                liwuname = "火箭";
                liwupname = "huojian";
                liwumoney = "5000";
                liwu_huojian.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_jindan.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_zhuguangwancan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 22;
                liwuname = "烛光晚餐";
                liwupname = "zhuguangwancan";
                liwumoney = "5200";
                liwu_zhuguangwancan.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_jindan.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_menghuanchengbao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 23;
                liwuname = "梦幻城堡";
                liwupname = "menghuanchengbao";
                liwumoney = "8888";
                liwu_menghuanchengbao.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_jindan.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_gongzhudechengbao.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });
        liwu_gongzhudechengbao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                liwu = 24;
                liwuname = "公主的城堡";
                liwupname = "gongzhudechengbao";
                liwumoney = "9999";
                liwu_gongzhudechengbao.setBackgroundResource(R.drawable.whitecircle4);
                liwu_liuliuliu.setBackground(null);
                liwu_lihe.setBackground(null);
                liwu_dianzan.setBackground(null);
                liwu_bixin.setBackground(null);
                liwu_xiangbin.setBackground(null);
                liwu_taohuayu.setBackground(null);
                liwu_kouhong.setBackground(null);
                liwu_zhaocaimao.setBackground(null);
                liwu_menghuanhudie.setBackground(null);
                liwu_lanseyaoji.setBackground(null);
                liwu_zijingbaobao.setBackground(null);
                liwu_yongbaorewen.setBackground(null);
                liwu_falali.setBackground(null);
                liwu_nvshendengchan.setBackground(null);
                liwu_feiji.setBackground(null);
                liwu_jindan.setBackground(null);
                liwu_zhuguangwancan.setBackground(null);
                liwu_menghuanchengbao.setBackground(null);
                liwu_huojian.setBackground(null);
                liwu_huangsepaoche.setBackground(null);
                liwu_hongsepaoche.setBackground(null);
                liwu_meiguihuachuan.setBackground(null);
                liwu_zaiyiqi.setBackground(null);
            }
        });

        //礼物人

        list.clear();
        list.addAll(UserRoomListBean.UserRoomListBean().getData());
        giftToUserAdapter.notifyDataSetChanged();
        giftToUserAdapter.setItemClickListener(new GiftToUserAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String did, String name, boolean ischeck, int position) {
                if (ischeck) {
                    songliid.add(did);
                    songliname.add(name);
                } else {
                    songliid.remove(did);
                    songliname.remove(name);
                }
            }
        });

        tv_pop_sendgift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (type == 0) {

                    if (liwu > 0) {
                        int qianqian = songliid.size() * Integer.parseInt(liwumoney);
                        if (Integer.parseInt(User.user().getUser_balance()) < qianqian) {
                            Toast.makeText(RoomActivity.this, "余额不足，请先充值", Toast.LENGTH_SHORT).show();
                        } else {
                            if (!listToString1(songliid).equals("")) {

                                popupWindow.dismiss();
                                Map<String, String> map = new HashMap<>();
                                map.put("user_id", user_ID);
                                map.put("token", token);
                                map.put("room_id", room_ID);
                                map.put("touser_id", listToString1(songliid));
                                map.put("gift_id", md5("a8r7wyr!$#@#^$&^*)(*&^5qW_QW_EWQE_W@!##!$&^$#^#" + liwu));
                                map.put("number", "1");
                                map.put("type", String.valueOf(type));


                                RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/gifts/giftLog", map, new NetCallBack<GiftSLogBean>() {
                                    @Override
                                    public void onSucceed(final GiftSLogBean giftSLogBean, int type) {
                                        if (giftSLogBean.getErr().equals("0")) {

                                            User.user().setUser_balance(giftSLogBean.getData());
                                            //消息类型 送礼人的名字
                                            // 送礼人的id  送礼人的性别
                                            // 送礼人的等级 送礼人的签名
                                            // 送礼人的在本房间的收礼数
                                            // 送礼人的魅力值  送礼人的头像
                                            // 收礼人的id  礼物名字(本地图片如:tangguo)  礼物数量 收礼人的名字(多个用-分割) 礼物名字(如 糖果 财神爷)
                                            new giftThread().start();

                                            String IM_MESSAGE_CONTENT = "5," + user_name + "," + user_ID + ","
                                                    + UserRoomListBean.UserRoomListBean().getData().get(0).getSex() + "," +
                                                    UserRoomListBean.UserRoomListBean().getData().get(0).getLevel() + "," +
                                                    UserRoomListBean.UserRoomListBean().getData().get(0).getSign() + "," +
                                                    UserRoomListBean.UserRoomListBean().getData().get(0).getGiftnum() + "," +
                                                    UserRoomListBean.UserRoomListBean().getData().get(0).getCharm() + "," +
                                                    UserRoomListBean.UserRoomListBean().getData().get(0).getHeadpic() + "," +
                                                    listToString1(songliid) + "," + liwupname + ",1,"
                                                    + listToString1(songliname) + "," + liwuname + "," + liwu;
                                            ZegoChatroom.shared().sendRoomMessage(IM_MESSAGE_CONTENT, 5, new ZegoChatroomSendRoomMessageCallback() {
                                                @Override
                                                public void onSendRoomMessage(ResultCode resultCode, ZegoChatroomMessage message) {

                                                    mMsgAdapter.addRoomMsg(user_name + "送给 " + listToString1(songliname) + "1个" + liwuname);


                                                    if (resultCode.isSuccess()) {
                                                        songliid.clear();
                                                        songliname.clear();
                                                        // 发送成功
                                                        // Toast.makeText(RoomActivity.this, "发送成功", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        // 发送失败
                                                        // Toast.makeText(RoomActivity.this, "发送失敗", Toast.LENGTH_SHORT).show();

                                                    }
                                                }
                                            });
                                        } else {
                                            Toast.makeText(RoomActivity.this, "余额不足", Toast.LENGTH_SHORT).show();
                                        }


                                    }

                                    @Override
                                    public void SucceedError(Exception e, int types) {
                                    }

                                    @Override
                                    public void onError(Throwable throwable, int type) {
                                        Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();
                                    }
                                }, 1);


                            } else {
                                Toast.makeText(RoomActivity.this, "请选择用户", Toast.LENGTH_SHORT).show();
                            }

                        }


                    } else {
                        Toast.makeText(RoomActivity.this, "请选择礼物", Toast.LENGTH_SHORT).show();
                    }


                }


                if (type == 1) {
                    int card = songliid.size();
                    if (card <= cardnumber) {

                        if (liwu > 0) {
                            if (!listToString1(songliid).equals("")) {

                                popupWindow.dismiss();
                                Map<String, String> map = new HashMap<>();
                                map.put("user_id", user_ID);
                                map.put("token", token);
                                map.put("room_id", room_ID);
                                map.put("touser_id", listToString1(songliid));
                                map.put("gift_id", md5("a8r7wyr!$#@#^$&^*)(*&^5qW_QW_EWQE_W@!##!$&^$#^#" + liwu));
                                map.put("number", "1");
                                map.put("type", String.valueOf(type));


                                RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/gifts/giftLog", map, new NetCallBack<GiftSLogBean>() {
                                    @Override
                                    public void onSucceed(final GiftSLogBean giftSLogBean, int type) {
                                        if (giftSLogBean.getErr().equals("0")) {


                                            //消息类型 送礼人的名字
                                            // 送礼人的id  送礼人的性别
                                            // 送礼人的等级 送礼人的签名
                                            // 送礼人的在本房间的收礼数
                                            // 送礼人的魅力值  送礼人的头像
                                            // 收礼人的id  礼物名字(本地图片如:tangguo)  礼物数量 收礼人的名字(多个用-分割) 礼物名字(如 糖果 财神爷)
                                            new giftThread().start();

                                            String IM_MESSAGE_CONTENT = "5," + user_name + "," + user_ID + ","
                                                    + UserRoomListBean.UserRoomListBean().getData().get(0).getSex() + "," +
                                                    UserRoomListBean.UserRoomListBean().getData().get(0).getLevel() + "," +
                                                    UserRoomListBean.UserRoomListBean().getData().get(0).getSign() + "," +
                                                    UserRoomListBean.UserRoomListBean().getData().get(0).getGiftnum() + "," +
                                                    UserRoomListBean.UserRoomListBean().getData().get(0).getCharm() + "," +
                                                    UserRoomListBean.UserRoomListBean().getData().get(0).getHeadpic() + "," +
                                                    listToString1(songliid) + "," + liwupname + ",1,"
                                                    + listToString1(songliname) + "," + liwuname + "," + liwu;
                                            ZegoChatroom.shared().sendRoomMessage(IM_MESSAGE_CONTENT, 5, new ZegoChatroomSendRoomMessageCallback() {
                                                @Override
                                                public void onSendRoomMessage(ResultCode resultCode, ZegoChatroomMessage message) {

                                                    mMsgAdapter.addRoomMsg(user_name + "送给 " + listToString1(songliname) + "1个" + liwuname);


                                                    if (resultCode.isSuccess()) {
                                                        songliid.clear();
                                                        songliname.clear();
                                                        // 发送成功
                                                        // Toast.makeText(RoomActivity.this, "发送成功", Toast.LENGTH_SHORT).show();
                                                    } else {
                                                        // 发送失败
                                                        // Toast.makeText(RoomActivity.this, "发送失敗", Toast.LENGTH_SHORT).show();

                                                    }
                                                }
                                            });
                                        } else {
                                            Toast.makeText(RoomActivity.this, "余额不足", Toast.LENGTH_SHORT).show();
                                        }


                                    }

                                    @Override
                                    public void SucceedError(Exception e, int types) {
                                    }

                                    @Override
                                    public void onError(Throwable throwable, int type) {
                                        Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();
                                    }
                                }, 1);


                            } else {
                                Toast.makeText(RoomActivity.this, "请选择用户", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(RoomActivity.this, "请选择礼物", Toast.LENGTH_SHORT).show();
                        }


                    } else {
                        Toast.makeText(RoomActivity.this, "礼物卡不足", Toast.LENGTH_SHORT).show();
                    }

                }


            }

        });


    }


    private void getliwukaback() {

        Map<String, String> map = new HashMap<>();
        map.put("user_id", user_ID);
        map.put("token", token);
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/user/card", map, new NetCallBack<MybackcardBean>() {
            @Override
            public void onSucceed(final MybackcardBean mybackcardBean, int type) {
                mybackcard = mybackcardBean.getData();


            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int type) {
                Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();
            }
        }, 1);


    }


    giftHandler giftHandler;


    class giftThread extends Thread {
        @Override
        public void run() {
            super.run();
            try {
                Message msg = giftHandler.obtainMessage();
                msg.what = liwu;
                giftHandler.sendMessage(msg);
            } catch (Exception e) {
                Log.e("giftthread", "" + e);
            }
        }
    }

    class giftHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {

            super.handleMessage(msg);
            SVGAParser parser = new SVGAParser(RoomActivity.this);
            svgaImageView.setLoops(1);
            if (view != null) {
                popupWindow.dismiss();
            }

            switch (msg.what) {
                case 0:
                    Toast.makeText(RoomActivity.this, "请选择礼物", Toast.LENGTH_SHORT).show();
                    break;
                case 1:
                    parser.parse("liuliuliu.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();

                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "liuliuliu.mp3"), "liuliuliu");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);

                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 2:
                    parser.parse("jindan.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();
                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "jindan.mp3"), "jindan");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);

                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 3:
                    parser.parse("shenmilihe.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();

                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "shenmilihe.mp3"), "shenmilihe");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);

                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 4:
                    parser.parse("dianzan.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();

                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "dianzan.mp3"), "dianzan");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);

                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 5:
                    parser.parse("bixin.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();

                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "bixin.mp3"), "bixin");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);


                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();
                        }

                    });
                    break;
                case 6:

                    parser.parse("xiangbin.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();

                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "xiangbin.mp3"), "xiangbin");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);


                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 7:

                    parser.parse("taohuayu.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();
                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "taohuayu.mp3"), "taohuayu");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);

                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 8:
                    parser.parse("kouhong.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();
                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "kouhong.mp3"), "kouhong");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);

                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 9:
                    parser.parse("zhaocaimao.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();

                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "zhaocaimao.mp3"), "zhaocaimao");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);

                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 10:
                    parser.parse("menghuanhudie.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();
                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "menghuanhudie.mp3"), "menghuanhudie");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);

                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 11:
                    parser.parse("lanseyaoji.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();
                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "lanseyaoji.mp3"), "lanseyaoji");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);

                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 12:
                    parser.parse("zijingbaobao.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();
                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "zijingbaobao.mp3"), "zijingbaobao");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);

                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 13:
                    parser.parse("yongbaorewen.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();
                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "yongbaorewen.mp3"), "yongbaorewen");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);


                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 14:
                    parser.parse("huangsepaoche.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();
                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "huangsepaoche.mp3"), "huangsepaoche");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);

                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 15:
                    parser.parse("hongsepaoche.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();
                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "hongsepaoche.mp3"), "hongsepaoche");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);

                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 16:
                    parser.parse("meiguihuachuan.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();
                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "meiguihuachuan.mp3"), "meiguihuachuan");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);


                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 17:
                    parser.parse("zaiyiqi.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();
                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "zaiyiqi.mp3"), "zaiyiqi");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);


                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 18:
                    parser.parse("falali.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();
                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "falali.mp3"), "falali");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);

                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 19:
                    parser.parse("nvshendengchang.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();
                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "nvshendengchang.mp3"), "nvshendengchang");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);

                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 20:
                    parser.parse("feiji.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();
                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "feiji.mp3"), "feiji");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);

                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 21:
                    parser.parse("huojian.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();
                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "huojian.mp3"), "huojian");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);

                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 22:
                    parser.parse("zhuguangwancan.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {

                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();
                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "zhuguangwancan.mp3"), "zhuguangwancan");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);

                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 23:
                    parser.parse("menghuanchengbao.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {


                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();
                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "menghuanchengbao.mp3"), "menghuanchengbao");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);


                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                case 24:
                    parser.parse("gongzhudechengbao.svga" + "", new SVGAParser.ParseCompletion() {

                        @Override
                        public void onComplete(SVGAVideoEntity mSVGAVideoEntity) {
                            svgaImageView.setVisibility(View.VISIBLE);
                            SVGADrawable drawable = new SVGADrawable(mSVGAVideoEntity);

                            svgaImageView.setImageDrawable(drawable);

                            svgaImageView.startAnimation();
                            //放音乐
                            ZegoMusicResource zegoMusicResource = new ZegoMusicResource(FileUtils.copyAssetsFile2Phone(RoomActivity.this, "gongzhudechengbao.mp3"), "gongzhudechengbao");
                            ZegoMusicPlayer mMusicPlayer = ZegoChatroom.shared().getMusicPlayer();
                            mMusicPlayer.playSoundEffect(zegoMusicResource);


                        }

                        @Override
                        public void onError() {

                            Toast.makeText(RoomActivity.this, "发送失败", Toast.LENGTH_SHORT).show();

                        }

                    });
                    break;
                default:
                    break;

            }

            liwu = 0;

        }

    }


    /**
     * 设置SearchView下划线透明
     **/
    public void setUnderLinetransparent(SearchView searchView) {
        try {
            Class<?> argClass = searchView.getClass();
            // mSearchPlate是SearchView父布局的名字
            Field ownField = argClass.getDeclaredField("mSearchPlate");
            ownField.setAccessible(true);
            View mView = (View) ownField.get(searchView);
            mView.setBackgroundColor(Color.TRANSPARENT);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    //遍历拼接输出的
    public static String listToString1(List<String> model) {
        StringBuilder sb = new StringBuilder();
        if (model != null && model.size() > 0) {
            for (int i = 0; i < model.size(); i++) {
                if (!model.get(i).equals("")) {
                    if (i < model.size() - 1) {
                        sb.append(model.get(i) + "-");
                    } else {
                        sb.append(model.get(i));
                    }
                }

            }
        }
        return sb.toString();
    }


    public static String md5(String string) {
        byte[] hash;
        try {
            hash = MessageDigest.getInstance("MD5").digest(string.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Huh, MD5 should be supported?", e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Huh, UTF-8 should be supported?", e);
        }
        StringBuilder hex = new StringBuilder(hash.length * 2);
        for (byte b : hash) {
            if ((b & 0xFF) < 0x10) hex.append("0");
            hex.append(Integer.toHexString(b & 0xFF));
        }
        return hex.toString();
    }

    protected void hideBottomUIMenu() {
        //隐藏虚拟按键，并且全屏
        if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) { // lower api
            View v = this.getWindow().getDecorView();
            v.setSystemUiVisibility(View.GONE);
        } else if (Build.VERSION.SDK_INT >= 19) {

            Window _window = getWindow();
            WindowManager.LayoutParams params = _window.getAttributes();
            params.systemUiVisibility = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE;
            _window.setAttributes(params);
        }
    }

    //去我的直播间
    private void gomyroom() {
        Map<String, String> map = new HashMap<>();
        map.put("user_id", User.user().getUser_ID());
        map.put("token", User.user().getToken());
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/room/room", map, new NetCallBack<MyRoomBean>() {
            @Override
            public void onSucceed(final MyRoomBean myRoomBean, int type) {
                room_id = myRoomBean.getData().getRoom_id();
                room_name = myRoomBean.getData().getName();
                getRoomInfo();
            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int types) {
                Toast.makeText(RoomActivity.this, "网络请求失败，请稍后重试！", Toast.LENGTH_SHORT).show();
            }
        }, 1);


    }

    //去我的直播间
    private void getRoomInfo() {

        Map<String, String> map = new HashMap<>();
        map.put("user_id", User.user().getUser_ID());
        map.put("token", User.user().getToken());
        map.put("room_id", room_id);


        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/index", map, new NetCallBack<RoomInforBean>() {
            @Override
            public void onSucceed(final RoomInforBean roomInforBean, int type) {


                Intent intent = new Intent(RoomActivity.this, RoomActivity.class);
                intent.putExtra("room_id", room_id);
                intent.putExtra("room_name", room_name);
                intent.putExtra("room_userid", roomInforBean.getRoom().getUser_id());
                startActivity(intent);


            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int type) {
            }
        }, 1);

    }

    //退出直播间必须离开
    private void exitChatRoom() {
        Map<String, String> map = new HashMap<>();
        map.put("user_id", User.user().getUser_ID());
        map.put("token", User.user().getToken());
        map.put("room_id", room_ID);
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/room/chase ", map, new NetCallBack<ExitChatRoomBean>() {
            @Override
            public void onSucceed(final ExitChatRoomBean exitChatRoomBean, int type) {
                exitChatRoomBean.getData();

            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int types) {
                Toast.makeText(RoomActivity.this, "网络请求失败，请稍后重试！", Toast.LENGTH_SHORT).show();
            }
        }, 1);


    }

    //我的直播间设置接口解析
    private void ChatRoomSetUp() {
        Map<String, String> map = new HashMap<>();
        map.put("token", User.user().getToken());
        map.put("room_id", room_ID);
        map.put("name", ROOMNAME);
        map.put("type", "0");
        map.put("roomtake", ROOMNAMEWANFA);
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/room/setup ", map, new NetCallBack<ExitChatRoomBean>() {
            @Override
            public void onSucceed(final ExitChatRoomBean exitChatRoomBean, int type) {
                exitChatRoomBean.getData();
                Toast.makeText(RoomActivity.this, exitChatRoomBean.getData().toString(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void SucceedError(Exception e, int types) {
                Toast.makeText(RoomActivity.this, "设置成功", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(Throwable throwable, int types) {
                Toast.makeText(RoomActivity.this, "网络请求失败，请稍后重试！", Toast.LENGTH_SHORT).show();
            }
        }, 1);
    }

    @Override
    public void getPosition(int position) {
        if (position == 0 || position == 1 || position == 2 || position == 3 || position == 4 || position == 5 || position == 6 || position == 7) {
            if (position == 0) {
                backimageID = position + "";
                llroombg.setBackgroundResource(R.mipmap.mlzsaz);
                ChatRoombackgroud();
            } else if (position == 1) {
                backimageID = position + "";
                llroombg.setBackgroundResource(R.mipmap.dfcaz);
                ChatRoombackgroud();
            } else if (position == 2) {
                backimageID = position + "";
                llroombg.setBackgroundResource(R.mipmap.dyaz);
                ChatRoombackgroud();
            } else if (position == 3) {
                backimageID = position + "";
                llroombg.setBackgroundResource(R.mipmap.mhwtaz);
                ChatRoombackgroud();
            } else if (position == 4) {
                backimageID = position + "";
                llroombg.setBackgroundResource(R.mipmap.tnaz);
                ChatRoombackgroud();
            } else if (position == 5) {
                backimageID = position + "";
                llroombg.setBackgroundResource(R.mipmap.xdaz);
                ChatRoombackgroud();
            } else if (position == 6) {
                backimageID = position + "";
                llroombg.setBackgroundResource(R.mipmap.xxzhaz);
                ChatRoombackgroud();
            } else if (position == 7) {
                backimageID = position + "";
                llroombg.setBackgroundResource(R.mipmap.yhyhaz);
                ChatRoombackgroud();
            }
        }
    }

    //我的直播间设置图片
    private void ChatRoombackgroud() {
        Map<String, String> map = new HashMap<>();
        map.put("token", User.user().getToken());
        map.put("room_id", room_ID);
        map.put("backimageID", backimageID);
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/room/background ", map, new NetCallBack<ExitChatRoomBean>() {
            @Override
            public void onSucceed(final ExitChatRoomBean exitChatRoomBean, int type) {
                exitChatRoomBean.getData();
                Toast.makeText(RoomActivity.this, exitChatRoomBean.getData().toString(), Toast.LENGTH_SHORT).show();
                Map<String, String> map = new HashMap<>();
                map.put("user_id", User.user().getUser_ID());
                map.put("token", User.user().getToken());
                map.put("room_id", room_ID);
                RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/room/information", map, new NetCallBack<PersonalDataBean>() {
                    @Override
                    public void onSucceed(final PersonalDataBean personalDataBean, int type) {
                        tv_play_method.setText("房间玩法" + personalDataBean.getRoom().getRoomtake());
                        String A = personalDataBean.getRoom().getBack();
                        if (A.equals("0")) {
                            llroombg.setBackgroundResource(R.mipmap.mlzsaz);
                        } else if (A.equals("1")) {
                            llroombg.setBackgroundResource(R.mipmap.dfcaz);
                        } else if (A.equals("2")) {
                            llroombg.setBackgroundResource(R.mipmap.dyaz);
                        } else if (A.equals("3")) {
                            llroombg.setBackgroundResource(R.mipmap.mhwtaz);
                        } else if (A.equals("4")) {
                            llroombg.setBackgroundResource(R.mipmap.tnaz);
                        } else if (A.equals("5")) {
                            llroombg.setBackgroundResource(R.mipmap.xdaz);
                        } else if (A.equals("6")) {
                            llroombg.setBackgroundResource(R.mipmap.xxzhaz);
                        } else if (A.equals("7")) {
                            llroombg.setBackgroundResource(R.mipmap.yhyhaz);
                        }
                    }

                    @Override
                    public void SucceedError(Exception e, int types) {
                    }

                    @Override
                    public void onError(Throwable throwable, int type) {
                    }
                }, 1);
            }

            @Override
            public void SucceedError(Exception e, int types) {
                Toast.makeText(RoomActivity.this, "设置成功", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(Throwable throwable, int types) {
                Toast.makeText(RoomActivity.this, "网络请求失败，请稍后重试！", Toast.LENGTH_SHORT).show();
            }
        }, 1);
    }

    private void initDataBK() {
        Map<String, String> map = new HashMap<>();
        map.put("user_id", User.user().getUser_ID());
        map.put("token", User.user().getToken());
        map.put("room_id", room_ID);
        map.put("isshoucang", isshoucang);
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/room/collection", map, new NetCallBack<CollectionRoomBean>() {
            @Override
            public void onSucceed(final CollectionRoomBean collectionRoomBean, int type) {
                if (collectionRoomBean.getMsg().equals("ok")) {
                    if (!SC) {
                        SC = true;
                        tvguanzhu.setBackgroundResource(R.mipmap.dianjiguanzhu);
                        Toast.makeText(RoomActivity.this, "收藏成功", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(RoomActivity.this, "取消收藏成功", Toast.LENGTH_SHORT).show();
                        SC = false;
                        tvguanzhu.setBackgroundResource(R.mipmap.love);
                    }
                } else {
                    Toast.makeText(RoomActivity.this, collectionRoomBean.getMsg(), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int type) {
            }
        }, 1);
    }

    private void addOrRemove(int position, View view) {


        CheckableLayout checkablelayout = view.findViewById(R.id.root_layout1);
        if (positionSet.contains(position)) {
            // 如果包含，则撤销选择
            positionSet.remove(position);
            checkTYpeNameSet.remove(mybackcard.get(position).getName());
            checkTYpeNameSet.remove(mybackcard.get(position).getGifid());
        } else {
            // 如果不包含，则添加
            positionSet.add(position);
            checkTYpeNameSet.add(mybackcard.get(position).getName());
            checkTYpeNameSet.add(mybackcard.get(position).getGifid());
        }
        if (positionSet.size() == 0) {
            // 如果没有选中任何的item，则退出多选模式
            mybackpackadapter.notifyDataSetChanged();
            selectMode = false;
        } else {
            // 更新列表界面，否则无法显示已选的item
            mybackpackadapter.notifyDataSetChanged();
        }


//        Log.e("info",positionSet.toString());
//        Toast.makeText(RoomActivity.this,checkTYpeNameSet.toString(),Toast.LENGTH_SHORT).show();
    }
}
