package com.xunshan.ZhenSheng.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.view.CircleTextProgressbar;

public class StartActivity extends AppCompatActivity {
    private CircleTextProgressbar mTvSkip;
    barHandler handler;
    Message msg;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.start_activity);

        initView();
        initData();


    }

    private void initView() {
        mTvSkip = (CircleTextProgressbar) findViewById(R.id.tv_red_skip);
        mTvSkip.setOutLineColor(Color.TRANSPARENT);
//        mTvSkip.setInCircleColor(Color.parseColor("#AAC6C6C6"));
        mTvSkip.setProgressColor(Color.DKGRAY);
        mTvSkip.setProgressLineWidth(1);
        mTvSkip.setCountdownProgressListener(0, progressListener);
        mTvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    private void initData() {
        handler = new barHandler();
        new barThread().start();
        mTvSkip.reStart();
    }

    private CircleTextProgressbar.OnCountdownProgressListener progressListener = new CircleTextProgressbar.OnCountdownProgressListener() {
        @Override
        public void onProgress(int what, int progress) {
            if (progress == 0) {

               //startActivity(new Intent(StartActivity.this, LoginActivity.class));

                if (!TextUtils.isEmpty(User.user().getUser_ID())) {
                    startActivity(new Intent(StartActivity.this, MainActivity.class));
                } else {
                    startActivity(new Intent(StartActivity.this, LoginActivity.class));
                }
                StartActivity.this.finish();
            }
        }
    };

    class barThread extends Thread {
        @Override
        public void run() {
            super.run();
            try {
                int i = 5;
                while (i > 0) {
                    msg = handler.obtainMessage();
                    msg.what = 1;
                    msg.obj = i;
                    sleep(1000);
                    handler.sendMessage(msg);
                    i--;
                }
            } catch (Exception e) {
                Log.e("bar", "" + e);
            }

        }
    }

    class barHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 1:
                    mTvSkip.setText(msg.obj + "");
                    break;
            }
        }
    }
}
