package com.xunshan.ZhenSheng.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.entity.JuBaoRoomBean;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.RetrofitUtils;

import java.util.HashMap;
import java.util.Map;


public class JuBaoActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView tvfanhuia;
    private TextView tvjubao;
    private String roomid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.activity_ju_bao);
        Intent intent = getIntent();

        roomid = intent.getStringExtra("room_idid");
        tvfanhuia = (TextView) findViewById(R.id.tv_fanhuia);
        tvjubao= (TextView) findViewById(R.id.tv_jubao);

        tvfanhuia.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_fanhuia:
                finish();
                break;
            case R.id.tv_jubao:
                Map<String, String> map = new HashMap<>();
                map.put("token", User.user().getToken());
                map.put("user_id", User.user().getUser_ID());
                map.put("room_id",roomid);
               // map.put("why", seatInfo.mUser.userID);
                RetrofitUtils.getInstace().postOkHttp(AllInterface.addr+"/"+"home/user/follow", map, new NetCallBack<JuBaoRoomBean>() {
                    @Override
                    public void onSucceed(final JuBaoRoomBean juBaoRoomBean, int type) {
                        Toast.makeText(JuBaoActivity.this,juBaoRoomBean.getData(),Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void SucceedError(Exception e, int types) {
                    }

                    @Override
                    public void onError(Throwable throwable, int type) {
                    }
                }, 1);
                break;
        }
    }
}
