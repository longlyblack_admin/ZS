package com.xunshan.ZhenSheng.activity;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.adapter.FollowRoomAdapter1;
import com.xunshan.ZhenSheng.entity.StoreUpBean;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.RetrofitUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FollowRoomActivity extends AppCompatActivity {
    public ListView lv_followroom;
   public List<StoreUpBean.DataBean> Storelist1;
    public FollowRoomAdapter1 adapter;
    public  TextView tv_followroomback;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.followroom_activity);
        initView();
        initData();
    }

    private void initView() {
        tv_followroomback = (TextView) findViewById(R.id.tv_followroomback);
        lv_followroom = (ListView) findViewById(R.id.lv_followroom);

        tv_followroomback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FollowRoomActivity.this.finish();
            }
        });
    }

    private void initData() {

        Map<String, String> map = new HashMap<>();
        map.put("user_id", User.user().getUser_ID());
        map.put("token", User.user().getToken());
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/room/roomlist", map, new NetCallBack<StoreUpBean>() {
            @Override
            public void onSucceed(StoreUpBean storeUpBean, int types) {
                Storelist1 = storeUpBean.getData();

                adapter = new FollowRoomAdapter1(FollowRoomActivity.this, Storelist1);
                lv_followroom.setAdapter(adapter);
            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int type) {
            }
        }, 1);


    }



}
