package com.xunshan.ZhenSheng.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.util.SystemStatusManager;

import java.util.Locale;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    Button btn_login_next;
    private AlertDialog.Builder builder;

    public static Activity mContext;

    private TextView tiaokuan;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.login_activity);
        initView();
        getPermission();
    }

    private void initView() {
        btn_login_next = (Button) findViewById(R.id.btn_login_next);

        tiaokuan = findViewById(R.id.tv_tiaokuan);
        btn_login_next.setOnClickListener(this);
        tiaokuan.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_login_next:
                showLogin();
                break;
            case R.id.tv_tiaokuan:
                Intent it = new Intent(this, ShenhaiActivity.class);
                it.putExtra("url", "https://api3.dayushaiwang.com/home/PrivacyPolicy.html");
                startActivity(it);
                break;
            default:
                break;
        }
    }

    private void getPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // 检查权限状态
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_PHONE_STATE)) {
                    //  用户彻底拒绝授予权限，一般会提示用户进入设置权限界面
                } else {
                    //  用户未彻底拒绝授予权限
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
                }
            }
        }
    }

    private void showLogin() {
        startActivity(new Intent(LoginActivity.this, PhoneCodeActivity.class));

    }

}
