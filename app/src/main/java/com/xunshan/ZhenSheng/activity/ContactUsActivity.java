package com.xunshan.ZhenSheng.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.xunshan.ZhenSheng.R;

public class ContactUsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_us_layout);

        WebView we = findViewById(R.id.wb__lainxiwo);


        WebSettings settings = we.getSettings();
        //如果访问的页面中有Javascript，则webview必须设置支持Javascript
        settings.setJavaScriptEnabled(true);
        //WebView加载web资源

        we.loadUrl("https://api3.dayushaiwang.com/baoxiang/contact.html");
    }
}
