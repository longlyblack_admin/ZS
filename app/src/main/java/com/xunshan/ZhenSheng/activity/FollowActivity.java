package com.xunshan.ZhenSheng.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.adapter.ViewPagerAdapter;
import com.xunshan.ZhenSheng.fragment.FansFragment;
import com.xunshan.ZhenSheng.fragment.FollowFragment;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.NavitationLayout;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的个人主页
 */
public class FollowActivity extends AppCompatActivity implements View.OnClickListener {
    ViewPager vp;
    List<Fragment> list;
    private TextView followback;
    private String token;
    private String userID;
    private String[] titles1 = new String[]{"关注", "粉丝"};
    private NavitationLayout navitationLayout;
    private ViewPagerAdapter viewPagerAdapter111;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.follow_activity);

        initView();
        initData();
    }

    private void initView() {
        followback = findViewById(R.id.tv_followback);
        vp = findViewById(R.id.vp);
        navitationLayout = (NavitationLayout) findViewById(R.id.bar111);
        followback.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_followback:
                finish();
                break;

            default:
                break;
        }
    }


    private void initData() {
        list = new ArrayList();
        list.add(new FollowFragment());
        list.add(new FansFragment());
        viewPagerAdapter111 = new ViewPagerAdapter(getSupportFragmentManager(), list);
        vp.setAdapter(viewPagerAdapter111);
        navitationLayout.setViewPager(this, titles1, vp, R.color.color_777777, R.color.color_333333, 18, 20, 0, 0, true);
        navitationLayout.setBgLine(this, 1, R.color.white);
        navitationLayout.setNavLine(this, 4, R.color.colorzhuzh, 0);


    }
}
