package com.xunshan.ZhenSheng.activity;

import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.adapter.ViewPagerAdapter;
import com.xunshan.ZhenSheng.fragment.MsgMsgFragment;
import com.xunshan.ZhenSheng.util.NavitationLayout;

import java.util.ArrayList;
import java.util.List;


public class MsgActivity extends AppCompatActivity {
   /* ViewPager vp_msg;
    List<Fragment> list;*/
    private NavitationLayout navitationLayout;
    private ViewPager viewPager1;
    private String[] titles1 = new String[]{"消息"};
    private ViewPagerAdapter viewPagerAdapter1;
    private List<MsgMsgFragment> fragments1;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.msg_activity);
        initView();
    }
    private void initView(){
        viewPager1 = (ViewPager) findViewById(R.id.viewpager1);
        navitationLayout = (NavitationLayout) findViewById(R.id.bar1);
        fragments1 =  new ArrayList<>();
        fragments1.add(new MsgMsgFragment());
        viewPagerAdapter1 = new ViewPagerAdapter(getSupportFragmentManager(), fragments1);
        viewPager1.setAdapter(viewPagerAdapter1);
        navitationLayout.setViewPager(this, titles1, viewPager1, R.color.color_777777, R.color.color_333333, 16, 18, 0, 0, true);
        navitationLayout.setBgLine(this, 1, R.color.white);
        navitationLayout.setNavLine(this, 4, R.color.color_zi, 0);
    }
}
