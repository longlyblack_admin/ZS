package com.xunshan.ZhenSheng.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bigkoo.pickerview.TimePickerView;
import com.google.gson.Gson;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.entity.LoginInfoBean;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.LogUtil;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.RetrofitUtils;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class LoginInfoActivity extends AppCompatActivity implements View.OnClickListener {
    TextView tv_date;
    Calendar c = null;
    DatePickerDialog dialog;
    EditText edtloginname;
    Button btn_logininfo_start;
    TextView tv_logininfo_go;
    String UserName;
    String UserName1;
    int UserID;
    String headphoto;
    String token;
    TimePickerView pvTime1;
    private RadioButton rbsexman;
    private RadioButton rbsexwoman;
    private RadioGroup radgroup;
    private String birthaday;
    private String phone;
    private String sex;
    private String name;
    private TextView ivrandom;
    private static String[] road = {"滚开我有煞气",
            "贱是一种态度",
            "时间偷走初衷",
            "邮个拥抱给你",
            "初吻给了奶瓶",
            "抒一封旧长信",
            "受大气，成大器",
            "帅到血肉模糊",
            "上课课╰睡覺覺",
            "心不动则不痛",
            "承蒙时光不弃",
            "一脸的美人痣",
            "青春不宜说爱",
            "寂寞寂寞就好",
            "暖阳下ら浅浅笑",
            "蒲公英の约定",
            "最凉不过人心",
            "差点成了帅哥",
            "璑佲扌旨の簦彳寺",
            "时间是测谎仪",
            "沫年℅夏至未至",
            "时光偷走初心",
            "深巷里的喵喵",
            "说多了都是泪",
            "风吹过的思念",
            "风一样旳莮孒",
            "纳痛〆依然犹存",
            "爱吃喵喵的鱼",
            "弦已断，曲终散",
            "时间会说真话",
            "这么近那么远",
            "別特麽假惺惺",
            "别逼我耍流氓",
            "再见再也不见",
            "我爱你，你随意",
            "我玩命~你随意",
            "心不动丶则不痛",
            "许沵①世柔情",
            "此昵称不存在",
            "心不狠站不稳",
            "娇气的小奶包〆",
            "后来，没有后来",
            "你长得好辟邪",
            "被啃弯的月亮",
            "我能说脏话么",
            "被判无妻徒刑",
            "给卟ㄋの咏远",
            "顆糖.两种味",
            "爷独霸怡荭院",
            "城已空，人已散",
            "谁欠谁の緈福",
            "别吵醒了记忆",
            "痞是一种态度",
            "冰是睡着的水",
            "风，吹走了誓言",
            "呛了眼凉了心",
            "一切都已淡漠",
            "一岁时就很帅",
            "说好dē幸福呢",
            "恋你朝朝暮暮"};


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.logininfo_activity);
//        SharedPreferences sharedPreferences = getSharedPreferences("data",MODE_PRIVATE);
//        phone = sharedPreferences.getString("phone","");
        Intent intent = getIntent();
        phone = intent.getStringExtra("Phone");
        Log.e("shuju0", "DIID" + phone);
        initView();
    }

    private void initView() {
        tv_date = (TextView) findViewById(R.id.tv_date);
        edtloginname = (EditText) findViewById(R.id.edt_login_name);
        btn_logininfo_start = (Button) findViewById(R.id.btn_logininfo_start);
        tv_logininfo_go = (TextView) findViewById(R.id.tv_logininfo_go);
        rbsexman = (RadioButton) findViewById(R.id.rb_sex_man);
        rbsexwoman = (RadioButton) findViewById(R.id.rb_sex_woman);
        radgroup = (RadioGroup) findViewById(R.id.radioGroup);
        ivrandom = (TextView) findViewById(R.id.iv_random);


        tv_logininfo_go.setOnClickListener(this);
        tv_date.setOnClickListener(this);
        btn_logininfo_start.setOnClickListener(this);
        edtloginname.setOnClickListener(this);
        ivrandom.setOnClickListener(this);
    }

    public static int getNum() {
        return (int) (Math.random() * road.length);
    }

    public static String getRoad() {
        int index = getNum();
        String first = road[index];

        return first;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_date:
                chooseDate();
                break;
            case R.id.tv_logininfo_go:
                startActivity(new Intent(this, MainActivity.class));
                break;
            case R.id.iv_random:
                name = getRoad();
                Log.e("nameTag1", "onClick: " + name);
                edtloginname.setText(name);
                Log.e("nameTag2", "onClick: " + name);
                break;
            case R.id.btn_logininfo_start:
                username();
                for (int i = 0; i < radgroup.getChildCount(); i++) {
                    RadioButton rd = (RadioButton) radgroup.getChildAt(i);
                    if (rd.isChecked()) {
                        // Toast.makeText(getApplicationContext(), "点击提交按钮,获取你选择的是:" + rd.getText(), Toast.LENGTH_LONG).show();
                        sex = rd.getText().toString();
                        break;
                    }
                }
                Map<String, String> map = new HashMap<>();
                map.put("phone", phone);
                map.put("sex", sex);
                map.put("birthaday", birthaday);
                map.put("name", UserName);

                RetrofitUtils.getInstace().postOkHttp("https://api3.dayushaiwang.com/revo/login/register", map, new NetCallBack<LoginInfoBean>() {
                    @Override
                    public void onSucceed(final LoginInfoBean loginInfoBean, int type) {
                        //code==200 String 数组
                        LogUtil.e("登录-->"+ new Gson().toJson(loginInfoBean));
                        if (loginInfoBean.getErr() == 0 && loginInfoBean.getData() != null) {

                            UserID = loginInfoBean.getData().getUser_id();
                            headphoto = AllInterface.addr + "/" + loginInfoBean.getData().getHeadpic();
                            loginInfoBean.getData().getIsone();
                            loginInfoBean.getData().getLevel();
                            loginInfoBean.getData().getSex();
                            token = loginInfoBean.getData().getToken();
                            loginInfoBean.getData().getSign();

                            User.user().setSex(String.valueOf(loginInfoBean.getData().getSex()));
                            User.user().setUser_ID(String.valueOf(UserID));
                            User.user().setUser_name(UserName);
                            User.user().setToken(token);
                            Intent intent = new Intent(LoginInfoActivity.this, MainActivity.class);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void SucceedError(Exception e, int types) {

                    }

                    @Override
                    public void onError(Throwable throwable, int type) {
                        Log.e("1234567", "onError: 1");
                    }
                }, 1);
        }


    }


    private void chooseDate() {
        c = Calendar.getInstance();
        final int[] mYear = {c.get(Calendar.YEAR)};
        final int[] mMonth = {c.get(Calendar.MONTH)};
        final int[] mDay = {c.get(Calendar.DAY_OF_MONTH)};
        dialog = new DatePickerDialog(this, AlertDialog.THEME_HOLO_LIGHT, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker dp, int year, int month, int dayOfMonth) {
                mYear[0] = year;
                mMonth[0] = month;
                mDay[0] = dayOfMonth;
                final String data = year + "年" + (month + 1) + "月-" + dayOfMonth + "日 ";
                if ((month + 1) < 10) {
                    if (dayOfMonth < 10) {
                        birthaday = year + "-" + "0" + (month + 1) + "-" + "0" + dayOfMonth;
                        tv_date.setText(data);
                    } else if (dayOfMonth >= 10) {
                        birthaday = year + "-" + "0" + (month + 1) + "-" + dayOfMonth;
                        Log.e("TAG1", "onDateSet: " + birthaday);
                        tv_date.setText(data);
                    }
                } else if ((month + 1) >= 10) {
                    if (dayOfMonth < 10) {
                        birthaday = year + "-" + (month + 1) + "-" + "0" + dayOfMonth;
                        Log.e("TAG12", "onDateSet: " + birthaday);
                        tv_date.setText(data);
                    } else if (dayOfMonth >= 10) {
                        birthaday = year + "-" + (month + 1) + "-" + dayOfMonth;
                        Log.e("TAG12", "onDateSet: " + birthaday);
                        tv_date.setText(data);
                    }
                }
            }
        }, mYear[0], mMonth[0], mDay[0]
        );
        dialog.show();
    }


    private void username() {
        UserName = edtloginname.getText().toString();
    }


}










