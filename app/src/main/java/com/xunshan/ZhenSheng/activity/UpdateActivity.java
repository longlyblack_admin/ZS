package com.xunshan.ZhenSheng.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.example.liangmutian.mypicker.DataPickerDialog;
import com.google.gson.Gson;
import com.hb.dialog.myDialog.MyAlertInputDialog;
import com.tencent.qcloud.tim.uikit.utils.ToastUtil;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.entity.AddGeRenZiLiaoBean;
import com.xunshan.ZhenSheng.entity.MyPrincipalBean;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.ApiService;
import com.xunshan.ZhenSheng.util.GlideRoundTransform;
import com.xunshan.ZhenSheng.util.LogUtil;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.RetrofitUtils;
import com.xunshan.ZhenSheng.util.RoundTransform;
import com.zxy.tiny.Tiny;
import com.zxy.tiny.callback.FileWithBitmapCallback;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import kankan.wheel.widget.OnWheelChangedListener;
import kankan.wheel.widget.WheelView;
import kankan.wheel.widget.adapters.ArrayWheelAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class UpdateActivity extends BaseActivity implements View.OnClickListener, OnWheelChangedListener {
    TextView tvupdateback;
    ImageView ivHead;
    TextView tvupdatenicknameright;
    TextView tvupdatedate;
    TextView tvupdatehometown;
    TextView tvupdatesex;
    TextView tvupdateautograph;
    TextView tvupdatesave;

    View view;
    PopupWindow popupWindow;
    private WheelView mViewProvince;
    private WheelView mViewCity;
    private WheelView mViewDistrict;
    private Button mBtnConfirm;
    private Button btn_cancel;
    private Dialog chooseDialog;
    private TextView sexTv;
    Calendar c = null;
    DatePickerDialog dialog;
    private String birthaday = "";
    private String[] fileFrom = new String[]{"拍照", "图库"};
    private String GeRenSign = "";
    private String GeRenname = "";
    private String GeRenAddRess = "";
    private String userID;
    private String sex = "";
    private String token;
    private Button btn_picture;
    private Button btn_photo;
    private Button btn_cancle;
    private Bitmap head;// 头像Bitmap
    @SuppressLint("SdCardPath")
    private static String path = "/sdcard/myHead/";// sd路径
    private byte[] pic;
    private Uri URI;
    private static String filePath;
    private File temp;
    private AddGeRenZiLiaoBean addGeRenZiLiaoBean;

    //调取系统摄像头的请求码
    private static final int MY_ADD_CASE_CALL_PHONE = 6;
    //打开相册的请求码
    private static final int MY_ADD_CASE_CALL_PHONE2 = 7;
    private AlertDialog.Builder builder;
    private AlertDialog dialog1;
    private LayoutInflater inflater;
    private ImageView imageView;
    private View layout;
    private Button takePhotoTV;
    private Button choosePhotoTV;
    private Button cancelTV;


    String strBack = "";
    JSONObject obj;
    JSONArray arr;
    private Context mContext;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        mContext = this;
        SharedPreferences sharedPreferences = getSharedPreferences("data", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        token = sharedPreferences.getString("token", "");
        userID = sharedPreferences.getString("user_ID", "");
//        initPhotoError();
        setContentView(R.layout.update_activity);
        initView();
        //  initData();
    }

    private void initView() {
        ivHead = findViewById(R.id.iv_head);
        tvupdateback = (TextView) findViewById(R.id.tv_updateback);
        tvupdatenicknameright = (TextView) findViewById(R.id.tv_updatenickname_right);
        tvupdatehometown = (TextView) findViewById(R.id.tv_updatehometown);
        tvupdatesex = (TextView) findViewById(R.id.tv_updatesex);
        tvupdatedate = (TextView) findViewById(R.id.tv_updatedate);
        tvupdateautograph = (TextView) findViewById(R.id.tv_updateautograph);
        tvupdatesave = (TextView) findViewById(R.id.tv_updatesave);

        tvupdateback.setOnClickListener(this);
        tvupdatehometown.setOnClickListener(this);
        tvupdatesex.setOnClickListener(this);
        tvupdatedate.setOnClickListener(this);
        tvupdatesave.setOnClickListener(this);
        tvupdateautograph.setOnClickListener(this);
        tvupdatenicknameright.setOnClickListener(this);
        ivHead.setOnClickListener(this);

        Map<String, String> map = new HashMap<>();
        map.put("user_id", User.user().getUser_ID());
        map.put("token", User.user().getToken());
        map.put("touser_id", getIntent().getStringExtra("intent_userId"));
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/home/index", map, new NetCallBack<MyPrincipalBean>() {
            @Override
            public void onSucceed(final MyPrincipalBean myPrincipalBean, int type) {
                LogUtil.e("获取个人信息-->"+ new Gson().toJson(myPrincipalBean));
                User.user().setUser_name(myPrincipalBean.getUser().getName());
                User.user().setAddress(myPrincipalBean.getUser().getAddress());
                User.user().setUser_ID(myPrincipalBean.getUser().getUser_id());
                User.user().setUser_level(myPrincipalBean.getUser().getLevel());
                User.user().setSex(myPrincipalBean.getUser().getSex());
                User.user().setBirthaday(myPrincipalBean.getUser().getBirthaday());
                User.user().setSign(myPrincipalBean.getUser().getSign());
                User.user().setUser_Headpic(myPrincipalBean.getUser().getHeadpic());

                if (!TextUtils.isEmpty(myPrincipalBean.getUser().getName())) {
                    tvupdatenicknameright.setText(myPrincipalBean.getUser().getName());//昵称
                }

                if (!TextUtils.isEmpty(myPrincipalBean.getUser().getSex())) {
                    if (myPrincipalBean.getUser().getSex().equals("0")) {
                        tvupdatesex.setText("男");//性别
                    } else {
                        tvupdatesex.setText("女");//性别
                    }
                }

                if (!TextUtils.isEmpty(myPrincipalBean.getUser().getBirthaday())) {
                    tvupdatedate.setText(myPrincipalBean.getUser().getBirthaday());//生日
                }

                if (!TextUtils.isEmpty(myPrincipalBean.getUser().getAddress())) {
                    tvupdatehometown.setText(myPrincipalBean.getUser().getAddress());//家乡
                }

                if (!TextUtils.isEmpty(myPrincipalBean.getUser().getSign())) {
                    tvupdateautograph.setText(myPrincipalBean.getUser().getSign());//个人简介
                }

                if (!TextUtils.isEmpty(myPrincipalBean.getUser().getHeadpic())) {
                    //头像
                    new RoundedCorners(16);
                    Glide.with(mContext)
                            .load(AllInterface.addr + "/" + myPrincipalBean.getUser().getHeadpic())
                            .skipMemoryCache(false)
                            .dontAnimate()
                            .apply(new RequestOptions().centerCrop().transform(new RoundTransform(mContext, 100)).error(R.mipmap.addshangmai)
                                    .transform(new GlideRoundTransform(100)))
                            .into(ivHead);
                }
            }

            @Override
            public void SucceedError(Exception e, int types) {
                LogUtil.e("个人信息-->"+ e.getMessage());
            }

            @Override
            public void onError(Throwable throwable, int types) {
                LogUtil.e("个人信息-->"+ throwable.getMessage());
            }
        }, 1);


    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_updateback:
                UpdateActivity.this.finish();
                break;
            case R.id.tv_updatehometown:
                showPop();
                break;
            case R.id.btn_cancel:
                popupWindow.dismiss();
                break;
            case R.id.btn_confirm:
                showSelectedResult();
                popupWindow.dismiss();
                break;
            case R.id.tv_updatesex:
                showSexPop();
                break;
            case R.id.tv_updatedate:
                chooseDate();
                break;
            case R.id.tv_updatesave:
                saveUpdate();
                break;
            case R.id.tv_updateautograph:
                GeRenJianJIeDialog();
                break;
            case R.id.tv_updatenickname_right:
                NameDialog();
                break;
            case R.id.iv_head:
                UpdatePhoto(v);


                break;
            case R.id.photograph:
                //"点击了照相";
                //  6.0之后动态申请权限 摄像头调取权限,SD卡写入权限
                //判断是否拥有权限，true则动态申请
                if (ContextCompat.checkSelfPermission(UpdateActivity.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission(UpdateActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(UpdateActivity.this,
                            new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_ADD_CASE_CALL_PHONE);
                } else {
                    try {
                        //有权限,去打开摄像头
                        takePhoto();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                dialog1.dismiss();
                break;
            case R.id.photo:
                //"点击了相册";
                //  6.0之后动态申请权限 SD卡写入权限
                if (ContextCompat.checkSelfPermission(UpdateActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(UpdateActivity.this,
                            new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            MY_ADD_CASE_CALL_PHONE2);

                } else {
                    //打开相册
                    choosePhoto();
                }
                dialog1.dismiss();
                break;
            case R.id.cancel:
                dialog1.dismiss();//关闭对话框
                break;
            default:
                break;


        }
    }

    private void saveUpdate() {
        uploadImage2(temp);
    }


    private void chooseDate() {
        c = Calendar.getInstance();
        final int[] mYear = {c.get(Calendar.YEAR)};
        final int[] mMonth = {c.get(Calendar.MONTH)};
        final int[] mDay = {c.get(Calendar.DAY_OF_MONTH)};
        dialog = new DatePickerDialog(this, AlertDialog.THEME_HOLO_LIGHT, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker dp, int year, int month, int dayOfMonth) {
                mYear[0] = year;
                mMonth[0] = month;
                mDay[0] = dayOfMonth;
                final String data = year + "年" + (month + 1) + "月-" + dayOfMonth + "日 ";
                if ((month + 1) < 10) {
                    if (dayOfMonth == 1) {
                        birthaday = year + "-" + "0" + (month + 1) + "-" + "0" + dayOfMonth;
                        tvupdatedate.setText(data);
                    } else if (dayOfMonth != 1) {
                        birthaday = year + "-" + "0" + (month + 1) + "-" + dayOfMonth;
                        Log.e("TAG1", "onDateSet: " + birthaday);
                        tvupdatedate.setText(data);
                    }
                } else if ((month + 1) >= 10) {
                    if (dayOfMonth == 1) {
                        birthaday = year + "-" + (month + 1) + "-" + "0" + dayOfMonth;
                        Log.e("TAG12", "onDateSet: " + birthaday);
                        tvupdatedate.setText(data);
                    } else if (dayOfMonth != 1) {
                        birthaday = year + "-" + (month + 1) + "-" + dayOfMonth;
                        Log.e("TAG12", "onDateSet: " + birthaday);
                        tvupdatedate.setText(data);
                    }
                }
            }
        }, mYear[0], mMonth[0], mDay[0]
        );
        dialog.show();
    }

    private void showSexPop() {
        view = LayoutInflater.from(this).inflate(R.layout.sex_pop, null);
        sexTv = (TextView) view.findViewById(R.id.tv_sex_pop);
        List<String> list = new ArrayList<>();
        list.add("男");
        list.add("女");
        showChooseDialog(list);
    }

    private void showChooseDialog(List<String> mlist) {
        DataPickerDialog.Builder builder = new DataPickerDialog.Builder(this);
        chooseDialog = builder.setData(mlist).setSelection(1).setTitle("取消")
                .setOnDataSelectedListener(new DataPickerDialog.OnDataSelectedListener() {
                    @Override
                    public void onDataSelected(String itemValue, int position) {
                        if (itemValue.equals("男")) {
                            tvupdatesex.setText(itemValue);
                            sex = "0";
                        } else if (itemValue.equals("女")) {
                            tvupdatesex.setText(itemValue);
                            sex = "1";
                        } else {
                            tvupdatesex.setText("空");
                        }
                        Log.e("TAGitemValue", "onDataSelected: " + itemValue);
                    }

                    @Override
                    public void onCancel() {

                    }
                }).create();

        chooseDialog.show();
    }

    private void showSelectedResult() {
        GeRenAddRess = mCurrentProviceName + mCurrentCityName + mCurrentDistrictName + mCurrentZipCode;
        tvupdatehometown.setText(GeRenAddRess);
    }

    private void showPop() {
        view = LayoutInflater.from(this).inflate(R.layout.hometown_pop, null);
        setUpViews();
        setUpListener();
        setUpData();
        popupWindow = new PopupWindow(view, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        popupWindow.setBackgroundDrawable(new BitmapDrawable());
        popupWindow.setOutsideTouchable(true);
        popupWindow.showAsDropDown(tvupdatehometown);
    }

    private void setUpViews() {
        mViewProvince = (WheelView) view.findViewById(R.id.id_province);
        mViewCity = (WheelView) view.findViewById(R.id.id_city);
        mViewDistrict = (WheelView) view.findViewById(R.id.id_district);
        mBtnConfirm = (Button) view.findViewById(R.id.btn_confirm);
        btn_cancel = (Button) view.findViewById(R.id.btn_cancel);
    }

    private void setUpListener() {
        // 添加change事件
        mViewProvince.addChangingListener(this);
        // 添加change事件
        mViewCity.addChangingListener(this);
        // 添加change事件
        mViewDistrict.addChangingListener(this);
        // 添加onclick事件
        mBtnConfirm.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
    }

    @Override
    public void onChanged(WheelView wheel, int oldValue, int newValue) {
        if (wheel == mViewProvince) {
            updateCities();
        } else if (wheel == mViewCity) {
            updateAreas();
        } else if (wheel == mViewDistrict) {
            mCurrentDistrictName = mDistrictDatasMap.get(mCurrentCityName)[newValue];
            mCurrentZipCode = mZipcodeDatasMap.get(mCurrentDistrictName);
        }
    }

    private void setUpData() {
        initProvinceDatas();
        mViewProvince.setViewAdapter(new ArrayWheelAdapter<String>(UpdateActivity.this, mProvinceDatas));
        mViewProvince.setVisibleItems(7);
        mViewCity.setVisibleItems(7);
        mViewDistrict.setVisibleItems(7);
        updateCities();
        updateAreas();
    }

    /**
     * 根据当前的省，更新市WheelView的信息
     */
    private void updateCities() {
        int pCurrent = mViewProvince.getCurrentItem();
        mCurrentProviceName = mProvinceDatas[pCurrent];
        String[] cities = mCitisDatasMap.get(mCurrentProviceName);
        if (cities == null) {
            cities = new String[]{""};
        }
        mViewCity.setViewAdapter(new ArrayWheelAdapter<String>(this, cities));
        mViewCity.setCurrentItem(0);
        updateAreas();
    }

    /**
     * 根据当前的市，更新区WheelView的信息
     */
    private void updateAreas() {
        int pCurrent = mViewCity.getCurrentItem();
        mCurrentCityName = mCitisDatasMap.get(mCurrentProviceName)[pCurrent];
        String[] areas = mDistrictDatasMap.get(mCurrentCityName);

        if (areas == null) {
            areas = new String[]{""};
        }
        mViewDistrict.setViewAdapter(new ArrayWheelAdapter<String>(this, areas));
        mViewDistrict.setCurrentItem(0);
    }

    private void GeRenJianJIeDialog() {
        final MyAlertInputDialog myAlertInputDialog = new MyAlertInputDialog(this).builder()
                .setTitle("请输入你的个人简介")
                .setEditText("");
        myAlertInputDialog.setPositiveButton("确认", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GeRenSign = myAlertInputDialog.getResult();
                tvupdateautograph.setText(GeRenSign);
                myAlertInputDialog.dismiss();
            }
        }).setNegativeButton("取消", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAlertInputDialog.dismiss();
            }
        });
        myAlertInputDialog.show();
    }

    private void NameDialog() {
        final MyAlertInputDialog myAlertInputDialog = new MyAlertInputDialog(this).builder()
                .setTitle("请输入你的姓名")
                .setEditText("");
        myAlertInputDialog.setPositiveButton("确认", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GeRenname = myAlertInputDialog.getResult();
                if (GeRenname.equals("")) {
                    Toast.makeText(UpdateActivity.this, "您输入的名字不能为空", Toast.LENGTH_SHORT).show();
                } else {
                    tvupdatenicknameright.setText(GeRenname);
                    myAlertInputDialog.dismiss();
                }

            }
        }).setNegativeButton("取消", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                myAlertInputDialog.dismiss();
            }
        });
        myAlertInputDialog.show();
    }


    /**
     * 照片转byte二进制
     *
     * @param imagepath 需要转byte的照片路径
     * @return 已经转成的byte
     * @throws Exception
     */
    public static byte[] readStream(String imagepath) throws Exception {
        FileInputStream fs = new FileInputStream(imagepath);
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while (-1 != (len = fs.read(buffer))) {
            outStream.write(buffer, 0, len);
        }
        outStream.close();
        fs.close();
        return outStream.toByteArray();
    }


    /**
     * 修改头像按钮执行方法
     *
     * @param view
     */
    public void UpdatePhoto(View view) {

        builder = new AlertDialog.Builder(this);//创建对话框
        inflater = getLayoutInflater();
        layout = inflater.inflate(R.layout.dialog_select_photo, null);//获取自定义布局
        builder.setView(layout);//设置对话框的布局
        dialog1 = builder.create();//生成最终的对话框
        dialog1.show();//显示对话框


        takePhotoTV = layout.findViewById(R.id.photograph);
        choosePhotoTV = layout.findViewById(R.id.photo);
        cancelTV = layout.findViewById(R.id.cancel);
        //设置监听
        takePhotoTV.setOnClickListener(this);
        choosePhotoTV.setOnClickListener(this);
        cancelTV.setOnClickListener(this);
    }

    private void takePhoto() throws IOException {
        Intent intent = new Intent();
        intent.setAction(MediaStore.ACTION_IMAGE_CAPTURE);
        // 获取文件
        File file = createFileIfNeed("UserIcon.png");
        //拍照后原图回存入此路径下
        Uri uri;
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            uri = Uri.fromFile(file);
        } else {
            /**
             * 7.0 调用系统相机拍照不再允许使用Uri方式，应该替换为FileProvider
             * 并且这样可以解决MIUI系统上拍照返回size为0的情况
             */
            uri = FileProvider.getUriForFile(this, "com.xunshan.ZhenSheng.provider", file);
        }
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1);
        startActivityForResult(intent, 1);
    }

    // 在sd卡中创建一保存图片（原图和缩略图共用的）文件夹
    private File createFileIfNeed(String fileName) throws IOException {
        String fileA = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/nbinpic";
        File fileJA = new File(fileA);
        if (!fileJA.exists()) {
            fileJA.mkdirs();
        }
        File file = new File(fileA, fileName);
        if (!file.exists()) {
            file.createNewFile();
        }
        return file;
    }

    /**
     * 打开相册
     */
    private void choosePhoto() {
        //这是打开系统默认的相册(就是你系统怎么分类,就怎么显示,首先展示分类列表)
        Intent picture = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(picture, 2);
    }

    /**
     * 申请权限回调方法
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        if (requestCode == MY_ADD_CASE_CALL_PHONE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                try {
                    takePhoto();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                Toast.makeText(this, "拒绝了你的请求", Toast.LENGTH_SHORT).show();
                //"权限拒绝");
                // TODO: 2018/12/4 这里可以给用户一个提示,请求权限被拒绝了
            }
        }


        if (requestCode == MY_ADD_CASE_CALL_PHONE2) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                choosePhoto();
            } else {
                Toast.makeText(this, "权限拒绝", Toast.LENGTH_SHORT).show();
                //"权限拒绝");
                // TODO: 2018/12/4 这里可以给用户一个提示,请求权限被拒绝了
            }
        }
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * startActivityForResult执行后的回调方法，接收返回的图片
     *
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode != Activity.RESULT_CANCELED) {

            String state = Environment.getExternalStorageState();
            if (!state.equals(Environment.MEDIA_MOUNTED)) return;
            // 把原图显示到界面上
            Tiny.FileCompressOptions options = new Tiny.FileCompressOptions();
            Tiny.getInstance().source(readpic()).asFile().withOptions(options).compress(new FileWithBitmapCallback() {
                @Override
                public void callback(boolean isSuccess, Bitmap bitmap, String outfile, Throwable t) {

                    pictu = bitmapToBase64(compressPixel(outfile));
                    saveImageToServer(bitmap, outfile);//显示图片到imgView上
                }
            });
        } else if (requestCode == 2 && resultCode == Activity.RESULT_OK
                && null != data) {
            try {
                Uri selectedImage = data.getData();//获取路径
                Tiny.FileCompressOptions options = new Tiny.FileCompressOptions();
                Tiny.getInstance().source(selectedImage).asFile().withOptions(options).compress(new FileWithBitmapCallback() {
                    @Override
                    public void callback(boolean isSuccess, Bitmap bitmap, String outfile, Throwable t) {

                        pictu = bitmapToBase64(compressPixel(outfile));
                        saveImageToServer(bitmap, outfile);
                    }
                });
            } catch (Exception e) {
                //"上传失败");
            }
        }
    }

    /**
     * 从保存原图的地址读取图片
     */
    private String readpic() {
        String filePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getAbsolutePath() + "/nbinpic/" + "UserIcon.png";
        return filePath;
    }

    String pictu = "";

    private void saveImageToServer(final Bitmap bitmap, String outfile) {
        temp = new File(outfile);
        // TODO: 2020/5/29  这里就可以将图片文件 file 上传到服务器,上传成功后可以将bitmap设置给你对应的图片展示
        ivHead.setImageBitmap(bitmap);
    }


    private void uploadImage2(File file) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api3.dayushaiwang.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        Map<String, String> map2 = new HashMap<>();
        map2.put("user_id", userID);
        map2.put("token", token);
        map2.put("birthadby", birthaday);
        map2.put("address", GeRenAddRess);
        map2.put("sign", GeRenSign);
        map2.put("name", GeRenname);
        map2.put("sex", sex);
        map2.put("pic", pictu);
        ApiService apiService = retrofit.create(ApiService.class);
        apiService.uploadMemberIcon(map2).enqueue(new Callback<AddGeRenZiLiaoBean>() {
            @Override
            public void onResponse(Call<AddGeRenZiLiaoBean> call, Response<AddGeRenZiLiaoBean> response) {
                LogUtil.e("提交编辑资料成功-->" + new Gson().toJson(response.body()));
                if (response.body().getMsg() != null) {
                    Toast.makeText(UpdateActivity.this, response.body().getMsg(), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<AddGeRenZiLiaoBean> call, Throwable t) {
                LogUtil.e("提交编辑资料失败-->" + new Gson().toJson(call));
            }
        });
    }

    /**
     * bitmap转为base64
     *
     * @param bitmap
     * @return
     */
    public static String bitmapToBase64(Bitmap bitmap) {
        String result = null;
        ByteArrayOutputStream baos = null;
        try {
            if (bitmap != null) {
                baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);

                baos.flush();
                baos.close();

                byte[] bitmapBytes = baos.toByteArray();
                result = Base64.encodeToString(bitmapBytes, Base64.DEFAULT);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (baos != null) {
                    baos.flush();
                    baos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    private Bitmap compressPixel(String filePath) {
        Bitmap bmp = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        //setting inSampleSize value allows to load a scaled down version of the original image
        options.inSampleSize = 4;

        //inJustDecodeBounds set to false to load the actual bitmap
        options.inJustDecodeBounds = false;
        options.inTempStorage = new byte[16 * 1024];
        try {
            //load the bitmap from its path
            bmp = BitmapFactory.decodeFile(filePath, options);
            if (bmp == null) {

                InputStream inputStream = null;
                try {
                    inputStream = new FileInputStream(filePath);
                    BitmapFactory.decodeStream(inputStream, null, options);
                    inputStream.close();
                } catch (FileNotFoundException exception) {
                    exception.printStackTrace();
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
        } catch (OutOfMemoryError exception) {
            exception.printStackTrace();
        } finally {
            return bmp;
        }
    }
}
