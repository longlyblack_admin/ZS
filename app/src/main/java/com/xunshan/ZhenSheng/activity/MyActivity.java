package com.xunshan.ZhenSheng.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.allen.library.SuperTextView;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.tencent.qcloud.tim.uikit.utils.ToastUtil;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.adapter.MyAdapter;
import com.xunshan.ZhenSheng.entity.InsertRoomBean;
import com.xunshan.ZhenSheng.entity.MyPrincipalBean;
import com.xunshan.ZhenSheng.entity.Room;
import com.xunshan.ZhenSheng.entity.RoomInforBean;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.LogUtil;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.RetrofitUtils;
import com.zego.ve.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MyActivity extends AppCompatActivity implements View.OnClickListener, MyAdapter.myCallBack {
    GridView gv_my;
    MyAdapter adapter;
    Room room;
    List<Integer> listRoom;
    List<String> listname;

    ImageView img_myface;
    TextView tv_myname;
    ImageView img_mysex;
    TextView tv_myid;
    TextView tv_mylevel;
    TextView tv_myinfo;
    TextView tv_fans;
    TextView tv_follow;

    LinearLayout LL_myfollow;
    LinearLayout LL_myfans;

    SuperTextView my_setup;
    SuperTextView my_contactus;
    String strBack = "";
    JSONObject obj;
    JSONArray arr;
    private String token;
    private String userID;
    private List<MyPrincipalBean.UserBean> Mylist;

    private String room_id;
    private String room_name;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.my_activity);


        initView();
        initData();
    }

    private void initView() {
        tv_myname = (TextView) findViewById(R.id.tv_myname);
        img_mysex = (ImageView) findViewById(R.id.img_mysex);
        tv_myid = (TextView) findViewById(R.id.tv_myid);
        tv_mylevel = (TextView) findViewById(R.id.tv_mylevel);
        tv_myinfo = (TextView) findViewById(R.id.tv_myinfo);
        tv_follow = (TextView) findViewById(R.id.tv_myfollow);
        tv_fans = (TextView) findViewById(R.id.tv_myfans);
        gv_my = (GridView) findViewById(R.id.gv_my);
        img_myface = (ImageView) findViewById(R.id.img_myface);


        LL_myfollow = findViewById(R.id.LL_myfollow);
        LL_myfans = findViewById(R.id.LL_myfans);

        my_setup = findViewById(R.id.tv_setup);
        my_contactus = findViewById(R.id.tv_contactus);


        my_setup.setOnClickListener(this);
        my_contactus.setOnClickListener(this);
        tv_myinfo.setOnClickListener(this);
        tv_follow.setOnClickListener(this);
        tv_fans.setOnClickListener(this);
        img_myface.setOnClickListener(this);
        LL_myfollow.setOnClickListener(this);
        LL_myfans.setOnClickListener(this);
    }

    private void initData() {
        tv_myid.setText("ID:" + User.user().getUser_ID());
        listRoom = new ArrayList<>();
        listname = new ArrayList();
        listRoom.add(R.mipmap.myroom);
        listRoom.add(R.mipmap.mycollectroom);
        listRoom.add(R.mipmap.myearnings);
        listRoom.add(R.mipmap.myknapsack);
        listname.add("我的房间");
        listname.add("我收藏的房间");
        listname.add("我的收益");
        listname.add("我的背包");

        adapter = new MyAdapter(listRoom, listname, this, this);
        gv_my.setAdapter(adapter);

        intdatainfo();
    }

    @Override
    protected void onResume() {
        super.onResume();
        intdatainfo();
    }

    private void intdatainfo() {
        if (TextUtils.isEmpty(User.user().getUser_ID())) {
            ToastUtil.toastShortMessage("userId==null");
            return;
        }

        if (TextUtils.isEmpty(User.user().getToken())) {
            ToastUtil.toastShortMessage("token==null");
            return;
        }

        Map<String, String> map = new HashMap<>();
        map.put("user_id", User.user().getUser_ID());
        map.put("token", User.user().getToken());
        map.put("touser_id", User.user().getUser_ID());
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/home/index", map, new NetCallBack<MyPrincipalBean>() {
            @Override
            public void onSucceed(final MyPrincipalBean myPrincipalBean, int type) {
                Log.e("myActivity", "home/home/index-->" + new Gson().toJson(myPrincipalBean));
                User.user().setUser_name(myPrincipalBean.getUser().getName());
                User.user().setAddress(myPrincipalBean.getUser().getAddress());
                User.user().setUser_ID(myPrincipalBean.getUser().getUser_id());
                User.user().setUser_level(myPrincipalBean.getUser().getLevel());
                User.user().setSex(myPrincipalBean.getUser().getSex());
                User.user().setBirthaday(myPrincipalBean.getUser().getBirthaday());
                User.user().setSign(myPrincipalBean.getUser().getSign());
                User.user().setUser_Headpic(myPrincipalBean.getUser().getHeadpic());

                tv_follow.setText(myPrincipalBean.getUser().getFollow());
                tv_fans.setText(myPrincipalBean.getUser().getFans());
                tv_myname.setText(myPrincipalBean.getUser().getName());

                Glide.with(MyActivity.this).load(AllInterface.addr + "/" + myPrincipalBean.getUser().getHeadpic()).into(img_myface);
                tv_mylevel.setText(myPrincipalBean.getUser().getLevel());
            }

            @Override
            public void SucceedError(Exception e, int types) {
                LogUtil.e("个人信息获取失败-->"+ e.getMessage());
            }

            @Override
            public void onError(Throwable throwable, int types) {
                LogUtil.e("个人信息获取失败-->"+ throwable.getMessage());
            }
        }, 1);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.LL_myfollow:
                startActivity(new Intent(this, FollowActivity.class));
                break;
            case R.id.LL_myfans:
                startActivity(new Intent(this, FollowActivity.class));
                break;
            case R.id.tv_myinfo:
                Intent it = new Intent(this, UserInfoActivity.class);
                it.putExtra("to_userid", User.user().getUser_ID());
                startActivity(it);
                break;
            case R.id.img_myface:
                break;
            case R.id.tv_setup:
                startActivity(new Intent(this, SetActivity.class));
                break;
            case R.id.tv_contactus:
                startActivity(new Intent(this, ContactUsActivity.class));
                break;
            default:
                break;
        }
    }

    @Override
    public void getPosition(int position) {
        Intent intent = null;
        if (position == 0 || position == 1 || position == 2 || position == 3 || position == 4 || position == 5) {
            if (position == 0) {
                gomyroom();

            } else if (position == 1) {
                intent = new Intent(this, FollowRoomActivity.class);
                startActivity(intent);
            } else if (position == 2) {

                intent = new Intent(this, MyEarnigsActivity.class);
                startActivity(intent);
            } else if (position == 3) {
                intent = new Intent(this, KnapsackActivity.class);
                startActivity(intent);
            }

        }
    }


    private void gomyroom() {
        Map<String, String> map = new HashMap<>();
        map.put("user_id", User.user().getUser_ID());
        map.put("token", User.user().getToken());
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/room/insertRoom", map, new NetCallBack<InsertRoomBean>() {
            @Override
            public void onSucceed(final InsertRoomBean insertRoomBean, int type) {
                room_id = insertRoomBean.getRoom().getRoom_id();
                room_name = insertRoomBean.getRoom().getName();
                getRoomInfo();


            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int types) {
                Toast.makeText(MyActivity.this, "网络请求失败，请稍后重试！", Toast.LENGTH_SHORT).show();
            }
        }, 1);


    }

    private void getRoomInfo() {

        Map<String, String> map = new HashMap<>();
        map.put("user_id", User.user().getUser_ID());
        map.put("token", User.user().getToken());
        map.put("room_id", room_id);


        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/index", map, new NetCallBack<RoomInforBean>() {
            @Override
            public void onSucceed(final RoomInforBean roomInforBean, int type) {


                Intent intent = new Intent(MyActivity.this, RoomActivity.class);
                intent.putExtra("room_id", room_id);
                intent.putExtra("room_name", room_name);
                intent.putExtra("room_userid", roomInforBean.getRoom().getUser_id());
                startActivity(intent);

            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int type) {
            }
        }, 1);

    }


}
