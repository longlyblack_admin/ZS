package com.xunshan.ZhenSheng.activity;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.entity.SearchRoomBean;
import com.xunshan.ZhenSheng.fragment.SearchRoomFragment;
import com.xunshan.ZhenSheng.fragment.SearchUserFragment;
import com.xunshan.ZhenSheng.util.SearchUserBean;

import java.util.ArrayList;
import java.util.List;

public class SearchActivity extends AppCompatActivity {
    ViewPager vp_search;
    List<Fragment> list;
    private EditText etsearchss;
    public String name;
    public String token;
    private TextView tvss;
    public List<SearchRoomBean.DataBean> SearListRoom ;
    public List<SearchUserBean.DataBean> SearListUser;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = getWindow();
            window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }
        setContentView(R.layout.search_activity);
        SharedPreferences sharedPreferences = getSharedPreferences("data", MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");
        initView();

    }
    private void initView(){
        vp_search = (ViewPager) findViewById(R.id.vp_search);
        etsearchss = (EditText) findViewById(R.id.et_search_ss);
        tvss  = (TextView) findViewById(R.id.tv_ss);
        tvss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getSharedPreferences("data", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                name = etsearchss.getText().toString();
                editor.putString("ssroomname", name);
                editor.commit();
                initData();
            }
        });

    }
    private void initData(){
//        Log.e("TAGname", "initData: "+name+token);
//        Map<String, String> map = new HashMap<>();
//        map.put("name", name);
//        map.put("token", token);
//        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/user/quan", map, new NetCallBack<SearchUserBean>() {
//            @Override
//            public void onSucceed(final SearchUserBean searchUserBean, int type) {
//                SearListUser = searchUserBean.getData();
//
//            }
//
//            @Override
//            public void SucceedError(Exception e, int types) {
//            }
//
//            @Override
//            public void onError(Throwable throwable, int type) {
//            }
//        }, 1);

        list = new ArrayList<>();
        list.add(new SearchRoomFragment());
        list.add(new SearchUserFragment());
        vp_search.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return list.get(position);
            }

            @Override
            public int getCount() {
                return list.size();
            }
        });
    }
}
