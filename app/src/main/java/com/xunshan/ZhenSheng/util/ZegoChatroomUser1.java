package com.xunshan.ZhenSheng.util;

import android.content.SharedPreferences;
import android.text.TextUtils;

import androidx.annotation.Nullable;

import com.xunshan.ZhenSheng.application.BaseApplication;
import com.zego.chatroom.manager.entity.ZegoUser;
import com.zego.chatroom.manager.log.ZLog;

import org.json.JSONException;
import org.json.JSONObject;

import static com.xunshan.ZhenSheng.adapter.MyRecyclerViewAdapter.MODE_PRIVATE;

public class ZegoChatroomUser1 implements Cloneable {
    private static final String TAG = com.zego.chatroom.entity.ZegoChatroomUser.class.getSimpleName();
    private static final String ZEGO_USER_ID_KEY = "i";
    private static final String ZEGO_USER_NAME_KEY = "n";
    public String userID;
    public String userName;

    public ZegoChatroomUser1() {
        SharedPreferences sharedPreferences = BaseApplication.sApplication.getSharedPreferences("data",MODE_PRIVATE);
        userName =  sharedPreferences.getString("user_name","");
        userID =  sharedPreferences.getString("user_ID","");
    }

    public boolean isValid() {
        return !TextUtils.isEmpty(this.userID) && !TextUtils.isEmpty(this.userName);
    }

    @Nullable
    public static com.zego.chatroom.entity.ZegoChatroomUser userFromJsonObject(JSONObject jsonObject) {
        if (jsonObject == null) {
            return null;
        } else {
            try {
                com.zego.chatroom.entity.ZegoChatroomUser user = new com.zego.chatroom.entity.ZegoChatroomUser();
                user.userID = jsonObject.getString("i");
                user.userName = jsonObject.getString("n");
                return user;
            } catch (JSONException var2) {
                ZLog.e(TAG, "zegoUserFromJsonObject " + var2.getMessage(), new Object[0]);
                return null;
            }
        }
    }

    @Nullable
    public JSONObject jsonObject() {
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("i", this.userID);
            jsonObject.put("n", this.userName);
            return jsonObject;
        } catch (JSONException var2) {
            ZLog.e(TAG, "jsonObject " + var2.getMessage(), new Object[0]);
            return null;
        }
    }

    public int hashCode() {
        return this.isValid() ? this.userID.hashCode() : -1;
    }

    public boolean equals(Object obj) {
        return this.isValid() && obj instanceof com.zego.chatroom.entity.ZegoChatroomUser && this.hashCode() == obj.hashCode();
    }

    public com.zego.chatroom.entity.ZegoChatroomUser clone() {
        try {
            return (com.zego.chatroom.entity.ZegoChatroomUser)super.clone();
        } catch (CloneNotSupportedException var2) {
            throw new IllegalStateException("CloneNotSupportedException ??");
        }
    }

    public String toString() {
        return "ZegoChatroomUser{userID='" + this.userID + '\'' + ", userName='" + this.userName + '\'' + '}';
    }

    public ZegoUser toZegoUser() {
        ZegoUser user = new ZegoUser();
        user.userID = this.userID;
        user.userName = this.userName;
        return user;
    }

    public static com.zego.chatroom.entity.ZegoChatroomUser initWithZegoUser(ZegoUser user) {
        com.zego.chatroom.entity.ZegoChatroomUser result = new com.zego.chatroom.entity.ZegoChatroomUser();
        result.userID = user.userID;
        result.userName = user.userName;
        return result;
    }
}
