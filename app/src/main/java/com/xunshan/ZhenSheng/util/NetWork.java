package com.xunshan.ZhenSheng.util;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ProtocolException;
import java.net.SocketTimeoutException;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.Headers;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class NetWork {
    static String resut = "";
    static RequestBody body = null;
    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static final MediaType MEDIA_TYPE_MARKDOWN = MediaType
            .parse("text/x-markdown; charset=utf-8");
    static Request request = null;
    static Response response = null;
    public final static int CONNECT_TIMEOUT = 30;
    public final static int READ_TIMEOUT = 30;
    public final static int WRITE_TIMEOUT = 30;
    public static final OkHttpClient client = new OkHttpClient.Builder()
            .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
            .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS).build();

    public static String post(String url, String json) {
        body = RequestBody.create(JSON, json);
        request = new Request.Builder()
                .url(url)
                .post(body).build();
        try {
            response = client.newCall(request).execute();
            if (response.isSuccessful()) {
                resut = response.body().string();
            } else {
                return "" + response;
            }
        } catch (ProtocolException e) {
            resut = "{\"status\":0,\"msg\":\"网络异常\"}";
        } catch (UnknownHostException e) {
            resut = "{\"status\":0,\"msg\":\"连接异常\"}";
        } catch (SocketTimeoutException e) {
            resut = "{\"status\":0,\"msg\":\"连接超时\"}";
        } catch (IOException e) {
            resut = "{\"status\":0,\"msg\":\"操作异常\"}";
        }
        return resut;
    }

    private static String getUserAgent() {

        String userAgent = "";
        StringBuffer sb = new StringBuffer();
        userAgent = System.getProperty("http.agent");//Dalvik/2.1.0 (Linux; U; Android 6.0.1; vivo X9L Build/MMB29M)
        for (int i = 0, length = userAgent.length(); i < length; i++) {
            char c = userAgent.charAt(i);
            if (c <= '\u001f' || c >= '\u007f') {
                sb.append(String.format("\\u%04x", (int) c));
            } else {
                sb.append(c);
            }
        }
        Log.e("User-Agent", "User-Agent: " + sb.toString());
        return sb.toString();
    }
}
