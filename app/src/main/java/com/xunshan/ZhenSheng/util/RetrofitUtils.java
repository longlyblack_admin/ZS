package com.xunshan.ZhenSheng.util;

import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitUtils {
    private static RetrofitUtils retrofitutils;
    private Retrofit retrofit;
    OkHttpClient okHttpClient = null;
    private RetrofitUtils() {
        try {
            okHttpClient = getOkHttpClient();
        } catch (IOException e) {
            e.printStackTrace();
        }
        retrofit = getRetrofit(okHttpClient);
    }

    public static RetrofitUtils getInstace() {
        if (retrofitutils == null) {
            synchronized (RetrofitUtils.class) {
                if (retrofitutils==null){
                    retrofitutils = new RetrofitUtils();
                }

                return retrofitutils;
            }
        }
        return retrofitutils;
    }


    public <T> void postOkHttp(String url, Map<String, String> map, final NetCallBack<T> netCallBack, int types) {
        Observable<ResponseBody> ob = retrofit.create(ApiService.class).sendPost(url, map);
        myObservable(netCallBack, ob, types);
    }

    public <T> void getOkHttp(String url, Map<String, String> map, final NetCallBack<T> netCallBack, int types) {
        Observable<ResponseBody> ob = retrofit.create(ApiService.class).sendGet(url, map);
        myObservable(netCallBack, ob, types);
    }

    public <T> T setCreate(Class<T> apiService) {
        //通过Java动态代理的方式获取动态代理对象
        return getRetrofit(okHttpClient).create(apiService);
    }
    private Retrofit getRetrofit(OkHttpClient okHttpClient) {
        return new Retrofit.Builder().baseUrl(AllInterface.addr)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create()).client(okHttpClient)
                .build();
    }


    private OkHttpClient getOkHttpClient() throws IOException {
        return new OkHttpClient.Builder().build();
    }


    private <T> void myObservable(final NetCallBack<T> netCallBack, Observable<ResponseBody> ob, final int types) {
        ob.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseBody>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }
                    @Override
                    public void onNext(ResponseBody value) {
                        Gson gson = new Gson();
                        Type[] typeInterface = netCallBack.getClass().getGenericInterfaces();
                        Type[] typess = ((ParameterizedType) typeInterface[0]).getActualTypeArguments();
                        try {
                            String string = value.string();
                            T retu_t = gson.fromJson(string, typess[0]);
                            netCallBack.onSucceed(retu_t, types);
                        } catch (Exception e) {
                            e.printStackTrace();
                            netCallBack.SucceedError(e,types);
                            Log.e("数据异常===========",""+e.getMessage());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        netCallBack.onError(e, types);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }


}
