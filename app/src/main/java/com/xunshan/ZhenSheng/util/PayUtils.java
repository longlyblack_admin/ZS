package com.xunshan.ZhenSheng.util;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.alipay.sdk.app.PayTask;
import com.google.gson.Gson;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.xunshan.ZhenSheng.entity.topaybean;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Map;

public class PayUtils {
    private static String TAG = PayUtils.class.getSimpleName();
    /**
     * 支付类型
     */
    public static final int PAY_TYPE_WX = 2;
    public static final int PAY_TYPE_ALI = 3;
    /**
     * 支付宝返回参数
     */
    final static int SDK_PAY_FLAG = 1001;

    private static PayUtils instance;
    private static Context mContext;
    private static Activity mActivity;

    private PayUtils() {
    }

    public static PayUtils getInstance(Context context) {
        if (instance == null) {
            instance = new PayUtils();
        }
        mContext = context;
        mActivity = (Activity) mContext;
        return instance;
    }

    public static void pay(int payType, String result) {
        switch (payType) {
            case PAY_TYPE_WX:
                //微信
                toWXPay(result);
                break;
            //
            case PAY_TYPE_ALI:
                toAliPay(result);
                break;
        }
    }


    /**
     * 微信支付
     *
     * @param result
     */
    private static void toWXPay(String result) {
        //result中是重服务器请求到的签名后各个字符串，可自定义
        //result是服务器返回结果
        if (mActivity == null) {
            return;
        }
        try {

            Gson json = new Gson();
            topaybean bean = json.fromJson(result, topaybean.class);


            // 将该app注册到微信
            final IWXAPI wxapi = WXAPIFactory.createWXAPI(mActivity, bean.getData().getAppid());

            if (!wxapi.isWXAppInstalled()) {
                Toast.makeText(mActivity, "您尚未安装微信客户端", Toast.LENGTH_SHORT).show();
                return;
            }

            PayReq request = new PayReq();
            request.appId = bean.getData().getAppid();
            request.partnerId = bean.getData().getPartnerid();
            request.prepayId = bean.getData().getPrepayid();
            request.packageValue = bean.getData().getPackagevalue();
            request.nonceStr = bean.getData().getNoncestr();
            request.timeStamp = bean.getData().getTimestamp();
            request.sign = bean.getData().getSign();
            request.signType = "MD5";
            wxapi.sendReq(request);

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    /**
     * 支付宝
     */
    private static void toAliPay(String result) {
        //result中是重服务器请求到的签名后字符串,赋值，此处随便写的

        final String orderInfo = result;
        // "app_id=2021001148662160&amp;biz_content=%7B%22body%22%3A%22%5Cu8d2d%5Cu4e70%5Cu94bb%5Cu77f3%22%2C%22subject%22%3A%22%5Cu5145%5Cu503c%22%2C%22out_trade_no%22%3A%22xunshanOR159015141768603680012%22%2C%22timeout_express%22%3A%2230m%22%2C%22total_amount%22%3A6%2C%22product_code%22%3A%22QUICK_MSECURITY_PAY%22%7D&amp;charset=UTF-8&amp;format=json&amp;method=alipay.trade.app.pay&amp;notify_url=http%3A%2F%2Fthink5.com%2Findex.php%2Fhome%2FIdentity%2Fnotify&amp;sign_type=RSA2&amp;timestamp=2020-05-22+20%3A43%3A37&amp;version=1.0&amp;sign=fZKRKRJEqvoWDbqhKlgcnl1YbtiZldVrU7k9d49YNpK4BiP%2BfRXgLUXvHd%2BZ5wRcP8CZ5I3rs5J7zjM0cXnHAXsCScnKPjRthY7gPUCqI%2BbOkeW5s%2F4uqbDsPRdChbEaSeM3DfyWgFMA98F9MJimqeU3D1kFa8W2CRVOXyBC6rOYjNBpVX3EoRLh2AHDoLP4XP2CjTjGXcT0g%2Bjvfdokej7XXdE815whPy0qzumdtpIPqUpSxnT5exwV5902a7IX2LhHCwB56TypS9EbV7DYQ52YDx%2BOeUcllZMejGsmyei65G1%2FJAhaj4d93LAzIO1nX%2BpsGCYuHigDboeXgSV1sA%3D%3D";   // 订单信息
        Runnable payRunnable = new Runnable() {

            @Override
            public void run() {
                PayTask alipay = new PayTask(mActivity);
                Map<String, String> result = alipay.payV2(orderInfo, true);
                Message msg = new Message();
                msg.what = SDK_PAY_FLAG;
                msg.obj = result;
                mHandler.sendMessage(msg);

                Log.i(TAG, "aliresult--->" + result);
            }
        };
        // 必须异步调用
        Thread payThread = new Thread(payRunnable);
        payThread.start();
    }

    /**
     * 支付宝状态
     * 9000 订单支付成功
     * 8000 正在处理中，支付结果未知（有可能已经支付成功），请查询商户订单列表中订单的支付状态
     * 4000 订单支付失败
     * 5000 重复请求
     * 6001 用户中途取消
     * 6002 网络连接出错
     * 6004 支付结果未知（有可能已经支付成功），请查询商户订单列表中订单的支付状态
     * 其它   其它支付错误
     */
    private static Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case SDK_PAY_FLAG: {
                    PayResult payResult = new PayResult((Map<String, String>) msg.obj);
                    /**
                     对于支付结果，请商户依赖服务端的异步通知结果。同步通知结果，仅作为支付结束的通知。
                     */
                    String resultInfo = payResult.getResult();// 同步返回需要验证的信息
                    String resultStatus = payResult.getResultStatus();
                    // 判断resultStatus 为9000则代表支付成功
                    if (TextUtils.equals(resultStatus, "9000")) {
                        // 该笔订单是否真实支付成功，需要依赖服务端的异步通知。
                        showMessage("支付成功");
                        PayListenerUtils.getInstance(mContext).addSuccess();
                    } else if (TextUtils.equals(resultStatus, "6001")) {
                        // 该笔订单是否真实支付成功，需要依赖服务端的异步通知。
                        showMessage("取消");
                        PayListenerUtils.getInstance(mContext).addCancel();
                    } else {
                        // 该笔订单真实的支付结果，需要依赖服务端的异步通知。
                        showMessage("支付失败");
                        PayListenerUtils.getInstance(mContext).addError();
                    }
                    break;
                }
            }
        }


    };

    private static void showMessage(String str) {
        Toast.makeText(mContext, str, Toast.LENGTH_SHORT).show();
    }
}
