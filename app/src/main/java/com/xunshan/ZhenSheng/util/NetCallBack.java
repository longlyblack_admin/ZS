package com.xunshan.ZhenSheng.util;

import com.xunshan.ZhenSheng.entity.HoomListBean;

/**
 * Created by Administrator on 2019/7/30.
 */

public interface NetCallBack<T> {
    void onSucceed(T t, int types);


    void SucceedError(Exception e, int types);
    void onError(Throwable throwable, int types);
}
