package com.xunshan.ZhenSheng.util;


import com.xunshan.ZhenSheng.entity.AddGeRenZiLiaoBean;
import com.xunshan.ZhenSheng.entity.AddPicBean;
import com.xunshan.ZhenSheng.entity.BaseResponse;

import java.util.Map;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;
import retrofit2.http.Url;


public interface ApiService {

    @GET
    Observable<ResponseBody> sendGet(@Url String url, @QueryMap Map<String, String> map);

    @FormUrlEncoded
    @POST
    Observable<ResponseBody> sendPost(@Url String url, @FieldMap Map<String, String> map);


    @FormUrlEncoded
    @POST("home/home/addpic")
    Call<AddPicBean> uploadImages(@FieldMap  Map<String, String> map1);

    /**
     * 上传头像
     */
//    @Multipart
//    @POST("https://api3.dayushaiwang.com/home/home/edit")
//    Call<AddGeRenZiLiaoBean> uploadMemberIcon(@Query("user_id") String id,
//                                              @Query("token") String token,
//                                              @Query("birthaday") String birthadby,
//                                              @Query("address") String address,
//                                              @Query("sign") String sign,
//                                              @Query("name") String name,
//                                              @Query("sex") String sex,
//                                              @Part List<MultipartBody.Part> map);

    /**
     * 上传头像
     * home/home/android
     * 2020.06.02 vic 修改
     */

    @POST("home/home/edit")
    @FormUrlEncoded
    Call<AddGeRenZiLiaoBean> uploadMemberIcon(@FieldMap  Map<String, String> map1);


//
//    //图片上传
//    @Multipart
//    @POST("Reimburse/Upload_Files.ashx")
//    Call<AddImageBean> addImage(@Part List<MultipartBody.Part> file);
//
//    //头像上传
//    @Multipart
//    @POST(UrlConfig.UPDATE_NICK_PHOTO)
//    Call<HeadBean> addheadImage(@Query("userGuid") String userGuid, @Query("user") String user, @Part List<MultipartBody.Part> file);
//
//    //工作圈上传
//    @Multipart
//    @POST(UrlConfig.Add_GongZuoQuan)
//    Call<HeadBean> addfileImage(@Part List<MultipartBody.Part> file);
//
//    //实名验证
//    @Multipart
//    @POST(UrlConfig.ShiMingRenZheng)
//    Call<HeadBean> addshimingImage(@Part List<MultipartBody.Part> file);*/
//
//
////    //添加 企业滚动图
////    @Multipart
////    @POST(UrlConfig.QiYeXuanChuanADD)
////    Call<HeadBean> addgdtImage(@Query("userGuid") String userGuid, @Query("companyId") String companyId,@Query("url") String url,@Query("title") String title, @Query("sort") String sort,@Part List<MultipartBody.Part> file);
////
////    //修改 企业滚动图
////    @Multipart
////    @POST(UrlConfig.QiYeXuanChuanUPDATE)
////    Call<HeadBean> updategdtImage(@Query("userGuid") String userGuid, @Query("companyId") String companyId,@Query("url") String url,@Query("title") String title, @Query("sort") String sort,@Query("imageId") String imageId,@Part List<MultipartBody.Part> file);


    public static final String SERVICE_PATH = "https://api3.dayushaiwang.com/";

    @FormUrlEncoded
    @POST(SERVICE_PATH + "home/home/edit")
    Observable<BaseResponse> editUserInfo(
            @Field("token") String token,
            @Field("user_id") String user_id,
            @Field("pic") String pic,
            @Field("sex") String sex,
            @Field("birthday") String birthday,
            @Field("address") String address,
            @Field("sign") String sign,
            @Field("name") String name);


}
