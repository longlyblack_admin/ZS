package com.xunshan.ZhenSheng.util;

import java.util.List;

public class SearchUserBean {

    /**
     * data : [{"user_id":"390071","name":"小鲜肉","sex":"0","level":"9","sign":"222","headpic":"static/update/1.png","status":"0","charm":"128","follow":"1"}]
     * msg : ok
     * err : 0
     */

    private String msg;
    private String err;
    private List<DataBean> data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * user_id : 390071
         * name : 小鲜肉
         * sex : 0
         * level : 9
         * sign : 222
         * headpic : static/update/1.png
         * status : 0
         * charm : 128
         * follow : 1
         */

        private String user_id;
        private String name;
        private String sex;
        private String level;
        private String sign;
        private String headpic;
        private String status;
        private String charm;
        private String follow;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getLevel() {
            return level;
        }

        public void setLevel(String level) {
            this.level = level;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }

        public String getHeadpic() {
            return headpic;
        }

        public void setHeadpic(String headpic) {
            this.headpic = headpic;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCharm() {
            return charm;
        }

        public void setCharm(String charm) {
            this.charm = charm;
        }

        public String getFollow() {
            return follow;
        }

        public void setFollow(String follow) {
            this.follow = follow;
        }
    }
}
