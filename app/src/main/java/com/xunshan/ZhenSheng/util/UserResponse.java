package com.xunshan.ZhenSheng.util;

import com.xunshan.ZhenSheng.entity.BaseResponse;
import com.xunshan.ZhenSheng.user.User;

import io.reactivex.Observable;
public class UserResponse {

    //    public static UserResponse instance;
    //
    //    public static UserResponse getInstance(){
    //        if (instance == null) {
    //            instance = new UserResponse();
    //        }
    //        return instance;
    //    }

    private final ApiService mApiService;

    private UserResponse(ApiService apiService) {
        mApiService = apiService;
    }


    /**
     * 编辑个人信息
     * @param token
     * @param user_id
     * @param pic
     * @param sex
     * @param birthday
     * @param address
     * @param sign
     * @param name
     * @return
     */
    public Observable<BaseResponse> editUserInfo(String token,String user_id,String pic,
                                                 String sex,String birthday,String address,
                                                 String sign,String name) {
        return mApiService.editUserInfo(token, user_id,pic,sex,birthday,address,sign,name);
    }

}
