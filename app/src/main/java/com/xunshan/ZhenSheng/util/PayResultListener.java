package com.xunshan.ZhenSheng.util;

public interface PayResultListener {
    public void onPaySuccess();

    public void onPayError();

    public void onPayCancel();
}
