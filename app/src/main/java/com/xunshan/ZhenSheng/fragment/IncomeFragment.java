package com.xunshan.ZhenSheng.fragment;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.adapter.MyEarnigsAdapter;
import com.xunshan.ZhenSheng.entity.Earnigs;
import com.xunshan.ZhenSheng.entity.MyEarningsBean;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.RetrofitUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 收入
 */
public class IncomeFragment extends Fragment {
    View v;
    ListView lv_income_fragment;
    Earnigs earnigs;
    List<MyEarningsBean.DataBean> listBean;
    MyEarnigsAdapter adapter;
    Calendar c = null;
    DatePickerDialog dialog;
    ImageView img_income_fragment_start;
    ImageView img_income_fragment_end;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.income_fragment, null);
        initView(v);
        return v;
    }

    private void initView(View v) {
        lv_income_fragment = (ListView) v.findViewById(R.id.lv_income_fragment);
        initData();
    }

    private void initData() {
        Map<String, String> map = new HashMap<>();
        map.put("status", "1");
        map.put("token", User.user().getToken());
        map.put("user_id", User.user().getUser_ID());
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/profit/profit", map, new NetCallBack<MyEarningsBean>() {
            @Override
            public void onSucceed(final MyEarningsBean myEarningsBean, int type) {
                listBean = myEarningsBean.getData();
                adapter = new MyEarnigsAdapter(listBean, getContext());
                lv_income_fragment.setAdapter(adapter);
            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int type) {
            }
        }, 1);

    }
}
