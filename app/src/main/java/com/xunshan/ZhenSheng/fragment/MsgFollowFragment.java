package com.xunshan.ZhenSheng.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.adapter.MyUserFollowAdapter;
import com.xunshan.ZhenSheng.entity.MyUserFollow;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.RetrofitUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by ywl on 2016/6/27.
 */
public class MsgFollowFragment extends Fragment{
    private RecyclerView rvmyuserfollow;
    private MyUserFollowAdapter myUserFollowAdapter;
    private List<MyUserFollow.DataBean> listMyFollow;
    View v;
    private String token;
    private String userID;
    private LinearLayoutManager linearLayoutManager;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.msg_follow_fragment, null);
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("data", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        token = sharedPreferences.getString("token", "");
        userID = sharedPreferences.getString("user_ID", "");
        initView(v);
        initData();
        return v;

    }


    private void initView(View view) {
        rvmyuserfollow = (RecyclerView) view.findViewById(R.id.rv_my_user_follow);


    }
    private void initData() {
        Map<String, String> map = new HashMap<>();
        map.put("user_id", userID);
        map.put("token", token);
        Log.e("TAGonClick", "onClick: " + token + userID);
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/user/user_follow", map, new NetCallBack<MyUserFollow>() {
            @Override
            public void onSucceed(final MyUserFollow myUserFollow, int type) {
                    listMyFollow = myUserFollow.getData();
                    Log.e("TAGonClick", "onClick: " +listMyFollow);
                    linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                    rvmyuserfollow.setLayoutManager(linearLayoutManager);
                    rvmyuserfollow.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL));
                    myUserFollowAdapter = new MyUserFollowAdapter(getContext(),listMyFollow);
                    rvmyuserfollow.setAdapter(myUserFollowAdapter);
                Toast.makeText(getContext(), myUserFollow.getMsg(), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int type) {
            }
        }, 1);

    }




}
