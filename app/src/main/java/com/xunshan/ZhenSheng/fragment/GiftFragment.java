package com.xunshan.ZhenSheng.fragment;


import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.adapter.GiftAdapter;
import com.xunshan.ZhenSheng.entity.Gift;
import com.xunshan.ZhenSheng.entity.Photo;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.NetWork;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * 礼物墙
 */
public class GiftFragment extends Fragment {
    GridView gv_usergift;
    View v;
    Gift gift;
    List<Gift> list;
    GiftAdapter adapter;
    String strBack = "";
    JSONObject obj;
    JSONArray arr;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.gift_fragment, null);
        initView(v);
        initData();
        return v;
    }

    private void initView(View v) {
        gv_usergift = (GridView) v.findViewById(R.id.gv_usergift);
    }

    private void initData() {
        try {
            strBack = NetWork.post(AllInterface.giftWall + "?userid=1", "");
            obj = new JSONObject(strBack);
            if (obj.getBoolean("success")) {
                arr = obj.getJSONArray("data");
                list = new ArrayList<>();
                for (int i = 0; i < arr.length(); i++) {
                    obj = arr.getJSONObject(i);
                    gift = new Gift();
                    gift.setgId(Integer.parseInt(obj.getString("id")));
                    gift.setgName(obj.getString("giftname"));
                    gift.setgImg(AllInterface.addr + "/" + obj.getString("imageurl"));
                    int number = obj.getInt("num");
                    gift.setgNum(number);
                    list.add(gift);
                }
                list.add(gift);
                list.add(gift);
                list.add(gift);
                adapter = new GiftAdapter(list, getContext());
                gv_usergift.setAdapter(adapter);
            }
        } catch (Exception e) {
            Log.e("giftwall", "" + e);
        }

    }
}