package com.xunshan.ZhenSheng.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.adapter.FollowAdapter;
import com.xunshan.ZhenSheng.entity.MySouSouBean;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.RetrofitUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class FollowFragment extends Fragment {
    View v;
    ListView lv_follow;
//    User user;
//    List<User> list;
//    FollowAdapter adapter;
    private EditText et_ss_guanzu_or_fans;
    private TextView tv_ss_fansorguanzu;
    SharedPreferences sharedPreferences;
    private String namessflloworfan;
    FollowAdapter adapter;
    public List<MySouSouBean.DataBean> SearListFansRoom ;
    private ImageView ivssfolloww;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.follow_fragment, null);
        initView(v);
        initData();
        return v;
    }

    private void initView(View v) {
        lv_follow = (ListView) v.findViewById(R.id.lv_follow);
        et_ss_guanzu_or_fans = v.findViewById(R.id.et_ss_guanzu_or_fans);
        tv_ss_fansorguanzu  = v.findViewById(R.id.tv_ss_fansorguanzu);

        tv_ss_fansorguanzu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getContext().getSharedPreferences("data", MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                namessflloworfan = et_ss_guanzu_or_fans.getText().toString();
                editor.putString("et_ss_guanzu_or_fansss", namessflloworfan);
                editor.commit();
                initData();
            }
        });

    }

    private void initData() {
        Map<String, String> map = new HashMap<>();
        map.put("name", namessflloworfan);
        map.put("token", User.user().getToken());
        map.put("user_id", User.user().getUser_ID());
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/user/sousuo", map, new NetCallBack<MySouSouBean>() {
            @Override
            public void onSucceed(final MySouSouBean mySouSouBean, int type) {
                SearListFansRoom = mySouSouBean.getData();
            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int type) {
            }
        }, 1);
        adapter = new FollowAdapter(getContext(),SearListFansRoom);
        lv_follow.setAdapter(adapter);
    }
}
