package com.xunshan.ZhenSheng.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.adapter.FollowRoomAdapter;
import com.xunshan.ZhenSheng.entity.SearchRoomBean;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.RetrofitUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class SearchRoomFragment extends Fragment {
    ListView lv_searchroom;
    public List<SearchRoomBean.DataBean> SearListRoom ;
    FollowRoomAdapter adapter;
    public String name;
    public String token;
    View v;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.searchroom_fragment, null);
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("data", MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");
        name = sharedPreferences.getString("ssroomname", "");
        initView(v);
        initData();
        return v;
    }

    private void initView(View v) {
        lv_searchroom = (ListView) v.findViewById(R.id.lv_searchroom);
    }

    private void initData() {
        Map<String, String> map = new HashMap<>();
        map.put("name", name);
        map.put("token", token);
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/user/quanroom", map, new NetCallBack<SearchRoomBean>() {
            @Override
            public void onSucceed(final SearchRoomBean searchRoomBean, int type) {
                SearListRoom = searchRoomBean.getData();
            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int type) {
            }
        }, 1);
        adapter = new FollowRoomAdapter(getContext(),SearListRoom);
        lv_searchroom.setAdapter(adapter);
    }
}
