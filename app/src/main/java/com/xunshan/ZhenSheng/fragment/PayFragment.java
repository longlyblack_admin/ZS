package com.xunshan.ZhenSheng.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.activity.RoomActivity;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.PayPwdView;
import com.xunshan.ZhenSheng.util.PwdInputMethodView;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by Laiyimin on 2017/4/20.
 */

public class PayFragment extends DialogFragment implements View.OnClickListener {

    public static final String EXTRA_CONTENT = "extra_content";    //提示框内容
    public static final String PASS_CONTENT = "pass_content";

    private PayPwdView psw_input;
    private PayPwdView.InputCallBack inputCallBack;
    private String extracontent;
    private String passcontent;
    SharedPreferences sharedPreferences;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // 使用不带Theme的构造器, 获得的dialog边框距离屏幕仍有几毫米的缝隙。
        Dialog dialog = new Dialog(getActivity(), R.style.BottomDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // 设置Content前设定
        dialog.setContentView(R.layout.fragment_pay);
        dialog.setCanceledOnTouchOutside(false); // 外部点击取消

        // 设置宽度为屏宽, 靠近屏幕底部。
        final Window window = dialog.getWindow();
        window.setWindowAnimations(R.style.AnimBottom);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        final WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT; // 宽度持平
        lp.gravity = Gravity.TOP;
        window.setAttributes(lp);

        initView(dialog);

        return dialog;


    }

    private void initView(Dialog dialog) {
        Bundle bundle = getArguments();
        psw_input = (PayPwdView) dialog.findViewById(R.id.payPwdView);
        extracontent = bundle.getString(EXTRA_CONTENT);
        passcontent = bundle.getString(PASS_CONTENT);
        sharedPreferences = getContext().getSharedPreferences("data", Context.MODE_PRIVATE);

        if (bundle != null) {
            TextView tv_content = (TextView) dialog.findViewById(R.id.tv_content);
            tv_content.setText(extracontent + " ");
        }

        PwdInputMethodView inputMethodView = (PwdInputMethodView) dialog.findViewById(R.id.inputMethodView);
        psw_input.setInputMethodView(inputMethodView);
        psw_input.setInputCallBack(inputCallBack);
        dialog.findViewById(R.id.iv_close).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_close:
                dismiss();
                break;

        }
    }


    public void isOk(String room_userid) {
        if (User.user().getRoompassword() != null) {

            md5("a8r7wyr!$#@#^$&^*)(*&^5qW_QW_EWQE_W@!##!$&^$#^#" + User.user().getRoompassword());
            if (passcontent.equals(md5("a8r7wyr!$#@#^$&^*)(*&^5qW_QW_EWQE_W@!##!$&^$#^#" + User.user().getRoompassword()))) {
                Intent intent = new Intent(getActivity(), RoomActivity.class);

                intent.putExtra("room_id", sharedPreferences.getString("room_id", ""));
                intent.putExtra("user_id", sharedPreferences.getString("user_ID", ""));
                intent.putExtra("room_name", sharedPreferences.getString("room_name", ""));
                intent.putExtra("Over_pipe", sharedPreferences.getString("Over_pipe", ""));
                intent.putExtra("room_userid", room_userid);

                startActivity(intent);

                dismiss();

            } else {
                Toast.makeText(getActivity(), "房间密码错误", Toast.LENGTH_SHORT).show();
            }

        }


    }


    public static String md5(String string) {
        byte[] hash;
        try {
            hash = MessageDigest.getInstance("MD5").digest(string.getBytes("UTF-8"));
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Huh, MD5 should be supported?", e);
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Huh, UTF-8 should be supported?", e);
        }
        StringBuilder hex = new StringBuilder(hash.length * 2);
        for (byte b : hash) {
            if ((b & 0xFF) < 0x10) hex.append("0");
            hex.append(Integer.toHexString(b & 0xFF));
        }
        return hex.toString();
    }


    /**
     * 设置输入回调
     *
     * @param inputCallBack
     */
    public void setPaySuccessCallBack(PayPwdView.InputCallBack inputCallBack) {

        this.inputCallBack = inputCallBack;

    }


}
