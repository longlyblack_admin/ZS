package com.xunshan.ZhenSheng.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.adapter.MyUserFansAdapter;
import com.xunshan.ZhenSheng.entity.MyUserFansBean;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.RetrofitUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by ywl on 2016/6/27.
 */
public class MsgFansFragment extends Fragment {
    private RecyclerView rvmyuserfans;
    private MyUserFansAdapter myUserFansAdapter;
    private List<MyUserFansBean.DataBean> listMyFans;
    View v;
    private String token;
    private String userID;
    private LinearLayoutManager linearLayoutManager;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.msg_fans_fragment, null);
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("data", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        token = sharedPreferences.getString("token", "");
        userID = sharedPreferences.getString("user_ID", "");
        initView(v);
        initData();
        return v;
    }

    private void initData() {
        Map<String, String> map = new HashMap<>();
        map.put("user_id", userID);
        map.put("token", token);
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/user/user_fans", map, new NetCallBack<MyUserFansBean>() {
            @Override
            public void onSucceed(final MyUserFansBean myUserFansBean, int type) {
                listMyFans = myUserFansBean.getData();
                linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
                rvmyuserfans.setLayoutManager(linearLayoutManager);
                rvmyuserfans.addItemDecoration(new DividerItemDecoration(getContext(),DividerItemDecoration.VERTICAL));
                myUserFansAdapter = new MyUserFansAdapter(getContext(),listMyFans);
                rvmyuserfans.setAdapter(myUserFansAdapter);
            }
            @Override
            public void SucceedError(Exception e, int types) {
            }
            @Override
            public void onError(Throwable throwable, int type) {
            }
        }, 1);
    }

    private void initView(View v) {
        rvmyuserfans = (RecyclerView) v.findViewById(R.id.rv_my_user_fans);
    }
}
