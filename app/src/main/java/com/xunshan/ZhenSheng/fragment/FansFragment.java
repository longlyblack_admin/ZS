package com.xunshan.ZhenSheng.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.adapter.FollowFansAdapter;
import com.xunshan.ZhenSheng.entity.MySouSouBean;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.RetrofitUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class FansFragment extends Fragment {
    View v;
    ListView lv_fans;
    User user;
    List<User> list;
    public List<MySouSouBean.DataBean> SearListFansRoom ;
    private FollowFansAdapter adapter;
    private String ssr;
 

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fans_fragment,null);
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("data", MODE_PRIVATE);
        ssr = sharedPreferences.getString("et_ss_guanzu_or_fansss", "");
        initView(v);
        initData();
        return v;
    }
    private void initView(View v){
        lv_fans = (ListView) v.findViewById(R.id.lv_fans);
    }
    private void initData(){
        Map<String, String> map = new HashMap<>();
        map.put("name", ssr);
        map.put("token", User.user().getToken());
        map.put("user_id", User.user().getUser_ID());
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/user/sousuo", map, new NetCallBack<MySouSouBean>() {
            @Override
            public void onSucceed(final MySouSouBean mySouSouBean, int type) {
                SearListFansRoom = mySouSouBean.getData();
            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int type) {
            }
        }, 1);
        adapter = new FollowFansAdapter(getContext(),SearListFansRoom);
        lv_fans.setAdapter(adapter);
    }
}
