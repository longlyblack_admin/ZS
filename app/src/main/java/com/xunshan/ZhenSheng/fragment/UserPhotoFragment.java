package com.xunshan.ZhenSheng.fragment;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.SnapHelper;

import com.huantansheng.easyphotos.EasyPhotos;
import com.huantansheng.easyphotos.callback.SelectCallback;
import com.huantansheng.easyphotos.models.album.entity.Photo;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.activity.MyDialog;
import com.xunshan.ZhenSheng.adapter.MainAdapter;
import com.xunshan.ZhenSheng.adapter.UserPhotoAdapter;
import com.xunshan.ZhenSheng.entity.PicAlbumWallBean;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.GlideEngine;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.RetrofitUtils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import okhttp3.OkHttpClient;

import static android.content.Context.MODE_PRIVATE;

public class UserPhotoFragment extends Fragment {
    View v;
    GridView gvuserinfo;
//    MainAdapter adapter;
    String strBack = "";
    JSONObject obj;
    JSONArray arr;
    private List<PicAlbumWallBean.DataBean> list;
    private String token;
    private ArrayList<HashMap<String, Object>> imageItem;
    private Bitmap bmp; // 导入临时图片
    private SimpleAdapter simpleAdapter; // 适配器
    private MyDialog dialog;// 图片选择对话框
    public static final int NONE = 0;
    public static final int PHOTOHRAPH = 1;// 拍照
    public static final int PHOTOZOOM = 2; // 缩放
    public static final int PHOTORESOULT = 3;// 结果
    public static final String IMAGE_UNSPECIFIED = "image/*";
    private TextView mTextView;
    private static final int REQUEST_SELECT_IMAGES_CODE = 0x01;
    private ArrayList<String> mImagePaths;
    private ImageView img;
    private Button btselectimages;
    private RecyclerView rvImage;
    private MainAdapter adapter;
    private String path;
    private byte[] pic;
    private String userID;

    private OkHttpClient okHttpClient;
    /**
     * 选择的图片集
     */
    private ArrayList<Photo> selectedPhotoList = new ArrayList<>();




    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        v = inflater.inflate(R.layout.userphoto_fragment, null);
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("data", MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");
        userID = sharedPreferences.getString("user_ID", "");

        initView(v);
        initData();

        return v;

    }


    private void initView(View v) {
//        mTextView = v.findViewById(R.id.tv_select_images);
//        img = v.findViewById(R.id.img);
        btselectimages = v.findViewById(R.id.bt_select_images);
        rvImage =  v.findViewById(R.id.rv_image);
        List<File> listTP = new ArrayList<>();
        GridLayoutManager layoutManager = new GridLayoutManager(getContext(), 3, LinearLayoutManager.VERTICAL, false);



        adapter = new MainAdapter(getContext(), selectedPhotoList);
        SnapHelper snapHelper = new PagerSnapHelper();

        btselectimages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                EasyPhotos.createAlbum((FragmentActivity) getContext(), true, GlideEngine.getInstance())//参数说明：上下文，是否显示相机按钮，[配置Glide为图片加载引擎](https://github.com/HuanTanSheng/EasyPhotos/wiki/12-%E9%85%8D%E7%BD%AEImageEngine%EF%BC%8C%E6%94%AF%E6%8C%81%E6%89%80%E6%9C%89%E5%9B%BE%E7%89%87%E5%8A%A0%E8%BD%BD%E5%BA%93)
                        .setFileProviderAuthority("com.xunshan.ZhenSheng.provider")//参数说明：见下方`FileProvider的配置`
                        .setCount(22)//参数说明：最大可选数，默认1
                        .start(new SelectCallback() {
                            @Override
                            public void onResult(ArrayList<Photo> photos, boolean isOriginal) {
                                for (int i = 0; i <photos.size() ; i++) {
                                    String pa = photos.get(i).path;
                                    File file = new File(pa);
                                    listTP.add(file);
                                 }
//                                uploadImage2(listTP);
                                selectedPhotoList.clear();
                                selectedPhotoList.addAll(photos);
                                adapter.notifyDataSetChanged();
                                rvImage.smoothScrollToPosition(0);
                            }
                        });
                rvImage.setLayoutManager(layoutManager);
                rvImage.setAdapter(adapter);
                snapHelper.attachToRecyclerView(rvImage);
            }
        });
    }

    private void initData() {
        Map<String, String> map = new HashMap<>();
        map.put("user_id", User.user().getUser_ID());
        map.put("token", token);
        map.put("currentPage", "1");
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/home/pic", map, new NetCallBack<PicAlbumWallBean>() {
            @Override
            public void onSucceed(final PicAlbumWallBean picAlbumWallBean, int type) {
                list = picAlbumWallBean.getData();
                // 2.为数据源设置适配器
                UserPhotoAdapter adapter = new UserPhotoAdapter(list, getContext());
                // 3.将适配过后点数据显示在GridView 上
                gvuserinfo.setAdapter(adapter);

            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int type) {
            }
        }, 1);
    }

    public static byte[] readStream(String imagepath) throws Exception {
        FileInputStream fs = new FileInputStream(imagepath);
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while (-1 != (len = fs.read(buffer))) {
            outStream.write(buffer, 0, len);
        }
        outStream.close();
        fs.close();
        return outStream.toByteArray();
    }
    public static void readBin2Image(byte[] byteArray, String targetPath) {
        InputStream in = new ByteArrayInputStream(byteArray);
        File file = new File(targetPath);
        String path = targetPath.substring(0, targetPath.lastIndexOf("/"));
        if (!file.exists()) {
            new File(path).mkdir();
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            int len = 0;
            byte[] buf = new byte[1024];
            while ((len = in.read(buf)) != -1) {
                fos.write(buf, 0, len);
            }
            fos.flush();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != fos) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
//    private void uploadImage2(List<File> compressFile) {
//        //多张图片
//        List<MultipartBody.Part> map = new ArrayList<>();
//        if (compressFile != null && !compressFile.isEmpty()) {
//            for (int i = 0; i < compressFile.size(); i++) {
//                File file = compressFile.get(i);
//                MultipartBody.Builder builder = new MultipartBody.Builder()
//                        .setType(MultipartBody.FORM);//表单类型
//                RequestBody imageBody = RequestBody.create(MediaType.parse("image/jpeg"),file);
//                builder.addFormDataPart("image_"+i, file.getName(), imageBody);//imgfile 后台接收图片流的参数名
//                map = builder.build().parts();
//            }
//        }
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl("https://api3.dayushaiwang.com/home/home/addpic/")
//                .addConverterFactory(GsonConverterFactory.create())
//                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
//                .build();
//        ApiService apiService = retrofit.create(ApiService.class);
//
//        apiService.uploadImages(
//                User.user().getUser_ID(),
//                User.user().getToken(),
//                map).enqueue(new Callback<AddPicBean>() {
//            @Override
//            public void onResponse(Call<AddPicBean> call, Response<AddPicBean> response) {
//                Toast.makeText(getContext(),  response.body().getMsg(), Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onFailure(Call<AddPicBean> call, Throwable t) {
//            }
//        });
//    }






    }
