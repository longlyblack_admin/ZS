package com.xunshan.ZhenSheng.fragment;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.tencent.qcloud.tim.uikit.component.video.util.DeviceUtil;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.adapter.ViewPagerAdapter;
import com.xunshan.ZhenSheng.util.NavitationLayout;

import java.util.ArrayList;
import java.util.List;

public class DialogFragmentUpper extends DialogFragment {
    View view;
    private ViewPagerAdapter viewPagerAdapter11;


    private String[] titles1 = new String[]{"麦上全员", "麦上申请列表"};

    public DialogFragmentUpper() {
    }


    public static DialogFragmentUpper newInstance(String tittle) {
        DialogFragmentUpper fragment = new DialogFragmentUpper();
        Bundle bundle = new Bundle();
        bundle.putString("tittle", tittle);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
//        Dialog dialog = getDialog();
//        if (dialog != null) {
//            DisplayMetrics dm = new DisplayMetrics();
//            getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
//            dialog.getWindow().setLayout((int) (dm.widthPixels * 0.90), ViewGroup.LayoutParams.WRAP_CONTENT);
//            dialog.getWindow().setLayout((int) (dm.heightPixels * 0.40), ViewGroup.LayoutParams.WRAP_CONTENT);
//        }
//        getDialog().getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        Window window = getDialog().getWindow();
        WindowManager.LayoutParams params = window.getAttributes();
        params.gravity = Gravity.BOTTOM;
        params.dimAmount = 0f;
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(params);
        window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.dialog_upper_wheat_apply, container, false);
        NavitationLayout navitationLayout = view.findViewById(R.id.bar_upper_wheat_apply_title);
        ViewPager viewPager = view.findViewById(R.id.vp_upper_wheat_apply_id);
        List<Fragment> listfragment = new ArrayList();
        //麦上全员
        listfragment.add(new UpperWheatApplyFragment1());
        //上买申请列表
        listfragment.add(new UpperWheatApplyFragment());
        viewPagerAdapter11 = new ViewPagerAdapter(getChildFragmentManager(), listfragment);
        viewPager.setAdapter(viewPagerAdapter11);
        navitationLayout.setViewPager(getContext(), titles1, viewPager, R.color.color_777777, R.color.color_333333, 18, 20, 0, 0, true);
        navitationLayout.setBgLine(getContext(), 1, R.color.white);
        navitationLayout.setNavLine(getActivity(), 1, R.color.color_zi, 0);
        slideToUp(view);
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        initSoftInputListener();


        return view;
    }


    public static void slideToUp(View view) {
        Animation slide = new TranslateAnimation(Animation.RELATIVE_TO_SELF, 0.0f,
                Animation.RELATIVE_TO_SELF, 0.0f, Animation.RELATIVE_TO_SELF,
                1.0f, Animation.RELATIVE_TO_SELF, 0.0f);
        slide.setDuration(400);
        slide.setFillAfter(true);
        slide.setFillEnabled(true);
        view.startAnimation(slide);
        slide.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
    }


//    @Override
//    public void onResume() {
//
//        ViewGroup.LayoutParams params = getDialog().getWindow().getAttributes();
//        params.width = WindowManager.LayoutParams.WRAP_CONTENT;
//        params.height = WindowManager.LayoutParams.WRAP_CONTENT;
//        getDialog().getWindow().setAttributes((WindowManager.LayoutParams) params);
//        super.onResume();
//    }


    @Override

    public void dismiss() {

        View view = getDialog().getCurrentFocus();

        if (view instanceof TextView) {

            InputMethodManager mInputMethodManager = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);

            mInputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);

        }

        super.dismiss();

    }

    private void initSoftInputListener() {

        getDialog().getWindow().getDecorView()

                .setOnTouchListener(new View.OnTouchListener() {

                    @Override

                    public boolean onTouch(View view, MotionEvent event) {

                        InputMethodManager manager = (InputMethodManager) getActivity()

                                .getSystemService(Context.INPUT_METHOD_SERVICE);

                        if (event.getAction() == MotionEvent.ACTION_DOWN) {

                            if (getDialog().getCurrentFocus() != null

                                    && getDialog().getCurrentFocus().getWindowToken() != null) {

                                manager.hideSoftInputFromWindow(

                                        getDialog().getCurrentFocus().getWindowToken(),

                                        InputMethodManager.HIDE_NOT_ALWAYS);

                            }

                        }

                        return false;

                    }

                });

    }


}
