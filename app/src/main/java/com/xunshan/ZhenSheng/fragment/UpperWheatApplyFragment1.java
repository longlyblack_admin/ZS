package com.xunshan.ZhenSheng.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.activity.RoomActivity;
import com.xunshan.ZhenSheng.adapter.MyItemRecyclerViewAdapter1;
import com.xunshan.ZhenSheng.entity.UpperWheatApplyBean;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.RetrofitUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UpperWheatApplyFragment1 extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private List<UpperWheatApplyBean.DataBean>  mUpperListener;
    private  String room_ID;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public UpperWheatApplyFragment1() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static UpperWheatApplyFragment1 newInstance(int columnCount) {
        UpperWheatApplyFragment1 fragment = new UpperWheatApplyFragment1();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initData();
    }

    private void initData() {
                Map<String, String> map = new HashMap<>();
                map.put("token", User.user().getToken());
                map.put("room_id", room_ID);
                RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/keylist", map, new NetCallBack<UpperWheatApplyBean>() {
                    @Override
                    public void onSucceed(final UpperWheatApplyBean upperWheatApplyBean, int type) {
                        if (upperWheatApplyBean.getErr().equals("0")) {
                            mUpperListener = upperWheatApplyBean.getData();

                        }
                    }

                    @Override
                    public void SucceedError(Exception e, int types) {
                    }

                    @Override
                    public void onError(Throwable throwable, int type) {
                    }
                }, 1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_upper_wheat_apply_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(new MyItemRecyclerViewAdapter1(mUpperListener,getContext()));
        }
        return view;
    }



    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        room_ID = ((RoomActivity) activity).getRoom_ID();
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}
