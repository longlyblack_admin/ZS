package com.xunshan.ZhenSheng.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.adapter.SearchUserAdapter;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.RetrofitUtils;
import com.xunshan.ZhenSheng.util.SearchUserBean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class SearchUserFragment extends Fragment {

    ListView lv_searchuser_fragment;
    public List<SearchUserBean.DataBean> SearListUser ;
    SearchUserAdapter adapter;
    public String name;
    public String token;
    View v;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.searchuser_fragment, null);
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("data", MODE_PRIVATE);
        token = sharedPreferences.getString("token", "");
        name = sharedPreferences.getString("ssroomname", "");
        Log.e("TAGtokenname", "onCreateView: "+token+name );
        initView(v);
        initData();
        return v;
    }
    private void initView(View v) {
        lv_searchuser_fragment = (ListView) v.findViewById(R.id.lv_searchuser_fragment);
    }

    private void initData() {
        Log.e("TAGnametoken", "initData: "+name+token);
        Map<String, String> map = new HashMap<>();
        map.put("name", name);
        map.put("token", token);
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/user/quanju", map, new NetCallBack<SearchUserBean>() {
            @Override
            public void onSucceed(final SearchUserBean searchUserBean, int type) {
                SearListUser = searchUserBean.getData();
                Log.e("TAGSearListUser", "onSucceed: "+SearListUser );

            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int type) {
            }
        }, 1);
        Log.e("TAGSearListUser1", "onSucceed: "+SearListUser );
        adapter = new SearchUserAdapter( getContext(),SearListUser);
        lv_searchuser_fragment.setAdapter(adapter);
    }
}
