package com.xunshan.ZhenSheng.fragment;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.activity.RoomActivity;
import com.xunshan.ZhenSheng.adapter.MyItemRecyclerViewAdapter;
import com.xunshan.ZhenSheng.entity.UpperWheatApplyBean;
import com.xunshan.ZhenSheng.entity.delapplybean;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.RetrofitUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class UpperWheatApplyFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    RecyclerView rvUpperwheatApply;
    private List<UpperWheatApplyBean.DataBean> mUpperListener;
    private String room_ID;
    LinearLayoutManager layoutManager;
    MyItemRecyclerViewAdapter myItemRecyclerViewAdapter;
    View v;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.fragment_upper_wheat_apply_list, null);
        initView(v);
        initData();

        return v;
    }


    private void initView(View v) {
        rvUpperwheatApply = v.findViewById(R.id.rv_Upper_wheat_Apply);
    }


    private void initData() {
        Map<String, String> map = new HashMap<>();
        map.put("token", User.user().getToken());
        map.put("room_id", room_ID);
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/room/keylist", map, new NetCallBack<UpperWheatApplyBean>() {
            @Override
            public void onSucceed(final UpperWheatApplyBean upperWheatApplyBean, int type) {
                mUpperListener = upperWheatApplyBean.getData();
                if (upperWheatApplyBean.getErr().equals("0")) {
                    layoutManager = new LinearLayoutManager(getContext());
                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    rvUpperwheatApply.setLayoutManager(layoutManager);
                    myItemRecyclerViewAdapter = new MyItemRecyclerViewAdapter(mUpperListener, getContext());
                    rvUpperwheatApply.setAdapter(myItemRecyclerViewAdapter);

                    myItemRecyclerViewAdapter.setItemClickListener(new MyItemRecyclerViewAdapter.OnItemClickListener() {
                        @Override
                        public void onItemClick(int did, String User_Id) {
                            shangmai(User_Id);


                        }
                    });


                }
            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int type) {
            }
        }, 1);


    }


    private void shangmai(String uerid) {

        Map<String, String> map = new HashMap<>();
        map.put("token", User.user().getToken());
        map.put("room_id", room_ID);
        map.put("user_id", uerid);
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/home/room/delapply", map, new NetCallBack<delapplybean>() {
            @Override
            public void onSucceed(final delapplybean delapplybean, int type) {

                if (delapplybean.getErr().equals("0")) {
                    Toast.makeText(getActivity(), delapplybean.getData(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity(), delapplybean.getMsg(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int type) {
            }
        }, 1);


        myItemRecyclerViewAdapter.notifyDataSetChanged();

    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        room_ID = ((RoomActivity) activity).getRoom_ID();
    }
}
