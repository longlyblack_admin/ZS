package com.xunshan.ZhenSheng.application;

import android.app.Application;
import android.content.Context;
import android.os.Build;

import androidx.annotation.RequiresApi;
import androidx.multidex.MultiDex;

import com.liys.doubleclicklibrary.DoubleClickHelper;
import com.lzy.okgo.OkGo;
import com.tencent.imsdk.TIMSdkConfig;
import com.tencent.qcloud.tim.uikit.TUIKit;
import com.tencent.qcloud.tim.uikit.config.CustomFaceConfig;
import com.tencent.qcloud.tim.uikit.config.GeneralConfig;
import com.tencent.qcloud.tim.uikit.config.TUIKitConfigs;
import com.xunshan.ZhenSheng.util.LogUtil;

public class BaseApplication extends Application {
    private final static String TAG = BaseApplication.class.getSimpleName();
    public static Context sApplication;

    public static Context applicationContext;
    public static final int SDKAPPID = 1400369294; // 您的 SDKAppID

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        sApplication = base;

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onCreate() {
        MultiDex.install(this);
        super.onCreate();
        applicationContext = this;

        OkGo.init(this);
        LogUtil.init(true);

        // 配置 Config，请按需配置
        TUIKitConfigs configs = TUIKit.getConfigs();
        configs.setSdkConfig(new TIMSdkConfig(SDKAPPID));
        configs.setCustomFaceConfig(new CustomFaceConfig());
        configs.setGeneralConfig(new GeneralConfig());

        TUIKit.init(this, SDKAPPID, configs);
        // V3.0采用单例模式
        DoubleClickHelper
                .getInstance()
                .delayTime(1000);  //间隔时间


    }


    public static Context getContext() {
        return applicationContext;
    }


}
