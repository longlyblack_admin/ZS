package com.xunshan.ZhenSheng.entity;

import java.util.List;

public class BannerLunBoBean {


    /**
     * msg : ok
     * err : 0
     * data : {"pic":[{"id":1,"pic":"static/update/chou.png","url":"","name":"1","pushtype":"0","dic":{"headName":"深海探险","url":"https://api3.dayushaiwang.com/baoxiang/index.html"}},{"id":2,"pic":"static/update/jingwang1.png","url":"","name":"2","pushtype":"0","dic":{"headName":"","url":""}}],"txt":[{"user_id":"415180","name":"文","text":"恭喜你中奖了","pushtype":"0"},{"user_id":"415160","name":"字","text":"早安","pushtype":"0"}],"label":["热门","男神","女神","点唱","娱乐","陪玩"]}
     * usersig : eJwtzF0LgjAYhuH-stNCXm0fTejEKAiiSOugw9mWvVQyp6wo*u-J9PC5Hri-5LgtIm8cSUkSAZmGjdrUHV4xsOCUMz4*rb4ra1GTNKYAMy4TSYfHvC060ztjLAGAQTt8BpNABZVCjhWs*vCq2el1perYyHzS7tFv2PLw4Fmm7PlyK2z5efncNXPFTgvy*wPXcjE6
     * identity : 1
     * headName_url : https://api3.dayushaiwang.com/baoxiang/realname.html
     */

    private String msg;
    private String err;
    private DataBean data;
    private String usersig;
    private String identity;
    private String headName_url;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getUsersig() {
        return usersig;
    }

    public void setUsersig(String usersig) {
        this.usersig = usersig;
    }

    public String getIdentity() {
        return identity;
    }

    public void setIdentity(String identity) {
        this.identity = identity;
    }

    public String getHeadName_url() {
        return headName_url;
    }

    public void setHeadName_url(String headName_url) {
        this.headName_url = headName_url;
    }

    public static class DataBean {
        private List<PicBean> pic;
        private List<TxtBean> txt;
        private List<String> label;

        public List<PicBean> getPic() {
            return pic;
        }

        public void setPic(List<PicBean> pic) {
            this.pic = pic;
        }

        public List<TxtBean> getTxt() {
            return txt;
        }

        public void setTxt(List<TxtBean> txt) {
            this.txt = txt;
        }

        public List<String> getLabel() {
            return label;
        }

        public void setLabel(List<String> label) {
            this.label = label;
        }

        public static class PicBean {
            /**
             * id : 1
             * pic : static/update/chou.png
             * url :
             * name : 1
             * pushtype : 0
             * dic : {"headName":"深海探险","url":"https://api3.dayushaiwang.com/baoxiang/index.html"}
             */

            private int id;
            private String pic;
            private String url;
            private String name;
            private String pushtype;
            private DicBean dic;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getPic() {
                return pic;
            }

            public void setPic(String pic) {
                this.pic = pic;
            }

            public String getUrl() {
                return url;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getPushtype() {
                return pushtype;
            }

            public void setPushtype(String pushtype) {
                this.pushtype = pushtype;
            }

            public DicBean getDic() {
                return dic;
            }

            public void setDic(DicBean dic) {
                this.dic = dic;
            }

            public static class DicBean {
                /**
                 * headName : 深海探险
                 * url : https://api3.dayushaiwang.com/baoxiang/index.html
                 */

                private String headName;
                private String url;

                public String getHeadName() {
                    return headName;
                }

                public void setHeadName(String headName) {
                    this.headName = headName;
                }

                public String getUrl() {
                    return url;
                }

                public void setUrl(String url) {
                    this.url = url;
                }
            }
        }

        public static class TxtBean {
            /**
             * user_id : 415180
             * name : 文
             * text : 恭喜你中奖了
             * pushtype : 0
             */

            private String user_id;
            private String name;
            private String text;
            private String pushtype;

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getText() {
                return text;
            }

            public void setText(String text) {
                this.text = text;
            }

            public String getPushtype() {
                return pushtype;
            }

            public void setPushtype(String pushtype) {
                this.pushtype = pushtype;
            }
        }
    }
}
