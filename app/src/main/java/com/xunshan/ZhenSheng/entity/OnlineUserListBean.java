package com.xunshan.ZhenSheng.entity;

import java.util.List;

public class OnlineUserListBean {


    /**
     * data : [{"user_id":"390071","name":"小鲜肉","level":"9","sex":"0","headpic":"static/update/5.png","charm":"3","giftnum":"0","sign":"222"}]
     * msg : ok
     * err : 0
     */

    private String msg;
    private String err;
    private List<DataBean> data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * user_id : 390071
         * name : 小鲜肉
         * level : 9
         * sex : 0
         * headpic : static/update/5.png
         * charm : 3
         * giftnum : 0
         * sign : 222
         */

        private String user_id;
        private String name;
        private String level;
        private String sex;
        private String headpic;
        private String charm;
        private String giftnum;
        private String sign;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLevel() {
            return level;
        }

        public void setLevel(String level) {
            this.level = level;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getHeadpic() {
            return headpic;
        }

        public void setHeadpic(String headpic) {
            this.headpic = headpic;
        }

        public String getCharm() {
            return charm;
        }

        public void setCharm(String charm) {
            this.charm = charm;
        }

        public String getGiftnum() {
            return giftnum;
        }

        public void setGiftnum(String giftnum) {
            this.giftnum = giftnum;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }
    }
}
