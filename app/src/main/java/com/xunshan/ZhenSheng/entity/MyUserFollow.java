package com.xunshan.ZhenSheng.entity;

import java.util.List;

public class MyUserFollow {

    /**
     * data : [{"user_id":"938728","name":"贾增辉","sex":"0","level":"100","charm":"519","sign":"","headpic":"static/update/1.png","ishaoyou":"0"},{"user_id":"686036","name":"Hhhh","sex":"0","level":"218","charm":"106","sign":"","headpic":"static/update/2.png","ishaoyou":"0"}]
     * msg : ok
     * err : 0
     */

    private String msg;
    private String err;
    private List<DataBean> data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * user_id : 938728
         * name : 贾增辉
         * sex : 0
         * level : 100
         * charm : 519
         * sign :
         * headpic : static/update/1.png
         * ishaoyou : 0
         */

        private String user_id;
        private String name;
        private String sex;
        private String level;
        private String charm;
        private String sign;
        private String headpic;
        private String ishaoyou;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getLevel() {
            return level;
        }

        public void setLevel(String level) {
            this.level = level;
        }

        public String getCharm() {
            return charm;
        }

        public void setCharm(String charm) {
            this.charm = charm;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }

        public String getHeadpic() {
            return headpic;
        }

        public void setHeadpic(String headpic) {
            this.headpic = headpic;
        }

        public String getIshaoyou() {
            return ishaoyou;
        }

        public void setIshaoyou(String ishaoyou) {
            this.ishaoyou = ishaoyou;
        }
    }
}
