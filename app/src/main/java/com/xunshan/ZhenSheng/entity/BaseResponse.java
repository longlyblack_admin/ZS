package com.xunshan.ZhenSheng.entity;


import java.io.Serializable;
import java.util.List;

public class BaseResponse<T> implements Serializable {
    private String msg;
    private List<T> room_id;
    private String err;
    private T user;
    private String isguanzhu;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<T> getRoom_id() {
        return room_id;
    }

    public void setRoom_id(List<T> room_id) {
        this.room_id = room_id;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public T getUser() {
        return user;
    }

    public void setUser(T user) {
        this.user = user;
    }

    public String getIsguanzhu() {
        return isguanzhu;
    }

    public void setIsguanzhu(String isguanzhu) {
        this.isguanzhu = isguanzhu;
    }
}

