package com.xunshan.ZhenSheng.entity;

import com.google.gson.annotations.SerializedName;

public class topaybean {


    /**
     * err : 0
     * data : {"prepayid":"wx2322033106054277a03c4d5b1676801200","appid":"wx37688b96e5a81d05","partnerid":"1588031911","package":"Sign=WXPay","noncestr":"JrUJP88ZdfkunTa0EizcmUHbfmRi78rW","timestamp":"1590242610","sign":"51406FC053CBB54E5EBAB76C389BE638"}
     * type : 2
     */

    private int err;
    private DataBean data;
    private String type;

    public int getErr() {
        return err;
    }

    public void setErr(int err) {
        this.err = err;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static class DataBean {
        /**
         * prepayid : wx2322033106054277a03c4d5b1676801200
         * appid : wx37688b96e5a81d05
         * partnerid : 1588031911
         * package : Sign=WXPay
         * noncestr : JrUJP88ZdfkunTa0EizcmUHbfmRi78rW
         * timestamp : 1590242610
         * sign : 51406FC053CBB54E5EBAB76C389BE638
         */

        private String zhifubao;

        private String prepayid;
        private String appid;
        private String partnerid;
        private String packagevalue;
        private String noncestr;
        private String timestamp;
        private String sign;

        public String getZhifubao() {
            return zhifubao;
        }

        public void setZhifubao(String zhifubao) {
            this.zhifubao = zhifubao;
        }

        public String getPrepayid() {
            return prepayid;
        }

        public void setPrepayid(String prepayid) {
            this.prepayid = prepayid;
        }

        public String getAppid() {
            return appid;
        }

        public void setAppid(String appid) {
            this.appid = appid;
        }

        public String getPartnerid() {
            return partnerid;
        }

        public void setPartnerid(String partnerid) {
            this.partnerid = partnerid;
        }

        public String getPackagevalue() {
            return packagevalue;
        }

        public void setPackagevalue(String packagevalue) {
            this.packagevalue = packagevalue;
        }

        public String getNoncestr() {
            return noncestr;
        }

        public void setNoncestr(String noncestr) {
            this.noncestr = noncestr;
        }

        public String getTimestamp() {
            return timestamp;
        }

        public void setTimestamp(String timestamp) {
            this.timestamp = timestamp;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }
    }
}
