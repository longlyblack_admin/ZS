package com.xunshan.ZhenSheng.entity;


import java.util.List;

public class MyPrincipalBean {


    /**
     * msg : ok
     * room_id : 123456
     * err : 0
     * user : {"address":"","charm":"136","user_id":"938728","level":"0","sex":"1","birthaday":"2020-04-24","name":"贾增辉","sign":"","follow":"7","headpic":"static/update/1.png","fans":"0"}
     * isguanzhu : 0
     */

    private String msg;
    private String room_id;
    private String err;
    private UserBean user;
    private String isguanzhu;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getRoom_id() {
        return room_id;
    }

    public void setRoom_id(String room_id) {
        this.room_id = room_id;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public String getIsguanzhu() {
        return isguanzhu;
    }

    public void setIsguanzhu(String isguanzhu) {
        this.isguanzhu = isguanzhu;
    }

    public static class UserBean {
        /**
         * address :
         * charm : 136
         * user_id : 938728
         * level : 0
         * sex : 1
         * birthaday : 2020-04-24
         * name : 贾增辉
         * sign :
         * follow : 7
         * headpic : static/update/1.png
         * fans : 0
         */

        private String address;
        private String charm;
        private String user_id;
        private String level;
        private String sex;
        private String birthaday;
        private String name;
        private String sign;
        private String follow;
        private String headpic;
        private String fans;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCharm() {
            return charm;
        }

        public void setCharm(String charm) {
            this.charm = charm;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getLevel() {
            return level;
        }

        public void setLevel(String level) {
            this.level = level;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getBirthaday() {
            return birthaday;
        }

        public void setBirthaday(String birthaday) {
            this.birthaday = birthaday;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }

        public String getFollow() {
            return follow;
        }

        public void setFollow(String follow) {
            this.follow = follow;
        }

        public String getHeadpic() {
            return headpic;
        }

        public void setHeadpic(String headpic) {
            this.headpic = headpic;
        }

        public String getFans() {
            return fans;
        }

        public void setFans(String fans) {
            this.fans = fans;
        }
    }
}
