package com.xunshan.ZhenSheng.entity;

import java.util.List;

public class SearchRoomBean {

    /**
     * data : [{"name":"张三","user_id":"686036","room_id":"654321","type":"0","marr":"[\"1\",\"0\",\"0\",\"0\",\"1\",\"0\",\"1\",\"1\"]","pic":"static/update/2.png","content":"系统公告:官方倡导绿色互动，请勿发布违法、低俗、暴力、广告等内容，禁止违规交易，违规者将被封禁账号。","roomtake":"娱乐娱乐娱乐娱乐娱乐娱乐娱乐"},{"name":"张三","user_id":"273187","room_id":"159753","type":"0","marr":"[\"0\",\"1\",\"1\",\"0\",\"1\",\"0\",\"0\",\"1\"]","pic":"static/update/10.png","content":"系统公告:官方倡导绿色互动，请勿发布违法、低俗、暴力、广告等内容，禁止违规交易，违规者将被封禁账号。","roomtake":"娱乐娱乐娱乐娱乐娱乐娱乐娱乐"}]
     * msg : ok
     * err : 0
     */

    private String msg;
    private String err;
    private List<DataBean> data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * name : 张三
         * user_id : 686036
         * room_id : 654321
         * type : 0
         * marr : ["1","0","0","0","1","0","1","1"]
         * pic : static/update/2.png
         * content : 系统公告:官方倡导绿色互动，请勿发布违法、低俗、暴力、广告等内容，禁止违规交易，违规者将被封禁账号。
         * roomtake : 娱乐娱乐娱乐娱乐娱乐娱乐娱乐
         */

        private String name;
        private String user_id;
        private String room_id;
        private String type;
        private String marr;
        private String pic;
        private String content;
        private String roomtake;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getRoom_id() {
            return room_id;
        }

        public void setRoom_id(String room_id) {
            this.room_id = room_id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getMarr() {
            return marr;
        }

        public void setMarr(String marr) {
            this.marr = marr;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getRoomtake() {
            return roomtake;
        }

        public void setRoomtake(String roomtake) {
            this.roomtake = roomtake;
        }
    }
}
