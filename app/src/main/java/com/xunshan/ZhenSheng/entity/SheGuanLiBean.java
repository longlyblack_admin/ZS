package com.xunshan.ZhenSheng.entity;

public class SheGuanLiBean {

    /**
     * err : 0
     * data : 设置成功
     */

    private String err;
    private String data;

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
