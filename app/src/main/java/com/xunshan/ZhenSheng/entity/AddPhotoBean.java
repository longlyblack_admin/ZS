package com.xunshan.ZhenSheng.entity;

public class AddPhotoBean {

    /**
     * msg : ok
     * err : 0
     * status : 0
     */

    private String msg;
    private String err;
    private String status;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
