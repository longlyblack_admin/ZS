package com.xunshan.ZhenSheng.entity;

public class KnapsackClassify {
    private int kId;
    private String kName;

    public int getkId() {
        return kId;
    }

    public void setkId(int kId) {
        this.kId = kId;
    }

    public String getkName() {
        return kName;
    }

    public void setkName(String kName) {
        this.kName = kName;
    }
}
