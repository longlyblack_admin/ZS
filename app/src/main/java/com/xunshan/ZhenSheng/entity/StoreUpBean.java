package com.xunshan.ZhenSheng.entity;

import java.util.List;

public class StoreUpBean {

    /**
     * data : [{"name":"张三","room_id":"654321","status":"1","number":"1574641","type":"0","pic":"static/update/1.png","islock":"0"},{"name":"贾增辉的直播间","room_id":"123456","status":"1","number":"2147483647","type":"0","pic":"static/update/1.png","islock":"0"}]
     * msg : ok
     * err : 0
     */

    private String msg;
    private String err;
    private List<DataBean> data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * name : 张三
         * room_id : 654321
         * status : 1
         * number : 1574641
         * type : 0
         * pic : static/update/1.png
         * islock : 0
         */

        private String name;
        private String room_id;
        private String status;
        private String number;
        private String type;
        private String pic;
        private String islock;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getRoom_id() {
            return room_id;
        }

        public void setRoom_id(String room_id) {
            this.room_id = room_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getIslock() {
            return islock;
        }

        public void setIslock(String islock) {
            this.islock = islock;
        }
    }
}
