package com.xunshan.ZhenSheng.entity;

import java.util.List;

public class GiftWallBean {

    /**
     * msg : ok
     * data : [{"giftname":"金蛋","date":"2020-05-29 18:05:46","number":"20","id":"2","pic":"jindan"}]
     * err : 0
     */

    private String msg;
    private String err;
    private List<DataBean> data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * giftname : 金蛋
         * date : 2020-05-29 18:05:46
         * number : 20
         * id : 2
         * pic : jindan
         */

        private String giftname;
        private String date;
        private String number;
        private String id;
        private String pic;

        public String getGiftname() {
            return giftname;
        }

        public void setGiftname(String giftname) {
            this.giftname = giftname;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }
    }
}
