package com.xunshan.ZhenSheng.entity;

public class PhoenLoginBean {

    /**
     * err : 0
     * msg : 登陆成功
     * isone : 1
     * token : OGU3NDViYjE5MGMzNThiMmMxZjEwOWY5ZDA5YjU5MjA
     * user_id : 182406
     * name : 张泽昊
     * level : 0
     * sex : 0
     * headpic : static/update/1.png
     * sign : 这个人很懒，什么也没有留下
     */

    private int err;
    private String msg;
    private int isone;
    private String token;
    private int user_id;
    private String name;
    private int level;
    private int sex;
    private String headpic;
    private String sign;
    private String usersig;

    public String getUsersig() {
        return usersig;
    }

    public void setUsersig(String usersig) {
        this.usersig = usersig;
    }

    public int getErr() {
        return err;
    }

    public void setErr(int err) {
        this.err = err;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getIsone() {
        return isone;
    }

    public void setIsone(int isone) {
        this.isone = isone;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getHeadpic() {
        return headpic;
    }

    public void setHeadpic(String headpic) {
        this.headpic = headpic;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }
}
