package com.xunshan.ZhenSheng.entity;

import java.util.List;

public class InsertRoomBean {


    /**
     * msg : ok
     * err : 0
     * room : {"name":"789900","user_id":"764656","room_id":"789900","type":"0","marr":["0","0","0","0","0","0","0","0"],"pic":"static/update/1.png","content":"系统公告:官方倡导绿色互动，请勿发布违法、低俗、暴力、广告等内容，禁止违规交易，违规者将被封禁账号。","roomtake":""}
     * user : {"user_id":"764656","name":"小鲜肉","level":"181","sex":"0","headpic":"static/update/1.png","charm":"240","giftnum":"0","sign":"这个人很懒，什么也没有留下!","comein":"static/update/150jitexiao.png","headbox":""}
     * room_auth : []
     */

    private String msg;
    private String err;
    private RoomBean room;
    private UserBean user;
    private List<?> room_auth;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public RoomBean getRoom() {
        return room;
    }

    public void setRoom(RoomBean room) {
        this.room = room;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public List<?> getRoom_auth() {
        return room_auth;
    }

    public void setRoom_auth(List<?> room_auth) {
        this.room_auth = room_auth;
    }

    public static class RoomBean {
        /**
         * name : 789900
         * user_id : 764656
         * room_id : 789900
         * type : 0
         * marr : ["0","0","0","0","0","0","0","0"]
         * pic : static/update/1.png
         * content : 系统公告:官方倡导绿色互动，请勿发布违法、低俗、暴力、广告等内容，禁止违规交易，违规者将被封禁账号。
         * roomtake :
         */

        private String name;
        private String user_id;
        private String room_id;
        private String type;
        private String pic;
        private String content;
        private String roomtake;
        private List<String> marr;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getRoom_id() {
            return room_id;
        }

        public void setRoom_id(String room_id) {
            this.room_id = room_id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getRoomtake() {
            return roomtake;
        }

        public void setRoomtake(String roomtake) {
            this.roomtake = roomtake;
        }

        public List<String> getMarr() {
            return marr;
        }

        public void setMarr(List<String> marr) {
            this.marr = marr;
        }
    }

    public static class UserBean {
        /**
         * user_id : 764656
         * name : 小鲜肉
         * level : 181
         * sex : 0
         * headpic : static/update/1.png
         * charm : 240
         * giftnum : 0
         * sign : 这个人很懒，什么也没有留下!
         * comein : static/update/150jitexiao.png
         * headbox :
         */

        private String user_id;
        private String name;
        private String level;
        private String sex;
        private String headpic;
        private String charm;
        private String giftnum;
        private String sign;
        private String comein;
        private String headbox;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLevel() {
            return level;
        }

        public void setLevel(String level) {
            this.level = level;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getHeadpic() {
            return headpic;
        }

        public void setHeadpic(String headpic) {
            this.headpic = headpic;
        }

        public String getCharm() {
            return charm;
        }

        public void setCharm(String charm) {
            this.charm = charm;
        }

        public String getGiftnum() {
            return giftnum;
        }

        public void setGiftnum(String giftnum) {
            this.giftnum = giftnum;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }

        public String getComein() {
            return comein;
        }

        public void setComein(String comein) {
            this.comein = comein;
        }

        public String getHeadbox() {
            return headbox;
        }

        public void setHeadbox(String headbox) {
            this.headbox = headbox;
        }
    }
}
