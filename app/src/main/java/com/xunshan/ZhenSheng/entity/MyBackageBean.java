package com.xunshan.ZhenSheng.entity;

import java.util.List;

public class MyBackageBean {

    /**
     * msg : ok
     * gift : [{"number":"62","gifid":"5","money":"52","name":"比心","pic":"static/update/bixin.png","type":"0"}]
     * head : [{"name":"炫彩头像框","pic":"static/update/xingyuetouxiangkuang.gif","id":"32","time":"155","type":"1"}]
     * err : 0
     */

    private String msg;
    private String err;
    private List<GiftBean> gift;
    private List<HeadBean> head;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public List<GiftBean> getGift() {
        return gift;
    }

    public void setGift(List<GiftBean> gift) {
        this.gift = gift;
    }

    public List<HeadBean> getHead() {
        return head;
    }

    public void setHead(List<HeadBean> head) {
        this.head = head;
    }

    public static class GiftBean {
        /**
         * number : 62
         * gifid : 5
         * money : 52
         * name : 比心
         * pic : static/update/bixin.png
         * type : 0
         */

        private String number;
        private String gifid;
        private String money;
        private String name;
        private String pic;
        private String type;

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getGifid() {
            return gifid;
        }

        public void setGifid(String gifid) {
            this.gifid = gifid;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }

    public static class HeadBean {
        /**
         * name : 炫彩头像框
         * pic : static/update/xingyuetouxiangkuang.gif
         * id : 32
         * time : 155
         * type : 1
         */

        private String name;
        private String pic;
        private String id;
        private String time;
        private String type;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }
    }
}
