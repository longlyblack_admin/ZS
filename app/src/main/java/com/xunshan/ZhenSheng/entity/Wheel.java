package com.xunshan.ZhenSheng.entity;

public class Wheel {
    private int wId;
    private String wUrl;
    private String wRoomId;
    private String wImg;


    public int getwId() {
        return wId;
    }

    public void setwId(int wId) {
        this.wId = wId;
    }

    public String getwUrl() {
        return wUrl;
    }

    public void setwUrl(String wUrl) {
        this.wUrl = wUrl;
    }

    public String getwRoomId() {
        return wRoomId;
    }

    public void setwRoomId(String wRoomId) {
        this.wRoomId = wRoomId;
    }

    public String getwImg() {
        return wImg;
    }

    public void setwImg(String wImg) {
        this.wImg = wImg;
    }
}
