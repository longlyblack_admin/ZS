package com.xunshan.ZhenSheng.entity;


import android.os.Parcelable;

import java.io.Serializable;
import java.util.List;

public class HoomListBean {


    /**
     * data : [{"user_id":"415180","room_id":"567774","status":"1","number":"10","pic":"static/update/2.png","name":"世界上最好的人是你"},{"user_id":"415180","room_id":"567774","status":"1","number":"10","pic":"static/update/2.png","name":"世界上最好的人是你"},{"user_id":"415180","room_id":"567774","status":"1","number":"10","pic":"static/update/2.png","name":"世界上最好的人是你"},{"user_id":"415180","room_id":"567774","status":"1","number":"10","pic":"static/update/2.png","name":"世界上最好的人是你"},{"user_id":"415180","room_id":"567774","status":"1","number":"10","pic":"static/update/2.png","name":"世界上最好的人是你"},{"user_id":"415180","room_id":"567774","status":"1","number":"10","pic":"static/update/2.png","name":"世界上最好的人是你"},{"user_id":"415180","room_id":"567774","status":"1","number":"10","pic":"static/update/2.png","name":"世界上最好的人是你"},{"user_id":"415180","room_id":"567774","status":"1","number":"10","pic":"static/update/2.png","name":"世界上最好的人是你"},{"user_id":"415180","room_id":"567774","status":"1","number":"10","pic":"static/update/2.png","name":"世界上最好的人是你"},{"user_id":"415180","room_id":"567774","status":"1","number":"10","pic":"static/update/2.png","name":"世界上最好的人是你"},{"user_id":"415180","room_id":"567774","status":"1","number":"10","pic":"static/update/2.png","name":"世界上最好的人是你"},{"user_id":"415180","room_id":"567774","status":"1","number":"10","pic":"static/update/2.png","name":"世界上最好的人是你"},{"user_id":"415180","room_id":"567774","status":"1","number":"10","pic":"static/update/2.png","name":"世界上最好的人是你"},{"user_id":"415180","room_id":"567774","status":"1","number":"10","pic":"static/update/2.png","name":"世界上最好的人是你"},{"user_id":"415180","room_id":"412228","status":"1","number":"1","pic":"static/update/1.png","name":"世界上最好的人是你"},{"user_id":"415180","room_id":"412228","status":"1","number":"1","pic":"static/update/1.png","name":"世界上最好的人是你"},{"user_id":"415180","room_id":"412228","status":"1","number":"1","pic":"static/update/1.png","name":"世界上最好的人是你"},{"user_id":"415180","room_id":"412228","status":"1","number":"1","pic":"static/update/1.png","name":"世界上最好的人是你"},{"user_id":"415180","room_id":"412228","status":"1","number":"1","pic":"static/update/1.png","name":"世界上最好的人是你"},{"user_id":"415180","room_id":"412228","status":"1","number":"1","pic":"static/update/1.png","name":"世界上最好的人是你"}]
     * msg : ok
     * err : 0
     * currentPage : 0
     * totalPage : 2
     */

    private String msg;
    private String err;
    private String currentPage;
    private String totalPage;
    private List<DataBean> data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(String totalPage) {
        this.totalPage = totalPage;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean /*implements Serializable*/ {//Parcelable
        /**
         * user_id : 415180
         * room_id : 567774
         * status : 1
         * number : 10
         * pic : static/update/2.png
         * name : 世界上最好的人是你
         */

        private String user_id;
        private String room_id;
        private String status;
        private String number;
        private String pic;
        private String name;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getRoom_id() {
            return room_id;
        }

        public void setRoom_id(String room_id) {
            this.room_id = room_id;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
