package com.xunshan.ZhenSheng.entity;

import java.util.List;

public class PersonalDataBean {


    /**
     * msg : ok
     * picarr : [{"headname":"","pic":"https://api3.dayushaiwang.com/static/update/kongtou.png","url":"https://api3.dayushaiwang.com/baoxing/index.html/?token=MDM1NmE3ODE2OTdhNGZlMmZhYWQzZjI1N2QwMDZkMmU&user_id=938728&room_id=123456"}]
     * err : 0
     * pass :
     * sharecon : 娱乐不可开交九块九
     * isshoucang : 1
     * sharetitle : 贾增辉的直播间
     * shareimage : static/update/21.png
     * room : {"user_id":"938728","pass":"","name":"贾增辉的直播间","back":"1","roomtake":"娱乐不可开交九块九 ","content":"系统公告:官方倡导绿色互动，请勿发布违法、低俗、暴力、广告等内容，禁止违规交易，违规者将被封禁账号。"}
     * room_user : {"charm":"240","giftnum":"0","user_id":"938728","level":"240","comein":"","sex":"0","name":"贾增辉","sign":"这个人很懒，什么也没有留下!","headbox":"","headpic":"static/update/21.png"}
     * url : https://api3.dayushaiwang.com/baoxiang/roomharvest.html?token=MDM1NmE3ODE2OTdhNGZlMmZhYWQzZjI1N2QwMDZkMmU&room_id=123456&time=2020-05-27
     * balance : 10
     * shareurl : https://www.baidu.com
     * user : {"charm":"240","giftnum":"0","balance":"10","user_id":"938728","level":"240","sex":"0","name":"贾增辉","sign":"这个人很懒，什么也没有留下!","follow":"3","headpic":"static/update/21.png"}
     */

    private String msg;
    private String err;
    private String pass;
    private String sharecon;
    private String isshoucang;
    private String sharetitle;
    private String shareimage;
    private RoomBean room;
    private RoomUserBean room_user;
    private String url;
    private String balance;
    private String shareurl;
    private UserBean user;
    private List<PicarrBean> picarr;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getSharecon() {
        return sharecon;
    }

    public void setSharecon(String sharecon) {
        this.sharecon = sharecon;
    }

    public String getIsshoucang() {
        return isshoucang;
    }

    public void setIsshoucang(String isshoucang) {
        this.isshoucang = isshoucang;
    }

    public String getSharetitle() {
        return sharetitle;
    }

    public void setSharetitle(String sharetitle) {
        this.sharetitle = sharetitle;
    }

    public String getShareimage() {
        return shareimage;
    }

    public void setShareimage(String shareimage) {
        this.shareimage = shareimage;
    }

    public RoomBean getRoom() {
        return room;
    }

    public void setRoom(RoomBean room) {
        this.room = room;
    }

    public RoomUserBean getRoom_user() {
        return room_user;
    }

    public void setRoom_user(RoomUserBean room_user) {
        this.room_user = room_user;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getShareurl() {
        return shareurl;
    }

    public void setShareurl(String shareurl) {
        this.shareurl = shareurl;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public List<PicarrBean> getPicarr() {
        return picarr;
    }

    public void setPicarr(List<PicarrBean> picarr) {
        this.picarr = picarr;
    }

    public static class RoomBean {
        /**
         * user_id : 938728
         * pass :
         * name : 贾增辉的直播间
         * back : 1
         * roomtake : 娱乐不可开交九块九
         * content : 系统公告:官方倡导绿色互动，请勿发布违法、低俗、暴力、广告等内容，禁止违规交易，违规者将被封禁账号。
         */

        private String user_id;
        private String pass;
        private String name;
        private String back;
        private String roomtake;
        private String content;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getPass() {
            return pass;
        }

        public void setPass(String pass) {
            this.pass = pass;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getBack() {
            return back;
        }

        public void setBack(String back) {
            this.back = back;
        }

        public String getRoomtake() {
            return roomtake;
        }

        public void setRoomtake(String roomtake) {
            this.roomtake = roomtake;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }
    }

    public static class RoomUserBean {
        /**
         * charm : 240
         * giftnum : 0
         * user_id : 938728
         * level : 240
         * comein :
         * sex : 0
         * name : 贾增辉
         * sign : 这个人很懒，什么也没有留下!
         * headbox :
         * headpic : static/update/21.png
         */

        private String charm;
        private String giftnum;
        private String user_id;
        private String level;
        private String comein;
        private String sex;
        private String name;
        private String sign;
        private String headbox;
        private String headpic;

        public String getCharm() {
            return charm;
        }

        public void setCharm(String charm) {
            this.charm = charm;
        }

        public String getGiftnum() {
            return giftnum;
        }

        public void setGiftnum(String giftnum) {
            this.giftnum = giftnum;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getLevel() {
            return level;
        }

        public void setLevel(String level) {
            this.level = level;
        }

        public String getComein() {
            return comein;
        }

        public void setComein(String comein) {
            this.comein = comein;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }

        public String getHeadbox() {
            return headbox;
        }

        public void setHeadbox(String headbox) {
            this.headbox = headbox;
        }

        public String getHeadpic() {
            return headpic;
        }

        public void setHeadpic(String headpic) {
            this.headpic = headpic;
        }
    }

    public static class UserBean {
        /**
         * charm : 240
         * giftnum : 0
         * balance : 10
         * user_id : 938728
         * level : 240
         * sex : 0
         * name : 贾增辉
         * sign : 这个人很懒，什么也没有留下!
         * follow : 3
         * headpic : static/update/21.png
         */

        private String charm;
        private String giftnum;
        private String balance;
        private String user_id;
        private String level;
        private String sex;
        private String name;
        private String sign;
        private String follow;
        private String headpic;

        public String getCharm() {
            return charm;
        }

        public void setCharm(String charm) {
            this.charm = charm;
        }

        public String getGiftnum() {
            return giftnum;
        }

        public void setGiftnum(String giftnum) {
            this.giftnum = giftnum;
        }

        public String getBalance() {
            return balance;
        }

        public void setBalance(String balance) {
            this.balance = balance;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getLevel() {
            return level;
        }

        public void setLevel(String level) {
            this.level = level;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }

        public String getFollow() {
            return follow;
        }

        public void setFollow(String follow) {
            this.follow = follow;
        }

        public String getHeadpic() {
            return headpic;
        }

        public void setHeadpic(String headpic) {
            this.headpic = headpic;
        }
    }

    public static class PicarrBean {
        /**
         * headname :
         * pic : https://api3.dayushaiwang.com/static/update/kongtou.png
         * url : https://api3.dayushaiwang.com/baoxing/index.html/?token=MDM1NmE3ODE2OTdhNGZlMmZhYWQzZjI1N2QwMDZkMmU&user_id=938728&room_id=123456
         */

        private String headname;
        private String pic;
        private String url;

        public String getHeadname() {
            return headname;
        }

        public void setHeadname(String headname) {
            this.headname = headname;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
}
