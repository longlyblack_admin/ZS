package com.xunshan.ZhenSheng.entity;

import java.util.List;

public class MyRoomBean {


    /**
     * data : {"name":"416619","room_id":"416619","marr":["0","0","0","0","0","0","0","0"],"room_auth":[]}
     * msg : ok
     * err : 0
     */

    private DataBean data;
    private String msg;
    private String err;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public static class DataBean {
        /**
         * name : 416619
         * room_id : 416619
         * marr : ["0","0","0","0","0","0","0","0"]
         * room_auth : []
         */

        private String name;
        private String room_id;
        private List<String> marr;
        private List<?> room_auth;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getRoom_id() {
            return room_id;
        }

        public void setRoom_id(String room_id) {
            this.room_id = room_id;
        }

        public List<String> getMarr() {
            return marr;
        }

        public void setMarr(List<String> marr) {
            this.marr = marr;
        }

        public List<?> getRoom_auth() {
            return room_auth;
        }

        public void setRoom_auth(List<?> room_auth) {
            this.room_auth = room_auth;
        }
    }
}
