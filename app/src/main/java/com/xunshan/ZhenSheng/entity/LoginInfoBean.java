package com.xunshan.ZhenSheng.entity;

public class LoginInfoBean {

    /**
     * err : 0
     * msg : 注册成功
     * data : {"user_id":787040,"name":"张泽昊","token":"Y2JlYTA4ZmZkZTkxOTUwZGYwMjg0Mjg5NjVkMzE3MDk","headpic":"static/update/1.png","isone":1,"level":0,"sex":0,"sign":"这个人很懒，什么也没有留下"}
     */

    private int err;
    private String msg;
    private DataBean data;

    public int getErr() {
        return err;
    }

    public void setErr(int err) {
        this.err = err;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * user_id : 787040
         * name : 张泽昊
         * token : Y2JlYTA4ZmZkZTkxOTUwZGYwMjg0Mjg5NjVkMzE3MDk
         * headpic : static/update/1.png
         * isone : 1
         * level : 0
         * sex : 0
         * sign : 这个人很懒，什么也没有留下
         */

        private int user_id;
        private String name;
        private String token;
        private String headpic;
        private int isone;
        private int level;
        private int sex;
        private String sign;

        public int getUser_id() {
            return user_id;
        }

        public void setUser_id(int user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getHeadpic() {
            return headpic;
        }

        public void setHeadpic(String headpic) {
            this.headpic = headpic;
        }

        public int getIsone() {
            return isone;
        }

        public void setIsone(int isone) {
            this.isone = isone;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }

        public int getSex() {
            return sex;
        }

        public void setSex(int sex) {
            this.sex = sex;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }
    }
}
