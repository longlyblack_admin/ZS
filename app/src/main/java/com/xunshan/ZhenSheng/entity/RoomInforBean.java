package com.xunshan.ZhenSheng.entity;

import java.util.List;

public class RoomInforBean {


    /**
     * msg : ok
     * err : 0
     * room : {"user_id":"288614","sex":"0","room_id":"258369","name":"方法","room_name":"世界上最好的人是你","giftnum":"0","pass":"7b09472555919d9813328174929f5093","islock":"1","level":"118","charm":"240","sign":"刚刚刚刚刚","follow":"0","headpic":"static/update/23.png","fans":"1","marr":["1","2","2","0","1","0","0","1"],"roomtake":"打牌娱乐娱乐娱乐娱乐娱乐","content":"系统公告:官方倡导绿色互动，请勿发布违法、低俗、暴力、广告等内容，禁止违规交易，违规者将被封禁账号。"}
     * user : {"user_id":"876843","name":"咕咕咕咕咕咕","sex":"0","giftnum":"0","level":"240","sign":"这个人很懒，什么也没有留下","headpic":"static/update/27.png","status":"0","charm":"240","follow":"0","mounts":"","headbox":"","comein":""}
     * room_auth : []
     * over_pipe : 0
     */

    private String msg;
    private String err;
    private RoomBean room;
    private UserBean user;
    private String over_pipe;
    private List<String> room_auth;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public RoomBean getRoom() {
        return room;
    }

    public void setRoom(RoomBean room) {
        this.room = room;
    }

    public UserBean getUser() {
        return user;
    }

    public void setUser(UserBean user) {
        this.user = user;
    }

    public String getOver_pipe() {
        return over_pipe;
    }

    public void setOver_pipe(String over_pipe) {
        this.over_pipe = over_pipe;
    }

    public List<String> getRoom_auth() {
        return room_auth;
    }

    public void setRoom_auth(List<String> room_auth) {
        this.room_auth = room_auth;
    }

    public static class RoomBean {
        /**
         * user_id : 288614
         * sex : 0
         * room_id : 258369
         * name : 方法
         * room_name : 世界上最好的人是你
         * giftnum : 0
         * pass : 7b09472555919d9813328174929f5093
         * islock : 1
         * level : 118
         * charm : 240
         * sign : 刚刚刚刚刚
         * follow : 0
         * headpic : static/update/23.png
         * fans : 1
         * marr : ["1","2","2","0","1","0","0","1"]
         * roomtake : 打牌娱乐娱乐娱乐娱乐娱乐
         * content : 系统公告:官方倡导绿色互动，请勿发布违法、低俗、暴力、广告等内容，禁止违规交易，违规者将被封禁账号。
         */

        private String user_id;
        private String sex;
        private String room_id;
        private String name;
        private String room_name;
        private String giftnum;
        private String pass;
        private String islock;
        private String level;
        private String charm;
        private String sign;
        private String follow;
        private String headpic;
        private String fans;
        private String roomtake;
        private String content;
        private List<String> marr;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getRoom_id() {
            return room_id;
        }

        public void setRoom_id(String room_id) {
            this.room_id = room_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getRoom_name() {
            return room_name;
        }

        public void setRoom_name(String room_name) {
            this.room_name = room_name;
        }

        public String getGiftnum() {
            return giftnum;
        }

        public void setGiftnum(String giftnum) {
            this.giftnum = giftnum;
        }

        public String getPass() {
            return pass;
        }

        public void setPass(String pass) {
            this.pass = pass;
        }

        public String getIslock() {
            return islock;
        }

        public void setIslock(String islock) {
            this.islock = islock;
        }

        public String getLevel() {
            return level;
        }

        public void setLevel(String level) {
            this.level = level;
        }

        public String getCharm() {
            return charm;
        }

        public void setCharm(String charm) {
            this.charm = charm;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }

        public String getFollow() {
            return follow;
        }

        public void setFollow(String follow) {
            this.follow = follow;
        }

        public String getHeadpic() {
            return headpic;
        }

        public void setHeadpic(String headpic) {
            this.headpic = headpic;
        }

        public String getFans() {
            return fans;
        }

        public void setFans(String fans) {
            this.fans = fans;
        }

        public String getRoomtake() {
            return roomtake;
        }

        public void setRoomtake(String roomtake) {
            this.roomtake = roomtake;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public List<String> getMarr() {
            return marr;
        }

        public void setMarr(List<String> marr) {
            this.marr = marr;
        }
    }

    public static class UserBean {
        /**
         * user_id : 876843
         * name : 咕咕咕咕咕咕
         * sex : 0
         * giftnum : 0
         * level : 240
         * sign : 这个人很懒，什么也没有留下
         * headpic : static/update/27.png
         * status : 0
         * charm : 240
         * follow : 0
         * mounts :
         * headbox :
         * comein :
         */

        private String user_id;
        private String name;
        private String sex;
        private String giftnum;
        private String level;
        private String sign;
        private String headpic;
        private String status;
        private String charm;
        private String follow;
        private String mounts;
        private String headbox;
        private String comein;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getGiftnum() {
            return giftnum;
        }

        public void setGiftnum(String giftnum) {
            this.giftnum = giftnum;
        }

        public String getLevel() {
            return level;
        }

        public void setLevel(String level) {
            this.level = level;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }

        public String getHeadpic() {
            return headpic;
        }

        public void setHeadpic(String headpic) {
            this.headpic = headpic;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCharm() {
            return charm;
        }

        public void setCharm(String charm) {
            this.charm = charm;
        }

        public String getFollow() {
            return follow;
        }

        public void setFollow(String follow) {
            this.follow = follow;
        }

        public String getMounts() {
            return mounts;
        }

        public void setMounts(String mounts) {
            this.mounts = mounts;
        }

        public String getHeadbox() {
            return headbox;
        }

        public void setHeadbox(String headbox) {
            this.headbox = headbox;
        }

        public String getComein() {
            return comein;
        }

        public void setComein(String comein) {
            this.comein = comein;
        }
    }
}
