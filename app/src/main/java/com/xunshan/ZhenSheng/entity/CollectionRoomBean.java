package com.xunshan.ZhenSheng.entity;

public class CollectionRoomBean {

    /**
     * msg : ok
     * err : 0
     * isshoucang : 0
     */

    private String msg;
    private String err;
    private String isshoucang;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public String getIsshoucang() {
        return isshoucang;
    }

    public void setIsshoucang(String isshoucang) {
        this.isshoucang = isshoucang;
    }
}
