package com.xunshan.ZhenSheng.entity;

import com.zego.chatroom.constants.ZegoChatroomSeatStatus;

import java.util.List;

public class UserRoomList2Bean {



    /**
     * data : [{"user_id":"938728","name":"贾增辉","level":"0","sex":"0","headpic":"static/update/1.png","charm":"80","giftnum":"0","sign":""}]
     * msg : ok
     * err : 0
     */

    private String msg;
    private String err;
    private List<DataBean> data;
    private List<ChatroomSeatInfo> chatroomSeatInfos;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * user_id : 938728
         * name : 贾增辉
         * level : 0
         * sex : 0
         * headpic : static/update/1.png
         * charm : 80
         * giftnum : 0
         * sign :
         */

        private String user_id;
        private String name;
        private String level;
        private String sex;
        private String headpic;
        private String charm;
        private String giftnum;
        private String sign;
        public boolean isNotUrlPic() {
            return isNotUrlPic;
        }

        public void setNotUrlPic(boolean notUrlPic) {
            isNotUrlPic = notUrlPic;
        }

        //这个不是说就一定一成不变的
        //比如你可以在这里添加一个判断是否为占位
        private boolean isNotUrlPic;//是否不是为后台传输的数据 改名字是为了避免反复绕


        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getLevel() {
            return level;
        }

        public void setLevel(String level) {
            this.level = level;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getHeadpic() {
            return headpic;
        }

        public void setHeadpic(String headpic) {
            this.headpic = headpic;
        }

        public String getCharm() {
            return charm;
        }

        public void setCharm(String charm) {
            this.charm = charm;
        }

        public String getGiftnum() {
            return giftnum;
        }

        public void setGiftnum(String giftnum) {
            this.giftnum = giftnum;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }
    }
    public static class ChatroomSeatInfo {

        public int mStatus;

        public boolean isMute = false;

        public com.zego.chatroom.entity.ZegoChatroomUser mUser;

        public float mSoundLevel;

        public int mDelay;

        public int mLiveStatus;

        /**
         * 获取一个新的 空 麦位
         *
         * @return 新的 空 麦位
         */
        public static ChatroomSeatInfo emptySeat() {
          ChatroomSeatInfo seat = new ChatroomSeatInfo();
            seat.mStatus = ZegoChatroomSeatStatus.Empty;
            return seat;
        }
    }

}
