package com.xunshan.ZhenSheng.entity;

public class GiftSLogBean {

    /**
     * msg : ok
     * data : 123
     * err : 0
     */

    private String msg;
    private String data;
    private String err;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }
}
