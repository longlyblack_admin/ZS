package com.xunshan.ZhenSheng.entity;

import java.util.List;

public class MybackcardBean {


    /**
     * data : [{"name":"比心","number":"63","title":"比心","pic":"bixin","gifid":"5","money":"52"},{"name":"点赞","number":"10","title":"点赞","pic":"dianzan","gifid":"4","money":"33"}]
     * msg : ok
     * err : 0
     */

    private String msg;
    private String err;
    private List<DataBean> data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * name : 比心
         * number : 63
         * title : 比心
         * pic : bixin
         * gifid : 5
         * money : 52
         */

        private String name;
        private String number;
        private String title;
        private String pic;
        private String gifid;
        private String money;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getNumber() {
            return number;
        }

        public void setNumber(String number) {
            this.number = number;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getGifid() {
            return gifid;
        }

        public void setGifid(String gifid) {
            this.gifid = gifid;
        }

        public String getMoney() {
            return money;
        }

        public void setMoney(String money) {
            this.money = money;
        }
    }
}
