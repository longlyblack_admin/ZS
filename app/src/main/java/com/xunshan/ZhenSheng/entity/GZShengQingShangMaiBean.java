package com.xunshan.ZhenSheng.entity;

public class GZShengQingShangMaiBean {

    /**
     * msg : ok
     * data : {"charm":"0","giftnum":"0","user_id":"123456","level":"0","sex":"0","name":"李四","sign":"快乐每一天","headpic":"static/update/20200424/74bd02b23c5ee9c232eb5687f162c668.png","key":"3"}
     * err : 0
     */

    private String msg;
    private DataBean data;
    private String err;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }

    public static class DataBean {
        /**
         * charm : 0
         * giftnum : 0
         * user_id : 123456
         * level : 0
         * sex : 0
         * name : 李四
         * sign : 快乐每一天
         * headpic : static/update/20200424/74bd02b23c5ee9c232eb5687f162c668.png
         * key : 3
         */

        private String charm;
        private String giftnum;
        private String user_id;
        private String level;
        private String sex;
        private String name;
        private String sign;
        private String headpic;
        private String key;

        public String getCharm() {
            return charm;
        }

        public void setCharm(String charm) {
            this.charm = charm;
        }

        public String getGiftnum() {
            return giftnum;
        }

        public void setGiftnum(String giftnum) {
            this.giftnum = giftnum;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getLevel() {
            return level;
        }

        public void setLevel(String level) {
            this.level = level;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getSign() {
            return sign;
        }

        public void setSign(String sign) {
            this.sign = sign;
        }

        public String getHeadpic() {
            return headpic;
        }

        public void setHeadpic(String headpic) {
            this.headpic = headpic;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }
    }
}
