package com.xunshan.ZhenSheng.entity;

public class SheZhiMaiXuFangShiBean {


    /**
     * data : 设置成功
     * msg : ok
     * err : 0
     */

    private String data;
    private String msg;
    private String err;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getErr() {
        return err;
    }

    public void setErr(String err) {
        this.err = err;
    }
}
