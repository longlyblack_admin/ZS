package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.RoundTransform;
import com.xunshan.ZhenSheng.util.SearchUserBean;

import java.util.List;

public class SearchUserAdapter extends BaseAdapter {
    Context mContext;
    LayoutInflater mInflater;
    private List<SearchUserBean.DataBean> list;

    public SearchUserAdapter(Context mContext,List<SearchUserBean.DataBean> list){
        this.list = list;
        this.mContext = mContext;
        this.mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return (list == null) ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        RequestOptions options = new RequestOptions().centerCrop().transform(new RoundTransform(mContext, 5)).error(R.mipmap.logo);
        view = mInflater.inflate(R.layout.adapter_searchuser,null);
        ImageView image_photo= (ImageView) view.findViewById(R.id.iv_photo_header_searchuser);
        TextView tvseatuserIDpersonal = (TextView)view.findViewById(R.id.tv_seat_user_ID_searchuser);
        TextView  tvseatcharmpersonal = (TextView)view.findViewById(R.id.tv_seat_charm_searchuser);
        TextView  tvseatdialognamepersonal = (TextView)view.findViewById(R.id.tv_seat_dialog_name_searchuser);
        TextView tvseatlevelpersonal = (TextView)view.findViewById(R.id.tv_seat_level_searchuser);
        TextView tvseatsexpersonal = (TextView)view.findViewById(R.id.tv_seat_sex_searchuser);
        TextView  tvseatsignpersonal = (TextView)view.findViewById(R.id.tv_seat_sign_searchuser);
        TextView tvseatfollowpersonal = (TextView)view.findViewById(R.id.tv_seat_followsearchuser);
        Glide.with(mContext)
                .load(AllInterface.addr+"/"+list.get(position).getHeadpic())
                .apply(options)
                .into( image_photo);
        tvseatdialognamepersonal.setText("姓名："+list.get(position).getName());
        tvseatuserIDpersonal.setText("ID："+list.get(position).getUser_id());
        tvseatlevelpersonal.setText("等级："+list.get(position).getLevel());
        tvseatcharmpersonal.setText("魅力值："+list.get(position).getCharm());
        tvseatsignpersonal.setText("签名："+list.get(position).getSign());
        tvseatsexpersonal.setText("性别："+list.get(position).getSex());
        tvseatfollowpersonal.setText("关注："+list.get(position).getFollow());
        Log.e("TAGlistlistNameUser_id", "getView: "+list.get(position).getName()+list.get(position).getUser_id());
        return view;

    }

}
