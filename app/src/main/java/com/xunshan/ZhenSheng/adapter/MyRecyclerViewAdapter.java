package com.xunshan.ZhenSheng.adapter;


import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.entity.HoomListBean;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.RoundTransform;

import java.util.List;

import pl.droidsonroids.gif.GifDrawable;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {
    private List<HoomListBean.DataBean> mList;
    private Context context;
    private ItemClickListener mItemClickListener;
    public static final int MODE_PRIVATE = 0x0000;

    public MyRecyclerViewAdapter(Context context, List<HoomListBean.DataBean> mList) {
        this.context = context;
        this.mList = mList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.hoom_main_notice_list, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public int getItemCount() {
        return (mList == null) ? 0 : mList.size();
    }

    //将数据绑定到控件上
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        RequestOptions options = new RequestOptions().centerCrop().transform(new RoundTransform(context, 5)).error(R.mipmap.logo);
        Glide.with(context)
                .load(AllInterface.addr + "/" + mList.get(position).getPic())
                .apply(options)
                .into(holder.imageView);
        holder.title.setText(mList.get(position).getName()+" ");
        holder.numberofpeople.setText(mList.get(position).getNumber() + " ");
        if (mItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // 这里利用回调来给RecyclerView设置点击事件
                    mItemClickListener.onItemClick(position);
                }
            });
        }

    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        View myView;
        ImageView imageView;
        TextView title;
        TextView numberofpeople;

        GifDrawable gifDrawable;


        public ViewHolder(View itemView) {
            super(itemView);
            myView = itemView;
            imageView = (ImageView) itemView.findViewById(R.id.iv_image);
            title = (TextView) itemView.findViewById(R.id.tv_title1);
            numberofpeople = (TextView) itemView.findViewById(R.id.tv_Number_of_People);

        }
    }


    public interface ItemClickListener {
         void onItemClick(int position);
    }

    public void setOnItemClickListener(ItemClickListener itemClickListener) {
        this.mItemClickListener = itemClickListener;

    }


    //下面两个方法提供给页面刷新和加载时调用
    public void add(List<HoomListBean.DataBean> addMessageList) {
        //增加数据
      //  int a =  currentPage++;
//        mList.removeAll(mList);
        int position = mList.size();
        mList.addAll(position, addMessageList);
        notifyItemInserted(position);
    }

    public void refresh(List<HoomListBean.DataBean> newList) {
        //刷新数据
        mList.remove(mList);
        mList.addAll(newList);
        notifyDataSetChanged();
    }
}