package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.xunshan.ZhenSheng.R;

import java.util.List;

public class MyAdapter1 extends BaseAdapter {

    private List<Integer> list;
    private LayoutInflater mInflater;
    private Context mContext;
    myCallBack callBack;

    public interface myCallBack{
         void getPosition(int position);
    }

    public MyAdapter1(List<Integer> list, Context context, myCallBack  callBack){
        this.list = list;
        this.mContext = context;
        mInflater = LayoutInflater.from(mContext);
        this.callBack = callBack;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        Holder holder;
        if (view == null){
            holder = new Holder();
            view = mInflater.inflate(R.layout.adapter_my1,null);
            holder.img_room = (ImageView) view.findViewById(R.id.img_adapter_homeroom1);
            view.setTag(holder);
        }else{
            holder = (Holder)view.getTag();
        }
        holder.img_room.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.getPosition(position);
            }
        });
        holder.img_room.setImageResource(list.get(position));
        return view;
    }
    class Holder{
        ImageView img_room;
    }
}
