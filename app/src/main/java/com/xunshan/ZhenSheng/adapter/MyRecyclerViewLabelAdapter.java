package com.xunshan.ZhenSheng.adapter;


import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.xunshan.ZhenSheng.R;

import java.util.List;

public class MyRecyclerViewLabelAdapter extends RecyclerView.Adapter<MyRecyclerViewLabelAdapter.ViewHolder> {
    //    private List<BannerLunBoBean.DataBean> mList2;
    private List<String> mList2;
    private Context context;
    private OnItemClickListener mItemClickListener;
    private int selectPosition;

    public interface OnItemClickListener {
         void onItemClick(int positon);
    }

    public void setOnItemClickListener(OnItemClickListener itemClickListener) {
        this.mItemClickListener = itemClickListener;

    }

    public MyRecyclerViewLabelAdapter(Context context, List<String> mList2) {
        this.context = context;
        this.mList2 = mList2;
    }
    public int getSelectPosition() {
        return selectPosition;
    }
    public void setSelectPosition(int selectPosition) {
        this.selectPosition = selectPosition;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.hoom_main_notice_list_label, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public int getItemCount() {
        return mList2 == null ? 0 : mList2.size();
    }


    //将数据绑定到控件上
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Log.e("123456789", "onBindViewHolder: position=" + position);
        Log.e("123456789", "onBindViewHolder: mList2.get(position)=" + mList2.get(position));
        holder.tvlable.setText(mList2.get(position));
        if (position == getSelectPosition()){
            holder.tvlable.setBackground(context.getDrawable(R.drawable.white_shape));
            holder.tvlable.setTextColor(context.getResources().getColor(R.color.white));
        } else {
            holder.tvlable.setBackgroundColor(Color.parseColor("#FFFFFF"));
            holder.tvlable.setTextColor(context.getResources().getColor(R.color.black));
        }



        if (mItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // 这里利用回调来给RecyclerView设置点击事件
                    mItemClickListener.onItemClick(position);
                }
            });
        }

    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        View myView;
        TextView tvlable;


        public ViewHolder(View itemView) {
            super(itemView);
            myView = itemView;
            tvlable = (TextView) itemView.findViewById(R.id.tv_label1);
        }
    }







}