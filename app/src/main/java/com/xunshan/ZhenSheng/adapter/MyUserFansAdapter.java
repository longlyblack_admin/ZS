package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.entity.MyUserFansBean;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.RoundTransform;

import java.util.List;

public class MyUserFansAdapter extends RecyclerView.Adapter<MyUserFansAdapter.ViewHolder> {
    private List<MyUserFansBean.DataBean> list;
    private Context context;
    //第二步自定义接口
    private OnItemClickListener  mOnItemClickListener ;


    public MyUserFansAdapter(Context context, List<MyUserFansBean.DataBean> list) {
        this.context = context;
        this.list = list;
    }
    @Override
    public int getItemCount() {
        return (list == null) ? 0 : list.size();
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_my_fans_list, parent, false);
        final MyUserFansAdapter.ViewHolder holder = new MyUserFansAdapter.ViewHolder(view);
        return holder;
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        RequestOptions options = new RequestOptions().centerCrop().transform(new RoundTransform(context, 100)).error(R.mipmap.addshangmai);
            Glide.with(context)
                    .load(AllInterface.addr + "/" + list.get(position).getHeadpic())
                    .apply(options)
                    .into(holder.ivphotoheaderfans);
            holder.tvseatdialognamefans.setText(list.get(position).getName());
            if (list.get(position).getSex().equals("0")){
                holder.tvseatsexfans.setImageResource(R.mipmap.zssexnan);

            }else if (list.get(position).getSex().equals("1")){
                holder.tvseatsexfans.setImageResource(R.mipmap.zssexnv);
            }
        holder.tvlevelfans.setText(list.get(position).getSex());
        holder.tvseatsignfans.setText("个性签名："+list.get(position).getSign());
    }
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //接口位置
        ImageView ivphotoheaderfans;
        TextView tvseatdialognamefans;
        ImageView tvseatsexfans;
        TextView tvlevelfans;
        TextView tvseatsignfans;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivphotoheaderfans = (ImageView) itemView.findViewById(R.id.iv_photo_header_fans);
            tvseatdialognamefans = (TextView) itemView.findViewById(R.id.tv_seat_dialog_name_fans);
            tvseatsexfans = (ImageView) itemView.findViewById(R.id.tv_seat_sex_fans);
            tvlevelfans = (TextView) itemView.findViewById(R.id.tv_level_fans);
            tvseatsignfans = (TextView) itemView.findViewById(R.id.tv_seat_sign_fans);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }
//第一步自定义回调接口
    public interface OnItemClickListener  {
        void onItemClick(View v, int position);
        void onItemLongClick(View v);
    }
    //第三步定义方法并暴露外面的调用者
    public void setOnItemClickListener(OnItemClickListener listener){
        this.mOnItemClickListener = listener ;
    }

}
