package com.xunshan.ZhenSheng.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xunshan.ZhenSheng.entity.BannerLunBoBean;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.activity.HomeActivity;

import java.util.List;

public class RoomTypeAdapter extends RecyclerView.Adapter {
    List<BannerLunBoBean.DataBean> list;
    Context mContext;
    LayoutInflater mInfalter;
    roomTypeCallBack callBack;

    public interface roomTypeCallBack{
        public void typePosition(int position);
    }
    public RoomTypeAdapter(List<BannerLunBoBean.DataBean> list, Context context,roomTypeCallBack callBack) {
        this.list = list;
        this.mContext = context;
        mInfalter = LayoutInflater.from(mContext);
        this.callBack = callBack;
    }
    MyViewHolder holder;
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_hometype, parent, false);
        holder = new MyViewHolder(v);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(150, 70);
        holder.tv.setLayoutParams(params);
        holder.tv = (TextView) v.findViewById(R.id.tv_adapter_roomtype);
        return holder;
    }
    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {
        for (String label : list.get(position).getLabel()) {
            ((MyViewHolder)holder).tv.setText(label);
        }

        ((MyViewHolder)holder).tv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.typePosition(position);
            }
        });
        if (position == HomeActivity.typeIndex){
            ((MyViewHolder)holder).tv.setTextColor(mContext.getResources().getColor(R.color.white));
            ((MyViewHolder)holder).tv.setBackgroundResource(R.drawable.changebg_shape);
        }else{
            ((MyViewHolder)holder).tv.setTextColor(mContext.getResources().getColor(R.color.black));
            ((MyViewHolder)holder).tv.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tv = (TextView) itemView.findViewById(R.id.tv_adapter_roomtype);
        }
    }

}
