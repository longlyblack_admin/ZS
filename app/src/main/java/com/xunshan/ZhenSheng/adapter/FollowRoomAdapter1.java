package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.entity.StoreUpBean;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.RoundTransform;

import java.util.List;


public class FollowRoomAdapter1 extends BaseAdapter {
    private Context mContext;
    private LayoutInflater mInflater;
    private List<StoreUpBean.DataBean> list1;

    public FollowRoomAdapter1(Context mContext, List<StoreUpBean.DataBean> list1){
        this.mContext = mContext;
        this.list1 = list1;
        mInflater = LayoutInflater.from(mContext);

    }


    @Override
    public int getCount() {
        return (list1 == null) ? 0 : list1.size();
    }

    @Override
    public Object getItem(int position) {
        return list1.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        RequestOptions options = new RequestOptions().centerCrop().transform(new RoundTransform(mContext, 5)).error(R.mipmap.logo);
        view = mInflater.inflate(R.layout.adapter_followroom, null);
        ImageView image_photo= (ImageView) view.findViewById(R.id.tv_Search_room_image);
        TextView search_room_name = (TextView) view.findViewById(R.id.search_room_name);
        TextView room_id_serach = (TextView) view.findViewById(R.id.room_id_serach);
        TextView tvzhibozhuangtai = (TextView) view.findViewById(R.id.tv_zhibozhuangtai);
        tvzhibozhuangtai.setVisibility(View.VISIBLE);

        Glide.with(mContext)
                .load(AllInterface.addr+"/"+list1.get(position).getPic())
                .apply(options)
                .into( image_photo);
        search_room_name.setText("房间名字："+list1.get(position).getName());
        room_id_serach.setText("房间ID："+list1.get(position).getRoom_id());
        if (list1.get(position).getStatus().equals("1")){
            tvzhibozhuangtai.setText(list1.get(position).getNumber()+"人在玩");
        }else if (list1.get(position).getStatus().equals("0")){
            tvzhibozhuangtai.setText("房间：主播未开播");
        }

        
        return view;
    }



}
