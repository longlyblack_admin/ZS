package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.data.ZegoDataCenter;
import com.xunshan.ZhenSheng.entity.ChatroomSeatInfo;
import com.xunshan.ZhenSheng.entity.UserRoomListBean;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.RoundTransform;
import com.zego.chatroom.constants.ZegoChatroomSeatStatus;

import java.text.DecimalFormat;
import java.util.List;

public class MaiWeiAdapter  extends RecyclerView.Adapter<MaiWeiAdapter.ViewHolder> {
    private List<UserRoomListBean.DataBean> list;
    private Context context;
    //第二步自定义接口
    private OnItemClickListener  mOnItemClickListener ;
    private List<ChatroomSeatInfo> mSeats;
    private StringBuilder mSeatStatus = new StringBuilder();
    private DecimalFormat mSoundLevelFormat = new DecimalFormat("0.00");


    public MaiWeiAdapter(Context context, List<UserRoomListBean.DataBean> list) {
        this.context = context;
        this.list = list;
    }
    @Override
    public int getItemCount() {
        return (list == null) ? 0 : list.size();
    }
    public void setSeats(List<ChatroomSeatInfo> seats) {
        mSeats = seats;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_chatroom_maiwei, parent, false);
        final MaiWeiAdapter.ViewHolder holder = new MaiWeiAdapter.ViewHolder(view);
        return holder;
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ChatroomSeatInfo seat = mSeats.get(position);

        if (seat.mStatus == ZegoChatroomSeatStatus.Empty || seat.mStatus == ZegoChatroomSeatStatus.Closed) {
            String status = (seat.mStatus == ZegoChatroomSeatStatus.Empty ? "空，" : "封，");
            mSeatStatus.append(status);
            holder.tvroomusernameroomuser.setText("none");
            holder.mTvDelay.setText("delay");
            holder.mTvSoundLevel.setText("soundLevel");
        } else if (seat.mStatus == ZegoChatroomSeatStatus.Used) {
            mSeatStatus.append("占，");
            holder.tvroomusernameroomuser.setText(seat.mUser.userName);
            if (ZegoDataCenter.ZEGO_USER.equals(seat.mUser)) {
                holder.mTvDelay.setText("delay");
            } else {
                holder.mTvDelay.setText(seat.mDelay + "");
            }
            holder.mTvSoundLevel.setText(mSoundLevelFormat.format(seat.mSoundLevel));
        }
        if (seat.isMute) {
            mSeatStatus.append("禁");
        }

        holder.mTvSeatStatus.setText(mSeatStatus.toString());
        mSeatStatus.setLength(0);
        holder.itemView.setTag(seat);
        RequestOptions options = new RequestOptions().centerCrop().transform(new RoundTransform(context, 100)).error(R.mipmap.addshangmai);
        if ( list.get(position).isNotUrlPic() == false){
            holder.imagehongxinxin.setVisibility(View.VISIBLE);
            holder.tvroomusernameroomuser.setVisibility(View.VISIBLE);
            holder.tvcharmroomuser.setVisibility(View.VISIBLE);
            holder.ivyinmai.setVisibility(View.VISIBLE);
            Glide.with(context)
                    .load(AllInterface.addr + "/" + list.get(position).getHeadpic())
                    .apply(options)
                    .into(holder.imguserinfoface1roomuser);
            holder.tvroomusernameroomuser.setText(list.get(position).getName()+" ");
            holder.tvcharmroomuser.setText(list.get(position).getCharm() + " ");
            Log.e("TAGmList", list.get(position).getName()+list.get(position).getCharm()+AllInterface.addr + "/" + list.get(position).getHeadpic());
        }else if (list.get(position).isNotUrlPic() == true){
            Glide.with(context)
                    .load(AllInterface.addr + "/" + list.get(position).getHeadpic())
                    .apply(options)
                    .into(holder.imguserinfoface1roomuser);
            holder.imagehongxinxin.setVisibility(View.GONE);
            holder.tvroomusernameroomuser.setVisibility(View.GONE);
            holder.tvcharmroomuser.setVisibility(View.GONE);
            holder.ivyinmai.setVisibility(View.GONE);
    }


    }
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //接口位置
        ImageView imguserinfoface1roomuser;
        TextView tvroomusernameroomuser;
        TextView tvcharmroomuser;
        ImageView imagehongxinxin;
        ImageView ivyinmai;
        private TextView mTvSeatStatus;
        private TextView mTvLiveStatus;
        private TextView mTvDelay;
        private TextView mTvSoundLevel;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imguserinfoface1roomuser = (ImageView) itemView.findViewById(R.id.img_userinfo_face1_roomuser);
            imagehongxinxin = (ImageView) itemView.findViewById(R.id.image_hongxinxin);
            ivyinmai = (ImageView) itemView.findViewById(R.id.iv_yinmai);
            tvroomusernameroomuser = (TextView) itemView.findViewById(R.id.tv_room_user_name_roomuser);
            tvcharmroomuser = (TextView) itemView.findViewById(R.id.tv_charm_roomuser);
            mTvDelay = itemView.findViewById(R.id.tv_delay);
            mTvSeatStatus = itemView.findViewById(R.id.tv_seat_status);
            mTvLiveStatus = itemView.findViewById(R.id.tv_live_status);
            mTvSoundLevel = itemView.findViewById(R.id.tv_sound_level);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(v, getAdapterPosition(),(ChatroomSeatInfo) v.getTag());
            }
        }
    }
//第一步自定义回调接口
    public interface OnItemClickListener  {
        void onItemClick(View v, int position,ChatroomSeatInfo chatroomSeatInfo);
        void onItemLongClick(View v);
    }
    //第三步定义方法并暴露外面的调用者
    public void setOnItemClickListener(OnItemClickListener listener){
        this.mOnItemClickListener = listener ;
    }
}
