package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.activity.KnapsackActivity;
import com.xunshan.ZhenSheng.entity.KnapsackClassify;

import java.util.List;

public class KnapsackClassifyAdapter extends BaseAdapter {
    List<KnapsackClassify> classifyList;
    Context mContext;
    LayoutInflater mInflater;
    classifyCallBack callBack;

    public interface classifyCallBack{
        public void classifyPositin(int position);
    }

    public KnapsackClassifyAdapter(List<KnapsackClassify> classifyList,Context context,classifyCallBack callBack){
        this.classifyList = classifyList;
        this.mContext = context;
        mInflater = LayoutInflater.from(mContext);
        this.callBack = callBack;
    }

    @Override
    public int getCount() {
        return classifyList.size();
    }

    @Override
    public Object getItem(int position) {
        return classifyList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        Holder holder;
        if (view == null){
            holder = new Holder();
            view = mInflater.inflate(R.layout.adapter_knapsackclassify,null);
            holder.ll_adapter_knapsackclassify = (LinearLayout) view.findViewById(R.id.ll_adapter_knapsackclassify);
            holder.img_adapter_knapsackclassify = (ImageView) view.findViewById(R.id.img_adapter_knapsackclassify);
            holder.tv_knapsackclassify_name = (TextView) view.findViewById(R.id.tv_knapsackclassify_name);
            view.setTag(holder);
        }else{
            holder = (Holder) view.getTag();
        }

        KnapsackClassify classify = classifyList.get(position);
        holder.tv_knapsackclassify_name.setText(classify.getkName());
        if (KnapsackActivity.classifyIndex != position){
            holder.img_adapter_knapsackclassify.setVisibility(View.GONE);
        }else{
            holder.img_adapter_knapsackclassify.setVisibility(View.VISIBLE);
        }
        holder.ll_adapter_knapsackclassify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.classifyPositin(position);
            }
        });
        return view;
    }
    class Holder{
        LinearLayout ll_adapter_knapsackclassify;
        ImageView img_adapter_knapsackclassify;
        TextView tv_knapsackclassify_name;
    }
}
