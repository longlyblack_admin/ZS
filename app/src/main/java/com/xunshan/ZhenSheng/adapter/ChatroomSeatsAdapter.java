package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.data.ZegoDataCenter;
import com.xunshan.ZhenSheng.entity.ChatroomSeatInfo;
import com.xunshan.ZhenSheng.entity.UserRoomListBean;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.GlideRoundTransform;
import com.xunshan.ZhenSheng.util.LogUtil;
import com.xunshan.ZhenSheng.util.RoundTransform;
import com.zego.chatroom.constants.ZegoChatroomSeatStatus;
import com.zego.chatroom.constants.ZegoChatroomUserLiveStatus;

import java.text.DecimalFormat;
import java.util.List;

public class ChatroomSeatsAdapter extends RecyclerView.Adapter<ChatroomSeatsAdapter.ViewHolder> {

    public List<ChatroomSeatInfo> mSeats;
    public Context mContext;
    List<UserRoomListBean.DataBean> mlist;
    public StringBuilder mSeatStatus = new StringBuilder();

    public OnChatroomSeatClickListener mOnChatroomSeatClickListener;
    public List<String> Marr;
    public String Room_id;


    public ChatroomSeatsAdapter(Context context) {
        this.mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_chatroom_seat_layout1, viewGroup, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        ChatroomSeatInfo seat = mSeats.get(position);
        if (seat.mStatus == ZegoChatroomSeatStatus.Empty || seat.mStatus == ZegoChatroomSeatStatus.Closed) {
            String status = (seat.mStatus == ZegoChatroomSeatStatus.Empty ? "空，" : "封，");
            mSeatStatus.append(status);
            //房主头像等相关信息赋值
            if (position == 0) {
                new RoundedCorners(16);
                LogUtil.e("头像赋值-->"+ AllInterface.addr + "/" + User.user().getRoomname_Headpic());
                Glide.with(mContext)
                        .load(AllInterface.addr + "/" + User.user().getRoomname_Headpic())
                        .skipMemoryCache(false)
                        .dontAnimate()
                        .apply(new RequestOptions().centerCrop().transform(new RoundTransform(mContext, 100)).error(R.mipmap.addshangmai)
                                .transform(new GlideRoundTransform(100)))
                        .into(viewHolder.rlitembg);
                viewHolder.tvusernameroomuserxin1.setText(User.user().getRoomname_name());
                viewHolder.tvcharmroomuserxin1.setText(User.user().getRoomname_Charm());
                viewHolder.imagehongxinxinxin1.setBackgroundResource(R.mipmap.lanselove);
                viewHolder.tvusernameroomuserxin1.setVisibility(View.VISIBLE);
                viewHolder.imagehongxinxinxin1.setVisibility(View.VISIBLE);
                viewHolder.tvcharmroomuserxin1.setVisibility(View.GONE);
                viewHolder.rlitembg.setVisibility(View.VISIBLE);
                viewHolder.llbeyong1.setVisibility(View.VISIBLE);
                viewHolder.llchatroomusermaiwei.setVisibility(View.GONE);

            } else if (position != 0) {
                if (Marr.get(position).equals("0")) {
                    viewHolder.rlitembg.setBackgroundResource(R.mipmap.addshangmai);
                    viewHolder.ivyinmairoomuserxin1.setBackgroundResource(R.mipmap.microphone);
                    viewHolder.tvusernameroomuserxin1.setText(position + "号麦");
                    viewHolder.llchatroomusermaiwei.setVisibility(View.GONE);
                    viewHolder.maiwei.setVisibility(View.VISIBLE);
                    viewHolder.llbeyong1.setVisibility(View.VISIBLE);
                    viewHolder.tvusernameroomuserxin1.setVisibility(View.VISIBLE);
                    viewHolder.tvcharmroomuserxin1.setVisibility(View.GONE);
                    viewHolder.imagehongxinxinxin1.setVisibility(View.GONE);

                }
                if (Marr.get(position).equals("1")) {
                    viewHolder.rlitembg.setBackgroundResource(R.mipmap.yizi);
                    viewHolder.ivyinmairoomuserxin1.setBackgroundResource(R.mipmap.microphone);
                    viewHolder.tvusernameroomuserxin1.setText(position + "号麦");
                    LogUtil.e("赋值名称one-->"+ position + "号麦");
                    viewHolder.llchatroomusermaiwei.setVisibility(View.GONE);
                    viewHolder.maiwei.setVisibility(View.VISIBLE);
                    viewHolder.llbeyong1.setVisibility(View.VISIBLE);
                    viewHolder.tvusernameroomuserxin1.setVisibility(View.VISIBLE);
                    viewHolder.tvcharmroomuserxin1.setVisibility(View.GONE);
                    viewHolder.imagehongxinxinxin1.setVisibility(View.GONE);
                }
            }
        } else if (seat.mStatus == ZegoChatroomSeatStatus.Used) {
            mSeatStatus.append("占,");
            if (ZegoDataCenter.ZEGO_USER.equals(seat.mUser)) {
                if (seat.mUser.userName.equals(User.user().getUser_name())) {
                    viewHolder.maiwei.setVisibility(View.GONE);
                    LogUtil.e("头像赋值-->"+ AllInterface.addr + "/" + User.user().getUser_Headpic());
                    new RoundedCorners(16);
                    Glide.with(mContext)
                            .load(AllInterface.addr + "/" + User.user().getUser_Headpic())
                            .skipMemoryCache(false)
                            .dontAnimate()
                            .apply(new RequestOptions().centerCrop().transform(new RoundTransform(mContext, 100)).error(R.mipmap.addshangmai)
                                    .transform(new GlideRoundTransform(100)))
                            .into(viewHolder.imguserinfofaceroomuserxin);
                    viewHolder.tvusernameroomuserxin.setText(User.user().getUser_name());
                    LogUtil.e("赋值名称-->"+ User.user().getUser_name());
                    viewHolder.tvcharmroomuserxin.setText(User.user().getUser_Charm());
                    viewHolder.imagehongxinxinxin1.setVisibility(View.GONE);
                }
                viewHolder.llchatroomusermaiwei.setVisibility(View.VISIBLE);
            } else {
                if (UserRoomListBean.UserRoomListBean().getData() != null) {
                    for (int i = 0; i < UserRoomListBean.UserRoomListBean().getData().size(); i++) {
                        if (seat.mUser.userID.equals(UserRoomListBean.UserRoomListBean().getData().get(i).getUser_id())) {
                            viewHolder.maiwei.setVisibility(View.GONE);
                            new RoundedCorners(16);
                            LogUtil.e("头像赋值-->" + AllInterface.addr + "/" + UserRoomListBean.UserRoomListBean().getData().get(i).getHeadpic());
                            Glide.with(mContext)
                                    .load(AllInterface.addr + "/" + UserRoomListBean.UserRoomListBean().getData().get(i).getHeadpic())
                                    .skipMemoryCache(false)
                                    .dontAnimate()
                                    .apply(new RequestOptions().centerCrop().transform(new RoundTransform(mContext, 100)).error(R.mipmap.addshangmai)
                                            .transform(new GlideRoundTransform(100)))
                                    .into(viewHolder.imguserinfofaceroomuserxin);
                            viewHolder.tvusernameroomuserxin.setText(seat.mUser.userName);
                            LogUtil.e("赋值昵称-->"+ seat.mUser.userName);
                            viewHolder.tvcharmroomuserxin.setText(UserRoomListBean.UserRoomListBean().getData().get(i).getCharm());
                            viewHolder.imagehongxinxinxin1.setVisibility(View.GONE);
                        }
                    }
                }
                viewHolder.llchatroomusermaiwei.setVisibility(View.VISIBLE);
                viewHolder.maiwei.setVisibility(View.GONE);
            }

        }

        // TODO: 2020/5/29 禁麦显示
        if (seat.isMute) {
            mSeatStatus.append("禁");
            viewHolder.llchatroomusermaiwei.setVisibility(View.VISIBLE);
            viewHolder.ivyinmairoomuserxin.setBackgroundResource(seat.isMute ? R.mipmap.microphone : R.mipmap.jinyanmai);
        }

        mSeatStatus.setLength(0);
        viewHolder.itemView.setTag(seat);
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }


    public ChatroomSeatsAdapter.OnItemClickListener mItemClickListener;

    public interface OnItemClickListener {
        void onItemClick();
    }

    public void setItemClickListener(ChatroomSeatsAdapter.OnItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }


    @Override
    public int getItemCount() {
        return mSeats == null ? 0 : mSeats.size();
    }

    public void setSeats(List<ChatroomSeatInfo> seats, List<String> marr) {
        mSeats = seats;
        Marr = marr;
        notifyDataSetChanged();
    }

    public void setOnChatroomSeatClickListener(OnChatroomSeatClickListener onChatroomSeatClickListener) {
        mOnChatroomSeatClickListener = onChatroomSeatClickListener;
    }

    public String getLiveStatusString(int liveStatus) {
        switch (liveStatus) {
            case ZegoChatroomUserLiveStatus.WAIT_CONNECT:
                return "待连接";
            case ZegoChatroomUserLiveStatus.CONNECTING:
                return "连接中";
            case ZegoChatroomUserLiveStatus.LIVE:
                return "已连接";
            default:
                return "";

        }
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        public ImageView rlitembg;
        public ImageView imguserinfofaceroomuserxin;
        public ImageView ivyinmairoomuserxin;
        public ImageView ivyinmairoomuserxin1;
        public ImageView imagehongxinxinxin;
        public ImageView imagehongxinxinxin1;
        public TextView tvcharmroomuserxin;
        public TextView tvusernameroomuserxin;
        public TextView tvusernameroomuserxin1;

        public TextView tvcharmroomuserxin1;
        public LinearLayout llchatroomusermaiwei;
        public RelativeLayout maiwei;
        public LinearLayout llbeyong1;


        public ViewHolder(View view) {
            super(view);

            rlitembg = view.findViewById(R.id.rlitembg);
            maiwei = view.findViewById(R.id.room_maiwei);

            imguserinfofaceroomuserxin = view.findViewById(R.id.img_userinfo_face_roomuser_xin);
            ivyinmairoomuserxin = view.findViewById(R.id.iv_yinmai_roomuser_xin);
            ivyinmairoomuserxin1 = view.findViewById(R.id.iv_yinmai_roomuser_xin1);
            imagehongxinxinxin = view.findViewById(R.id.image_hongxinxin_xin);

            imagehongxinxinxin1 = view.findViewById(R.id.image_hongxinxin_xin1);
            tvcharmroomuserxin = view.findViewById(R.id.tv_charm_roomuser_xin);


            tvcharmroomuserxin1 = view.findViewById(R.id.tv_charm_roomuser_xin1);

            llchatroomusermaiwei = view.findViewById(R.id.ll_chatroom_user_maiwei);
            tvusernameroomuserxin = view.findViewById(R.id.tv_user_name_roomuser_xin);
            tvusernameroomuserxin1 = view.findViewById(R.id.tv_user_name_roomuser_xin1);
            llbeyong1 = view.findViewById(R.id.ll_beyong1);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mOnChatroomSeatClickListener != null) {
                mOnChatroomSeatClickListener.onChatroomSeatClick((ChatroomSeatInfo) view.getTag(), view);
            }
        }
    }

    public interface OnChatroomSeatClickListener {
        void onChatroomSeatClick(ChatroomSeatInfo chatroomSeatInfo, View view);
    }


    public void setStatus(TextView tvFirst ,TextView tvSecond){
        String first = tvFirst.getText().toString().trim();
        String second = tvSecond.getText().toString().trim();
        if (TextUtils.isEmpty(first)) {
            tvFirst.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(second)) {
            tvSecond.setVisibility(View.GONE);
        }
    }

}
