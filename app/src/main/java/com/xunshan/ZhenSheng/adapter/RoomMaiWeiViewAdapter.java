package com.xunshan.ZhenSheng.adapter;


import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.data.ZegoDataCenter;
import com.xunshan.ZhenSheng.entity.ChatroomSeatInfo;
import com.xunshan.ZhenSheng.entity.HoomListBean;
import com.xunshan.ZhenSheng.entity.RoomInforBean;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.RoundTransform;
import com.zego.chatroom.constants.ZegoChatroomSeatStatus;
import com.zego.chatroom.constants.ZegoChatroomUserLiveStatus;

import java.text.DecimalFormat;
import java.util.List;

public class RoomMaiWeiViewAdapter extends RecyclerView.Adapter<RoomMaiWeiViewAdapter.ViewHolder> {
    private List<ChatroomSeatInfo> mList;

    private StringBuilder mSeatStatus = new StringBuilder();

    private OnChatroomSeatClickListener mOnChatroomSeatClickListener;

    private DecimalFormat mSoundLevelFormat = new DecimalFormat("0.00");

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_chatroom_seat_layout1, viewGroup, false);
        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        ChatroomSeatInfo mlist = mList.get(position);

        if (mlist.mStatus == ZegoChatroomSeatStatus.Empty || mlist.mStatus == ZegoChatroomSeatStatus.Closed) {
            String status = (mlist.mStatus == ZegoChatroomSeatStatus.Empty ? "空，" : "封，");
            mSeatStatus.append(status);
            viewHolder.mTvUserName.setText("none");
            viewHolder.mTvDelay.setText("delay");
            viewHolder.mTvSoundLevel.setText("soundLevel");
        } else if (mlist.mStatus == ZegoChatroomSeatStatus.Used) {
            mSeatStatus.append("占，");
            viewHolder.mTvUserName.setText(mlist.mUser.userName);
            if (ZegoDataCenter.ZEGO_USER.equals(mlist.mUser)) {
                viewHolder.mTvDelay.setText("delay");
            } else {
                viewHolder.mTvDelay.setText(mlist.mDelay + "");
            }
            viewHolder.mTvSoundLevel.setText(mSoundLevelFormat.format(mlist.mSoundLevel));
        }
        if (mlist.isMute) {
            mSeatStatus.append("禁");
        }

        viewHolder.mTvSeatStatus.setText(mSeatStatus.toString());
        mSeatStatus.setLength(0);

        viewHolder.mTvLiveStatus.setText(getLiveStatusString(mlist.mLiveStatus));

        viewHolder.itemView.setTag(mlist);
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }

    public void setSeats(List<ChatroomSeatInfo> mlist) {
        mList = mlist;
        notifyDataSetChanged();
    }

    public void setOnChatroomSeatClickListener(OnChatroomSeatClickListener onChatroomSeatClickListener) {
        mOnChatroomSeatClickListener = onChatroomSeatClickListener;
    }

    private String getLiveStatusString(int liveStatus) {
        switch (liveStatus) {
            case ZegoChatroomUserLiveStatus.WAIT_CONNECT:
                return "待连接";
            case ZegoChatroomUserLiveStatus.CONNECTING:
                return "连接中";
            case ZegoChatroomUserLiveStatus.LIVE:
                return "已连接";
            default:
                return "";

        }
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView mTvUserName;
        private TextView mTvSeatStatus;
        private TextView mTvLiveStatus;
        private TextView mTvDelay;
        private TextView mTvSoundLevel;

        private ViewHolder(View view) {
            super(view);
            mTvUserName = view.findViewById(R.id.tv_user_name);
            mTvSeatStatus = view.findViewById(R.id.tv_seat_status);
            mTvLiveStatus = view.findViewById(R.id.tv_live_status);
            mTvDelay = view.findViewById(R.id.tv_delay);
            mTvSoundLevel = view.findViewById(R.id.tv_sound_level);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mOnChatroomSeatClickListener != null) {
                mOnChatroomSeatClickListener.onChatroomSeatClick((ChatroomSeatInfo) v.getTag());
            }
        }
    }

    public interface OnChatroomSeatClickListener {
        void onChatroomSeatClick(ChatroomSeatInfo chatroomSeatInfo);
    }


}