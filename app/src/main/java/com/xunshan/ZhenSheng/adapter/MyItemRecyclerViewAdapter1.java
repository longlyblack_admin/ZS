package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.entity.UpperWheatApplyBean;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.RoundTransform;

import java.util.List;

public class MyItemRecyclerViewAdapter1 extends RecyclerView.Adapter<MyItemRecyclerViewAdapter1.ViewHolder> {


    private List<UpperWheatApplyBean.DataBean>  mUpperListener;
    private Context context;

    public MyItemRecyclerViewAdapter1(List<UpperWheatApplyBean.DataBean>  mupperListener, Context mContext) {
        mUpperListener = mupperListener;
        context = mContext;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_upper_wheat_apply, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        RequestOptions options = new RequestOptions().centerCrop().transform(new RoundTransform(context, 5)).error(R.mipmap.logo);
        Glide.with(context)
                .load(AllInterface.addr + "/" + mUpperListener.get(position).getHeadpic())
                .apply(options)
                .into(holder.ivUpperwheatApplyphoto);
        holder.tvUpperwheatApplyName.setText(mUpperListener.get(position).getName());
        holder.tvUpperwheatApplysex.setText(mUpperListener.get(position).getSex());
        holder.tvUpperwheatApplymai.setText("同意上麦");

    }

    @Override
    public int getItemCount() {
        return (mUpperListener == null) ? 0 : mUpperListener.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tvUpperwheatApplymai;
        public final TextView tvUpperwheatApplyName;
        public final TextView tvUpperwheatApplysex;
        public final ImageView ivUpperwheatApplyphoto;
        public UpperWheatApplyBean upperWheatApplyBean;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ivUpperwheatApplyphoto =  view.findViewById(R.id.iv_Upper_wheat_Apply_photo);
            tvUpperwheatApplyName =  view.findViewById(R.id.tv_Upper_wheat_Apply_Name);
            tvUpperwheatApplysex =  view.findViewById(R.id.tv_Upper_wheat_Apply_sex);
            tvUpperwheatApplymai =  view.findViewById(R.id.tv_Upper_wheat_Apply_mai);
        }

    }
}
