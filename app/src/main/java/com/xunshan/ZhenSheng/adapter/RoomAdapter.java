package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.entity.Wheel;

import java.util.List;

public class RoomAdapter extends RecyclerView.Adapter {
    List<Wheel> list;
    Context mContext;
    LayoutInflater mInfalter;

    public RoomAdapter(List<Wheel> list, Context context) {
        this.list = list;
        this.mContext = context;
        mInfalter = LayoutInflater.from(mContext);
    }

    MyViewHolder holder;

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_rotainchart, parent, false);
        holder = new MyViewHolder(v);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(mContext.getResources().getDisplayMetrics().widthPixels, 500);
        holder.img = (ImageView) v.findViewById(R.id.img_adapter_rotainchart);
        holder.img.setLayoutParams(params);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Glide.with(mContext).load(list.get(position).getwUrl()).placeholder(R.mipmap.ic_launcher).into(((MyViewHolder) holder).img);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView img;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.img_adapter_rotainchart);
        }
    }

}
