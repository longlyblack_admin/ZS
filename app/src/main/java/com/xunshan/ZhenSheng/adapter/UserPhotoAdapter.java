package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.entity.PicAlbumWallBean;
import com.xunshan.ZhenSheng.util.AllInterface;

import java.util.List;

public class UserPhotoAdapter extends BaseAdapter {
    private List<PicAlbumWallBean.DataBean> list;
    private Context mContext;
    private LayoutInflater mInflater;
    private userPhotoCallBack callBack;


    public interface userPhotoCallBack {
        public void getPosition(int position);

        void camera();
    }

    public UserPhotoAdapter(List<PicAlbumWallBean.DataBean> list, Context context) {
        this.list = list;
        this.mContext = context;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return (list == null) ? 0 : list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        Holder holder;
        if (view == null) {
            view = mInflater.inflate(R.layout.adapter_userphoto, null);
            holder = new Holder();
            holder.img_adapter_usrinfo = (ImageView) view.findViewById(R.id.img_adapter_usrinfo);
            view.setTag(holder);
        }else{
            holder = (Holder) view.getTag();
        }
        Glide.with(mContext).load(AllInterface.addr+"/"+list.get(position).getPic()).into(holder.img_adapter_usrinfo);
        return view;
    }

    class Holder {
        ImageView img_adapter_usrinfo;
    }
}
