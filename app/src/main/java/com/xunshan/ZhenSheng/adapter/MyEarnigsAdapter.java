package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.entity.Earnigs;
import com.xunshan.ZhenSheng.entity.MyEarningsBean;
import com.xunshan.ZhenSheng.entity.MySouSouBean;

import java.util.List;

public class MyEarnigsAdapter extends BaseAdapter {

    private List<MyEarningsBean.DataBean> list;
    private Context mContext;
    private LayoutInflater mInflater;

    public MyEarnigsAdapter(List<MyEarningsBean.DataBean> list, Context context) {
        this.list = list;
        this.mContext = context;
        mInflater = LayoutInflater.from(mContext);
    }


    @Override
    public int getCount() {
        return (list == null) ? 0 : list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        view = mInflater.inflate(R.layout.adapter_myearnigs, null);
        TextView tv_namesrzc = (TextView) view.findViewById(R.id.tv_namesrzc);
        TextView tv_currency = (TextView) view.findViewById(R.id.tv_currency);
        TextView tv_sjsrzc = (TextView) view.findViewById(R.id.tv_sjsrzc);
        TextView tv_numbersrzc = (TextView) view.findViewById(R.id.tv_numbersrzc);
        TextView tv_typezcsr = (TextView) view.findViewById(R.id.tv_typezcsr);


        tv_namesrzc.setText(list.get(position).getName());
        tv_currency.setText(list.get(position).getCurrency());
        tv_sjsrzc.setText(list.get(position).getDate());
        tv_numbersrzc.setText("数量*"+list.get(position).getNumber()+"个数");
        tv_typezcsr.setText(list.get(position).getType());
        return view;
    }



}
