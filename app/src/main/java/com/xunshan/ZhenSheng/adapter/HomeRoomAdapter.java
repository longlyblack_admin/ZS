package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.entity.Room;

import java.util.List;

public class HomeRoomAdapter extends BaseAdapter {

    private List<Room> list;
    private LayoutInflater mInflater;
    private Context mContext;
    roomAdapterCallBack callBack;

    public interface roomAdapterCallBack{
        public void getPosition(int position);
    }
    public HomeRoomAdapter(List<Room> list,Context context,roomAdapterCallBack callBack){
        this.list = list;
        this.mContext = context;
        mInflater = LayoutInflater.from(mContext);
        this.callBack = callBack;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        Holder holder;
        if (view == null){
            holder = new Holder();
            view = mInflater.inflate(R.layout.adapter_homeroom,null);
            holder.ll_adapter_homeroom = (LinearLayout) view.findViewById(R.id.ll_adapter_homeroom);
            holder.img_room = (ImageView) view.findViewById(R.id.img_adapter_homeroom);
            holder.tv_room = (TextView) view.findViewById(R.id.tv_adapter_homeroom);
            view.setTag(holder);
        }else{
            holder = (Holder)view.getTag();
        }
        Glide.with(mContext).load(list.get(position).getrImg()).error(R.mipmap.ic_launcher).into(holder.img_room);
        holder.tv_room.setText(list.get(position).getrInfo());
        holder.ll_adapter_homeroom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.getPosition(position);
            }
        });
        return view;
    }
    class Holder{
        LinearLayout ll_adapter_homeroom;
        ImageView img_room;
        TextView tv_room;
    }
}
