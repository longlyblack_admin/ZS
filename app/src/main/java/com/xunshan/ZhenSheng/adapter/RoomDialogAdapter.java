package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.entity.OnlineUserListBean;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.RoundTransform;

import java.util.List;

public class RoomDialogAdapter extends RecyclerView.Adapter<RoomDialogAdapter.ViewHolder> {

    private List<OnlineUserListBean.DataBean> MlistOline;

    private Context mContext;
    private ItemdiClickListener mDIItemClickListener;

    public RoomDialogAdapter(Context mContext, List<OnlineUserListBean.DataBean> MlistOline) {
        this.MlistOline = MlistOline;
        this.mContext = mContext;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.online_dialog_layout, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public int getItemCount() {
        return (MlistOline == null) ? 0 : MlistOline.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        RequestOptions options = new RequestOptions().centerCrop().transform(new RoundTransform(mContext, 5)).error(R.mipmap.logo);
        Glide.with(mContext)
                .load(AllInterface.addr + "/" + MlistOline.get(position)
                        .getHeadpic()).error(R.mipmap.ic_launcher)
                .into(holder.ivOnlineDisplayNumber1);
        holder.tvOnlineDisplayName.setText("姓名：" + MlistOline.get(position).getName());
        holder.tvonlineseatuserID.setText("用户ID：" + MlistOline.get(position).getUser_id());


        holder.tvonlineseatsign.setText("个性签名" + MlistOline.get(position).getSign());
        if (mDIItemClickListener != null) {
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // 这里利用回调来给RecyclerView设置点击事件
                    mDIItemClickListener.onItemClick(position);
                }
            });
        }

    }


    public interface ItemdiClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(ItemdiClickListener itemdiClickListener) {
        this.mDIItemClickListener = itemdiClickListener;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        View myView;
        ImageView ivOnlineDisplayNumber1;
        TextView tvOnlineDisplayName;
        TextView tvonlineseatuserID;


        TextView tvonlineseatsign;
        RelativeLayout rvOnlineDisplayNumber;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            myView = itemView;
            rvOnlineDisplayNumber = (RelativeLayout) itemView.findViewById(R.id.rv_Online_Display_Number);
            ivOnlineDisplayNumber1 = (ImageView) itemView.findViewById(R.id.iv_Online_Display_Number1);
            tvOnlineDisplayName = (TextView) itemView.findViewById(R.id.tv_Online_Display_Name);
            tvonlineseatuserID = (TextView) itemView.findViewById(R.id.tv_online_seat_user_ID_);

            tvonlineseatsign = (TextView) itemView.findViewById(R.id.tv_online_seat_sign);


        }
    }
}

