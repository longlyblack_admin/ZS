package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.entity.MyUserFollow;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.RoundTransform;

import java.util.List;

public class MyUserFollowAdapter extends RecyclerView.Adapter<MyUserFollowAdapter.ViewHolder> {
    private List<MyUserFollow.DataBean> list;
    private Context context;
    //第二步自定义接口
    private OnItemClickListener  mOnItemClickListener ;


    public MyUserFollowAdapter(Context context, List<MyUserFollow.DataBean> list) {
        this.context = context;
        this.list = list;
    }
    @Override
    public int getItemCount() {
        return (list == null) ? 0 : list.size();
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_my_follow_list, parent, false);
        final MyUserFollowAdapter.ViewHolder holder = new MyUserFollowAdapter.ViewHolder(view);
        return holder;
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        RequestOptions options = new RequestOptions().centerCrop().transform(new RoundTransform(context, 100)).error(R.mipmap.addshangmai);
            Glide.with(context)
                    .load(AllInterface.addr + "/" + list.get(position).getHeadpic())
                    .apply(options)
                    .into(holder.ivphotoheaderfollow);
            holder.tvseatdialognamefollow.setText(list.get(position).getName());
        if (list.get(position).getSex().equals("0")){
            holder.tvseatsexfollow.setImageResource(R.mipmap.zssexnan);
        }else if (list.get(position).getSex().equals("1")){
            holder.tvseatsexfollow.setImageResource(R.mipmap.zssexnv);
        }
        holder.tvlevelfollow.setText(list.get(position).getSex());
        holder.tvseatsignfollow.setText("个性签名："+list.get(position).getSign());
        Log.e("TAGposition", "onBindViewHolder: "+list.get(position).getName()+"//"+list.get(position).getSex());
    }
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        //接口位置
        ImageView ivphotoheaderfollow;
        TextView tvseatdialognamefollow;
        ImageView tvseatsexfollow;
        TextView tvlevelfollow;
        TextView tvsjfollow;
        TextView tvseatsignfollow;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivphotoheaderfollow = (ImageView) itemView.findViewById(R.id.iv_photo_header_follow);
            tvseatdialognamefollow = (TextView) itemView.findViewById(R.id.tv_seat_dialog_name_follow);
            tvseatsexfollow = (ImageView) itemView.findViewById(R.id.tv_seat_sex_follow);
            tvlevelfollow = (TextView) itemView.findViewById(R.id.tv_level_follow);
            tvsjfollow = (TextView) itemView.findViewById(R.id.tv_sj_follow);
            tvseatsignfollow = (TextView) itemView.findViewById(R.id.tv_seat_sign_follow);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mOnItemClickListener != null) {
                mOnItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }
//第一步自定义回调接口
    public interface OnItemClickListener  {
        void onItemClick(View v, int position);
        void onItemLongClick(View v);
    }
    //第三步定义方法并暴露外面的调用者
    public void setOnItemClickListener(OnItemClickListener listener){
        this.mOnItemClickListener = listener ;
    }

}
