package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.bumptech.glide.Glide;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.entity.Gift;

import java.util.List;

public class GiftAdapter extends BaseAdapter {
    private List<Gift> list;
    private Context mContext;
    private LayoutInflater mInflater;

    public GiftAdapter(List<Gift> list, Context context) {
        this.list = list;
        this.mContext = context;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        Holder holder;
        if (view == null) {
            holder = new Holder();
            view = mInflater.inflate(R.layout.adapter_gift, null);
            holder.img_giftwall = (ImageView) view.findViewById(R.id.img_giftwall);
            holder.tv_giftwallname = (TextView) view.findViewById(R.id.tv_giftwallname);
            holder.tv_giftwallnum = (TextView) view.findViewById(R.id.tv_giftwallnum);
            view.setTag(holder);
        }else{
            holder = (Holder) view.getTag();
        }
        Glide.with(mContext).load(list.get(position).getgImg()).into(holder.img_giftwall);
        holder.tv_giftwallname.setText(list.get(position).getgName());
        holder.tv_giftwallnum.setText(list.get(position).getgNum() + "");
        return view;
    }

    class Holder {
        ImageView img_giftwall;
        TextView tv_giftwallname;
        TextView tv_giftwallnum;

    }
}
