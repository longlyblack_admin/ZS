package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.user.User;

import java.util.List;

public class MsgFragmentAdapter extends BaseAdapter {
    List<User> list;
    Context mContext;
    LayoutInflater mInflater;

    public MsgFragmentAdapter(List<User> list, Context context) {
        this.list = list;
        this.mContext = context;
        mInflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        Holder holder = null;
        if (view == null) {
            holder = new Holder();
            view = mInflater.inflate(R.layout.adapter_msgfragment, null);
            holder.tv_adapter_msgfragment_name = (TextView) view.findViewById(R.id.tv_adapter_msgfragment_name);
            view.setTag(holder);
        }else{
            holder = (Holder) view.getTag();
        }
       // holder.tv_adapter_msgfragment_name.setText(list.get(position).getuName());
        return view;
    }

    class Holder {
        TextView tv_adapter_msgfragment_name;
    }
}
