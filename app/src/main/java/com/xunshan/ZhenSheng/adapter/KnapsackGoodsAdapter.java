package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.entity.MyBackageBean;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.RoundTransform;

import java.util.List;

public class KnapsackGoodsAdapter extends BaseAdapter {
    List<MyBackageBean.GiftBean> giftBeanList;
    List<MyBackageBean.HeadBean> headBeanList;
    Context mContext;
    LayoutInflater mInflater;
    knapsackGoodsCallBack callBack;
    int classifyIndex;


    public interface knapsackGoodsCallBack {
        void goodsPosition(int position);
    }

    public KnapsackGoodsAdapter(List<MyBackageBean.GiftBean> giftBeanList, Context context, knapsackGoodsCallBack callBack, List<MyBackageBean.HeadBean> headBeanList, int classifyIndex) {
        this.giftBeanList = giftBeanList;
        this.headBeanList = headBeanList;
        this.mContext = context;
        this.mInflater = LayoutInflater.from(mContext);
        this.callBack = callBack;
        this.classifyIndex = classifyIndex;
    }

    @Override
    public int getCount() {
        if (classifyIndex == 0){
            return (giftBeanList == null) ? 0 : giftBeanList.size();
        }
        if (classifyIndex==1){
            return (headBeanList == null) ? 0 : headBeanList.size();
        }
        return  0 ;
    }

    @Override
    public Object getItem(int position) {
        if (classifyIndex == 0){
            return giftBeanList.get(position);
        }
        if (classifyIndex == 1){
            return headBeanList.get(position);
        }
        return giftBeanList.get(position);

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        Holder holder;
        if (view == null) {
            holder = new Holder();
            view = mInflater.inflate(R.layout.adapter_knapsackgoods, null);
            holder.ll_adapter_knapsackgoods = view.findViewById(R.id.ll_adapter_knapsackgoods);
            holder.img_adapter_knapsackgoods = view.findViewById(R.id.img_adapter_knapsackgoods);
            holder.tv_adapter_knapsackgoodsname = view.findViewById(R.id.tv_adapter_knapsackgoodsname);
            holder.tv_adapter_knapsackgoodstime = view.findViewById(R.id.tv_adapter_knapsackgoodstime);
            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }
        if (classifyIndex==0){
            RequestOptions options = new RequestOptions().centerCrop().transform(new RoundTransform(mContext, 5)).error(R.mipmap.logo);
            Glide.with(mContext)
                    .load(AllInterface.addr + "/" + giftBeanList.get(position).getPic())
                    .apply(options)
                    .into(holder.img_adapter_knapsackgoods);
            holder.tv_adapter_knapsackgoodsname.setText(giftBeanList.get(position).getName());
            holder.tv_adapter_knapsackgoodstime.setText("x" + giftBeanList.get(position).getNumber());
        }
        if (classifyIndex==1){
            RequestOptions options = new RequestOptions().centerCrop().transform(new RoundTransform(mContext, 5)).error(R.mipmap.logo);
            Glide.with(mContext)
                    .load(AllInterface.addr + "/" + headBeanList.get(position).getPic())
                    .apply(options)
                    .into(holder.img_adapter_knapsackgoods);
            holder.tv_adapter_knapsackgoodsname.setText(headBeanList.get(position).getName());
            holder.tv_adapter_knapsackgoodstime.setText("剩余时间" + headBeanList.get(position).getTime()+"天");
        }


        holder.ll_adapter_knapsackgoods.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.goodsPosition(position);
            }
        });
        return view;
    }

    class Holder {
        LinearLayout ll_adapter_knapsackgoods;
        ImageView img_adapter_knapsackgoods;
        TextView tv_adapter_knapsackgoodsname;
        TextView tv_adapter_knapsackgoodstime;
    }
}
