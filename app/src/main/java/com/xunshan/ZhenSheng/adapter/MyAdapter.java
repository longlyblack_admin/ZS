package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xunshan.ZhenSheng.R;

import java.util.List;

public class MyAdapter extends BaseAdapter {

    private List<Integer> list;
    private List<String> listname;
    private LayoutInflater mInflater;
    private Context mContext;
    myCallBack callBack;

    public interface myCallBack {
         void getPosition(int position);
    }

    public MyAdapter(List<Integer> list, List listname, Context context, myCallBack callBack) {
        this.list = list;
        this.mContext = context;
        this.listname = listname;
        mInflater = LayoutInflater.from(mContext);
        this.callBack = callBack;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        Holder holder;
        if (view == null) {
            holder = new Holder();
            view = mInflater.inflate(R.layout.adapter_my, null);
            holder.img_room = (ImageView) view.findViewById(R.id.img_adapter_homeroom);
            holder.tv_name = view.findViewById(R.id.tv_adapter_buttonname);

            view.setTag(holder);
        } else {
            holder = (Holder) view.getTag();
        }
        holder.img_room.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBack.getPosition(position);
            }
        });
        holder.img_room.setImageResource(list.get(position));
        holder.tv_name.setText(listname.get(position));

        return view;
    }

    class Holder {
        ImageView img_room;
        TextView tv_name;
    }
}
