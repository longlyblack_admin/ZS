package com.xunshan.ZhenSheng.adapter;

import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.entity.Goods;

import java.util.List;

public class GiftListAdapter extends RecyclerView.Adapter {

    private List<Goods> list;
    MyViewHolder holder;

    public GiftListAdapter(List<Goods> list) {
        this.list = list;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_giftlist, viewGroup, false);
        holder = new MyViewHolder(view);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(50,100);
        holder.img = (ImageView) view.findViewById(R.id.img_giftlist);
        holder.img.setLayoutParams(params);
        return holder;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
    }
    @Override
    public int getItemCount() {
        return  list.size();
    }


    class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView img;
        public TextView tv;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

        }
    }

}
