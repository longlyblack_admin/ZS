package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;
import com.bumptech.glide.request.RequestOptions;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.entity.UserRoomListBean;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.GlideRoundTransform;
import com.xunshan.ZhenSheng.util.RoundTransform;

import java.util.List;

public class GiftToUserAdapter extends RecyclerView.Adapter<GiftToUserAdapter.MyViewHolder> {

    private List<UserRoomListBean.DataBean> list;
    MyViewHolder holder;
    private Context mContext;
    private boolean ischeck = false;

    public GiftToUserAdapter(List<UserRoomListBean.DataBean> list, Context context) {
        this.mContext = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_gifttouser, parent, false);
        return new MyViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        if (list.get(position).getHeadpic() != null) {
            new RoundedCorners(16);
            Glide.with(mContext)
                    .load(AllInterface.addr + "/" + list.get(position).getHeadpic())
                    .skipMemoryCache(false)
                    .dontAnimate()
                    .apply(new RequestOptions().centerCrop().transform(new RoundTransform(mContext, 100)).error(R.mipmap.addshangmai)
                            .transform(new GlideRoundTransform(100)))
                    .into(holder.img);
            holder.tv.setText(list.get(position).getName());

            holder.img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    if (ischeck) {
                        ischeck = false;
                        holder.img.setBackground(null);
                        mItemClickListener.onItemClick(list.get(position).getUser_id(), list.get(position).getName(), ischeck, position);
                    } else {
                        ischeck = true;
                        holder.img.setBackgroundResource(R.drawable.whitecircle20);
                        mItemClickListener.onItemClick(list.get(position).getUser_id(), list.get(position).getName(), ischeck, position);
                    }
                }
            });
        }
    }

    private GiftToUserAdapter.OnItemClickListener mItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(String did, String name, boolean ischeck, int position);
    }

    public void setItemClickListener(GiftToUserAdapter.OnItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return (list == null) ? 0 : list.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        public ImageView img;
        public TextView tv;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            img = itemView.findViewById(R.id.img_adapter_gifttouser);
            tv = itemView.findViewById(R.id.liwu_name);
        }
    }

}
