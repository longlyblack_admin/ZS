package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xunshan.ZhenSheng.R;

import com.xunshan.ZhenSheng.entity.SheGuanLiBean;
import com.xunshan.ZhenSheng.entity.UpperWheatApplyBean;
import com.xunshan.ZhenSheng.entity.UserRoomListBean;
import com.xunshan.ZhenSheng.user.User;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.RetrofitUtils;
import com.xunshan.ZhenSheng.util.RoundTransform;

import java.util.HashMap;
import java.util.IllegalFormatCodePointException;
import java.util.List;
import java.util.Map;

public class MyAuthSetingRecyclerViewAdapter extends RecyclerView.Adapter<MyAuthSetingRecyclerViewAdapter.ViewHolder> {


    private List Room_auth;
    private Context context;
    private String room_ID;

    public MyAuthSetingRecyclerViewAdapter(List room_auth, Context mContext,String roomid) {
        this.Room_auth = room_auth;
        this.context = mContext;
        this.room_ID = roomid;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_upper_wheat_apply, parent, false);
        final MyAuthSetingRecyclerViewAdapter.ViewHolder holder = new MyAuthSetingRecyclerViewAdapter.ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        RequestOptions options = new RequestOptions().centerCrop().transform(new RoundTransform(context, 5)).error(R.mipmap.logo);

        Map<String, String> map = new HashMap<>();
        map.put("token", com.xunshan.ZhenSheng.user.User.user().getToken());
        map.put("userArr",Room_auth.get(position)+"");
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/userslist", map, new NetCallBack<UserRoomListBean>() {
            @Override
            public void onSucceed(final UserRoomListBean userRoomListBean, int type) {
             userRoomListBean.getData().get(position);
                    Glide.with(context)
                            .load(AllInterface.addr + "/" +  userRoomListBean.getData().get(position).getHeadpic())
                            .apply(options)
                            .into(holder.ivUpperwheatApplyphoto);
                    holder.tvUpperwheatApplyName.setText( userRoomListBean.getData().get(position).getName());
                    if (userRoomListBean.getData().get(position).getSex().equals("0")){
                        holder.tvUpperwheatApplysex.setBackgroundResource(R.mipmap.zssexnan);
                    }else if (userRoomListBean.getData().get(position).getSex().equals("1")){
                        holder.tvUpperwheatApplysex.setBackgroundResource(R.mipmap.zssexnv);
                    }
                    holder.tvUpperwheatApplymai.setText("点击同意设置管理");
                    holder.tvUpperwheatApplymai.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Map<String, String> map = new HashMap<>();
                            map.put("userArr", Room_auth.get(position)+"");
                            map.put("token", User.user().getToken());
                            map.put("room_id", room_ID);
                            RetrofitUtils.getInstace().postOkHttp(AllInterface.addr + "/" + "home/room/auth", map, new NetCallBack<SheGuanLiBean>() {
                                @Override
                                public void onSucceed(final SheGuanLiBean sheGuanLiBean, int type) {
                                    sheGuanLiBean.getData();
                                }

                                @Override
                                public void SucceedError(Exception e, int types) {
                                }

                                @Override
                                public void onError(Throwable throwable, int type) {
                                }
                            }, 1);
                        }
                    });
            }

            @Override
            public void SucceedError(Exception e, int types) {
            }

            @Override
            public void onError(Throwable throwable, int type) {
            }
        }, 1);


    }

    @Override
    public int getItemCount() {
        return (Room_auth == null) ? 0 : Room_auth.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tvUpperwheatApplymai;
        public final TextView tvUpperwheatApplyName;
        public final TextView tvUpperwheatApplysex;
        public final ImageView ivUpperwheatApplyphoto;
        public UpperWheatApplyBean upperWheatApplyBean;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            ivUpperwheatApplyphoto =  view.findViewById(R.id.iv_Upper_wheat_Apply_photo);
            tvUpperwheatApplyName =  view.findViewById(R.id.tv_Upper_wheat_Apply_Name);
            tvUpperwheatApplysex =  view.findViewById(R.id.tv_Upper_wheat_Apply_sex);
            tvUpperwheatApplymai =  view.findViewById(R.id.tv_Upper_wheat_Apply_mai);
        }

    }
}
