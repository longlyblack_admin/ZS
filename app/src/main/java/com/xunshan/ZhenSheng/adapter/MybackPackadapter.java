package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.activity.MainActivity;
import com.xunshan.ZhenSheng.activity.RoomActivity;
import com.xunshan.ZhenSheng.entity.MybackcardBean;
import com.xunshan.ZhenSheng.util.CheckableLayout;

import java.util.List;
import java.util.Set;

public class MybackPackadapter extends RecyclerView.Adapter<MybackPackadapter.ViewHolder> {

    List<MybackcardBean.DataBean> bean;
    private Context context;

    private boolean ischeck = false;
    private RelativeLayout ll_liwubackimage1;
    private int cuuttt = -1;

    public MybackPackadapter(Context context, List<MybackcardBean.DataBean> mybackcardBean) {
        this.bean = mybackcardBean;
        this.context = context;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_mybackcard_item, parent, false);
        final ViewHolder holder = new ViewHolder(view);

          /*
        添加选中的打勾显示
         */
        holder.ll_liwubackimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //将点击的位置传出去
                cuuttt = holder.getAdapterPosition();
                //在点击监听里最好写入setVisibility(View.VISIBLE);这样可以避免效果会闪
                holder.ll_liwubackimage.setBackgroundResource(R.drawable.whitecircle4);
                //刷新界面 notify 通知Data 数据set设置Changed变化
                //在这里运行notifyDataSetChanged 会导致下面的onBindViewHolder 重新加载一遍
                notifyDataSetChanged();
            }
        });


        return new ViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Set<Integer> positionSet = RoomActivity.positionSet;

        String giftname = bean.get(position).getPic();
        if (giftname.equals("liuliuliu")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.liuliuliu));
        } else if (giftname.equals("jindan")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.jindan));

        } else if (giftname.equals("lihe")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.shenmiliwu));

        } else if (giftname.equals("dianzan")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.dianzan));

        } else if (giftname.equals("bixin")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.bixin));

        } else if (giftname.equals("xiangbin")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.xiangbin));

        } else if (giftname.equals("taohuayu")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.taohuayu));

        } else if (giftname.equals("kouhong")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.kouhong));
        } else if (giftname.equals("zhaocaimao")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.zhaocaimao));
        } else if (giftname.equals("menghuanhudie")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.menghuanhudie));
        } else if (giftname.equals("lanseyaoji")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.lanseyaoji));
        } else if (giftname.equals("zijingbaobao")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.zijingbaobao));
        } else if (giftname.equals("yongbaorewen")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.yongbaorewen));
        } else if (giftname.equals("huangsepaoche")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.huangsepaoche));
        } else if (giftname.equals("hongsepaoche")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.hongsepaoche));
        } else if (giftname.equals("meiguihuachuan")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.meiguihuachuan));
        } else if (giftname.equals("zaiyiqi")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.zaiyiqi));
        } else if (giftname.equals("falali")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.falali));
        } else if (giftname.equals("nvshendengchang")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.nvshendengchang));
        } else if (giftname.equals("feiji")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.feiji));
        } else if (giftname.equals("huojian")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.huojian));
        } else if (giftname.equals("zhuguangwancan")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.zhuguangwancan));
        } else if (giftname.equals("menghuanchengbao")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.menghuanchengbao));
        } else if (giftname.equals("gongzhudechengbao")) {
            holder.iv_liwuimage.setImageDrawable(ContextCompat.getDrawable(context, R.mipmap.gongzhudechengbao));
        }

        holder.tv_jiage.setText(bean.get(position).getMoney());
        holder.tv_liwuname.setText(bean.get(position).getName());
        holder.tv_liwunumber.setText("X" + bean.get(position).getNumber());


        //检查set里是否包含position，包含则显示选中的背景色，不包含则反之
        if (positionSet.contains(position)) {
            holder.ll_liwubackimage.setBackgroundResource(R.drawable.whitecircle4);
        } else {
            holder.ll_liwubackimage.setBackground(null);
        }


        holder.ll_liwubackimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mItemClickListener.onItemClick(holder.itemView, position, bean.get(position).getGifid(), bean.get(position).getNumber(),bean.get(position).getPic(),bean.get(position).getName());

            }
        });


    }

    public void update(List<MybackcardBean.DataBean> list) {
        if (list != null && list.size() > 0) {
            bean.addAll(list);
            notifyDataSetChanged();
        }
    }


    private MybackPackadapter.OnItemClickListener mItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(View view, int position, String liwuid, String number, String liwupname, String liwuname);
    }

    public void setItemClickListener(MybackPackadapter.OnItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }

    @Override
    public int getItemCount() {
        return bean == null ? 0 : bean.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView iv_liwuimage;
        private TextView tv_jiage;
        private TextView tv_liwuname;
        private TextView tv_liwunumber;
        private RelativeLayout ll_liwubackimage;
        private CheckableLayout rootlayout1;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            iv_liwuimage = itemView.findViewById(R.id.iv_liwuimage);
            rootlayout1 = itemView.findViewById(R.id.root_layout1);

            tv_jiage = itemView.findViewById(R.id.tv_jiage);
            tv_liwunumber = itemView.findViewById(R.id.tv_liwunumber);
            tv_liwuname = itemView.findViewById(R.id.tv_liwuname);
            ll_liwubackimage = itemView.findViewById(R.id.ll_liwubackimage);

        }
    }
}
