package com.xunshan.ZhenSheng.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.entity.GiftWallBean;

import java.util.List;

public class MyGiftWallRecyclerViewAdapter extends RecyclerView.Adapter<MyGiftWallRecyclerViewAdapter.ViewHolder> {


    private List<GiftWallBean.DataBean> mUpperListener;
    private Context context;

    public MyGiftWallRecyclerViewAdapter(List<GiftWallBean.DataBean> mupperListener, Context mContext) {
        mUpperListener = mupperListener;
        context = mContext;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.gift_wall_activity, parent, false);
        final MyGiftWallRecyclerViewAdapter.ViewHolder holder = new MyGiftWallRecyclerViewAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
      String giftname = mUpperListener.get(position).getPic();
      if (giftname.equals("liuliuliu")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.liuliuliu);
      }else if (giftname.equals("jindan")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.jindan);
      }else if (giftname.equals("lihe")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.shenmiliwu);
      }else if (giftname.equals("dianzan")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.dianzan);
      }else if (giftname.equals("bixin")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.bixin);
      }else if (giftname.equals("xiangbin")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.xiangbin);
      }else if (giftname.equals("taohuayu")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.taohuayu);
      }else if (giftname.equals("kouhong")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.kouhong);
      }else if (giftname.equals("zhaocaimao")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.zhaocaimao);
      }else if (giftname.equals("menghuanhudie")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.menghuanhudie);
      }else if (giftname.equals("lanseyaoji")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.lanseyaoji);
      }else if (giftname.equals("zijingbaobao")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.zijingbaobao);
      }else if (giftname.equals("yongbaorewen")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.yongbaorewen);
      }else if (giftname.equals("huangsepaoche")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.huangsepaoche);
      }else if (giftname.equals("hongsepaoche")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.hongsepaoche);
      }else if (giftname.equals("meiguihuachuan")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.meiguihuachuan);
      }else if (giftname.equals("zaiyiqi")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.zaiyiqi);
      }else if (giftname.equals("falali")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.falali);
      }else if (giftname.equals("nvshendengchang")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.nvshendengchang);
      }else if (giftname.equals("feiji")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.feiji);
      }else if (giftname.equals("huojian")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.huojian);
      }else if (giftname.equals("zhuguangwancan")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.zhuguangwancan);
      }else if (giftname.equals("menghuanchengbao")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.menghuanchengbao);
      }else if (giftname.equals("gongzhudechengbao")){
          holder.imgadaptergiftwall.setBackgroundResource(R.mipmap.gongzhudechengbao);
      }
        holder.tvadapternamegiftwall.setText(mUpperListener.get(position).getGiftname());
        holder.tvadapternumberngiftwall.setText("x"+mUpperListener.get(position).getNumber()+"个");

    }

    @Override
    public int getItemCount() {
        return (mUpperListener == null) ? 0 : mUpperListener.size();
    }


    private MyGiftWallRecyclerViewAdapter.OnItemClickListener mItemClickListener;

    public interface OnItemClickListener {
        void onItemClick(int did, String user_id);
    }

    public void setItemClickListener(MyGiftWallRecyclerViewAdapter.OnItemClickListener itemClickListener) {
        mItemClickListener = itemClickListener;
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView tvadapternamegiftwall;
        public final TextView tvadapternumberngiftwall;
        public final ImageView imgadaptergiftwall;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            imgadaptergiftwall = view.findViewById(R.id.img_adapter_gift_wall);
            tvadapternamegiftwall = view.findViewById(R.id.tv_adapter_name_gift_wall);
            tvadapternumberngiftwall = view.findViewById(R.id.tv_adapter_numbern_gift_wall);
        }

    }
}
