package com.xunshan.ZhenSheng.handler;



import com.xunshan.ZhenSheng.entity.City;
import com.xunshan.ZhenSheng.entity.District;
import com.xunshan.ZhenSheng.entity.Province;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

public class XmlParserHandler extends DefaultHandler {

	/**
	 * 存储所有的解析对象
	 */
	private List<Province> provinceList = new ArrayList<Province>();

	public XmlParserHandler() {

	}

	public List<Province> getDataList() {
		return provinceList;
	}

	@Override
	public void startDocument() throws SAXException {
		// 当读到第一个开始标签的时候，会触发这个方法
	}

	Province province = new Province();
	City city = new City();
	District district = new District();

	@Override
	public void startElement(String uri, String localName, String qName,
							 Attributes attributes) throws SAXException {
		// 当遇到开始标记的时候，调用这个方法
		if (qName.equals("province")) {
			province = new Province();
			province.setName(attributes.getValue(0));
			province.setCityList(new ArrayList<City>());
		} else if (qName.equals("city")) {
			city = new City();
			city.setName(attributes.getValue(0));
			city.setDistrictList(new ArrayList<District>());
		} else if (qName.equals("district")) {
			district = new District();
			district.setName(attributes.getValue(0));
			district.setZipcode(attributes.getValue(1));
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		// 遇到结束标记的时候，会调用这个方法
		if (qName.equals("district")) {
			city.getDistrictList().add(district);
		} else if (qName.equals("city")) {
			province.getCityList().add(city);
		} else if (qName.equals("province")) {
			provinceList.add(province);
		}
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
	}

}
