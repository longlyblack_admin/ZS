package com.xunshan.ZhenSheng.view;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.xunshan.ZhenSheng.R;
import com.xunshan.ZhenSheng.data.ZegoDataCenter;
import com.xunshan.ZhenSheng.entity.ChatroomSeatInfo;
import com.xunshan.ZhenSheng.entity.UserRoomListBean;
import com.xunshan.ZhenSheng.util.AllInterface;
import com.xunshan.ZhenSheng.util.NetCallBack;
import com.xunshan.ZhenSheng.util.RetrofitUtils;
import com.xunshan.ZhenSheng.util.RoundTransform;
import com.zego.chatroom.constants.ZegoChatroomSeatStatus;

import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

public class SeatOperationDialog extends Dialog implements View.OnClickListener  {

    public static final int OPERATION_TYPE_TAKE_SEAT = 0;
    public static final int OPERATION_TYPE_CHANGE_SEAT = 1;
    public static final int OPERATION_TYPE_LEAVE_SEAT = 2;
    public static final int OPERATION_TYPE_PICK_UP = 3;
    public static final int OPERATION_TYPE_KIT_OUT = 4;
    public static final int OPERATION_TYPE_MUTE_SEAT = 5;
    public static final int OPERATION_TYPE_CLOSE_SEAT = 6;
    public static final int OPERATION_TYPE_MUTE_ALL_SEATS = 7;
    public static final int OPERATION_TYPE_SEND_GIFT = 8;
    public static final int OPERATION_TYPE_SEND_JUBAO = 9;

    private TextView mTvSeatIndex;
    private TextView mTvTakeSeat;
    private View mVTakeSeatLine;
    private TextView mTvChangeSeat;
    private View mVChangeSeatLine;
    private TextView mTvLeaveSeat;
    private View mVLeaveSeatLine;
    private TextView mTvPickUp;
    private View mVPickUpLine;
    private TextView mTvKitOut;
    private View mVKitOutLine;
    private TextView mTvMuteSeat;
    private View mVMuteSeatLine;
    private TextView mTvCloseSeat;
    private View mVCloseSeatLine;
    private TextView tv_sendgift;
    private TextView mTvMuteAllSeats;
    private  TextView tvjubao;
    private ImageView ivphotoheader;
    private TextView  tvseatdialogname,tvseatuserID,tvseatlevel,tvseatcharm,tvseatsign,tvseatsex;



    private String usernmae;
    private String userID;
    private String token;
    View vi;

    private OnOperationItemClickListener mOnOperationItemClickListener;

    private ChatroomSeatInfo mSeatInfo;
    private int mPosition;

    public SeatOperationDialog(@NonNull Context context) {
        super(context, R.style.CommonDialog);


        initView(context);
        SharedPreferences sharedPreferences = context.getSharedPreferences("data",MODE_PRIVATE);
        token = sharedPreferences.getString("token","");
        usernmae =  sharedPreferences.getString("user_name","");
        userID =  sharedPreferences.getString("user_ID","");
        Log.e("TAGLABELseat", userID+usernmae+token+" ");;
        initData();


    }



    private void initView(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_seat_operation_layout, null);
        setContentView(view);
        // 设置可以取消
        setCancelable(true);
        setCanceledOnTouchOutside(true);
        // 设置Dialog高度位置
        WindowManager.LayoutParams layoutParams = getWindow().getAttributes();
        // 经计算，dialog_enqueue_mode_choose_layout的高度为250dp
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT;
        layoutParams.gravity = Gravity.CENTER;
        // 设置没有边框
        getWindow().getDecorView().setPadding(0, 0, 0, 0);
        getWindow().setAttributes(layoutParams);
        //



        mTvSeatIndex = findViewById(R.id.tv_seat_index);
        mTvTakeSeat = findViewById(R.id.tv_take_seat);
        mTvChangeSeat = findViewById(R.id.tv_change_seat);
        mTvLeaveSeat = findViewById(R.id.tv_leave_seat);
        mTvPickUp = findViewById(R.id.tv_pick_up);
        mTvKitOut = findViewById(R.id.tv_kick_out);
        mTvMuteSeat = findViewById(R.id.tv_mute_seat);
        mTvCloseSeat = findViewById(R.id.tv_close_seat);
        tv_sendgift = (TextView) findViewById(R.id.tv_sendgift);
        mTvMuteAllSeats = findViewById(R.id.tv_mute_all_seats);
        tvjubao = findViewById(R.id.tv_jubao);
        ivphotoheader=findViewById(R.id.iv_photo_header);

        tvseatdialogname=findViewById(R.id.tv_seat_dialog_name);
        tvseatuserID=findViewById(R.id.tv_seat_user_ID_);
        tvseatlevel=findViewById(R.id.tv_seat_level);
        tvseatcharm=findViewById(R.id.tv_seat_charm);
        tvseatsign=findViewById(R.id.tv_seat_sign);
        tvseatsex=findViewById(R.id.tv_seat_sex);




        mVTakeSeatLine = findViewById(R.id.v_take_seat_line);
        mVChangeSeatLine = findViewById(R.id.v_change_seat_line);
        mVLeaveSeatLine = findViewById(R.id.v_leave_seat_line);
        mVPickUpLine = findViewById(R.id.v_pick_up_line);
        mVKitOutLine = findViewById(R.id.v_kick_out_line);
        mVMuteSeatLine = findViewById(R.id.v_mute_seat_line);
        mVCloseSeatLine = findViewById(R.id.v_close_seat_line);

        mTvTakeSeat.setOnClickListener(this);

        mTvChangeSeat.setOnClickListener(this);
        mTvLeaveSeat.setOnClickListener(this);
        mTvPickUp.setOnClickListener(this);
        mTvKitOut.setOnClickListener(this);
        mTvMuteSeat.setOnClickListener(this);
        mTvCloseSeat.setOnClickListener(this);
        mTvMuteAllSeats.setOnClickListener(this);
        tv_sendgift.setOnClickListener(this);
        tvjubao.setOnClickListener(this);
        findViewById(R.id.tv_cancel).setOnClickListener(this);
    }

    public void setOnOperationItemClickListener(OnOperationItemClickListener onOperationItemClickListener) {
        mOnOperationItemClickListener = onOperationItemClickListener;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void adaptBySeatInfo(int position, ChatroomSeatInfo seatInfo, boolean isOwner, boolean isOnMic) {
        mTvSeatIndex.setText(position + "号麦");
        this.mSeatInfo = seatInfo;
        this.mPosition = position;

        mTvTakeSeat.setVisibility(View.GONE);
        mVTakeSeatLine.setVisibility(View.GONE);
        mTvChangeSeat.setVisibility(View.GONE);
        mVChangeSeatLine.setVisibility(View.GONE);
        mTvLeaveSeat.setVisibility(View.GONE);
        mVLeaveSeatLine.setVisibility(View.GONE);
        mTvPickUp.setVisibility(View.GONE);
        mVPickUpLine.setVisibility(View.GONE);
        mTvKitOut.setVisibility(View.GONE);
        mVKitOutLine.setVisibility(View.GONE);
        mTvMuteSeat.setVisibility(View.GONE);
        mVMuteSeatLine.setVisibility(View.GONE);
        mTvCloseSeat.setVisibility(View.GONE);
        mVCloseSeatLine.setVisibility(View.GONE);
        mTvMuteAllSeats.setVisibility(View.GONE);

        boolean isClosed = seatInfo.mStatus == ZegoChatroomSeatStatus.Closed;
        boolean isMute = seatInfo.isMute;

        int visibility = isOwner ? View.VISIBLE : View.GONE;
        mTvMuteSeat.setVisibility(visibility);
        mVMuteSeatLine.setVisibility(visibility);
        mTvCloseSeat.setVisibility(visibility);
        mVCloseSeatLine.setVisibility(visibility);
        mTvMuteAllSeats.setVisibility(visibility);
        if (isOwner) {
            mTvMuteSeat.setText(isMute ? "解禁" : "禁麦");
            mTvCloseSeat.setText(isClosed ? "解封" : "封麦");
        }

        if (seatInfo.mStatus == ZegoChatroomSeatStatus.Empty) {
            if (isOnMic) {
                // 换麦
                mTvChangeSeat.setVisibility(View.VISIBLE);
                mVChangeSeatLine.setVisibility(View.VISIBLE);
            } else {
                // 上麦
                mTvTakeSeat.setVisibility(View.VISIBLE);
                mVTakeSeatLine.setVisibility(View.VISIBLE);
            }
            if (isOwner) {
                // 抱麦
                mTvPickUp.setVisibility(View.VISIBLE);
                mVPickUpLine.setVisibility(View.VISIBLE);
            }
        } else if (seatInfo.mStatus == ZegoChatroomSeatStatus.Used) {
            if (ZegoDataCenter.ZEGO_USER.equals(seatInfo.mUser)) {
                mTvLeaveSeat.setVisibility(View.VISIBLE);
                mVLeaveSeatLine.setVisibility(View.VISIBLE);
            } else {
                if (isOwner) {
                    mTvKitOut.setVisibility(View.VISIBLE);
                    mVKitOutLine.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        dismiss();
        if (mOnOperationItemClickListener == null) {
            return;
        }
        switch (v.getId()) {
            case R.id.tv_take_seat:
                mOnOperationItemClickListener.onOperationItemClick(mPosition, OPERATION_TYPE_TAKE_SEAT, mSeatInfo,vi);
                break;
            case R.id.tv_change_seat:
                mOnOperationItemClickListener.onOperationItemClick(mPosition, OPERATION_TYPE_CHANGE_SEAT, mSeatInfo,vi);
                break;
            case R.id.tv_leave_seat:
                mOnOperationItemClickListener.onOperationItemClick(mPosition, OPERATION_TYPE_LEAVE_SEAT, mSeatInfo,vi);
                break;
            case R.id.tv_pick_up:
                mOnOperationItemClickListener.onOperationItemClick(mPosition, OPERATION_TYPE_PICK_UP, mSeatInfo,vi);
                break;
            case R.id.tv_kick_out:
                mOnOperationItemClickListener.onOperationItemClick(mPosition, OPERATION_TYPE_KIT_OUT, mSeatInfo,vi);
                break;
            case R.id.tv_mute_seat:
                mOnOperationItemClickListener.onOperationItemClick(mPosition, OPERATION_TYPE_MUTE_SEAT, mSeatInfo,vi);
                break;
            case R.id.tv_close_seat:
                mOnOperationItemClickListener.onOperationItemClick(mPosition, OPERATION_TYPE_CLOSE_SEAT, mSeatInfo,vi);
                break;
            case R.id.tv_sendgift:
                mOnOperationItemClickListener.onOperationItemClick(mPosition, OPERATION_TYPE_SEND_GIFT, mSeatInfo,vi);
                break;
            case R.id.tv_mute_all_seats:
                mOnOperationItemClickListener.onOperationItemClick(mPosition, OPERATION_TYPE_MUTE_ALL_SEATS, mSeatInfo,vi);
                break;
            case R.id.tv_jubao:
                mOnOperationItemClickListener.onOperationItemClick(mPosition, OPERATION_TYPE_SEND_JUBAO, mSeatInfo,vi);
                break;

        }
    }

    public interface OnOperationItemClickListener {
        void onOperationItemClick(int position, int operationType, ChatroomSeatInfo seat,View vi);
    }

    private void initData() {
        RequestOptions options = new RequestOptions().centerCrop().transform(new RoundTransform(getContext(), 50)).error(R.mipmap.logo);
        Map<String, String> map = new HashMap<>();
        map.put("token", token);
        map.put("userArr", userID);
        Log.e("TAGLABELseat", map+" ");
        RetrofitUtils.getInstace().postOkHttp(AllInterface.addr+"/" + "home/room/userslist", map, new NetCallBack<UserRoomListBean>() {
            @Override
            public void onSucceed(final UserRoomListBean userRoomListBean, int type) {
                userRoomListBean.getData();
                String[] arr = new String[userRoomListBean.getData().size()];
                for(int i = 0; i < userRoomListBean.getData().size(); i++){
                    Glide.with(getContext())
                            .load(AllInterface.addr+"/" +userRoomListBean.getData().get(i).getHeadpic())
                            .apply(options)
                            .into(ivphotoheader);
                    tvseatdialogname.setText("姓名："+userRoomListBean.getData().get(i).getName());
                    tvseatuserID.setText("ID："+userRoomListBean.getData().get(i).getUser_id());
                    tvseatlevel.setText("等级："+userRoomListBean.getData().get(i).getLevel());
                    tvseatcharm.setText("魅力值："+userRoomListBean.getData().get(i).getCharm());
                    tvseatsign.setText("签名："+userRoomListBean.getData().get(i).getSign());
                    tvseatsex.setText("性别："+userRoomListBean.getData().get(i).getSex());
                    Log.e("TAGLABELseat", arr[i] .toString());
                }
            }
            @Override
            public void SucceedError(Exception e, int types) {
            }
            @Override
            public void onError(Throwable throwable, int type) {
                Log.e("1234567", "onError: 1" );
            }
        }, 1);


    }


}