package com.xunshan.ZhenSheng.data;

import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.xunshan.ZhenSheng.application.BaseApplication;
import com.xunshan.ZhenSheng.user.User;
import com.zego.chatroom.entity.ZegoChatroomUser;

import static android.content.Context.MODE_PRIVATE;

/**
 * Zego App ID 和 Sign Data，需从Zego主页申请。
 */
@RequiresApi(api = Build.VERSION_CODES.O)
public class ZegoDataCenter {
    public static final boolean IS_TEST_ENV = false;  // TODO 默认使用测试环境，正式上线的时候，需修改为正式环境

    private static final String SP_NAME = "sp_name_base";
    private static final String SP_KEY_USER_ID = "sp_key_user_id";
    private static final String SP_KEY_USER_NAME = "sp_key_user_name";
    private static final String GET_ROOM_LIST_URL_DEV = "https://liveroom%d-api.zego.im/demo/roomlist?appid=%d";
    private static final String GET_ROOM_LIST_URL_TEST = "https://test2-liveroom-api.zego.im/demo/roomlist?appid=%d";
    public static final long APP_ID = 1538609876;
    public static byte[] APP_SIGN =
            (new byte[]{(byte)0x41,(byte)0xba,(byte)0xf7,(byte)0x61,(byte)0x76,
                    (byte)0x6f,(byte)0x1c,(byte)0x8a,(byte)0x07,(byte)0x62,(byte)0x87,
                    (byte)0xe6,(byte)0xbf,(byte)0x6f,(byte)0x3a,(byte)0x2a,(byte)0xf7,
                    (byte)0x69,(byte)0x33,(byte)0xc6,(byte)0xa8,(byte)0x8f,(byte)0x13,
                    (byte)0xc8,(byte)0x62,(byte)0x3d,(byte)0x61,(byte)0xe0,(byte)0xf9,
                    (byte)0x6d,(byte)0xdc,(byte)0x59});
    public static final ZegoChatroomUser ZEGO_USER = new ZegoChatroomUser(); // 根据自己情况初始化唯一识别USER
    static {
        ZEGO_USER.userID = User.user().getUser_ID(); // 使用 SERIAL 作为用户的唯一识别
        Log.e("TagZEGO_USER.userName","TAG_ ZEGO_USER.userName"+ ZEGO_USER.userName+ZEGO_USER.userID);
        ZEGO_USER.userName = User.user().getUser_name();
    }


    public static String getRoomListUrl() {
        if (IS_TEST_ENV) {
            return GET_ROOM_LIST_URL_TEST;
        } else {
            return GET_ROOM_LIST_URL_DEV;
        }
    }
}
